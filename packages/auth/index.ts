export { authOptions } from "./src/auth-options";
export { getServerSession } from "./src/get-session";
export type { Session } from "next-auth";

export { getAuth } from "@clerk/nextjs/server";
export type {
  SignedInAuthObject,
  SignedOutAuthObject,
} from "@clerk/nextjs/api";
