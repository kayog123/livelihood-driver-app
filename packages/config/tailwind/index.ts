import type { Config } from "tailwindcss";
import defaultTheme from "tailwindcss/defaultTheme";

export default {
  content: [""],
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
    },
    extend: {
      fontFamily: {
        sans: ["Quicksand", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        app: {
          primary: "#009387",
          primaryDark: "#004943",
          primaryColorShadow: "#118586",
          bgWhite: "#fff",
          bgWhiteShadow: "#f3f0f0",
          secondaryLight: "#f05d2a",
          loading: "rgba(255, 255, 255, 0.5)",
          textColorDark: "#05375a",
          textColorShadow: "#394867",
          colorDarkOutline: "#e6e6e6",
          errorColor: "#E76161",
        },
      },
    },
  },
  plugins: [],
} satisfies Config;
