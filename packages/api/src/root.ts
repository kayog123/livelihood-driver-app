import { authRouter } from "./router/auth";
import { bookingRouter } from "./router/booking";
import { chatRoomRouter } from "./router/chatRoom";
import { contactRouter } from "./router/contact";
import { historyRouter } from "./router/history";
import { messageRouter } from "./router/message";
import { offeredTripRouter } from "./router/offeredTrip";
import { infoRouter } from "./router/info"; 
import { passengersRouter } from "./router/passengers";
import { roomRouter } from "./router/rooms";
import { scheduleRouter } from "./router/schedule";
import { userRouter } from "./router/user";
import { createTRPCRouter } from "./trpc";

export const appRouter = createTRPCRouter({
  auth: authRouter,
  passengers: passengersRouter,
  user: userRouter,
  contact: contactRouter,
  schedule: scheduleRouter,
  booking: bookingRouter,
  offeredTrip: offeredTripRouter,
  history: historyRouter,
  chatRoom: chatRoomRouter,
  message: messageRouter,
  info: infoRouter, 
  room: roomRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
