import { TRPCError } from "@trpc/server";
import { any, z } from "zod";

import { TranspoType, Trip, TripStatus } from "@acme/db";

import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const bookingRouter = createTRPCRouter({
  create: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        tripId: z.number().optional(),
        pkgId: z.number().optional(),
        places: z.string(),
        transpo_type: z.union([
          z.literal("VANS"),
          z.literal("BOAT"),
          z.literal("MOTORCYCLE"),
          z.literal("TRICYCLE"),
        ]),
        transpo_rate: z.number(),
        pickup_lat: z.number(),
        pickup_long: z.number(),
        pickup_area_name: z.string(),
        passenger_capacity: z.number(),
        description: z.string(),
        dateScheduled: z.date(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      return ctx.prisma.booking.create({
        data: {
          userId: input.userId,
          places: input.places,
          type: Trip.LATLONG,
          status: TripStatus.PENDING,
          transpo_type:
            input.transpo_type.toLowerCase() == "motorcycle"
              ? TranspoType.MOTORCYCLE
              : TranspoType.TRICYCLE,
          transpo_rate: input.transpo_rate,
          pickup_lat: input.pickup_lat,
          passengers: {
            create: {
              passenger_count: 1,
              userId: input.userId,
            },
          },
          pickup_long: input.pickup_long,
          pickup_area_name: input.pickup_area_name,
          passenger_capacity: input.passenger_capacity,
          description: input.description,
          dateScheduled: input.dateScheduled,
        },
      });
    }),
  fetchByTransType: publicProcedure
    .input(
      z.object({
        transpo_type: z.enum([
          TranspoType.BOAT,
          TranspoType.MOTORCYCLE,
          TranspoType.TRICYCLE,
          TranspoType.VANS,
        ]),
      }),
    )
    .query(({ ctx, input }) => {
      const { transpo_type } = input;
      return ctx.prisma.booking.findMany({
        orderBy: { dateCreated: "desc" },
        where: {
          AND: [
            { transpo_type: TranspoType[transpo_type] },
            { ownerId: null },
            { status: TripStatus.PENDING },
          ],
        },
        include: {
          user: {
            select: {
              firstname: true,
              lastname: true,
              phone: true,
              profile: true,
            },
          },
        },
      });
    }),
  updateBooking: publicProcedure
    .input(
      z.object({
        ownerId: z.string() || null,
        bookingId: z.number(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { ownerId, bookingId } = input;

      const bookingDetails = await ctx.prisma.booking.findFirst({
        select: {
          ownerId: true,
          status: true,
        },
        where: {
          id: bookingId,
        },
      });

      if (
        bookingDetails?.ownerId != null &&
        bookingDetails.status != TripStatus.PENDING
      ) {
        throw new TRPCError({
          code: "INTERNAL_SERVER_ERROR",
          message: "Order was locked in to another driver",
        });
      }

      const updatedBookingDetails = await ctx.prisma.booking.update({
        where: { id: bookingId },
        data: {
          ownerId: ownerId,
          status: TripStatus.ACCEPTED,
        },
      });

      const history = await ctx.prisma.history.create({
        data: {
          bookingId: bookingId,
          actorId: ownerId,
          action: TripStatus.ACCEPTED,
        },
      });
      const QueryDetail = {
        history,
        updatedBookingDetails,
      };

      return QueryDetail;
    }),
  updateBookingStatus: publicProcedure
    .input(
      z.object({
        status: z.enum([
          TripStatus.ACCEPTED,
          TripStatus.CANCELLED,
          TripStatus.COMPLETED,
          TripStatus.PICKED_UP,
          TripStatus.PENDING,
          TripStatus.ON_THE_WAY,
        ]),
        ownerId: z.string(),
        reason: z.string(),
        bookingId: z.number(),
        cancelledByUser: z.boolean().nullish(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { status, reason, bookingId, ownerId, cancelledByUser } = input;

      interface DataUpdateType {
        ownerId?: null;
        status: TripStatus;
      };

      let dataUpdate: DataUpdateType = {
        status: status,
      };

      let dataCreateHistory = {
        bookingId: bookingId,
        actorId: ownerId,
        action: status,
        reason: "",
      };

      if (status == TripStatus.CANCELLED) {
        if (cancelledByUser) {
          dataUpdate = { status: TripStatus.CANCELLED, ownerId: null };
        } else {
          dataUpdate = { status: TripStatus.PENDING, ownerId: null };
        }

        dataCreateHistory = { ...dataCreateHistory, reason: reason };
      }

      await ctx.prisma.booking.update({
        where: { id: bookingId },
        data: dataUpdate,
      });
      return ctx.prisma.history.create({
        data: dataCreateHistory,
      });
    }),
  fetchByBookingId: publicProcedure
    .input(
      z.object({
        bookingId: z.number(),
      }),
    )
    .query(({ ctx, input }) => {
      const { bookingId } = input;
      return ctx.prisma.booking.findFirst({
        orderBy: { dateCreated: "desc" },
        where: { id: bookingId },
        include: {
          user: {
            select: {
              firstname: true,
              lastname: true,
              phone: true,
              profile: true,
            },
          },
        },
      });
    }),
  fetchByBookingHithHistoryById: publicProcedure
    .input(
      z.object({
        bookingId: z.number(),
      }),
    )
    .query(({ ctx, input }) => {
      const { bookingId } = input;
      const BookingDetails = ctx.prisma.booking.findFirst({
        orderBy: { dateCreated: "desc" },
        where: { id: bookingId },
        include: {
          histories: {
            include: {
              user: {
                select: {
                  firstname: true,
                  lastname: true,
                },
              },
            },
          },
          owner: true,
          user: true,
        },
      });
      return BookingDetails;
    }),
  infiniteBooking: publicProcedure
    .input(
      z.object({
        limit: z.number().min(1).max(100).nullish(),
        userId: z.string(),
        cursor: z.number().nullish(), // <-- "cursor" needs to exist, but can be any type
      }),
    )
    .query(async ({ ctx, input }) => {
      const limit = input.limit ?? 20;
      const { cursor, userId } = input;
      const items = await ctx.prisma.booking.findMany({
        take: limit + 1, // get an extra item at the end which we'll use as next cursor
        orderBy: {
          dateCreated: "desc",
        },
        where: {
          AND: [
            // { ownerId: null },
            {
              OR: [
                {
                  status: TripStatus.ACCEPTED,
                },
                {
                  status: TripStatus.PENDING,
                },
                {
                  status: TripStatus.PICKED_UP,
                },
                {
                  status: TripStatus.ON_THE_WAY,
                },
              ],
            },
            { userId: userId },
          ],
        },
        cursor: cursor ? { id: cursor } : undefined,
      });
      let nextCursor: typeof cursor | undefined = undefined;
      if (items.length > limit) {
        const nextItem = items.pop();
        nextCursor = nextItem!.id;
      }
      return {
        items,
        nextCursor,
      };
    }),
  infiniteBookingHistory: publicProcedure
    .input(
      z.object({
        limit: z.number().min(1).max(100).nullish(),
        userId: z.string(),
        cursor: z.number().nullish(), // <-- "cursor" needs to exist, but can be any type
      }),
    )
    .query(async ({ ctx, input }) => {
      const limit = input.limit ?? 20;
      const { cursor, userId } = input;
      const items = await ctx.prisma.booking.findMany({
        take: limit + 1, // get an extra item at the end which we'll use as next cursor
        orderBy: {
          dateCreated: "desc",
        },
        where: {
          AND: [
            //{ ownerId: null },
            {
              OR: [
                {
                  status: TripStatus.CANCELLED,
                },
                {
                  status: TripStatus.COMPLETED,
                },
              ],
            },
            { userId: userId },
          ],
        },
        cursor: cursor ? { id: cursor } : undefined,
      });
      let nextCursor: typeof cursor | undefined = undefined;
      if (items.length > limit) {
        const nextItem = items.pop();
        nextCursor = nextItem!.id;
      }
      return {
        items,
        nextCursor,
      };
    }),
  all: publicProcedure
    .input(
      z
        .object({
          ownerId: z.string(),
        })
        .optional(),
    )
    .query(({ ctx, input }) =>
      ctx.prisma.booking.findMany({
        where: {
          OR: [
            { status: "PENDING" },
            {
              ownerId: input?.ownerId,
            },
          ],
        },
        orderBy: {
          dateScheduled: "asc",
        },
        include: {
          user: true,
        },
      }),
    ),
  createBooking: protectedProcedure
    .input(
      z.object({
        userId: z.string(),
        ownerId: z.string(),
        tripId: z.number(),
        pkgId: z.number(),
        type: z.union([z.literal("LATLONG"), z.literal("TRIP")]),
        status: z.union([
          z.literal("PENDING"),
          z.literal("ACCEPTED"),
          z.literal("CANCELLED"),
          z.literal("PICKED_UP"),
          z.literal("COMPLETED"),
          z.literal("ON_THE_WAY"),
        ]),
        transpo_type: z.union([
          z.literal("VANS"),
          z.literal("BOAT"),
          z.literal("MOTORCYCLE"),
          z.literal("TRICYCLE"),
        ]),
        land_transpo_type: z.union([
          z.literal("SUV"),
          z.literal("CAR"),
          z.literal("VANS"),
        ]),
        place_activity_fee: z.number(),
        transpo_rate: z.number(),
        passenger_capacity: z.number(),
        description: z.string(),
        places: z.string(),
        pickup_lat: z.number(),
        pickup_long: z.number(),
        pickup_area_name: z.string(),
        dateCreated: z.date(),
        dateUpdated: z.date(),
        dateScheduled: z.date(),
        allowed_other: z.boolean(),
      }),
    )
    .mutation(({ ctx, input }) =>
      ctx.prisma.booking.create({
        data: input,
      }),
    ),
});
