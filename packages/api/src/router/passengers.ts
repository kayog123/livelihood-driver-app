import { TRPCError } from "@trpc/server";
import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "../trpc";

export const passengersRouter = createTRPCRouter({
  create: publicProcedure
    .input(
      z.object({
        bookingId: z.number(),
        userId: z.string(),
        passenger_count: z.number(),
        capacity: z.number(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const data = await ctx.prisma.passengers.groupBy({
        by: ["bookingId"],
        where: {
          AND: [{ bookingId: input.bookingId }],
        },
        _sum: {
          passenger_count: true,
        },
      });

      const currentPassengerCount =
        data[0]?._sum.passenger_count == undefined
          ? 0
          : data[0]?._sum.passenger_count;

      if (currentPassengerCount != undefined) {
        const handleCountPassenger = input.capacity - currentPassengerCount;
        if (handleCountPassenger >= input.passenger_count) {
          return ctx.prisma.passengers.create({
            data: {
              bookingId: input.bookingId,
              userId: input.userId,
              passenger_count: input.passenger_count,
            },
          });
        } else {
          throw new TRPCError({
            code: "BAD_REQUEST",
            message:
              "Not enough passenger slots for this trip. " +
              handleCountPassenger +
              "slots available",
            // optional: pass the original error to retain stack trace
          });
        }
      } else {
        throw new TRPCError({
          code: "INTERNAL_SERVER_ERROR",
          message: "An unexpected error occurred, please try again later.",
          // optional: pass the original error to retain stack trace
        });
      }
    }),
  // deleteAllRecord: publicProcedure
  //   .input(z.object({}))
  //   .mutation(async ({ ctx, input }) => {
  //     const deleteUsers = await ctx.prisma.passengers.deleteMany({});
  //     return deleteUsers;
  //   }),
});
