import { observable } from "@trpc/server/observable";
import { z } from "zod";

import { type Message, type User } from "@acme/db";
import { pusherServer } from "@acme/pusher";

import { createTRPCRouter, publicProcedure } from "../trpc";

interface MessageData extends Message {
  user: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
}

export const messageRouter = createTRPCRouter({
  byChatRoomIdInfinite: publicProcedure
    .input(
      z.object({
        chatRoomId: z.number(),
        limit: z.number().optional(),
        cursor: z
          .object({
            id: z.number(),
          })
          .optional(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { chatRoomId, limit = 20, cursor } = input;

      const messages = await ctx.prisma.message.findMany({
        where: { chatRoomId },
        take: limit + 1,
        orderBy: {
          dateCreated: "desc",
        },
        include: {
          user: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
        },
        cursor,
      });

      let nextCursor: typeof cursor | undefined;

      if (messages.length > limit) {
        const nextMessage = messages.pop();
        nextCursor = { id: nextMessage!.id };
      }

      return { messages, nextCursor };
    }),
  create: publicProcedure
    .input(
      z.object({
        chatRoomId: z.number(),
        userId: z.string(),
        message: z.string(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { chatRoomId, userId } = input;

      const [message, chatRoom] = await ctx.prisma.$transaction([
        ctx.prisma.message.create({
          data: {
            ...input,
            isRead: "",
          },
          include: {
            user: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                profile: true,
              },
            },
          },
        }),
        ctx.prisma.chatRoom.update({
          where: { id: input.chatRoomId },
          data: {
            dateUpdated: new Date(),
          },
          include: {
            owner: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                profile: true,
              },
            },
            user: {
              select: {
                id: true,
                email: true,
                firstname: true,
                lastname: true,
                profile: true,
              },
            },
            messages: {
              take: 1,
              orderBy: {
                dateCreated: "desc",
              },
            },
          },
        }),
      ]);

      await pusherServer.trigger(
        `user__${userId}-chatrooms`,
        "chatrooms-update",
        chatRoom,
      );

      const chatReceiverId =
        userId == chatRoom.userId ? chatRoom.ownerId : chatRoom.userId;

      await pusherServer.trigger(
        `user__${chatReceiverId}-chatrooms`,
        "chatrooms-count",
        1,
      );
      await pusherServer.trigger(
        `chatroom__${chatRoomId}-messages`,
        "chatroom-messages-update",
        message,
      );

      return message;
    }),
  createMessage: publicProcedure
    .input(
      z.object({
        messageId: z.string(),
        roomId: z.number(),
        userId: z.string(),
        isRead: z.string(),
        message: z.string().min(1),
        receiverId: z.string(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { roomId, userId, isRead, message, messageId, receiverId } = input;
      const messageDetails = {
        chatRoomId: roomId,
        userId,
        isRead,
        message,
      };
      const [createMessage, chatRoom] = await ctx.prisma.$transaction([
        ctx.prisma.message.create({
          data: messageDetails,
        }),
        ctx.prisma.chatRoom.update({
          where: {
            id: roomId,
          },
          data: {
            dateUpdated: new Date(),
          },
        }),
      ]);

      console.log("chatRoom triggered", chatRoom);
      await pusherServer.trigger(
        `user__${userId}-chatrooms`,
        "chatrooms-update",
        chatRoom,
      );

      const chatReceiverId =
        userId == chatRoom.userId ? chatRoom.ownerId : chatRoom.userId;

      await pusherServer.trigger(
        `user__${chatReceiverId}-chatrooms`,
        "chatrooms-count",
        1,
      );

      await pusherServer.trigger(
        `chatroom__${roomId}-messages`,
        "chatroom-messages-update",
        createMessage,
      );

      return { ...createMessage, messageId, receiverId };
    }),
  queryInfiniteMessage: publicProcedure
    .input(
      z.object({
        limit: z.number().min(1).max(100).nullish(),
        userId: z.string(),
        chatRoom: z.string(),
        cursor: z.number().nullish(), //  <-- "cursor" needs to exist, but can be any type
      }),
    )
    .query(async ({ ctx, input }) => {
      const limit = input.limit ?? 20;
      const { cursor, userId, chatRoom } = input;
      const items = await ctx.prisma.message.findMany({
        take: limit + 1, // get an extra item at the end which we'll use as next cursor
        orderBy: {
          dateCreated: "desc",
        },
        where: {
          AND: [{ chatRoomId: Number(chatRoom) }],
        },
        include: {
          user: {
            select: {
              firstname: true,
              lastname: true,
              id: true,
              profile: true,
            },
          },
        },
        cursor: cursor ? { id: cursor } : undefined,
      });
      let nextCursor: typeof cursor | undefined = undefined;
      if (items.length > limit) {
        const nextItem = items.pop();
        nextCursor = nextItem!.id;
      }
      return {
        items,
        nextCursor,
      };
    }),
  markMessageAsRead: publicProcedure
    .input(
      z.object({
        dataList: z.array(z.array(z.string())),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { dataList } = input;
      const updateRowList = [];
      dataList.forEach(async (messageRow: Array<string>) => {
        const messageId = Number(messageRow[0]);
        const isRead = messageRow[1];

        if (messageId != undefined && isRead != undefined) {
          const updateRowMessage = await ctx.prisma.message.update({
            where: {
              id: messageId,
            },
            data: {
              isRead: isRead,
            },
          });
          updateRowList.push(updateRowMessage);
        }
      });
    }),
});
