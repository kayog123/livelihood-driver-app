import { TRPCError } from "@trpc/server";
import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "../trpc";

enum UserRole {
  ADMIN = "ADMIN",
  USER = "USER",
  MODERATOR = "MODERATOR",
}
enum TripStatus {
  PENDING = "PENDING",
  ACCEPTED = "ACCEPTED",
  CANCELLED = "CANCELLED",
  COMPLETED = "COMPLETED",
}

enum TranspoType {
  VANS = "VANS",
  BOAT = "BOAT",
  MOTORCYCLE = "MOTORCYCLE",
  TRICYCLE = "TRICYCLE",
}
enum LandTranspoType {
  SUV = "SUV",
  CAR = "CAR",
  VANS = "VANS", //private VAN
}

enum Trip {
  LATLONG = "LATLONG",
  TRIP = "TRIP",
}

export const scheduleRouter = createTRPCRouter({
  create: publicProcedure
    .input(
      z.object({
        userId: z.string().min(1),
        type: z.string().min(1),
        pickup_lat: z.number(),
        pickup_long: z.number(),
        pickup_area_name: z.string().min(1),
        notes: z.string(),
        places: z.string(),
        dateScheduled: z.date(),
        transpo_rate: z.number(),
        passengers: z.number(),
        place_activity_fee: z.number(),
        transpo_type: z.string(),
        land_transpo_type: z.string(),
        allowed_other: z.boolean(),
        tripId: z.number(),
        passenger_capacity: z.number(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const {
        userId,
        tripId,
        type,
        pickup_lat,
        pickup_long,
        pickup_area_name,
        places,
        passengers,
        place_activity_fee,
        transpo_type,
        transpo_rate,
        notes,
        dateScheduled,
        land_transpo_type,
        allowed_other,
        passenger_capacity,
      } = input;

      let TT;
      if (transpo_type != undefined) {
        switch (transpo_type) {
          case "boat":
            TT = TranspoType.BOAT;
            break;
          case "motorcycle":
            TT = TranspoType.MOTORCYCLE;
            break;
          case "vans":
            TT = TranspoType.VANS;
            break;
          case "tricycle":
            TT = TranspoType.TRICYCLE;
            break;
          default:
            TT = TranspoType.BOAT;
        }
      } else {
        throw new TRPCError({
          code: "INTERNAL_SERVER_ERROR",
          message: "An unexpected error occurred, please try again later.",
          // optional: pass the original error to retain stack trace
        });
      }

      let LTP;
      if (land_transpo_type != undefined) {
        switch (land_transpo_type) {
          case "suv":
            LTP = LandTranspoType.SUV;
            break;
          case "car":
            LTP = LandTranspoType.CAR;
            break;
          case "vans":
            LTP = LandTranspoType.VANS;
            break;
        }
      } else {
        throw new TRPCError({
          code: "INTERNAL_SERVER_ERROR",
          message: "An unexpected error occurred, please try again later.",
          // optional: pass the original error to retain stack trace
        });
      }

      const booking = {
        userId,
        type: type == "TRIP" ? Trip.TRIP : Trip.LATLONG,
        pickup_lat,
        pickup_long,
        pickup_area_name,
        places,
        transpo_rate,
        description: notes,
        dateScheduled,
        allowed_other,
        passengers: {
          create: {
            passenger_count: passengers,
            userId,
          },
        },
        passenger_capacity,
        tripId,
        transpo_type: TT,
        place_activity_fee,
      };

      if (transpo_type == "vans") {
        (booking as any).land_transpo_type = LTP;
      }

      const bookingResponse = await ctx.prisma.booking.create({
        data: booking,
      });

      return bookingResponse;
    }),
  fetchByUserId: publicProcedure
    .input(z.object({ userId: z.string() }))
    .query(({ ctx, input }) => {
      const { userId } = input;
      return ctx.prisma.booking.findMany({
        orderBy: {
          dateCreated: "asc",
        },
        where: { userId: userId },
      });
    }),
  fetchByTranspoType: publicProcedure
    .input(z.object({ transpo_type: z.string() }))
    .query(async ({ ctx, input }) => {
      const { transpo_type } = input;
      let transpo;
      if (transpo_type == "vans") {
        transpo = TranspoType.VANS;
      } else {
        transpo = TranspoType.BOAT;
      }
      const result = await ctx.prisma.booking.findMany({
        where: {
          AND: [
            { transpo_type: transpo },
            { allowed_other: true },
            { type: Trip.TRIP },
          ],
        },
        include: {
          passengers: {
            select: { passenger_count: true },
          },
        },
      });

      const updatedData = result.map((items: any) => {
        let ptc = 0;
        items.passengers.forEach(
          (passenger: {
            id: number;
            userId: string;
            passenger_count: number;
            bookingId: number;
          }) => {
            ptc = ptc + passenger.passenger_count;
          },
        );
        return {
          ...items,
          passengers_total_count: ptc,
        };
      });

      return updatedData;
    }),
});
