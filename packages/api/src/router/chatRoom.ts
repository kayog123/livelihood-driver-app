import { observable } from "@trpc/server/observable";
import { z } from "zod";

import { type ChatRoom, type Message, type User } from "@acme/db";
import { pusherServer } from "@acme/pusher";

import { createTRPCRouter, publicProcedure } from "../trpc";

interface ChatRoomData extends ChatRoom {
  user: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
  owner: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
  messages: Message[];
}

export const chatRoomRouter = createTRPCRouter({
  all: publicProcedure
    .input(
      z.object({
        userId: z.string(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { userId } = input;

      const chatRooms = await ctx.prisma.chatRoom.findMany({
        where: {
          OR: [{ ownerId: userId }, { userId }],
        },
        include: {
          owner: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          user: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          messages: {
            take: 1,
            orderBy: {
              dateCreated: "desc",
            },
          },
        },
      });

      return chatRooms;
    }),
  allInfinite: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        limit: z.number().optional(),
        cursor: z
          .object({
            id: z.number(),
          })
          .optional(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { userId, limit = 20, cursor } = input;

      const chatRooms = await ctx.prisma.chatRoom.findMany({
        where: {
          OR: [{ ownerId: userId }, { userId }],
        },
        take: limit + 1,
        orderBy: {
          dateUpdated: "desc",
        },
        include: {
          owner: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          user: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          messages: {
            take: -1,
          },
        },
        cursor,
      });

      let nextCursor: typeof cursor | undefined;

      if (chatRooms.length > limit) {
        const nextChatRoom = chatRooms.pop();
        nextCursor = { id: nextChatRoom!.id };
      }

      return { chatRooms, nextCursor };
    }),
  create: publicProcedure
    .input(
      z.object({
        ownerId: z.string(),
        userId: z.string(),
        isEnabled: z.boolean(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { ownerId, userId } = input;

      const chatRoom = await ctx.prisma.chatRoom.create({
        data: input,
        include: {
          owner: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          user: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          messages: {
            take: 1,
            orderBy: {
              dateCreated: "desc",
            },
          },
        },
      });

      await pusherServer.trigger(
        `user__${ownerId}-chatroom`,
        "chatroom-new",
        chatRoom,
      );
      await pusherServer.trigger(
        `user__${userId}-chatroom`,
        "chatroom-new",
        chatRoom,
      );
      return chatRoom;
    }),
  byOwnerId: publicProcedure
    .input(
      z.object({
        ownerId: z.string(),
      }),
    )
    .query(({ ctx, input }) => {
      const { ownerId } = input;

      return ctx.prisma.chatRoom.findMany({
        where: { ownerId },
        include: {
          user: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          messages: {
            take: 1,
            orderBy: {
              dateCreated: "desc",
            },
          },
        },
      });
    }),
  byUserId: publicProcedure
    .input(
      z.object({
        userId: z.string(),
      }),
    )
    .query(({ ctx, input }) => {
      const { userId } = input;

      return ctx.prisma.chatRoom.findMany({
        where: { userId },
        include: {
          owner: {
            select: {
              id: true,
              email: true,
              firstname: true,
              lastname: true,
              profile: true,
            },
          },
          messages: {
            take: 1,
            orderBy: {
              dateCreated: "desc",
            },
          },
        },
      });
    }),
});
