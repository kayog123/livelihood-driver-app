import { z } from "zod";

import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const userRouter = createTRPCRouter({
  all: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.user.findMany({ orderBy: { id: "desc" } });
  }),
  byId: publicProcedure
    .input(z.object({ id: z.string() }))
    .query(async ({ ctx, input }) =>
      ctx.prisma.user.findFirst({ where: { id: input.id } }),
    ),
  create: publicProcedure
    .input(
      z.object({
        //id: z.number(),
        firstname: z.string(),
        lastname: z.string(),
        //middlename: z.string(),
        gender: z.string(),
        role: z.string(),
        email: z.string().email(),
        address1: z.string(),
        address2: z.string(),
        city: z.string(),
        state: z.string(),
        country: z.string(),
        zipCode: z.string(),
        phone: z.string(),
        status: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.user.create({ data: input });
    }),
  update: publicProcedure
    .input(
      z.object({
        id: z.string(),
        firstname: z.string(),
        lastname: z.string(),
        birthdate: z.string(),
        gender: z.string(),
        phone: z.string(),
        address1: z.string(),
        address2: z.string(),
        city: z.string(),
        state: z.string(),
        country: z.string(),
        zipCode: z.string(),
        role: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      const { id, ...rest } = input;
      return ctx.prisma.user.update({
        where: { id },
        data: rest,
      });
    }),
  delete: publicProcedure.input(z.string()).mutation(({ ctx, input }) => {
    return ctx.prisma.user.delete({ where: { id: input } });
  }),
});
