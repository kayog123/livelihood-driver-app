import { TRPCError } from "@trpc/server";
import { z } from "zod";

import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const roomRouter = createTRPCRouter({
  createRoom: publicProcedure
    .input(
      z.object({
        ownerId: z.string(),
        userId: z.string(),
        isEnabled: z.boolean(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { userId, ownerId } = input;

      const roomResult = await ctx.prisma.chatRoom.findFirst({
        where: {
          AND: [{ userId: userId }, { ownerId: ownerId }],
        },
      });

      if (!roomResult) {
        return await ctx.prisma.chatRoom.create({
          data: {
            ...input,
          },
        });
      }
    }),
  infiniteRoomList: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        cursor: z.number().nullish(),
        limit: z.number().min(1).max(100).nullish(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { userId, cursor, limit } = input;
      const DEFAULT_LIMIT = 20;
      const limitCount = limit ?? DEFAULT_LIMIT;
      const USER_DETAIL = {
        select: {
          firstname: true,
          lastname: true,
          profile: true,
        },
      };
      const roomList = await ctx.prisma.chatRoom.findMany({
        orderBy: {
          dateUpdated: "desc",
        },
        include: {
          _count: {
            select: { messages: true },
          },
          owner: USER_DETAIL,
          user: USER_DETAIL,
          messages: {
            select: {
              message: true,
              dateCreated: true,
              userId: true,
            },
            orderBy: {
              dateCreated: "desc", // Assuming you have a "createdAt" field for ordering by the latest records
            },
            take: 1,
          },
        },
        where: {
          AND: [
            {
              OR: [
                {
                  userId: userId,
                },
                {
                  ownerId: userId,
                },
              ],
            },
            {
              messages: {
                some: {},
              },
            },
          ],
        },
        cursor: cursor ? { id: cursor } : undefined,
      });

      let nextCursor: typeof cursor | undefined = undefined;
      if (roomList.length > limitCount) {
        const nextItem = roomList.pop();
        nextCursor = nextItem!.id;
      }
      return {
        roomList,
        nextCursor,
      };
    }),

  roomById: publicProcedure
    .input(
      z.object({
        roomId: z.number(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { roomId } = input;

      const USER_INFO = {
        select: {
          id: true,
          firstname: true,
          lastname: true,
          phone: true,
          email: true,
          profile: true,
        },
      };

      return ctx.prisma.chatRoom.findFirst({
        where: {
          id: Number(roomId),
        },
        include: {
          owner: USER_INFO,
          user: USER_INFO,
        },
      });
    }),
  getRoomId: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        ownerId: z.string(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { userId, ownerId } = input;

      return ctx.prisma.chatRoom.findFirst({
        where: {
          AND: [{ userId }, { ownerId }],
        },
        select: { id: true },
      });
    }),
  getChatRoomId: publicProcedure
    .input(
      z.object({
        ownerId: z.string().nullish(),
        userId: z.string().nullish(),
      }),
    )
    .query(async ({ ctx, input }) => {
      const { ownerId, userId } = input;

      if (userId == null || ownerId == null) {
        throw new TRPCError({
          code: "UNAUTHORIZED",
          //message: "Order was locked in to another driver",
        });
      }
      return ctx.prisma.chatRoom.findFirst({
        where: {
          AND: [{ ownerId }, { userId }],
        },
        select: { id: true },
      });
    }),
});
