import { z } from "zod";

import { createTRPCRouter, publicProcedure } from "../trpc";

export const contactRouter = createTRPCRouter({
  all: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.contact.findMany({
      orderBy: { id: "desc" },
    });
  }),
  byId: publicProcedure
    .input(z.object({ userId: z.string() }))
    .query(({ ctx, input }) => {
      const data = ctx.prisma.contact.findFirst({
        where: { userId: input.userId },
      });
      return data;
    }),
  byIdAllData: publicProcedure
    .input(z.object({ userId: z.string() }))
    .query(({ ctx, input }) => {
      const data = ctx.prisma.contact.findMany({
        where: {
          AND: [{ userId: input.userId }, { show: true }],
        },
      });
      return data;
    }),
  create: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        firstname: z.string(),
        lastname: z.string(),
        relationship: z.string(),
        email: z.string(),
        address1: z.string(),
        address2: z.string(),
        city: z.string(),
        state: z.string(),
        country: z.string(),
        zipCode: z.string(),
        phone: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.contact.create({ data: input });
    }),
  update: publicProcedure
    .input(
      z.object({
        id: z.number(),
        firstname: z.string(),
        lastname: z.string(),
        relationship: z.string(),
        email: z.string(),
        address1: z.string(),
        address2: z.string(),
        city: z.string(),
        state: z.string(),
        country: z.string(),
        zipCode: z.string(),
        phone: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.contact.update({
        where: { id: input.id },
        data: {
          firstname: input.firstname,
          lastname: input.lastname,
          relationship: input.relationship,
          email: input.email,
          address1: input.address1,
          address2: input.address2,
          city: input.city,
          state: input.state,
          country: input.country,
          zipCode: input.zipCode,
          phone: input.phone,
        },
      });
    }),
  updateVisibilityById: publicProcedure
    .input(
      z.object({
        id: z.number(),
        show: z.boolean(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.contact.update({
        where: { id: input.id },
        data: {
          show: input.show,
        },
      });
    }),
  delete: publicProcedure.input(z.string()).mutation(({ ctx, input }) => {
    return ctx.prisma.user.delete({ where: { id: input } });
  }),
});
