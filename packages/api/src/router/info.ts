import { z } from "zod";

import { createTRPCRouter, protectedProcedure, publicProcedure } from "../trpc";

export const infoRouter = createTRPCRouter({
  byId: publicProcedure
    .input(z.object({ userId: z.string() }))
    .query(({ ctx, input }) => {
      const data = ctx.prisma.info.findFirst({
        where: { userId: input.userId },
      });
      return data;
    }),
  create: publicProcedure
    .input(
      z.object({
        userId: z.string(),
        license: z.string(),
        type: z.string(),
        vehicle: z.string(),
        permit: z.string(),
        description: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      //const { userId, license, type, vehicle, permit, description } = input;
      return ctx.prisma.info.create({ data: input });
    }),
  update: publicProcedure
    .input(
      z.object({
        id: z.number(),
        userId: z.string(),
        license: z.string(),
        type: z.string(),
        vehicle: z.string(),
        permit: z.string(),
        description: z.string(),
      }),
    )
    .mutation(({ ctx, input }) => {
      return ctx.prisma.info.update({ where: { id: input.id }, data: input });
    }),
});
