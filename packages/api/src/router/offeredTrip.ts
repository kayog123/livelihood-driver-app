import { z } from "zod";

import { createTRPCRouter, protectedProcedure } from "../trpc";

export const offeredTripRouter = createTRPCRouter({
  byUserId: protectedProcedure
    .input(
      z.object({
        userId: z.string(),
      }),
    )
    .query(({ ctx, input }) => {
      const { userId } = input;

      return ctx.prisma.offeredTrip.findMany({
        where: { userId },
      });
    }),
  byTripId: protectedProcedure
    .input(
      z.object({
        tripId: z.number(),
      }),
    )
    .query(({ ctx, input }) => {
      const { tripId } = input;

      return ctx.prisma.offeredTrip.findMany({
        where: { tripId },
      });
    }),
  byUnique: protectedProcedure
    .input(
      z.object({
        userId: z.string(),
        tripId: z.number(),
        pkgId: z.number(),
        type: z.union([
          z.literal("VANS"),
          z.literal("BOAT"),
          z.literal("MOTORCYCLE"),
          z.literal("TRICYCLE"),
        ]),
      }),
    )
    .query(({ ctx, input }) => {
      const { userId, tripId, pkgId, type } = input;

      return ctx.prisma.offeredTrip.findUnique({
        where: {
          userId_tripId_pkgId_type: {
            userId,
            tripId,
            pkgId,
            type,
          },
        },
      });
    }),
  toggleActivate: protectedProcedure
    .input(
      z.object({
        userId: z.string(),
        tripId: z.number(),
        pkgId: z.number(),
        type: z.union([
          z.literal("VANS"),
          z.literal("BOAT"),
          z.literal("MOTORCYCLE"),
          z.literal("TRICYCLE"),
        ]),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const { userId, tripId, pkgId, type } = input;

      const exists = await ctx.prisma.offeredTrip.findUnique({
        where: {
          userId_tripId_pkgId_type: {
            userId,
            tripId,
            pkgId,
            type,
          },
        },
      });

      if (exists) {
        return ctx.prisma.offeredTrip.update({
          where: {
            userId_tripId_pkgId_type: { userId, tripId, pkgId, type },
          },
          data: { isActive: !exists.isActive },
        });
      }

      return ctx.prisma.offeredTrip.create({
        data: { userId, tripId, pkgId, type, isActive: true },
      });
    }),
});
