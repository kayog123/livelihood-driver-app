generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider     = "mysql"
  url          = env("DATABASE_URL")
  relationMode = "prisma"
}

model User {
  id              String        @id @default(cuid())
  email           String
  address1        String        @default("")
  address2        String        @default("")
  city            String        @default("")
  country         String        @default("")
  firstname       String        @default("")
  gender          String        @default("")
  lastname        String        @default("")
  phone           String        @default("")
  role            String        @default("")
  state           String        @default("")
  status          String        @default("")
  zipCode         String        @default("")
  birthdate       String        @default("")
  profile         String        @default("") @db.LongText
  info            Info?
  contacts        Contact[]
  bookingsAsOwner Booking[]     @relation("bookingsAsOwner")
  bookingsAsUser  Booking[]     @relation("bookingsAsUser")
  passengers      Passengers[]
  histories       History[]
  offeredTrips    OfferedTrip[]
  messages        Message[]
  ownerChatRooms  ChatRoom[]    @relation("owner")
  userChatRooms   ChatRoom[]    @relation("user")
}

model Contact {
  id           Int     @id @default(autoincrement())
  userId       String?
  firstname    String
  lastname     String
  email        String
  phone        String
  address1     String
  address2     String
  city         String
  state        String
  country      String
  zipCode      String
  relationship String
  show         Boolean @default(true)
  user         User?   @relation(fields: [userId], references: [id])

  @@index([userId])
}

model Passengers {
  id              Int     @id @default(autoincrement())
  userId          String
  passenger_count Int
  bookingId       Int
  booking         Booking @relation(fields: [bookingId], references: [id])
  user            User    @relation(fields: [userId], references: [id])

  @@index([bookingId])
  @@index([userId])
}

model Booking {
  id                 Int          @id @default(autoincrement())
  userId             String
  ownerId            String?
  tripId             Int?
  pkgId              Int?
  type               Trip         @default(TRIP)
  status             TripStatus   @default(PENDING)
  transpo_type       TranspoType
  land_transpo_type  LandCarType?
  place_activity_fee Int?
  transpo_rate       Decimal      @db.Decimal(9, 2)
  passenger_capacity Int
  description        String       @db.LongText
  places             String       @db.VarChar(200)
  pickup_lat         Decimal
  pickup_long        Decimal
  pickup_area_name   String
  dateCreated        DateTime     @default(now())
  dateUpdated        DateTime     @updatedAt
  dateScheduled      DateTime
  allowed_other      Boolean      @default(false)
  histories          History[]
  passengers         Passengers[]
  owner              User?        @relation("bookingsAsOwner", fields: [ownerId], references: [id])
  user               User         @relation("bookingsAsUser", fields: [userId], references: [id])

  @@index([ownerId])
  @@index([userId])
}

model History {
  id          Int        @id @default(autoincrement())
  bookingId   Int
  actorId     String
  dateCreated DateTime   @default(now())
  reason      String?    @default("")
  action      TripStatus
  booking     Booking    @relation(fields: [bookingId], references: [id])
  user        User       @relation(fields: [actorId], references: [id])

  @@index([bookingId])
  @@index([actorId])
}

model Info {
  id          Int      @id @default(autoincrement())
  userId      String   @unique
  license     String
  type        String
  vehicle     String
  permit      String
  description String
  dateCreated DateTime @default(now())
  dateUpdated DateTime @updatedAt
  user        User     @relation(fields: [userId], references: [id])
}

model ChatRoom {
  id          Int       @id @default(autoincrement())
  ownerId     String
  userId      String
  isEnabled   Boolean
  dateCreated DateTime  @default(now())
  dateUpdated DateTime  @updatedAt
  messages    Message[]
  owner       User      @relation("owner", fields: [ownerId], references: [id])
  user        User      @relation("user", fields: [userId], references: [id])

  @@unique([ownerId, userId])
  @@index([ownerId])
  @@index([userId])
}

model Message {
  id          Int      @id @default(autoincrement())
  chatRoomId  Int
  userId      String
  message     String   @default("") @db.LongText
  isRead      String
  dateCreated DateTime @default(now())
  dateUpdated DateTime @updatedAt
  chatRoom    ChatRoom @relation(fields: [chatRoomId], references: [id])
  user        User     @relation(fields: [userId], references: [id])

  @@index([chatRoomId])
  @@index([userId])
}

model OfferedTrip {
  id       Int         @id @default(autoincrement())
  userId   String
  tripId   Int
  pkgId    Int
  isActive Boolean
  type     TranspoType
  user     User        @relation(fields: [userId], references: [id])

  @@unique([tripId, pkgId])
  @@unique([tripId, pkgId, type])
  @@unique([userId, tripId, pkgId, type])
}

enum Trip {
  LATLONG
  TRIP
}

enum TripStatus {
  PENDING
  ACCEPTED
  CANCELLED
  PICKED_UP
  COMPLETED
  ON_THE_WAY
}

enum TranspoType {
  VANS
  BOAT
  MOTORCYCLE
  TRICYCLE
}

enum LandCarType {
  SUV
  CAR
  VANS
}
