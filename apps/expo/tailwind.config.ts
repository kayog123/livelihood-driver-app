// TODO: Add support for TS config files in Nativewind.

// import { type Config } from "tailwindcss";

// import baseConfig from "@acme/tailwind-config";

// export default {
//   presets: [baseConfig],
//   content: ["./app/**/*.{ts,tsx}", "./src/**/*.{ts,tsx}"],
// } satisfies Config;

const config = {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#29990f",
        primaryColorShadow: "#53cd37",
        bgWhite: "#fff",
        bgWhiteShadow: "#f3f0f0",
        secondaryLight: "#f05d2a",
        loading: "rgba(255, 255, 255, 0.5)",
        textColorDark: "#05375a",
        textColorShadow: "#394867",
        colorDarkOutline: "#e6e6e6",
        blueColor: "#00C4FF",
        warningColor: "#E8AA42",
        errorColor: "#FF6969",
        successColor: "#18e630",
      },
      fontFamily: {
        sans: ["Poppins_100Thin"],
        poppinsThin: ["Poppins_200ExtraLight"],
        poppinsReg: ["Poppins_400Regular"],
        Poppins_700Bold: ["Poppins_700Bold"],
        Poppins_800ExtraBold: ["Poppins_800ExtraBold"],
      },
      fontSize: {
        smallFont: "0.275rem", // Custom size
      },
    },
  },
  variants: {
    // ...
  },
  extend: {},
  plugins: [
    // ...
  ],
};

module.exports = config;
