import "expo-dev-client";
import { useState } from "react";
import { registerRootComponent } from "expo";
import { ExpoRoot } from "expo-router";
import { ClerkProvider, SignedIn, SignedOut } from "@clerk/clerk-expo";

import "react-native-reanimated";
import "react-native-gesture-handler";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Provider } from "react-redux";

import Layout from "~/components/Layout";
import ForgotPassword from "~/components/auth/forgotPassword";
import { store } from "~/redux/store";
import Login from "./src/components/auth/login";
import Register from "./src/components/auth/registration";
import VerificationCode from "./src/components/auth/verificationCode";

// Must be exported or Fast Refresh won't update the context
export function App() {
  const ctx = require.context("./src/app");
  const CLERK_PUBLISHABLE_KEY =
    "pk_test_aG9seS13ZWV2aWwtOTQuY2xlcmsuYWNjb3VudHMuZGV2JA";
  type currentSreen = "login" | "register" | "verify" | "forgot";
  const [screen, setScreen] = useState<currentSreen>("login");
  const tokenCache = {
    async getToken(key: string) {
      try {
        return await AsyncStorage.getItem(key);
      } catch (err) {
        console.log("err get", err);
        return null;
      }
    },
    async saveToken(key: string, value: string) {
      try {
        return await AsyncStorage.setItem(key, value);
      } catch (err) {
        console.log("err save", err);
        return;
      }
    },
  };
  return (
    <ClerkProvider
      tokenCache={tokenCache}
      publishableKey={CLERK_PUBLISHABLE_KEY}
    >
      <SignedIn>
        <Provider store={store}>
          <ExpoRoot context={ctx} />
        </Provider>
      </SignedIn>
      <SignedOut>
        {screen == "login" && <Login setScreen={setScreen} />}
        {screen == "register" && <Register setScreen={setScreen} />}
        {screen == "verify" && <VerificationCode setScreen={setScreen} />}
        {screen == "forgot" && <ForgotPassword setScreen={setScreen} />}
      </SignedOut>
    </ClerkProvider>
  );
}

registerRootComponent(App);
