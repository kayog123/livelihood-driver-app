import { useEffect, useState } from "react";

type debounceProps = {
  value: string;
  delay?: number;
};

export default function useDebounce({ value, delay = 500 }: debounceProps) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
}
