import React from "react";

export const useIdExists = (array: any, object: any, attr_selected: string) => {
  const objectId = object[attr_selected];

  const idExists = React.useMemo(() => {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === objectId) {
        return true;
      }
    }
    return false;
  }, [array, objectId]);

  return idExists;
};
