import React, { useState } from "react";
import {
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  type ImageSourcePropType,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Stack, useRouter } from "expo-router";
import { MotiView } from "moti";
import { Skeleton } from "moti/skeleton";
import { useDispatch } from "react-redux";

import { BusinessProps } from "~/helper/type";
import { setSelectedBusiness } from "~/redux/reducer/businessSlice";
import businessList from "../../assets/jsonList/businesslist.json";

type BusinessItemProps = {
  name: string;
  location: string;
  image: ImageSourcePropType;
  selected?: boolean;
  id: number;
  data: BusinessProps;
  grid?: boolean;
  tags?: string;
  desc?: Array<string>;
  width?: string;
  paddingX?: string;
};

const BusinessItemCard = ({
  name,
  location,
  selected = false,
  image,
  id,
  data,
  tags = "boat",
  grid = false,
  desc = [],
  paddingX = "px-5",
}: BusinessItemProps) => {
  const navigation = useRouter();
  const dispatch = useDispatch();
  return (
    <TouchableOpacity
      onPress={() => {
        dispatch(setSelectedBusiness(data));
        navigation.push({
          pathname: "/book",
          params: {
            choice: "motorcycle",
            tagsFrom: "business",
          },
        });
      }}
      className={`h-56 flex-1  `}
    >
      <View className="overflow-hidden rounded-xl ">
        <ImageBackground
          source={image}
          resizeMode="cover"
          className="flex h-full flex-wrap items-center justify-center"
        >
          <View
            className={`absolute bottom-0 w-full items-center bg-[#1c1e216e] ${paddingX}`}
          >
            <Text
              className={`text-bgWhite font-poppinsReg rounded-full  text-center
              text-${grid ? "md" : "lg"} shadow-lg`}
              numberOfLines={2}
            >
              {name}
            </Text>
            <Text
              className="text-bgWhiteShadow font-poppinsReg text-center text-xs"
              numberOfLines={2}
            >
              {location}
            </Text>
          </View>

          {selected && (
            <View className="absolute left-0 right-0 ">
              <View className=" bg-primary border-bgWhite mx-auto flex flex-row items-center justify-center rounded-full border-4">
                <Ionicons name="checkmark-outline" size={40} color="#fff" />
              </View>
            </View>
          )}
        </ImageBackground>
      </View>
    </TouchableOpacity>
  );
};

export default BusinessItemCard;

const styles = StyleSheet.create({
  textShadow: {
    textShadowColor: "#e3e6e9c2",
    textShadowOffset: { width: -1, height: 0.2 },
    textShadowRadius: 1,
  },
});
