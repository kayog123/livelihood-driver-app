import React, { useImperativeHandle, useState } from "react";
import {
  Button,
  Platform,
  Pressable,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import DateTimePicker, {
  type Event,
} from "@react-native-community/datetimepicker";

import { primaryColor } from "~/helper/const";

type DatePickerProps = {
  date: Date;
  setDate: (date: Date) => void;
  dateSelected: boolean;
  setSelectedDate: (value: boolean) => void;
};

const DatePicker: React.FC<DatePickerProps> = ({
  date,
  setDate,
  dateSelected,
  setSelectedDate,
}) => {
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState<boolean>(false);
  const initialDate = new Date();
  const newDate = new Date(initialDate.setDate(initialDate.getDate() + 2));
  const onChange = (event: any, selectedDate: any) => {
    const currentDate = selectedDate;
    setShow(false);
    setDate(currentDate);
    setSelectedDate(true);
  };

  const showMode = (currentMode: any) => {
    if (Platform.OS === "android") {
      setShow(true);
      // for iOS, add a button that closes the picker
    }

    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  return (
    <View className=" rounded-md p-5">
      <TouchableOpacity
        className="bg-primary flex flex-row items-center justify-center space-x-2 rounded-md p-5"
        onPress={showDatepicker}
      >
        <Ionicons name="calendar-outline" size={20} color={"#fff"} />
        <Text className="text-bgWhite font-Poppins_400Regular text-center text-lg">
          {dateSelected
            ? `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`
            : "Select a date"}
        </Text>
      </TouchableOpacity>

      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={newDate}
          mode={"date"}
          //is24Hour={true}
          minimumDate={newDate}
          onChange={onChange}
        />
      )}
    </View>
  );
};

export default DatePicker;
