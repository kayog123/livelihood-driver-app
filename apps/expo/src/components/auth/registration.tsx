import React, { useState } from "react";
import {
  Dimensions,
  Platform,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Toast from "react-native-toast-message";
import Ionicons from "react-native-vector-icons/Ionicons";
import Checkbox from "expo-checkbox";
import { LinearGradient } from "expo-linear-gradient";
import { Stack } from "expo-router";
import { useSignUp } from "@clerk/clerk-expo";
// import { animated, config, useSpring } from "@react-spring/native";
import { MotiView } from "moti";
import { Controller, useForm } from "react-hook-form";

import LoadingScreen from "../../components/Loading";
import SignInWithOAuth from "../../components/SignInWithOAuth";
import { primaryColor, primaryColorShadow } from "../../helper/const";
import PopView from "../animation/PopView";
import SlideView from "../animation/SlideView";

const SocialSignIn = () => {
  return (
    <View className="mt-10 w-full items-center">
      <Text className="text-[#05375a]">or with</Text>
      <View className="flex flex-row gap-2 p-2">
        <View>
          <PopView delay={1500}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-facebook" size={24} color={primaryColor} />
              }
              social="oauth_facebook"
            />
          </PopView>
        </View>
        <View>
          <PopView delay={2000}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-google" size={24} color={primaryColor} />
              }
              social="oauth_google"
            />
          </PopView>
        </View>
        <View>
          <PopView delay={2500}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-twitter" size={24} color={primaryColor} />
              }
              social="oauth_twitter"
            />
          </PopView>
        </View>
      </View>
    </View>
  );
};

type currentSreen = "login" | "register" | "verify";

type RegisterProps = {
  setScreen: (value: currentSreen) => void;
};

const Register = ({ setScreen }: RegisterProps) => {
  const { isLoaded, signUp } = useSignUp();
  const [isChecked, setChecked] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  // const [emailAddress, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<string>("");
  const [confirmPassword, setConfirmPassword] = useState<string>("");
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
  const [passwordConfVisible, setPasswordConfVisible] =
    useState<boolean>(false);
  //const navigation = useRouter();

  const onSignUpPress = async (data: FormValues) => {
    setLoading(true);
    if (!isLoaded) {
      return;
    }

    if (!isChecked) {
      Toast.show({
        type: "error",
        text1: "Please check the aggrement",
      });
      setLoading(false);
      return;
    }

    if (password !== confirmPassword) {
      Toast.show({
        type: "error",
        text1: "Password mismatch!",
      });
      setLoading(false);
      return;
    }

    try {
      await signUp.create({
        emailAddress: data.emailAddress,
        password: data.password,
      });

      // send the email.
      await signUp.prepareEmailAddressVerification({ strategy: "email_code" });

      // change the UI to our pending section.
      //navigation.push("/auth/verificationCode");
      setScreen("verify");
    } catch (err: any) {
      // console.error(JSON.stringify(err, null, 2));
      err.errors?.map(
        (error: { message: string; code: string; longMessage: string }) => {
          Toast.show({
            type: "error",
            text1: error.message,
          });
        },
      );
    }
    setLoading(false);
  };

  type FormValues = {
    emailAddress: string;
    password: string;
    confPassword: string;
  };

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    defaultValues: {
      emailAddress: "",
      password: "",
      confPassword: "",
    },
  });

  return (
    <SafeAreaView>
      {/* <Stack.Screen options={{ title: "Register", headerShown: false }} /> */}
      {loading && <LoadingScreen />}

      <ScrollView className="bg-primary relative flex h-full w-full flex-row">
        <View style={styles.header}>
          <Toast position="top" bottomOffset={20} autoHide={true} />
        </View>
        <View style={styles.footerBox}>
          <MotiView
            from={{ translateY: height }}
            animate={{ translateY: 0 }}
            transition={{
              type: "timing",
              duration: 800,
              scale: {
                type: "spring",
                delay: 50,
              },
            }}
          >
            <View style={styles.footerBoxChild}>
              <Text className={`text-center text-xl text-[#05375a]`}>
                Create an Account
              </Text>
              <View className="mt-2 flex flex-row justify-center py-2">
                <Text>Already have an account? </Text>
                <TouchableOpacity
                  onPress={() => {
                    setScreen("login");
                  }}
                >
                  <Text className="text-primary underline">Login</Text>
                </TouchableOpacity>
              </View>
              <View className="flex-column mt-3 flex w-full">
                <SlideView fromStart="left">
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                      <View className="mb-3">
                        <TextInput
                          placeholder="Email address"
                          value={value}
                          onBlur={onBlur}
                          onChangeText={(value) => onChange(value)}
                          className="bg-bgWhiteShadow  h-14 w-full rounded-md border-0 border-slate-200 p-2 pt-3"
                        />
                        {errors.emailAddress && (
                          <Text className="text-errorColor">
                            {errors.emailAddress.message}
                          </Text>
                        )}
                      </View>
                    )}
                    name="emailAddress"
                    rules={{
                      required: "Email address is required",
                      pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                        message: "Invalid email address",
                      },
                    }}
                  />
                </SlideView>
                <SlideView fromStart="right">
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                      <View className="mb-3">
                        <View className=" bg-bgWhiteShadow flex w-full flex-row  items-center justify-center rounded-md pl-2 pr-2">
                          <TextInput
                            secureTextEntry={!passwordVisible}
                            placeholder="Password"
                            onChangeText={(value) => onChange(value)}
                            value={value}
                            onBlur={onBlur}
                            className="h-14 flex-grow border-0 border-slate-200"
                          />
                          <Pressable
                            onPress={() => {
                              setPasswordVisible(!passwordVisible);
                            }}
                          >
                            <Ionicons
                              name={
                                passwordVisible
                                  ? "eye-outline"
                                  : "eye-off-outline"
                              }
                              size={24}
                              color="black"
                            />
                          </Pressable>
                        </View>
                        {errors.password && (
                          <Text className="text-errorColor">
                            {errors.password.message}
                          </Text>
                        )}
                      </View>
                    )}
                    name="password"
                    rules={{
                      required: "Password is required",
                      maxLength: {
                        value: 100,
                        message: "Maximum password length is 100",
                      },
                      minLength: {
                        value: 8,
                        message: "Minimum password length is 8",
                      },
                    }}
                  />
                </SlideView>
                <SlideView fromStart="left">
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                      <View className="mb-3">
                        <View className=" bg-bgWhiteShadow flex w-full flex-row  items-center justify-center rounded-md pl-2 pr-2">
                          <TextInput
                            secureTextEntry={!passwordConfVisible}
                            placeholder="Confirm password"
                            onChangeText={(value) => onChange(value)}
                            value={value}
                            onBlur={onBlur}
                            className="h-14 flex-grow border-0 border-slate-200"
                          />
                          <Pressable
                            onPress={() => {
                              setPasswordConfVisible(!passwordConfVisible);
                            }}
                          >
                            <Ionicons
                              name={
                                passwordConfVisible
                                  ? "eye-outline"
                                  : "eye-off-outline"
                              }
                              size={24}
                              color="black"
                            />
                          </Pressable>
                        </View>
                        {errors.confPassword && (
                          <Text className="text-errorColor">
                            {errors.confPassword.message}
                          </Text>
                        )}
                      </View>
                    )}
                    name="confPassword"
                    rules={{
                      required: "Confirm password is required",
                      maxLength: {
                        value: 100,
                        message: "Maximum password length is 100",
                      },
                      minLength: {
                        value: 8,
                        message: "Minimum password length is 8",
                      },
                    }}
                  />
                </SlideView>
                <View className="mt-3 flex flex-row ">
                  <Checkbox
                    // style={styles.checkbox}
                    className="mr-1"
                    value={isChecked}
                    onValueChange={setChecked}
                    color={isChecked ? "#4630EB" : undefined}
                  />
                  <Text className="text-md  text-textColorDark">
                    By checking this box, I agree to the terms and condition.
                  </Text>
                </View>
              </View>
              <View className="flex w-full">
                <MotiView
                  from={{ scale: 0, opacity: 0 }}
                  animate={{ scale: 1, opacity: 1 }}
                  transition={{
                    type: "timing",
                    duration: 800,
                    scale: {
                      type: "spring",
                      delay: 1000,
                    },
                  }}
                >
                  <TouchableOpacity onPress={handleSubmit(onSignUpPress)}>
                    <LinearGradient
                      style={styles.loginBtn}
                      colors={[primaryColor, primaryColor]}
                    >
                      <Text style={styles.textSign}>Register</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </MotiView>
              </View>
              <SocialSignIn />
            </View>
          </MotiView>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  header: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  footerBox: {
    flex: 5,
  },
  footerBoxChild: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    width: width,
    height: "100%",
  },
  loginBtn: {
    paddingVertical: 18,
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    marginTop: 30,
  },
  textSign: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
  },
  loading: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    zIndex: 99999,
    height: height,
    width: width,
  },
});

export default Register;
