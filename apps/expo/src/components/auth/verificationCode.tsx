import { useEffect, useState } from "react";
import {
  Dimensions,
  Pressable,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useSignUp } from "@clerk/clerk-expo";

import { primaryColor } from "~/helper/const";
import LoadingScreen from "../Loading";

type currentSreen = "login" | "register" | "verify";

type RegisterProps = {
  setScreen: (value: currentSreen) => void;
};

const { height } = Dimensions.get("screen");

const VerificationCode = ({ setScreen }: RegisterProps) => {
  const { isLoaded, signUp, setActive } = useSignUp();
  const [code, setCode] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [seconds, setSeconds] = useState<number>(8);
  const [retry, setTry] = useState<boolean>(false);

  useEffect(() => {
    if (seconds > 0) {
      const timer = setTimeout(() => {
        setTry(false);
        setSeconds(seconds - 1);
      }, 1000);

      return () => clearTimeout(timer);
    } else {
      setTry(true);
    }
  }, [seconds]);

  // This verifies the user using email code that is delivered.
  const onPressVerify = async () => {
    setLoading(true);
    if (!isLoaded) {
      return;
    }

    try {
      const completeSignUp = await signUp.attemptEmailAddressVerification({
        code,
      });
      await setActive({ session: completeSignUp.createdSessionId });
      setScreen("login");
    } catch (err: any) {
      console.error(JSON.stringify(err, null, 2));
    }
  };
  const resendVerification = async () => {
    await signUp?.prepareEmailAddressVerification({ strategy: "email_code" });
    setSeconds(100);
    setTry(false);
  };

  return (
    <SafeAreaView className="bg-[#ffff]">
      {loading && <LoadingScreen />}
      {/* Changes page title visible on the header */}
      {/* <Stack.Screen
        options={{ title: "Verify Identity", headerShown: false }}
      /> */}

      <View
        className={` flex min-h-screen flex-wrap items-center justify-center p-5`}
      >
        <View className=" p-4e flex w-full flex-wrap items-center justify-center">
          <Text className="text-textColorDark pb-2 text-4xl font-bold">
            Verification Code
          </Text>
          <Text className="text-textColorDark p-5 pb-20 text-center ">
            Please enter 6 digit verification code that have been sent to your
            email address.
          </Text>
          <View className="flex w-full flex-row bg-slate-100">
            <TextInput
              className="    bg-bgWhiteShadow h-16 w-full rounded-xl border-2 border-[#eeeded] p-2"
              onChangeText={(value) => setCode(value)}
              value={code}
              placeholder="Please input verification code here"
            />
          </View>
          <View className="flex flex-row p-3 pb-20">
            {retry ? (
              <Pressable onPress={resendVerification}>
                <Text className="text-primary underline">
                  Resend verification code
                </Text>
              </Pressable>
            ) : (
              <Text className=" text-center text-gray-500">
                {" "}
                Resend verification code ({seconds})
              </Text>
            )}
          </View>
        </View>
        <View className="absolute bottom-0 flex w-full flex-row gap-x-1 pt-5">
          <TouchableOpacity
            onPress={() => {
              setScreen("register");
            }}
            className="flex-1"
          >
            <LinearGradient
              className="  items-center justify-center rounded-lg p-2 pb-[18] pt-[18]"
              colors={["#f8f8f8", "#dddcdc"]}
            >
              <Text className="text-textColorDark">Back</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity onPress={onPressVerify} className="flex-1">
            <LinearGradient
              className="  items-center justify-center rounded-lg p-2 pb-[18] pt-[18]"
              colors={[primaryColor, primaryColor]}
            >
              <Text className="text-bgWhite">Verify</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default VerificationCode;
