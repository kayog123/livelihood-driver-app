import { useState } from "react";
import {
  Dimensions,
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Toast from "react-native-toast-message";
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";
import { Stack } from "expo-router";
import { useSignIn } from "@clerk/clerk-expo";
// import { animated, config, useSpring } from "@react-spring/native";
import { MotiView } from "moti";
import { Controller, useForm } from "react-hook-form";

import LoadingScreen from "../../components/Loading";
import {
  STATUSBAR_HEIGHT,
  primaryColor,
  primaryColorShadow,
} from "../../helper/const";
import SignInWithOAuth from "../SignInWithOAuth";
import PopView from "../animation/PopView";
import SlideView from "../animation/SlideView";

type currentSreen = "login" | "register" | "verify" | "forgot";
const SocialSignIn = () => {
  return (
    <View className="mt-10 w-full items-center">
      <Text className="text-[#05375a]">or with</Text>
      <View className="flex flex-row gap-2 p-2">
        <View>
          <PopView delay={1500}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-facebook" size={24} color={primaryColor} />
              }
              social="oauth_facebook"
            />
          </PopView>
        </View>
        <View>
          <PopView delay={2000}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-google" size={24} color={primaryColor} />
              }
              social="oauth_google"
            />
          </PopView>
        </View>
        <View>
          <PopView delay={2500}>
            <SignInWithOAuth
              icon={
                <Ionicons name="logo-twitter" size={24} color={primaryColor} />
              }
              social="oauth_twitter"
            />
          </PopView>
        </View>
      </View>
    </View>
  );
};
type LoginProps = {
  setScreen: (value: currentSreen) => void;
};
const Login = ({ setScreen }: LoginProps) => {
  const { width } = Dimensions.get("screen");
  const logoWidth = width * 0.9;
  const { isLoaded, signIn, setActive } = useSignIn();

  const [loading, setLoading] = useState<boolean>(false);
  //const [emailAddress, setEmailAddress] = useState<string>("");
  // const [password, setPassword] = useState<string>("");
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);

  type FormValues = {
    emailAddress: string;
    password: string;
  };

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    defaultValues: {
      emailAddress: "",
      password: "",
    },
  });

  const onSignInPress = async (data: FormValues) => {
    setLoading(true);
    if (!isLoaded) {
      return;
    }

    try {
      const completeSignIn = await signIn.create({
        identifier: data.emailAddress,
        password: data.password,
      });
      // This is an important step,
      // This indicates the user is signed in
      await setActive({ session: completeSignIn.createdSessionId });
    } catch (err: any) {
      err.errors?.map(
        (error: { message: string; code: string; longMessage: string }) => {
          Toast.show({
            type: "error",
            text1: error.message,
          });
        },
      );
    }

    setLoading(false);
  };

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      {loading && <LoadingScreen />}
      <ScrollView className={` flex h-full w-full flex-row bg-[#29990f]`}>
        <View style={styles.header}>
          <Toast position="top" bottomOffset={20} autoHide={true} />
          <PopView delay={200}>
            <Image // we will use i for the key because no two (or more) elements in an array will have the same index
              style={{
                width: logoWidth,
                height: logoWidth,
                marginTop: STATUSBAR_HEIGHT,
              }}
              source={require("./../../../assets/logo.png")}
              alt="images"
              className={`z-[-10]  `}
            />
          </PopView>
        </View>
        <View style={styles.footerBox}>
          <MotiView
            from={{ translateY: height }}
            animate={{ translateY: 0 }}
            transition={{
              type: "timing",
              duration: 800,
              scale: {
                type: "spring",
                delay: 50,
              },
            }}
          >
            <View style={styles.footerBoxChild}>
              <Text className={`text-center text-xl text-[#05375a]`}>
                Sign In
              </Text>

              <View className="flex-column mt-3 flex w-full">
                <SlideView delay={1000} fromStart="left">
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                      <View className="mb-3">
                        <TextInput
                          placeholder="Email address"
                          value={value}
                          onBlur={onBlur}
                          onChangeText={(value) => onChange(value)}
                          className="bg-bgWhiteShadow  h-14 w-full rounded-md border-0 border-slate-200 p-2 pt-3"
                        />
                        {errors.emailAddress && (
                          <Text className="text-errorColor">
                            {errors.emailAddress.message}
                          </Text>
                        )}
                      </View>
                    )}
                    name="emailAddress"
                    rules={{
                      required: "Email address is required",
                      pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                        message: "Invalid email address",
                      },
                    }}
                  />
                </SlideView>
                <SlideView delay={1000} fromStart="right">
                  <Controller
                    control={control}
                    render={({ field: { onChange, onBlur, value } }) => (
                      <View className=" bg-bgWhiteShadow flex w-full flex-row items-center  justify-center rounded-md pl-2 pr-2 ">
                        <TextInput
                          secureTextEntry={!passwordVisible}
                          placeholder="Password"
                          onChangeText={(value) => onChange(value)}
                          value={value}
                          onBlur={onBlur}
                          className="h-14 flex-grow border-0 border-slate-200"
                        />
                        <Pressable
                          onPress={() => {
                            setPasswordVisible(!passwordVisible);
                          }}
                        >
                          <Ionicons
                            name={
                              passwordVisible
                                ? "eye-outline"
                                : "eye-off-outline"
                            }
                            size={24}
                            color="black"
                          />
                        </Pressable>
                      </View>
                    )}
                    name="password"
                    rules={{
                      required: "Password is required",
                      maxLength: {
                        value: 100,
                        message: "Maximum password length is 100",
                      },
                      minLength: {
                        value: 8,
                        message: "Minimum password length is 8",
                      },
                    }}
                  />
                  {errors.password && (
                    <Text className="text-errorColor">
                      {errors.password.message}
                    </Text>
                  )}
                </SlideView>
                <View className="mt-3 flex flex-row ">
                  <Pressable
                    onPress={() => {
                      setScreen("forgot");
                    }}
                  >
                    <Text className="text-md  flex-1 text-left text-[#05375a] underline">
                      Forgot password?
                    </Text>
                  </Pressable>
                  <Text className="text-md  flex-1 text-left text-[#05375a] underline">
                    {" "}
                  </Text>
                  <Pressable
                    onPress={() => {
                      setScreen("register");
                    }}
                  >
                    <Text className="text-md  text-[#29990f] flex-1 text-right underline">
                      Register
                    </Text>
                  </Pressable>
                </View>
              </View>
              <View className="mt-[30] flex w-full">
                <MotiView
                  from={{ scale: 0, opacity: 0 }}
                  animate={{ scale: 1, opacity: 1 }}
                  transition={{
                    type: "timing",
                    duration: 800,
                    scale: {
                      type: "spring",
                      delay: 1000,
                    },
                  }}
                >
                  <TouchableOpacity
                    className="bg-[#29990f] w-full items-center justify-center rounded-md py-4"
                    onPress={handleSubmit(onSignInPress)}
                  >
                    <Text className=" text-bgWhite">Login</Text>
                  </TouchableOpacity>
                </MotiView>
              </View>
              <SocialSignIn />
            </View>
          </MotiView>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  header: {
    flex: 1.5,
    justifyContent: "center",
    alignItems: "center",
  },
  footerBox: {
    flex: 3,
    display: "flex",
  },
  footerBoxChild: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    width: width,
    bottom: 0,
    height: "100%",
  },
  loginBtn: {
    paddingVertical: 18,
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
  },
  textSign: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
  },
});

export default Login;
