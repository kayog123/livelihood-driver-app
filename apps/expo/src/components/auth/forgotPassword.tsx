import { Fragment, SyntheticEvent, useState } from "react";
import {
  Dimensions,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

import "react-native-safe-area-context";
import { LinearGradient } from "expo-linear-gradient";
import { useSignIn, useSignUp } from "@clerk/clerk-expo";

import LoadingScreen from "../Loading";

type currentSreen = "login" | "register" | "verify";

type RegisterProps = {
  setScreen: (value: currentSreen) => void;
};

const { height } = Dimensions.get("screen");

const ForgotPassword = ({ setScreen }: RegisterProps) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [code, setCode] = useState("");
  const [successfulCreation, setSuccessfulCreation] = useState(false);
  const [complete, setComplete] = useState(false);
  const [secondFactor, setSecondFactor] = useState(false);
  const [loading, setLoading] = useState<boolean>(false);
  const { isLoaded, signIn, setActive } = useSignIn();
  if (!isLoaded) {
    return null;
  }

  async function create(e: SyntheticEvent) {
    // e.preventDefault();
    // await signIn
    //   ?.create({
    //     strategy: 'reset_password_code',
    //     identifier: email,
    //   })
    //   .then(_ => {
    //     setSuccessfulCreation(true);
    //   })
    //   .catch(err => console.error('error', err.errors[0].longMessage));
  }

  async function reset(e: SyntheticEvent) {
    e.preventDefault();
    // await signIn
    //   ?.attemptFirstFactor({
    //     strategy: 'reset_password_code',
    //     code,
    //     password,
    //   })
    //   .then(result => {
    //     if (result.status === 'needs_second_factor') {
    //       setSecondFactor(true);
    //     } else if (result.status === 'complete') {
    //       setActive({ session: result.createdSessionId });
    //       setComplete(true);
    //     } else {
    //       console.log(result);
    //     }
    //   })
    //   .catch(err => console.error('error', err.errors[0].longMessage));
  }

  return (
    <SafeAreaView className={`bg-[#ffff] `}>
      {loading && <LoadingScreen />}
      {/* Changes page title visible on the header */}
      {/* <Stack.Screen
        options={{ title: "Verify Identity", headerShown: false }}
      /> */}

      <View
        className={`bg flex  h-full flex-wrap items-center justify-center p-5`}
      >
        <View className=" p-4e flex w-full flex-wrap items-center justify-center">
          <Text className="text-textColorDark mb-3 pb-2 text-4xl font-bold">
            Forgot Password
          </Text>
          <Text className="text-textColorDark p-5 pb-20 text-center">
            Please enter 6 digit reset code that have been sent to your email
            address.
          </Text>
          <View className="flex gap-y-2">
            {!successfulCreation && !complete && (
              <View className="flex w-full flex-row bg-slate-100">
                <TextInput
                  className="bg-bgWhiteShadow h-16 w-full rounded-xl border-2 border-[#eeeded] p-2"
                  onChangeText={(value) => setCode(value)}
                  value={code}
                  placeholder="Email address"
                />
              </View>
            )}
            {successfulCreation && !complete && (
              <Fragment>
                <View className="flex w-full flex-row bg-slate-100">
                  <TextInput
                    className="    bg-bgWhiteShadow h-16 w-full rounded-xl border-2 border-[#eeeded] p-2"
                    onChangeText={(value) => setCode(value)}
                    value={code}
                    placeholder="New Password"
                  />
                </View>
                <View className="flex w-full flex-row bg-slate-100">
                  <TextInput
                    className="    bg-bgWhiteShadow h-16 w-full rounded-xl border-2 border-[#eeeded] p-2"
                    onChangeText={(value) => setCode(value)}
                    value={code}
                    placeholder="Code"
                  />
                </View>
              </Fragment>
            )}
          </View>
        </View>
        <View className="absolute bottom-0 flex w-full flex-row gap-x-1 pt-5">
          <TouchableOpacity
            onPress={() => {
              setScreen("login");
            }}
            className="flex-1"
          >
            <LinearGradient
              className="  items-center justify-center rounded-lg p-2 pb-[18] pt-[18]"
              colors={["#f8f8f8", "#dddcdc"]}
            >
              <Text className="text-textColorDark">Back</Text>
            </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity
            //onPress={!successfulCreation ? create : reset}
            className="flex-1"
          >
            <LinearGradient
              className="  items-center justify-center rounded-lg p-2 pb-[18] pt-[18]"
              colors={["#08d4c4", "#01ab9d"]}
            >
              <Text className="text-bgWhite">Verify</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ForgotPassword;
