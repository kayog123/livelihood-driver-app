import {
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import { api } from "~/utils/api";
import businessList from "../../../assets/jsonList/businesslist.json";
import BusinessItemCard from "../BusinessItemCard";
import ItemCard from "../ItemCard";
import LoadingScreen from "../Loading";

const PopularPlaces = ({ title = "Popular Business" }: { title?: string }) => {
  return (
    <View className="mt-10">
      <View className="flex flex-row items-center justify-between">
        <Text
          className="font-Poppins_700Bold text-textColorDark  px-2 text-left text-xl"
          numberOfLines={1}
        >
          {title}
        </Text>
        {/* <Text className="font-poppinsReg text-md text-textColorDark px-2 text-right underline ">
          View All
        </Text> */}
      </View>
      <ScrollView
        horizontal={true}
        className="flex flex-row gap-x-2 p-3"
        showsHorizontalScrollIndicator={false}
      >
        {businessList?.map((result: any, index: number) => {
          return (
            <View className="h-[180] w-40 flex-1" key={`popular_${index}`}>
              <BusinessItemCard
                name={result.name}
                location={result.address}
                image={require("../../../assets/business/bread.jpg")}
                id={result.id}
                data={result}
                paddingX="px-1"
              />
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default PopularPlaces;
