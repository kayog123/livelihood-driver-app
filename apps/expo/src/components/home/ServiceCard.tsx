import { Fragment, type ReactNode } from "react";
import {
  Image,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
  type ImageSourcePropType,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useRouter } from "expo-router";

import { primaryColor, secondaryColor } from "../../helper/const";
import PopView from "../animation/PopView";
import PressScale from "../animation/PressScale";

// type CardProps = {
//   title: string;
//   image: ImageSourcePropType;
//   icon: ReactNode;
//   onPress: () => void;
//   classNames?: string;
// };

// const Card = ({ title, icon, onPress, image, classNames }: CardProps) => {
//   return (
//     <TouchableOpacity
//       className={`flex-1 overflow-hidden rounded-lg ${classNames}`}
//       onPress={onPress}
//     >
//       <ImageBackground source={image} resizeMode="cover" className=" h-28 ">
//         <View className="flex  h-full items-center justify-center bg-[#000000c0]">
//           {icon}
//           <Text className=" rounded-lg text-[#fff]">{title}</Text>
//         </View>
//       </ImageBackground>
//     </TouchableOpacity>
//   );
// };

const Service = () => {
  const navigation = useRouter();

  return (
    <View className="flex-column mb-5 mt-5 flex ">
      <View className="mb-2 mt-2 flex flex-row items-center justify-center gap-x-2">
        <View className="border-textColorDark h-[1] flex-grow border-0 border-t"></View>
        <Text className="font-poppinsReg text-center">Travel with our</Text>
        <View className="border-textColorDark h-[1] flex-grow border border-0 border-t"></View>
      </View>
      <View className="mb-2 flex flex-row flex-wrap justify-between gap-2">
        <View className="w-[30%]">
          <PopView>
            <PressScale
              onPress={() => {
                navigation.push("/boat");
              }}
            >
              <View className="overflow-hidden rounded-lg">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28 "
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="boat-outline" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff]">
                      Island Tour
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>
        <View className="w-[30%]">
          <PopView delay={1200}>
            <PressScale
              onPress={() => {
                navigation.push("/van");
              }}
            >
              <View className=" overflow-hidden rounded-lg ">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28 "
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="bus-outline" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff] ">
                      Land Tour
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>
        <View className="w-[30%]">
          <PopView delay={1400}>
            <PressScale
              onPress={() => {
                navigation.push({
                  pathname: "/book",
                  params: { choice: "tricycle" },
                });
              }}
            >
              <View className=" overflow-hidden rounded-lg ">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28 "
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="bus-outline" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff] ">
                      Tricycle
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>
        <View className="w-[30%]">
          <PopView delay={1400}>
            <PressScale
              onPress={() => {
                navigation.push({
                  pathname: "/book",
                  params: { choice: "motorcycle" },
                });
              }}
            >
              <View className=" overflow-hidden rounded-lg ">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28"
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="bicycle-outline" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff] ">
                      Motorcycle
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>

        <View className="w-[30%]">
          <PopView delay={1400}>
            <PressScale
              onPress={() => {
                navigation.push("/maintenance");
              }}
            >
              <View className=" overflow-hidden rounded-lg ">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28 "
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="car-sport" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff] ">
                      Rent a Car
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>
        <View className="w-[30%]">
          <PopView delay={1400}>
            <PressScale
              onPress={() => {
                navigation.push("/maintenance");
              }}
            >
              <View className=" overflow-hidden rounded-lg ">
                <ImageBackground
                  source={require("./../../../assets/boat-trip.jpg")}
                  resizeMode="cover"
                  className=" h-28 "
                >
                  <View className="flex  h-full items-center justify-center bg-[#000000c0]">
                    <Ionicons name="bicycle-outline" size={56} color={`#fff`} />
                    <Text className=" font-poppinsReg rounded-lg text-[#fff] ">
                      Rent a Bike
                    </Text>
                  </View>
                </ImageBackground>
              </View>
            </PressScale>
          </PopView>
        </View>
      </View>
    </View>
  );
};

export default Service;
