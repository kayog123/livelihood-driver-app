import { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  Pressable,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";

import { BusinessProps } from "~/helper/type";
import businessList from "../../../assets/jsonList/businesslist.json";
import BusinessItemCard from "../BusinessItemCard";
import Card from "../Card";
import ItemCard from "../ItemCard";

const AllBusinesses = () => {
  const [grid, setGrid] = useState(true);

  const [data, setData] = useState<BusinessProps[]>(businessList);
  const navigation = useRouter();

  return (
    <View className="mt-5">
      <View className="flex flex-row items-center justify-between px-5">
        <View className="flex flex-row items-center justify-center">
          <Ionicons name="business-outline" size={30} color="#05375a" />
          <Text className="font-Poppins_700Bold text-textColorDark px-2 text-left text-xl">
            Business List
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.push("/categories");
          }}
          className="flex flex-row items-center justify-center space-x-2"
        >
          <Text className="text-textColorDark font-poppinsReg underline">
            Categories
          </Text>
        </TouchableOpacity>
      </View>

      <View className="m-4 flex pb-10">
        <View className=" space-y-4">
          {/* <Text>{JSON.stringify(businessList)}</Text> */}
          {businessList.map((result: BusinessProps, index: number) => {
            const busImg = require("../../../assets/business/basket.jpg");
            return (
              <View
                className={`bg-primary w-full h-56 rounded-xl `}
                key={`allplaces_${index}`}
              >
                <BusinessItemCard
                  name={result.name}
                  location={result.address}
                  image={busImg}
                  id={result.id}
                  data={result}
                  paddingX="px-1"
                />
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};

export default AllBusinesses;
