import React, { useState } from "react";
import {
  Dimensions,
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { useUser } from "@clerk/clerk-expo";

import Card from "../../components/Card";
import Layout from "../../components/Layout";
import AllBusinesses from "./AllBusiness";
import TopBarHeader from "../TopBarHearder";

const Home = () => {
  const { width } = Dimensions.get("screen");
  const LOGO_SIZE = width / 3;

  return ( 
      <SafeAreaView>
        <TopBarHeader title="Home"/>
      <ScrollView className="flex min-h-screen p-3 pb-2 pt-5">
        <Card>
          <View className=" flex-1 space-y-3">
            <View className="space-y-[-8]">
              <Text className="font-Poppins_700Bold text-md text-lg text-[#fff]">
                Welcome,
              </Text>
            </View>
            <Text className="font-poppinsReg text-[#fff]">
              Have a pleasant day to you!
            </Text>
            {/* <View className="mt-5 ">
              <Pressable onPress={() => {}}>
                <Text className="decoration-green font-poppinsReg flex text-[#fff] underline dark:text-white">
                  Verify Now
                </Text>
              </Pressable>
            </View> */}
          </View>
          <Image
            className="flex-1"
            source={require("./../../../assets/logo.png")}
            style={{ width: LOGO_SIZE, height: LOGO_SIZE }}
            alt=""
          />
        </Card>
        <AllBusinesses />
        {/* <SkeletonCard /> */}
        {/* <View className=" mt-5 h-[40px] "></View> */}
      </ScrollView>
      </SafeAreaView> 
  );
};

export default Home;
