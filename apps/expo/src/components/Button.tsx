import { Pressable, Text } from "react-native";

type BtnProps = {
  title: string;
  onPress?: () => void;
};

const Button = ({ title, onPress }: BtnProps) => {
  return (
    <Pressable
      className="flex h-16 items-center justify-center rounded bg-blue-500 font-bold text-white hover:bg-blue-700"
      onPress={onPress}
    >
      <Text className="flex text-white dark:text-white">{title}</Text>
    </Pressable>
  );
};

export default Button;
