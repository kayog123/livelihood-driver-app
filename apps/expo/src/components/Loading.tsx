import { useEffect, useRef } from "react";
import {
  Animated,
  Dimensions,
  Easing,
  Image,
  StyleSheet,
  Text,
  View,
} from "react-native";
import Svg, { Circle, Rect } from "react-native-svg";

type LayoutProps = {
  text?: string;
};

const LoadingScreen = ({ text = "Loading..." }: LayoutProps) => {
  const spinValue = useRef(new Animated.Value(0)).current;
  const spinValue2 = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    Animated.loop(
      Animated.timing(spinValue, {
        toValue: 1,
        duration: 800,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }, []);

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ["90deg", "270deg"],
  });

  useEffect(() => {
    Animated.loop(
      Animated.timing(spinValue2, {
        toValue: 1,
        duration: 800,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }, []);

  const spin2 = spinValue2.interpolate({
    inputRange: [0, 1],
    outputRange: ["360deg", "0deg"],
  });
  return (
    <View
      //style={styles.loading}
      className="bg-loading z-20 flex h-screen w-full items-center justify-center absolute"
    >
      <View className="relative flex items-center justify-center">
        <Animated.View style={{ transform: [{ rotate: spin }] }}>
          <Svg
            style={styles.container}
            width="120px"
            height="120px"
            viewBox="0 0 100 100"
            preserveAspectRatio="xMidYMid"
            //{...props}
          >
            <Circle
              cx={50}
              cy={50}
              r={32}
              strokeWidth={8}
              stroke="#009387"
              strokeDasharray="50.26548245743669 50.26548245743669"
              fill="none"
              strokeLinecap="round"
            ></Circle>
          </Svg>
        </Animated.View>

        <Animated.View
          style={{ transform: [{ rotate: spin2 }] }}
          className="absolute"
        >
          <Svg
            style={styles.container}
            width="120px"
            height="120px"
            viewBox="0 0 100 100"
            preserveAspectRatio="xMidYMid"
            //{...props}
          >
            <Circle
              cx={50}
              cy={50}
              r={23}
              strokeWidth={8}
              stroke="#0a504b"
              strokeDasharray="36.12831551628262 36.12831551628262"
              strokeDashoffset={38.12831551628262}
              fill="none"
              strokeLinecap="round"
            ></Circle>
          </Svg>
        </Animated.View>
      </View>
      <Text className="text-lg text-[#05375a]">{text}</Text>
    </View>
  );
};

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  loading: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, 0.8)",
    zIndex: 99999,
    height: height,
    width: width,
  },
  container: {
    margin: "auto",
    background: "rgb(255, 255, 255)",
    //display: "block",
    shapeRendering: "auto",
  },
});

export default LoadingScreen;
