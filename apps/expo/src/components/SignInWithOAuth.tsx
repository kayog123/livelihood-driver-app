import { useCallback, type ReactNode } from "react";
import { TouchableOpacity } from "react-native";
import { Stack, useRouter } from "expo-router";
import * as WebBrowser from "expo-web-browser";
import { useOAuth } from "@clerk/clerk-expo";

import { useWarmUpBrowser } from "../hooks/useWarmUpBrowser";

type SignInProps = {
  icon: ReactNode;
  social: "oauth_facebook" | "oauth_google" | "oauth_twitter";
};

WebBrowser.maybeCompleteAuthSession();

const SignInWithOAuth = ({ icon, social }: SignInProps) => {
  useWarmUpBrowser();

  const { startOAuthFlow } = useOAuth({ strategy: social });

  const onPress = useCallback(async () => {
    try {
      const { createdSessionId, signIn, signUp, setActive } =
        await startOAuthFlow();

      if (createdSessionId) {
        if (setActive) {
          await setActive({ session: createdSessionId });
        }
      } else {
        // Use signIn or signUp for next steps such as MFA444
        // await signUp?.update({
        //   lastName: "User",
        //   firstName: "User"
        // })
        
      }
    } catch (err) {
      console.error("OAuth error", err);
    }
  }, []);

  return (
    <TouchableOpacity
      className="bg-bgWhiteShadow rounded-md p-3"
      onPress={onPress}
    >
      {icon}
    </TouchableOpacity>
  );
};
export default SignInWithOAuth;
