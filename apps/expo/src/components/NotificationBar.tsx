import { Fragment, useEffect, useState } from "react";
import { ActivityIndicator, Text, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const NotificationBar = ({
  message = "Message here",
  type,
  showNofify = false,
}: {
  message: string;
  type: "success" | "error" | "warning" | "waiting";
  showNofify?: boolean;
}) => {
  const [barColor, setBarColor] = useState<string>("bg-[#79E0EE]");
  const [iconBar, setIconBar] = useState<string>("close-circle-outline");

  useEffect(() => {
    if (type == "success") {
      setBarColor("bg-[#18e630]");
      setIconBar("checkmark-circle-outline");
    }
    if (type == "error") {
      setBarColor("bg-[#FF6969]");
      setIconBar("close-circle-outline");
    }
    if (type == "warning") {
      setBarColor("bg-[#E8AA42]");
      setIconBar("information-circle-outline");
    }
    if (type == "waiting") {
      setBarColor("bg-[#79E0EE]");
    }
  });

  return (
    <Fragment>
      {showNofify && (
        <View
          className={`absolute flex h-[30] w-full flex-row items-center justify-center space-x-2 ${barColor} z-30 px-5`}
        >
          {type == "waiting" && <ActivityIndicator color="#fff" />}
          {type == ("warning" || "error" || "success") && (
            <Ionicons name={`${iconBar}`} size={26} color="#fff" />
          )}
          <Text className="text-bgWhite font-poppinsReg" numberOfLines={1}>
            {message}
          </Text>
        </View>
      )}
    </Fragment>
  );
};

export default NotificationBar;
