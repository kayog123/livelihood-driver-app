import { useEffect, useRef } from "react";
import { Dimensions, SafeAreaView, StyleSheet, View } from "react-native";
import { useAuth } from "@clerk/clerk-expo";

import Layout from "../Layout";
import MessageList, { type ChildComponentHandles } from "./MessageList";
import TopBarHeader from "../TopBarHearder";

const Messages = ({ navigation }: { navigation: any }) => {
  const { userId, isLoaded } = useAuth();
  const childRef = useRef<ChildComponentHandles | null>(null);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      childRef.current?.refetchMessageList();
    });
  }, [navigation]);
  return (
    <SafeAreaView>
      <TopBarHeader title="Messages List"/>
      <View style={styles.container}>
        {userId && <MessageList userId={userId} ref={childRef} />}
        <View className="absolute bottom-0 h-56 w-full rounded-t-[8px] bg-[#fff]"></View>
      </View>
      </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    height: height,
    width: width,
  },
});

export default Messages;
