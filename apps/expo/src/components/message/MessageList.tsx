import React, {
  Ref,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import {
  Animated,
  Dimensions,
  Image,
  ImageBackground,
  Pressable,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";
import moment from "moment";
import { setChatRoom } from "~/redux/reducer/messageSlice";
import { api } from "~/utils/api";
import { textColorDark } from "~/helper/const";
import { useDispatch, useSelector } from "react-redux";
const SPACING = 20;
const AVATAR_SIZE = 70;
const ITEM_SIZE = AVATAR_SIZE + SPACING * 3;

const MessageCard = ({
  roomId,
  opacity,
  scale,
  usersData,
}: {
  roomId: number;
  opacity: any;
  scale: any;
  usersData: {
    loginUserId: string;
    lastMessageOwner: string;
    firstname: string;
    lastname: string;
    profile: string;
    lastMessage: string;
    dateCreated: string;
    messageCount: number;
    messageData: object;
    isRead: string;
  };
}) => {
  const navigation = useRouter();
  const dispatch = useDispatch();
  const animatedViewByScale = (opacity: any, scale: any) => {
    const styleArray = {
      shadowColor: "#000",
      shadowOffset: { width: 0, height: 10 },
      shadowOpacity: 0.3,
      shadowRadius: 20,
      elevation: 9,
      marginBottom: 13,
      backgroundColor: "rgba(255,255,255,0.92)",
      opacity,
      transform: [{ scale }],
    };
    return styleArray;
  };
  const { width } = Dimensions.get("screen");
  const PROFILE_IMAGE_SIZE = width / 8;
  const LIMIT = 10;
  //const haveReadUser = usersData.isRead.split(",");

  const checkIfUserReadMessage = (arrayList: Array<string>, userId: string) => {
    return arrayList.includes(userId);
  };

  return (
    <TouchableOpacity
      onPress={() => {
        dispatch(setChatRoom(roomId));
        navigation.push(`/message`);
      }}
    >
      <Animated.View
        className=" flex flex-row items-center rounded-[10px]  px-3 py-5 "
        style={animatedViewByScale(opacity, scale)}
      >
        <View className="bg-primaryColorShadow border-primary mr-3 flex h-[45] w-[45] items-center justify-center rounded-full border-4">
          <Image
            className="rounded-full "
            source={
              usersData.profile
                ? { uri: usersData.profile }
                : require("./../../../assets/user.jpg")
            }
            style={{ width: PROFILE_IMAGE_SIZE, height: PROFILE_IMAGE_SIZE }}
            alt=""
          />
        </View>
        <View className="flex-1">
          <View className="w-full">
            {/* <Text className="text-secondaryLight font-poppinsReg text-[15px] font-semibold">
              Trip #{String(item.id).padStart(6, "0")}
            </Text> */}
            <Text
              className=" text-textColorDark font-Poppins_700Bold text-lg font-semibold"
              numberOfLines={1}
            >
              {`${usersData.firstname} ${usersData.lastname}`}
            </Text>
          </View>
          <View className="w-full space-y-[-5]">
            <View className="flex flex-row items-center space-x-2">
              {usersData.lastMessageOwner == usersData.loginUserId && (
                <Ionicons
                  name="checkmark-circle-outline"
                  size={20}
                  color={textColorDark}
                />
              )}
              <Text
                className={`${
                  //checkIfUserReadMessage(haveReadUser, usersData.loginUserId)
                  true
                    ? "font-poppinsReg"
                    : "font-Poppins_700Bold"
                } text-textColorDark  mt-1 text-[15px] font-normal `}
                numberOfLines={2}
              >
                {usersData.lastMessage}
              </Text>
            </View>
            <Text
              className={`${
                //checkIfUserReadMessage(haveReadUser, usersData.loginUserId)
                true
                  ? "font-poppinsReg"
                  : "font-Poppins_700Bold"
              } text-textColorDark  mt-1 text-[15px] font-normal `}
            >
              {moment(usersData.dateCreated).fromNow()}
            </Text>
          </View>
        </View>
      </Animated.View>
    </TouchableOpacity>
  );
};

export interface ChildComponentHandles {
  refetchMessageList: () => void;
}
const MessageList = (
  { userId }: { userId: string },
  ref: Ref<ChildComponentHandles>,
) => {
  const scrollY = React.useRef(new Animated.Value(0)).current;
  const LIMIT = 2;
  const [allMessageList, setAllMessageList] = useState<NewMessageProps[]>([]);
  const navigation = useRouter();
  type NewMessageProps = {
    id: number;
    users: string;
    data: {
      fromUsers: object;
      toUsers: object;
    } | null;
  };

  const utils = api.useContext();
  const { data, fetchNextPage, hasNextPage, isFetching } =
    api.room.infiniteRoomList.useInfiniteQuery(
      {
        limit: LIMIT, // this is optional - remember
        userId: userId,
      },
      {
        getNextPageParam: (lastPage: any) => {
          return lastPage.nextCursor;
        },
      },
    );

  useImperativeHandle(ref, () => ({
    refetchMessageList: async () => {
      await utils.room.infiniteRoomList.cancel();
      await utils.room.infiniteRoomList.refetch({
        limit: LIMIT, // this is optional - remember
        userId: userId,
      });
    },
  }));

  useEffect(() => {
    messageList().then((messageList: any) => {
      setAllMessageList(messageList);
    });
  }, [data]);

  const messageList = () => {
    const messagesResult = new Promise((resolve, reject) => {
      let messageList: any = [];
      data?.pages.forEach((result: any) => {
        messageList = [...messageList, ...result.roomList];
      });
      resolve(messageList);
      // reject(new Error('Failed to fetch data')); // Uncomment this line to simulate an error
    });

    return messagesResult;
  };

  return (
    <ImageBackground
      source={require("./../../../assets/listbg.jpg")}
      style={{ flex: 1 }}
      resizeMode="cover"
    >
      <View style={styles.overlay} className="w-full  pb-[220px]">
        {data && (
          <Animated.FlatList
            data={allMessageList}
            contentContainerStyle={{
              padding: SPACING,
              paddingTop: StatusBar.currentHeight || 42,
            }}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: scrollY } } }],
              { useNativeDriver: true },
            )}
            renderItem={({ item, index }: { item: any; index: number }) => {
              const inputRange = [
                -1,
                0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + 2),
              ];
              const opacityInputRange = [
                -1,
                0,
                ITEM_SIZE * index,
                ITEM_SIZE * (index + 0.8),
              ];
              const scale = scrollY.interpolate({
                inputRange,
                outputRange: [1, 1, 1, 0],
              });
              const opacity = scrollY.interpolate({
                inputRange: opacityInputRange,
                outputRange: [1, 1, 1, 0],
              });

              const chatterInfo =
                userId == item.ownerId ? item.user : item.owner;

              return (
                <MessageCard
                  key={index}
                  roomId={item.id}
                  opacity={opacity}
                  scale={scale}
                  usersData={{
                    ...chatterInfo,
                    loginUserId: userId,
                    messsageData: item.messages,
                    messageCount: item._count.messages,
                    lastMessageOwner: item.messages[0].userId,
                    lastMessage: item.messages[0].message, //item.data.message
                    dateCreated: item.messages[0].dateCreated,
                    isRead: item.messages[0].isRead,
                  }}
                />

                // <Pressable
                //   onPress={() => {
                //     navigation.push(`message/${1}`);
                //   }}
                // >
                //   <Text>sadsdsa</Text>
                // </Pressable>
              );
            }}
            ListEmptyComponent={
              <Text className="font-poppinsReg text-bgWhite text-center">
                No message found!
              </Text>
            }
            onEndReachedThreshold={0.2}
            onEndReached={() => {
              fetchNextPage();
            }}
            keyExtractor={(item: any) => item.id}
          />
        )}
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0,0,0,0.1)",
  },
});
export default forwardRef(MessageList);
