import { useEffect, useState } from "react";
import {
  Pressable,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";

import { LandTranpoType } from "~/helper/const";
import TitleText from "../TitleText";
import type { LandTranpoProps } from "./../../helper/type";

const CarType = ({
  people,
  setSelectedTranspo,
  selectedTranspo,
  setSelectedTranspoRate,
  setTranspoCapacity,
}: {
  people: number;
  setSelectedTranspo: (value: string) => void;
  selectedTranspo: string;
  setSelectedTranspoRate: (value: number) => void;
  setTranspoCapacity: (value: number) => void;
}) => {
  const validateServiceType = (people: number) => {
    let conditionMet = false;
    LandTranpoType.map((listType) => {
      const maxLimit = listType.capacity[1];
      if (maxLimit != undefined && maxLimit > people && !conditionMet) {
        setSelectedTranspo(listType.id);
        setSelectedTranspoRate(listType.rate);
        const capacity =
          listType.capacity[1] != undefined ? listType.capacity[1] : 0;
        setTranspoCapacity(capacity);
        conditionMet = true;
      }
    });
  };

  return (
    <View>
      <View className="px-5">
        <TitleText title="Choose transportion service" />
      </View>
      <View className="flex flex-row justify-between px-5">
        {LandTranpoType.map((items: LandTranpoProps, index: number) => {
          const limitMax = items.capacity[1];
          const limitMin = items.capacity[0];
          const validate =
            limitMax != undefined && limitMin != undefined && people > limitMax;
          if (validate && items.id == selectedTranspo) {
            validateServiceType(people);
          }
          return (
            <Pressable
              onPress={() => {
                if (!validate) {
                  setSelectedTranspo(items.id);
                  const capacity =
                    items.capacity[1] != undefined ? items.capacity[1] : 0;
                  setTranspoCapacity(capacity);
                  setSelectedTranspoRate(items.rate);
                } else {
                  ToastAndroid.show(
                    "Can't select this type of service. It could only handle " +
                      limitMax +
                      " passenger",
                    ToastAndroid.SHORT,
                  );
                }
              }}
              key={index}
              className={`bg-bgWhite ${
                validate ? "border-bgWhite" : "border-primary"
              } flex w-[29%] items-center justify-between rounded-md border p-3 bg-${
                selectedTranspo == items.id ? "primary" : "bgWhite"
              }`}
            >
              <Text
                className={` ${
                  selectedTranspo == items.id
                    ? "text-bgWhite"
                    : validate
                    ? "text-[#C4DFDF]"
                    : "text-primary"
                } font-Poppins_700Bold text-center `}
              >
                {items.service_name}
              </Text>
              <View>
                <Text
                  className={`${
                    selectedTranspo == items.id
                      ? "text-bgWhite"
                      : validate
                      ? "text-[#C4DFDF]"
                      : "text-textColorDark"
                  } font-poppinsReg text-bottom`}
                >
                  {items.capacity[0]} - {items.capacity[1]} pax
                </Text>
                <Text
                  className={`${
                    selectedTranspo == items.id
                      ? "text-bgWhite"
                      : validate
                      ? "text-[#C4DFDF]"
                      : "text-textColorDark"
                  } font-poppinsReg text-bottom`}
                >
                  ( ₱
                  {items.rate.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")})
                </Text>
              </View>
            </Pressable>
          );
        })}
      </View>
    </View>
  );
};

export default CarType;
