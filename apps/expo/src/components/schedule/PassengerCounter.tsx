import { Pressable, Text, TextInput, View } from "react-native";
import { MaskedTextInput } from "react-native-mask-text";
import Checkbox from "expo-checkbox";
import { LinearGradient } from "expo-linear-gradient";

import TitleText from "../TitleText";

const PassengerCounter = ({
  passenger,
  setPassenger,
  min = 0,
  max = 15,
  setAllowOther,
  allowOther,
  setTranspoCapacity,
}: {
  passenger: number;
  setPassenger: (value: number) => void;
  min?: number;
  max?: number;
  setAllowOther: (value: boolean) => void;
  allowOther: boolean;
  setTranspoCapacity?: (value: number) => void;
}) => {
  const increment = () => {
    if (passenger >= max) return;
    setPassenger(passenger + 1);
  };

  const decrement = () => {
    if (passenger <= min) return;
    setPassenger(passenger - 1);
  };

  return (
    <View className="flex items-center justify-center px-5">
      <TitleText title="Number of Passenger" />
      <View className=" flex flex-row items-center justify-center">
        <Pressable onPress={increment}>
          <LinearGradient
            className="bg-textColorDark flex-column  flex-row items-center justify-center rounded-bl-lg rounded-tl-lg  p-3"
            colors={["#009387", "#118586"]}
          >
            <Text className="font-poppinsReg text-bgWhite">+</Text>
          </LinearGradient>
        </Pressable>
        <TextInput
          className="bg-bgWhite w-[15%] px-2 py-3 text-center"
          keyboardType="numeric"
          value={passenger.toString()}
          onChangeText={(value) => {
            if (/^\d+$/.test(value)) {
              setPassenger(min);
            }
            if (Number(value) <= 0) {
              return;
            }
            if (Number(value) > max) {
              setPassenger(15);
              return;
            }
            setPassenger(Number(value));
          }}
        />
        <Pressable onPress={decrement}>
          <LinearGradient
            className="bg-textColorDark flex-column  flex-row items-center justify-center rounded-br-lg  rounded-tr-lg  p-3"
            colors={["#009387", "#118586"]}
          >
            <Text className="font-poppinsReg text-bgWhite">-</Text>
          </LinearGradient>
        </Pressable>
      </View>
      <View className="font-poppinsReg mt-3 flex flex-row space-x-2">
        <Checkbox
          value={allowOther}
          onValueChange={(value) => setAllowOther(value)}
          color={allowOther ? "#009387" : "#05375a"}
        />
        <Text className="text-textColorDark font-poppinsReg text-center">
          Allow other's to hop in
        </Text>
      </View>
    </View>
  );
};

export default PassengerCounter;
