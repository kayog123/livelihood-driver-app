import { Text, TouchableOpacity, View } from "react-native";
import { useRouter } from "expo-router";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import LottieView from "lottie-react-native";

const HopppingPassenger = ({ tagsFrom }: { tagsFrom: string }) => {
  const navigation = useRouter();
  return (
    <View className="bg-blueColor my-5 flex-row rounded-md p-5">
      <View className="w-[40%]">
        <LottieView
          autoPlay
          style={{
            width: "100%",
          }}
          source={require("./../../../assets/beach.json")}
        />
      </View>
      <View className="flex w-[60%] flex-col space-y-6">
        <View>
          <Text className="font-Poppins_700Bold text-bgWhite text-lg">
            Join Our Trips
          </Text>
          <Text className="font-poppinsReg text-bgWhite" numberOfLines={3}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.push({
              pathname: "/schedule/bookinglist",
              params: {
                tagsNav: tagsFrom,
              },
            });
          }}
          className=" flex flex-row items-center justify-center rounded-md bg-[#30A2FF] p-2"
        >
          <Text className="font-poppinsReg text-bgWhite ">Join Booking</Text>
          <Ionicons name="arrow-forward-outline" color="#fff" size={20} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HopppingPassenger;
