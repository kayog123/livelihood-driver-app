import { Fragment, useEffect, useState } from "react";
import {
  Alert,
  Dimensions,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Checkbox from "expo-checkbox";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";

import { PlaceEvent, PlaceList, TripList } from "~/helper/type";
import { RootState } from "~/redux/store";
import TitleText from "../TitleText";

const ItemCard = ({
  result,
  placeIndex,
  toggleEvents, // initialize for viewOnly
  togglePlace, // initialize for viewOnly
  lastItem = false,
  viewOnly = false, // initialize for viewOnly
}: {
  result: PlaceList;
  placeIndex: number;
  toggleEvents: (placeId: number, EventId: number, value: boolean) => void;
  togglePlace: (placeIndex: number, value: boolean) => void;
  lastItem?: boolean;
  viewOnly?: boolean;
}) => {
  const [check, setCheck] = useState<boolean>(result.show!!);
  const navigation = useRouter();
  const toggleCheck = (value: boolean) => {
    setCheck(value);
    togglePlace(placeIndex, value);
  };
  return (
    <View className=" flex flex-row space-x-2" key={placeIndex}>
      {/* <View className="border-secondaryLight bg-bgWhite absolute left-[-15] top-[30%] z-50 rounded-full border-4">
        <Text className="text-textColorDark p-[5]">#1</Text>
      </View> */}
      <View className="flex w-[15%] items-center justify-center">
        <View
          className={`border-textColorDark g w-[1] flex-grow ${
            placeIndex != 0 && "border-l-2"
          } border-dashed`}
        ></View>
        <View className="border-primary bg-White rounded-full border-4 p-3">
          <View className="h-[20] w-[20]">
            {result.optional && !viewOnly ? (
              <Checkbox
                //style={styles.checkbox}
                value={check}
                onValueChange={toggleCheck}
                color={result.show ? "#009387" : "#05375a"}
              />
            ) : (
              <Ionicons name="checkmark-outline" size={20} color="#05375a" />
            )}
          </View>
        </View>
        <View
          className={`border-textColorDark w-[1] flex-grow ${
            !lastItem && "border-l-2"
          }  border-dashed`}
        ></View>
      </View>
      <View className="bg-bgWhite my-2 w-[85%] flex-grow flex-row justify-between rounded-lg">
        <View className="flex grow flex-row space-y-2 p-3">
          {result.fee && (
            <View className="absolute z-10 flex-row">
              <Text className="text-bgWhite bg-primary border-bgWhite  left-0  border-r border-dashed px-5">
                Ent. Fee: ₱
                {result.fee.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              </Text>
              {result.optional && (
                <Text className="text-bgWhite bg-textColorDark border-bgWhite  left-0 z-10 border-r border-dashed px-5">
                  Optional
                </Text>
              )}
            </View>
          )}
          {/* {result.optional && (
            <Text className="text-bgWhite bg-textColorDark border-bgWhite  absolute left-0 z-10 border-r border-dashed px-5">
              Optional
            </Text>
          )} */}
          <View className="">
            <View className="stretch flex flex-row space-x-2">
              <Image
                className="w-[30%] rounded-md"
                source={{
                  uri:
                    "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
                    result.image[0],
                }}
                style={{
                  width: Dimensions.get("screen").height / 12,
                  height: Dimensions.get("screen").height / 12,
                }}
              />
              <View className="flex w-[70%] flex-wrap overflow-hidden">
                <Text className="font-poppinsReg font-Poppins_800ExtraBold inline-flex items-center text-sm leading-5">
                  Route #{placeIndex + 1}
                </Text>
                <TouchableOpacity
                  onPress={() => navigation.push("/boat/place")}
                >
                  <Text
                    numberOfLines={1}
                    style={styles.textShadow}
                    className="font-poppinsReg text-primary  text-lg leading-5 underline"
                  >
                    {result.name}
                  </Text>
                </TouchableOpacity>
                <Text
                  style={styles.textShadow}
                  className="font-poppinsReg text-textColorDark flex-wrap truncate text-xs font-medium leading-4 "
                >
                  {result.address}
                </Text>
              </View>
            </View>

            {result.events && (
              <Fragment>
                <View className=" mb-2 mt-5 flex flex-row items-center justify-center space-x-2 ">
                  <View className="border-textColorDark flex h-[1] flex-grow border-0 border-t border-dotted"></View>
                  <Text className="text-bgWhite bg-primary rounded-full px-2">
                    Choose Activities
                  </Text>
                  <View className="border-textColorDark flex h-[0.5] flex-grow border-0 border-t border-dotted"></View>
                </View>
                <View className="bg-bgWhiteShadow flex w-full flex-wrap space-y-1 p-3">
                  {result.events?.map(
                    (resultEvent: PlaceEvent, EventArrayIndex: number) => {
                      return (
                        <View className="flex flex-row flex-wrap space-x-2">
                          {resultEvent.optional && !viewOnly ? (
                            <Checkbox
                              value={resultEvent.show}
                              onValueChange={(value: boolean) => {
                                toggleEvents(result.id, resultEvent.id, value);
                              }}
                            />
                          ) : (
                            <Ionicons
                              name="checkmark-outline"
                              size={20}
                              color="#05375a"
                            />
                          )}

                          <Text
                            className="text-textColorDark flex-wrap"
                            key={EventArrayIndex}
                          >
                            {resultEvent.event_name}
                          </Text>
                        </View>
                      );
                    },
                  )}
                </View>
              </Fragment>
            )}
          </View>
        </View>
        {/* <View className="bg-textColorDark border-bgWhite border-r-1 items-center justify-center   border-l-2 border-dashed p-5">
          <Checkbox
            //style={styles.checkbox}
            value={check}
            onValueChange={toggleCheck}
            color={result.show ? "#009387" : "#f3f0f0"}
          />
        </View> */}
      </View>
    </View>
  );
};

const ItemPlaceList = ({
  data,
  navigateTo,
  toggleEvents = () => {}, // initialize for viewOnly
  togglePlace = () => {}, // initialize for viewOnly
  viewOnly = false,
}: {
  data?: PlaceList[];
  navigateTo?: () => void;
  togglePlace?: (arrayIndex: number, value: boolean) => void;
  toggleEvents?: (
    placeId: number,
    eventArrayIndex: number,
    value: boolean,
  ) => void;
  viewOnly?: boolean;
}) => {
  return (
    <View className=" p-5">
      {/* divide-y divide-gray-200 dark:divide-gray-700 */}
      <View className="max-w-md space-y-2 rounded-md">
        {data?.length == 0 && (
          <Text className="text-center">No data found!</Text>
        )}
        {data?.map((result: PlaceList, placeIndex: number) => (
          <ItemCard
            key={placeIndex}
            lastItem={placeIndex == data.length - 1}
            result={result}
            placeIndex={placeIndex}
            toggleEvents={toggleEvents}
            togglePlace={togglePlace}
            viewOnly={viewOnly}
          />
        ))}
      </View>
    </View>
  );
};

export default ItemPlaceList;

const styles = StyleSheet.create({
  textShadow: {
    textShadowColor: "#e3e6e9c2",
    textShadowOffset: { width: -1, height: 0.5 },
    textShadowRadius: 1,
  },
});
