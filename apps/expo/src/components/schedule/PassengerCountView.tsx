import { useEffect, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from "@expo/vector-icons";

type TravelIconProp = "boat" | "vans";
const PassengerCountView = ({
  countLimit,
  currentCount,
  tripId,
  travelBy = "boat",
  status,
  schedule,
}: {
  countLimit: number;
  currentCount: number;
  tripId: number;
  travelBy: "boat" | "vans";
  status: string;
  schedule: string;
}) => {
  const [statusColor, setStatusColor] = useState<string>("bg-blueColor");
  useEffect(() => {
    if (status == "PENDING") {
      setStatusColor("bg-warningColor");
    }
    if (status == "ACCEPTED") {
      setStatusColor("bg-successColor");
    }
    if (status == "CANCELLED") {
      setStatusColor("bg-errorColor");
    }
    if (status == "COMPLETED") {
      setStatusColor("bg-successColor");
    }
  });
  return (
    <View className="bg-bgWhite flex flex-row space-x-4 p-5">
      <View className="flex-3 bg-primary flex-col justify-center rounded-md p-5">
        <Ionicons
          name={travelBy == "boat" ? "boat" : "car"}
          size={80}
          color="#fff"
        />
        <Text className="text-bgWhite font-poppinsReg">Travel by:</Text>
        <Text className="text-bgWhite font-Poppins_700Bold text-center text-2xl">
          {travelBy.toUpperCase()}
        </Text>
      </View>
      <View className="flex flex-1 space-y-4">
        <LinearGradient
          className="bg-blueColor rounded-md p-3"
          colors={["#05375a", "#031e30d4"]}
        >
          {/* <Text className="font-poppinsReg text-bgWhite my-2 text-center">
            Number of Passenger
          </Text> */}
          <View className="flex-row   ">
            <View className="bg-primary rounded-md p-3">
              <Ionicons name="people-circle-outline" size={50} color="#fff" />
            </View>
            <View className="s mx-4">
              <Text className="font-poppinsReg text-bgWhite">Passengers</Text>
              <View className="flex-row">
                <Text className="text-bgWhite text-3xl">{currentCount} </Text>
                <Text className="text-bgWhite text-3xl">/ {countLimit}</Text>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                console.log("asdfasdf");
              }}
              className={`absolute bottom-0 right-0 rounded-full bg-[#e49a11] p-[1px]`}
            >
              <Ionicons
                name="information-circle-outline"
                size={20}
                color="#fff"
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>
        <LinearGradient
          className=" rounded-md"
          colors={["#05375a", "#031e30d4"]}
        >
          <Text className="font-poppinsReg text-bgWhite bg-primary rounded-tl-md rounded-tr-md px-3">
            Additional Details
          </Text>
          <View className="flex-col space-y-1 p-5">
            <View className="flex-row space-x-2">
              <Text className="text-primary text-bgWhite font-Poppins_700Bold">
                BOOKING ID:
              </Text>
              <Text className="text-primary text-bgWhite font-poppinsReg">
                #{String(tripId).padStart(6, "0")}
              </Text>
            </View>
            <View className="flex-col space-x-2">
              <Text className="text-primary text-bgWhite font-Poppins_700Bold">
                DATE SCHEDULE:
              </Text>
              <Text className="text-primary text-bgWhite font-poppinsReg">
                {schedule}
              </Text>
            </View>
            <View className="flex-row space-x-2">
              <Text className="text-primary text-bgWhite font-Poppins_700Bold">
                Status:
              </Text>
              <Text
                className={`text-primary text-bgWhite font-poppinsReg rounded-sm ${statusColor} px-2`}
              >
                {status}
              </Text>
            </View>
          </View>
        </LinearGradient>
      </View>
    </View>
  );
};

export default PassengerCountView;
