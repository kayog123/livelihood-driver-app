import React, { useEffect, useState, type ReactNode } from "react";
import { SafeAreaView } from "react-native";
import { Stack } from "expo-router";
import { useAuth } from "@clerk/clerk-expo";
import NetInfo from "@react-native-community/netinfo";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import { setMessageCountAdd } from "~/redux/reducer/messageSlice";
import Header from "../Header";
import NotificationBar from "../NotificationBar";
import TopBarHeader from "../TopBarHearder";

type LayoutProps = {
  children: ReactNode;
  topBarTitle: string;
  tabBarShow?: boolean;
};
type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};

const Layout: React.FC<LayoutProps> = ({
  children,
  topBarTitle,
  tabBarShow = true,
}) => {
  const dispatch = useDispatch();
  const [isOffline, setOfflineStatus] = useState(false);
  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });
  const { isLoaded, userId } = useAuth();

  // In case the user signs out while on the page.
  if (!isLoaded || !userId) {
    return null;
  }

  // api.message.subscribeUserMessage.useSubscription(
  //   { userIdMessageListener: `message_listener_${userId}`.toString()},
  //   {
  //     onData(messageReceive) {
  //       //const senderProfile =  == usersData.userId ? usersData.owner : usersData.user
  //       console.log('executed')
  //       dispatch(
  //         setMessageCountAdd(1),
  //       );
  //     },
  //     onError(err) {
  //       console.error("Subscription error:", err);
  //       // we might have missed a message - invalidate cache
  //       //utils.post.infinite.invalidate();
  //     },
  //   },
  // );

  useEffect(() => {
    if (isOffline) {
      setNotifyBar({
        show: true,
        message: "",
        type: "success",
      });
    } else {
      setNotifyBar({
        show: false,
        message: "No internet",
        type: "error",
      });
    }
  }, [isOffline]);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });

    return () => removeNetInfoSubscription();
  }, []);
  return (
    <SafeAreaView>
      <Stack.Screen
        options={{
          headerShown: tabBarShow,
        }}
      />
      {tabBarShow && <TopBarHeader title={topBarTitle} />}
      <NotificationBar
        showNofify={notifyBar.show} //notifyBar.show
        message={notifyBar.message} //notifyBar.message
        type={notifyBar.type}
      />
      {children}
    </SafeAreaView>
  );
};

export default Layout;
