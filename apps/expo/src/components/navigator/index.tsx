import React, {
  Component,
  Fragment,
  ReactComponentElement,
  type ReactNode,
} from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MotiView } from "moti";
import { useSelector } from "react-redux";

import { RootState } from "~/redux/store";
import { Account, Activity, Home, Messages, Wallet } from "../";
import { primaryColor, textColorDark } from "../../helper/const";
import Bookings from "../bookings";
import DriverHomePage from "../driver/HomePage";
import ReceiveBookingComp from "../driver/ReceiveBooking";

type navIconProps = {
  title: string;
  icon: string;
  size: number;
  focused: boolean;
  color: string;
};
type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];

type customNavIcon = {
  children: ReactNode;
  onPress: () => void;
};

type ScreenListProps = {
  name: string;
  component: any;
  icon: IonicIonsname;
  title: string;
};
const ScreenList: ScreenListProps[] = [
  {
    name: "Home",
    component: DriverHomePage, //Home = user homepage //DriverHomePage = driver home page
    title: "Home",
    icon: "home",
  },
  {
    name: "Bookings",
    component: ReceiveBookingComp, //true if user=Bookings and false=ReceiveBookingComp if driver
    title: "Bookings",
    icon: "calendar-outline",
  },
  {
    name: "Message",
    component: Messages,
    title: "Messages",
    icon: "chatbubble-ellipses",
  },
  {
    name: "Account",
    component: Account,
    title: "Account",
    icon: "person-circle",
  },
];

const navSettings = {
  animationEnabled: true,
  swipeEnabled: false,
  tabBarShowLabel: false,
  lazy: true,
  tabBarStyle: {
    backgroundColor: "#ffffff",
    height: 80,
    //...styles.shadow,
  },
};

const NavigatorTab = () => {
  const Tab = createBottomTabNavigator();

  const settings = {
    ...navSettings,
    tabBarStyle: { ...navSettings.tabBarStyle, ...styles.shadow },
  };
  const { countMessage } = useSelector((state: RootState) => state.message);

  const NavIcons = ({ icon, size, focused, color, title }: navIconProps) => {
    return (
      <View className="flex items-center justify-center">
        <View>
          {"Messages" == title && countMessage > 0 && (
            <Text className="text-bgWhite bg-secondaryLight absolute right-0 top-0 z-10 rounded-full px-1 text-[8px]">
              {countMessage}
            </Text>
          )}
          <Ionicons
            name={icon as IonicIonsname}
            size={size}
            color={focused ? primaryColor : textColorDark}
          />
        </View>
        <Text className="font-poppinsReg text-textColorDark">{title}</Text>
      </View>
    );
  };

  return (
    <Tab.Navigator screenOptions={({ route }) => settings}>
      {ScreenList.map((screens: ScreenListProps, index: number) => (
        <Tab.Screen
          key={index}
          name={screens.name}
          component={screens.component}
          options={{
            tabBarIcon: ({ focused }) => (
              <Fragment>
                <NavIcons
                  title={screens.title}
                  icon={screens.icon}
                  color="gray"
                  size={24}
                  focused={focused}
                />
              </Fragment>
            ),
          }}
        />
      ))}
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#7F5DF0",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
});

export default NavigatorTab;
