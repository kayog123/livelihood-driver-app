import { type ReactNode } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter } from "expo-router";

import { primaryColor, primaryColorShadow } from "~/helper/const";

type ConfirmProps = {
  cancelName?: string;
  cancelPress: () => void;
  cancelIcon?: ReactNode;
  confirmName?: string;
  confirmPress: () => void;
  confirmIcon?: ReactNode;
};

const ConfirmBox = ({
  cancelName = "Cancel",
  cancelPress,
  cancelIcon = (
    <Ionicons name="arrow-back-outline" size={24} color={`#05375a`} />
  ),
  confirmName = "Confirm",
  confirmPress,
  confirmIcon = (
    <Ionicons name="checkmark-done-outline" size={24} color={`#fff`} />
  ),
}: ConfirmProps) => {
  const navigation = useRouter();
  return (
    <View className=" flex w-full flex-row">
      <TouchableOpacity className="flex-1 " onPress={cancelPress}>
        <LinearGradient
          className="flex-row items-center justify-center p-3 "
          colors={["#fff", "#ecf0f6"]}
        >
          {cancelIcon}
          <Text className="text-textColorDark">{cancelName}</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity className="flex-1 " onPress={confirmPress}>
        <LinearGradient
          className="flex-1 flex-row items-center justify-center p-3"
          colors={[primaryColor, primaryColor]}
        >
          {confirmIcon}
          <Text className="text-[#fff]">{confirmName}</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

export default ConfirmBox;
