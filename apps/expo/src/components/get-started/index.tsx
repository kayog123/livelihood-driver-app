import React, { useEffect, useRef } from "react";
import {
  Animated,
  Dimensions,
  Image,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter } from "expo-router";

import { primaryColor } from "../../helper/const";

const AnimatedLinear = Animated.createAnimatedComponent(LinearGradient);

const css = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

const GetStarted = () => {
  const navigation = useRouter();
  const buttonAnimation = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    startAnimation();
  }, []);

  const startAnimation = () => {
    Animated.timing(buttonAnimation, {
      toValue: 1,
      duration: 300, // Adjust the duration as needed
      useNativeDriver: true,
    }).start();
  };

  const buttonScale = buttonAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1], // Adjust the scale values as needed
  });

  const textScale = buttonAnimation.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1], // Adjust the reverse scale value as needed
  });

  return (
    <SafeAreaView style={css.droidSafeArea}>
      <Stack.Screen options={{ title: "Login", headerShown: false }} />
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            source={require("./../../../assets/bohol-trip.png")}
            style={styles.logo}
            resizeMode="stretch"
            alt="Bohol Trips Logo"
          />
        </View>
        <View style={styles.footerBox}>
          <View>
            <Text style={styles.title}>
              Let us make Bohol a better place to live in.
            </Text>
            <Text style={styles.titleDesc}>
              Travel Safe with our "Buotan" drivers
            </Text>
          </View>
          {/* <View
            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
            className="flex flex-row"
          >
            <TouchableOpacity style={{ width: "100%" }}>
              <Animated.View
                className="bg-primary row mx-auto my-0  flex  h-[70px] w-[50%] items-center justify-center rounded-[20px]"
                style={{
                  transform: [{ scaleX: buttonScale }],
                }}
              >
                <Text className="text-[15px] text-[#fff] ">Button</Text>
              </Animated.View>
            </TouchableOpacity>
          </View> */}

          <View className="flex w-full flex-row items-center justify-center">
            <TouchableOpacity
              style={{ width: "200%" }}
              onPress={() => {
                navigation.push("/auth/login");
              }}
            >
              <Animated.View
                className=" mx-auto my-0 flex w-[50%] items-center justify-center"
                style={{
                  transform: [{ scaleX: buttonScale }],
                }}
              >
                <LinearGradient
                  style={styles.signIn}
                  colors={["#08d4c4", "#01ab9d"]}
                >
                  <Animated.View
                    style={{
                      transform: [{ scaleX: textScale }],
                    }}
                  >
                    <Text style={styles.textSign} className="text-normal">
                      Get Started
                    </Text>
                  </Animated.View>
                </LinearGradient>
              </Animated.View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default GetStarted;

const { height } = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: primaryColor,
  },
  header: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  footerBox: {
    flex: 1,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: "#05375a",
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center",
  },
  titleDesc: {
    textAlign: "center",
    marginVertical: 50,
    fontSize: 16,
    color: "#05375a",
  },
  text: {
    color: "grey",
    marginTop: 5,
  },
  button: {
    alignItems: "flex-end",
    marginTop: 30,
  },
  signIn: {
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
    flexDirection: "row",
    width: "100%",
  },
  textSign: {
    color: "white",
    fontWeight: "bold",
  },
});
