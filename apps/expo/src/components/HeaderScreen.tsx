import { useState } from "react";
import {
  ImageBackground,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

import SearchBar from "./SearchBar";

type HeaderSreenProps = {
  titleTop?: string;
  titleMain?: string;
  search: string;
  setSearch: (value: string) => void;
};

const HeaderScreen = ({
  titleTop = "Schedule a",
  titleMain = "Tourist Van",
  search,
  setSearch,
}: HeaderSreenProps) => {
  //const [search, setSearch] = useState<string>("");
  return (
    <View>
      <ImageBackground
        source={{
          uri: "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/chocolate.jpg",
        }}
        resizeMode="cover"
        className="flex h-[200] "
      >
        <View className="absolute bottom-2 left-2">
          <Text className="text-bgWhite font-poppinsReg text-md  ">
            {titleTop}
          </Text>
          <Text className="text-bgWhite font-poppinsReg text-3xl">
            {titleMain}
          </Text>
        </View>
      </ImageBackground>
      <View className="bg-primaryColorShadow flex items-center justify-center px-5 py-3">
        <SearchBar
          searchValue={search}
          setSearch={setSearch}
          placeholder="Search popular places..."
        />
      </View>
    </View>
  );
};

export default HeaderScreen;
