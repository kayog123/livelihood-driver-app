import { Fragment, ReactNode } from "react";
import {
  Modal,
  Pressable,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Stack, useRouter } from "expo-router";

import { primaryColor } from "~/helper/const";

const TopBarHeader = ({
  title = "Title header",
  headerRight,
}: {
  title: string;
  headerRight?: () => ReactNode;
}) => {
  const settings = {
    headerTitle: () => (
      <Text className="font-poppinsReg text-[#fff]">{title}</Text>
    ),
    headerStyle: {
      backgroundColor: primaryColor,
    },
    headerTintColor: "#fff",
  } as {
    headerTitle: () => ReactNode;
    headerStyle: { backgroundColor: string };
    headerTintColor: string;
    headerRight?: any;
  };

  if (headerRight) {
    settings.headerRight = headerRight;
  }

  return <Stack.Screen options={settings} />;
};

export default TopBarHeader;
