import React, { useState, type ReactNode } from "react";
import { Dimensions, SafeAreaView, Text, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useAuth } from "@clerk/clerk-expo";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import { navSetting } from "../../helper/const";
import Layout from "../Layout";
import ActiveBookings from "./ActiveBookings";
import TopBarHeader from "../TopBarHearder";

type TripCardProps = {
  locationFrom: string;
  locationTo: string;
  statusColor: string;
  icon: ReactNode;
};

const TripCard = ({
  locationFrom,
  locationTo,
  statusColor,
  icon,
}: TripCardProps) => {
  const [height, setHeight] = useState(0);

  const handleLayout = (event: any) => {
    const { height } = event.nativeEvent.layout;
    setHeight(height);
  };
  return (
    <View className="mb-3 flex w-full flex-row flex-wrap items-center justify-between rounded-xl bg-[#fff] px-2 py-4">
      <View className="flex w-[14%] flex-row self-center">{icon}</View>
      <View
        className="relative flex w-[60%] justify-center"
        onLayout={handleLayout}
      >
        <View className="relative left-[-7px]  flex flex-row ">
          <Ionicons name="location" size={16} color="#009387" />
          <Text numberOfLines={1}>{locationFrom}</Text>
        </View>

        <View className="border-l-[2px] border-dotted border-gray-300 py-4"></View>
        <View className="relative left-[-7px]  flex flex-row ">
          <Ionicons name="location" size={16} color="#f05d2a" />
          <Text numberOfLines={1}>{locationTo}</Text>
        </View>
      </View>
      <View className=" flex w-[23%] self-end ">
        <Text>₱ 15.00</Text>
        <Text className={`${statusColor}`}>Completed</Text>
      </View>
    </View>
  );
};
const Tab = createMaterialTopTabNavigator();
const Bookings = () => {
  //   const navigation = useRouter();
  const { isLoaded, userId } = useAuth();
  const [page, setPage] = useState<number>(0);
  const [mapModal, setMapModal] = useState<boolean>(false);

  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];

  type BookingTabsProps = {
    name: string;
    component: any;
    label: string;
    icon: IonicIonsname;
    size: number;
    initialParams: { typeQuery: string };
  };
  const BookingTabs: BookingTabsProps[] = [
    {
      name: "Active",
      component: ActiveBookings,
      label: "Bookings",
      icon: "list-outline",
      size: 24,
      initialParams: { typeQuery: "infiniteBooking" },
    },
    {
      name: "History",
      component: ActiveBookings,
      label: "History",
      icon: "folder-open-outline",
      size: 24,
      initialParams: { typeQuery: "infiniteBookingHistory" },
    },
  ];

  return ( 
      <SafeAreaView>
        <TopBarHeader title="My Bookings"/>
        <View className={` bg-primary flex h-full w-full flex-col`}>
          <Tab.Navigator initialRouteName="BoatList" screenOptions={navSetting}>
            {BookingTabs.map((result: any, index: number) => (
              <Tab.Screen
                key={index}
                name={result.name}
                component={result.component}
                initialParams={result.initialParams}
                options={{
                  tabBarLabel: result.label,
                  tabBarIcon: ({ color }) => (
                    <View>
                      <Ionicons
                        name={`${result.icon as IonicIonsname}`}
                        color={color}
                        size={result.size}
                      />
                    </View>
                  ),
                }}
              />
            ))}
          </Tab.Navigator>
        </View> 
        </SafeAreaView> 
  );
};

export default Bookings;

const { width, height } = Dimensions.get("screen");
