import { Image, Text, TouchableOpacity, View } from "react-native";

const TravelServiceOpt = ({
  transpoSelected,
  setSelectedTranspo,
  textColor = "text-textColorDark",
  text = "Travel with",
}: {
  transpoSelected: string;
  setSelectedTranspo: (value: string) => void;
  textColor?: string;
  text?: string;
}) => {
  return (
    <View className="">
      <Text className={`${textColor} mt- font-poppinsReg text-center`}>
        {text}
      </Text>

      <View className="flex w-full flex-row gap-1 p-3">
        <TouchableOpacity
          onPress={() => setSelectedTranspo("tricycle")}
          className={`${
            transpoSelected == "tricycle" ? "bg-primary" : "bg-[#fff]"
          } border-primaryColorShadow flex  flex-1 items-center justify-center rounded-lg border p-2`}
        >
          <Image
            resizeMode="contain"
            source={
              transpoSelected == "tricycle"
                ? require("./../../../assets/tricycle.png")
                : require("./../../../assets/tricycledark.png")
            }
            style={{ width: 70, height: 56 }}
            alt=""
          />
          <Text
            className={`${
              transpoSelected == "tricycle"
                ? "text-[#fff]"
                : "text-textColorDark"
            }`}
          >
            Tricycle
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setSelectedTranspo("motorcycle")}
          className={`${
            transpoSelected == "motorcycle" ? "bg-primary" : "bg-[#fff]"
          } border-primaryColorShadow flex flex-1 items-center justify-center rounded-lg border p-2`}
        >
          <Image
            resizeMode="contain"
            source={
              transpoSelected == "motorcycle"
                ? require("./../../../assets/motor.png")
                : require("./../../../assets/motordark.png")
            }
            style={{ width: 70, height: 56 }}
            alt=""
          />
          <Text
            className={`${
              transpoSelected == "motorcycle"
                ? "text-[#fff]"
                : "text-textColorDark"
            }`}
          >
            Motorcycle
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TravelServiceOpt;
