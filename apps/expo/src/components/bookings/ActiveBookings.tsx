import { useEffect, useState } from "react";
import { Dimensions, FlatList, Text, View } from "react-native";
import { useAuth } from "@clerk/clerk-expo";
import { MotiView } from "moti";

import { api } from "~/utils/api";
import PlaceModal from "../modal/PlaceModal";
import boatTrips from "./../../../assets/jsonList/boatTrips.json";
import vansTrip from "./../../../assets/jsonList/vanTrips.json";
import BookingCard from "./BookingCard";
import CancelBooking from "./CancelBooking";
import ViewBookingInfo from "./ViewBookingInfo";

type ModalInfoProps = {
  title: string;
  action: "CANCELLED" | "VIEW";
  tripid: number;
};
const ActiveBookings = ({
  navigation,
  route,
}: {
  navigation: any;
  route: any;
}) => {
  const query = route.params["typeQuery"] as object; //.initialParams.typeQuery;
  const { height } = Dimensions.get("screen");
  const { isLoaded, userId } = useAuth();
  const [trips, setTrips] = useState<any>([]);
  const [modaInfo, setModalInfo] = useState<ModalInfoProps>({
    title: "",
    action: "CANCELLED",
    tripid: 0,
  });
  const [mapModal, setMapModal] = useState<boolean>(false);
  const limit = 10;
  if (!isLoaded || !userId) return;

  const utils = api.useContext();
  const dynamicQuery = api.booking[query];
  const refetchQuery = utils.booking[query];
  const { data, fetchNextPage } = dynamicQuery.useInfiniteQuery(
    {
      limit: limit, // this is optional - remember
      userId: userId,
    },
    {
      getNextPageParam: (lastPage: any) => lastPage.nextCursor,
    },
  );
  useEffect(() => {
    let items: any = [];
    data?.pages.map((pages: any) => {
      items = [...items, ...pages.items];
    });
    setTrips(items);
  }, [data]);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      refetchResult();
    });
  }, [navigation]);

  const refetchResult = () => {
    refetchQuery.refetch({
      limit: limit, // this is optional - remember
      userId: userId,
    });
  };

  const findPackageById = (id: number, data: any) => {
    for (const packageObj of data) {
      if (packageObj.id === id) {
        return packageObj;
      }
    }
    return null; // Return null if no matching object is found
  };
  const handleFetchNextPage = () => {
    fetchNextPage();
    //setPage((prev: number) => prev + 1);
  };
  const closeModal = () => {
    setMapModal(false);
  };

  return (
    <View className="bg-bgWhite">
      {/* <TopBarHeader title="My Bookings" /> */}
      <PlaceModal
        modalVisible={mapModal}
        setModalVisible={setMapModal}
        title={modaInfo.title}
        icons="close-circle-outline"
      >
        {modaInfo.action == "CANCELLED" && (
          <CancelBooking
            tripId={modaInfo.tripid}
            closeModal={closeModal}
            refetchResult={refetchResult}
            userId={userId}
          />
        )}
        {modaInfo.action == "VIEW" && (
          <ViewBookingInfo tripId={modaInfo.tripid} />
        )}
      </PlaceModal>
      <MotiView
        from={{ translateY: height }}
        animate={{ translateY: 0 }}
        transition={{
          type: "timing",
          duration: 800,
          scale: {
            type: "spring",
            delay: 50,
          },
        }}
      >
        <View>
          <FlatList
            className="flex-grow p-5"
            data={trips}
            renderItem={({ item, index }) => {
              let bookingInfor;
              if (item.transpo_type == "VANS") {
                bookingInfor = findPackageById(item.tripId, boatTrips);
              } else if (item.transpo_type == "BOAT") {
                bookingInfor = findPackageById(item.tripId, boatTrips);
              } else {
                bookingInfor = {
                  id: item.id,
                  name: item["pickup_area_name"],
                };
              }
              return (
                <BookingCard
                  key={index}
                  ownerId={item.ownerId ? item.ownerId : ""}
                  userId={item.userId}
                  tripId={item.id}
                  name={bookingInfor.name}
                  transpoType={item.transpo_type}
                  status={item.status}
                  setMapModal={setMapModal}
                  setModalInfo={setModalInfo}
                  dateCreated={bookingInfor.dateCreated}
                  dateScheduled={bookingInfor.dateScheduled}
                />
              );
            }}
            ListEmptyComponent={
              <Text className="font-poppinsReg text-center">
                No data found!
              </Text>
            }
            onEndReachedThreshold={0.2}
            onEndReached={handleFetchNextPage}
            // refreshControl={<Text>Refress</Text>}
          />
          {/* <View className="flex-column mt-3 flex w-full"></View>
              <View className="mt-[30] flex w-full"></View> */}
        </View>
        
        <View className="h-50 bg-textColorDark"></View>
      </MotiView>
    </View>
  );
};

export default ActiveBookings;
