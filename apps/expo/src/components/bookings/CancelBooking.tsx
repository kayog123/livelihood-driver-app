import { useState } from "react";
import { Pressable, Text, TextInput, View } from "react-native";

import { api } from "~/utils/api";
import { TripStatus } from "~/helper/const";

const CancelBooking = ({
  tripId,
  closeModal,
  refetchResult,
  userId,
}: {
  tripId: number;
  closeModal: () => void;
  refetchResult: () => void;
  userId: string;
}) => {
  const [reason, setReason] = useState<string>("");
  const [processing, setProcessing] = useState<boolean>(false);
  const [errorMsg, setErrorMsg] = useState<string>("");

  const { mutate, error } = api.booking.updateBookingStatus.useMutation({
    async onMutate(newPost: any) {
      setProcessing(true);
    },
    async onSuccess() {
      closeModal();
      refetchResult();
    },
    async onError(err: any, newPost: any, ctx: any) {
      setErrorMsg(err.message);
    },
    onSettled() {
      setProcessing(false);
    },
  });

  const cancelBooking = () => {
    mutate({
      status: TripStatus.CANCELLED,
      reason: reason,
      bookingId: tripId,
      ownerId: userId,
      cancelledByUser: true,
    });
  };
  return (
    <View className="p-5 pt-20">
      {errorMsg && (
        <View className="bg-errorColor mb-5 px-5 py-3">
          <Text className="font-poppinsReg text-bgWhite">
            <Text className="Poppins_700Bold">Error:</Text> {errorMsg}
          </Text>
        </View>
      )}
      <Text className="font-poppinsReg ">
        Trip #: {String(tripId).padStart(6, "0")}
      </Text>

      <Text className="font-poppinsReg ">Reason for cancellation:</Text>
      <TextInput
        multiline={true}
        numberOfLines={8}
        className="bg-bgWhiteShadow font-poppinsReg p-3"
        value={reason}
        placeholder="Reason"
        onChangeText={(value) => {
          setReason(value);
        }}
        style={{ textAlignVertical: "top" }}
      />
      <View className="mt-5">
        <Pressable
          onPress={() => cancelBooking()}
          // disabled={processing}
          className="bg-errorColor items-center justify-center rounded-md py-3"
        >
          <Text className="text-bgWhite font-poppinsReg">
            {processing ? "Processing..." : "Submit Cancellation"}
          </Text>
        </Pressable>
      </View>
    </View>
  );
};

export default CancelBooking;
