import { ScrollView, Text, View } from "react-native";
import { MotiView } from "moti";
import { Skeleton } from "moti/skeleton";

const ViewBookingSkeleton = () => {
  return (
    <View className="mb-5 flex max-w-sm animate-pulse flex-col space-y-4">
      <View className="flex h-20 w-full animate-pulse flex-row items-center justify-center rounded bg-gray-300 dark:bg-gray-700">
        <View className=" m-0 w-[30%] items-center justify-center  space-y-2  border-0 p-2 outline-none">
          <View className="  m-0 w-[70%] space-y-2  p-2">
            {[1, 2].map((items: any, index: number) => (
              <View
                className="bg-primary h-2.5 w-full rounded-full"
                key={index}
              >
                <MotiView
                  transition={{
                    type: "timing",
                  }}
                  animate={
                    {
                      // backgroundColor: "#fff"
                    }
                  }
                >
                  <Skeleton
                    colorMode={"light"}
                    width={"100%"}
                    height={"100%"}
                    //width={Dimensions.get("screen").width * 0.4}
                  />
                </MotiView>
              </View>
            ))}
          </View>
        </View>
        <View className="  m-0 w-[70%] space-y-2  p-2">
          {[1, 2, 3].map((items: any, index: number) => (
            <View className="bg-primary h-2.5 w-full rounded-full" key={index}>
              <MotiView
                transition={{
                  type: "timing",
                }}
                animate={
                  {
                    // backgroundColor: "#fff"
                  }
                }
              >
                <Skeleton
                  colorMode={"light"}
                  width={"100%"}
                  height={"100%"}
                  //width={Dimensions.get("screen").width * 0.4}
                />
              </MotiView>
            </View>
          ))}
        </View>
      </View>
      <View className="flex h-20 w-full animate-pulse items-center justify-center rounded ">
        <MotiView
          transition={{
            type: "timing",
          }}
          animate={{
            backgroundColor: "#fff",
          }}
        >
          <Skeleton colorMode={"light"} width={"100%"} height={"100%"} />
        </MotiView>
      </View>
    </View>
  );
};

export default ViewBookingSkeleton;
