import { useEffect, useState } from "react";
import {
  Alert,
  Linking,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import LottieView from "lottie-react-native";
import moment from "moment";
import { MotiView } from "moti";
import { Skeleton } from "moti/skeleton";

import { api } from "~/utils/api";
import { HERE_MAP_API_KEY, primaryColor, secondaryColor } from "~/helper/const";
import { TripList } from "~/helper/type";
import { SkeletonCard } from "../Card";
import HereMapView from "../map/Here/HereMapView";
import boatTrip from "./../../../assets/jsonList/boatTrips.json";
import vansTrip from "./../../../assets/jsonList/vanTrips.json";
import ViewBookingSkeleton from "./ViewBookingSkeleton";

type ViewBookingInfoProps = {
  tripId: number;
};

const BookTourCard = ({
  transpoType,
  dateCreated,
  tripId,
  id,
}: {
  transpoType: string;
  dateCreated: string;
  tripId: number;
  id: number;
}) => {
  const [tripDetails, setTripDetails] = useState<TripList>();
  useEffect(() => {
    const tripData = findPackageById(
      tripId,
      transpoType == "VANS" ? vansTrip : boatTrip,
    );
    setTripDetails(tripData);
  }, []);

  const findPackageById = (id: number, data: any) => {
    for (const packageObj of data) {
      console.log(packageObj, id);
      if (packageObj.id === id) {
        return packageObj;
      }
    }
    return null; // Return null if no matching object is found
  };
  return (
    <View>
      <Text className="font-Poppins_700Bold text-textColorDark mb-3 text-xl">
        Booking Information
      </Text>
      <View className=" flex-row ">
        <View className="bg-primary border-primary m-0 w-[30%] items-center justify-center   border-0 p-2 outline-none">
          <Text className="font-Poppins_700Bold text-bgWhite text-center">
            {moment(dateCreated).format("MMMM D, YYYY")}
          </Text>
          <Text className="font-poppinsReg text-bgWhite text-xs">
            Booking Date
          </Text>
        </View>
        <View className="border-primary m-0 w-[70%] space-y-2 border-2 border-l-0 p-2">
          <View className="flex-row items-center ">
            <Ionicons name="location" size={20} color={primaryColor} />
            <Text
              className="font-Poppins_700Bold text-textColorDark w-[90%] text-lg"
              numberOfLines={1}
            >
              {tripDetails?.name}
            </Text>
          </View>
          <View className="flex-row items-center ">
            {/* <Ionicons name="location" size={20} color={secondaryColor} /> */}
            <Text
              className="font-poppinsReg text-textColorDark left-4 w-[90%] text-xs"
              numberOfLines={2}
            >
              {tripDetails?.description}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

type DropOffProps = {
  lat: number;
  long: number;
  pick_up_name: string;
};

const BookInfoCard = ({
  dateCreated,
  pickUpName,
  dropoffLong,
  dropoffLat,
}: {
  dateCreated: string;
  pickUpName: string;
  dropoffLong: number;
  dropoffLat: number;
}) => {
  const [dropOffLocation, setDropOffLocation] = useState<DropOffProps>({
    lat: 0,
    long: 0,
    pick_up_name: "",
  });

  useEffect(() => {
    fetchGeoDetails(dropoffLat, dropoffLong);
  }, []);
  const fetchGeoDetails = async (lat: number, long: number) => {
    await fetch(
      `https://revgeocode.search.hereapi.com/v1/revgeocode?at=${lat}%2C${long}&lang=en-US&apiKey=${HERE_MAP_API_KEY}`,
      {
        method: "get",
      },
    )
      .then((response) => response.json())
      .then((result) => {
        setDropOffLocation({
          lat: result.items[0].position.lat,
          long: result.items[0].position.lng,
          pick_up_name: result.items[0].address.label,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };
  return (
    <View>
      <Text className="font-Poppins_700Bold text-textColorDark mb-3 text-xl">
        Booking Information
      </Text>
      <View className=" flex-row ">
        <View className="bg-primary border-primary m-0 w-[30%] items-center justify-center   border-0 p-2 outline-none">
          <Text className="font-Poppins_700Bold text-bgWhite text-center">
            {moment(dateCreated).format("MMMM D, YYYY")}
          </Text>
          <Text className="font-poppinsReg text-bgWhite text-xs">
            Booking Date
          </Text>
        </View>
        <View className="border-primary m-0 w-[70%] space-y-2 border-2 border-l-0 p-2">
          <View className="flex-row items-center ">
            <Ionicons name="location" size={20} color={primaryColor} />
            <Text
              className="font-Poppins_700Bold text-textColorDark w-[90%]"
              numberOfLines={1}
            >
              {pickUpName}
            </Text>
          </View>
          <View className="flex-row items-center ">
            <Ionicons name="location" size={20} color={secondaryColor} />
            <Text
              className="font-poppinsReg text-textColorDark w-[90%] text-xs"
              numberOfLines={1}
            >
              {dropOffLocation.pick_up_name}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const AssignedDriverDetails = ({
  name,
  phone,
  email,
}: {
  name: string;
  phone: string;
  email: string;
}) => {
  const callNumber = (phone: string) => { 
    let phoneNumber = phone;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then((supported) => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <View>
      <Text className="font-Poppins_700Bold text-textColorDark mb-3 text-xl">
        Assigned Driver
      </Text>
      <View className=" flex-row ">
        <View className="bg-primary border-primary m-0 w-[30%] items-center justify-center   border-0 p-2 outline-none">
          <Ionicons name="person-circle-outline" size={32} color="#fff" />
          <Text className="font-poppinsReg text-bgWhite text-xs">Driver</Text>
        </View>
        <View className="border-primary m-0 w-[70%] space-y-2 border-2 border-l-0 p-2">
          <View className="flex-row items-center ">
            <Text
              className="font-Poppins_700Bold text-textColorDark w-[90%] text-lg"
              numberOfLines={1}
            >
              {name}
            </Text>
          </View>
          <View>
            <View className="flex-row items-center space-x-2">
              <Ionicons name="mail-outline" size={20} color={secondaryColor} />
              <Text
                className="font-poppinsReg text-textColorDark w-[90%] text-xs"
                numberOfLines={1}
              >
                {email}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                console.log(phone);
                callNumber(phone);
              }}
              className="flex-row items-center space-x-2"
            >
              <Ionicons name="call-outline" size={20} color={secondaryColor} />
              <Text
                className="font-poppinsReg text-textColorDark w-[90%] text-xs underline"
                numberOfLines={1}
              >
                {phone}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const MapsDetails = ({
  height,
  className = "",
  initialRegionLat,
  initialRegionLong,
  initialPickUpName,
  dropoffLong,
  dropoffLat,
  dropoffPickUp,
}: {
  height: number;
  className?: string;
  initialRegionLat: number;
  initialRegionLong: number;
  initialPickUpName: string;
  dropoffLong: number;
  dropoffLat: number;
  dropoffPickUp: string;
}) => {
  return (
    <View className="mt-10 overflow-hidden rounded-md">
      <HereMapView
        height={height}
        initialRegionLat={initialRegionLat}
        initialRegionLong={initialRegionLong}
        initialPickUpName={initialPickUpName}
        dropoffLong={dropoffLong}
        dropoffLat={dropoffLat}
        dropoffPickUp={dropoffPickUp}
        setFair={() => {}}
        transpoSelected="MOTOR"
      />
    </View>
  );
};

const HistoryCardBooking = ({ history }: { history: any }) => {
  return (
    <View className="w-full">
      <Text className="font-Poppins_700Bold text-textColorDark mb-3 mt-10 text-xl">
        History
      </Text>
      <View className=" flex">
        {history.length == 0 && (
          <Text className="font-poppinsReg text-textColorDark border-primary border py-5 text-center">
            No data available
          </Text>
        )}
        {history.map((item: any, index: number) => {
          let statBarColor;
          if (item.action == "ACCEPTED") {
            statBarColor = "text-successColor";
          } else if (item.action == "CANCELLED") {
            statBarColor = "text-errorColor";
          } else if (item.action == "COMPLETED") {
            statBarColor = "text-textColorDark";
          } else if (item.action == "ON_THE_WAY") {
            statBarColor = "text-blueColor";
          } else {
            statBarColor = "text-warningColor";
          }
          const timeOfAction = moment(item.dateCreated);
          return (
            <View className="w-full flex-row" key={index}>
              <View className="flex w-[20%] items-center justify-center">
                <View className="border-textColorDark flex-grow border-2 border-dashed"></View>
                <View className="border-primary bg-White h-10 w-10 rounded-full border-4"></View>
                <View className="border-textColorDark flex-grow border-2 border-dashed"></View>
              </View>
              {/* <Text>{JSON.stringify(item)}</Text> */}
              <View className=" w-[80%]  p-2 py-4">
                <View className="bg-bgWhiteShadow rounded-md p-3">
                  <Text className="font-poppinsReg text-primary text-lg">
                    {item.user.firstname} {item.user.lastname}
                  </Text>
                  <Text className={`${statBarColor} font-poppinsReg mt-[-5]`}>
                    {item.action.replaceAll("_", " ")}
                  </Text>
                  <View className="flex-row space-x-2">
                    <Ionicons name="time-outline" size={20} color="#797676" />
                    <Text className="font-poppinsReg italic text-[#797676]">
                      {timeOfAction.fromNow()}
                    </Text>
                  </View>
                  {item.action == "CANCELLED" && (
                    <View className="mt-5 flex-col">
                      <Text className="font-poppinsReg  text-textColorDark">
                        Cancel Reason :
                      </Text>

                      {item.reason ? (
                        <Text className="font-poppinsReg  text-textColorDark text-xs">
                          {item.reason}
                        </Text>
                      ) : (
                        <Text className="font-poppinsReg   text-xs italic text-[#797676]">
                          No reason provided
                        </Text>
                      )}
                    </View>
                  )}
                </View>
              </View>
            </View>
          );
        })}
      </View>
      {/* <Text>{JSON.stringify(item)}</Text> */}
    </View>
  );
};

const ViewBookingInfo = ({ tripId }: ViewBookingInfoProps) => {
  const { data } = api.booking.fetchByBookingHithHistoryById.useQuery({
    bookingId: tripId,
  });
  return (
    <ScrollView>
      <View className="mt-20 w-full p-5">
        {!data ? (
          <ViewBookingSkeleton />
        ) : (
          <View className="w-full">
            {/* <Text>{JSON.stringify(data)}</Text> */}
            {(data.transpo_type == "MOTORCYCLE" ||
              data.transpo_type == "TRICYCLE") && (
              <View>
                <BookInfoCard
                  dateCreated={data.dateCreated.toISOString()}
                  pickUpName={data.pickup_area_name}
                  dropoffLong={Number(data.places.split(",")[1])}
                  dropoffLat={Number(data.places.split(",")[0])}
                />
                <MapsDetails
                  height={200}
                  className="mt-5"
                  initialRegionLat={Number(data.pickup_lat)}
                  initialRegionLong={Number(data.pickup_long)}
                  initialPickUpName={data.pickup_area_name}
                  dropoffLong={Number(data.places.split(",")[1])}
                  dropoffLat={Number(data.places.split(",")[0])}
                  dropoffPickUp={""}
                />
              </View>
            )}
            {(data.transpo_type == "BOAT" || data.transpo_type == "VANS") &&
              data.tripId != null && (
                <BookTourCard
                  transpoType={data.transpo_type}
                  dateCreated={data.dateCreated.toDateString()}
                  tripId={data.tripId}
                  id={data.id}
                />
              )}
            {/* <Text>{JSON.stringify(data.user)}</Text> */}
            {data.owner && (
              <View className="mt-5">
                <AssignedDriverDetails
                  name={`${data.user.firstname} ${data.user.lastname}`}
                  phone={data.user.phone}
                  email={data.user.email}
                />
              </View>
            )}
            <HistoryCardBooking history={data.histories} />
            {data.status == "PENDING" && (
              <View className="flex-row items-center justify-center">
                <LottieView
                  autoPlay
                  style={{
                    width: 50,
                    height: 50,
                    // backgroundColor: "green",
                  }}
                  source={require("./../../../assets/searching.json")}
                />
                <Text className="font-poppinsReg text-textColorDark">
                  Still looking for available driver...
                </Text>
              </View>
            )}
          </View>
        )}
      </View>
    </ScrollView>
  );
};

export default ViewBookingInfo;
