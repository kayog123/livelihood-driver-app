import { Dimensions, Image, Pressable, Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useRouter } from "expo-router";
import moment from "moment";
import { api } from "~/utils/api";
import { primaryColor } from "~/helper/const";

type ModalInfoProps = {
  title: string;
  action: "CANCELLED" | "VIEW";
  tripid: number;  
  ownerId?: string;
};

const BookingCard = ({
  tripId,
  name,
  transpoType,
  status,
  setMapModal,
  setModalInfo,
  dateScheduled,
  dateCreated,
  ownerId,
  userId,
}: {
  tripId: number;
  name: string;
  transpoType: string;
  ownerId: string;
  userId: string;
  status: string;
  setMapModal: (value: boolean) => void;
  setModalInfo: (value: ModalInfoProps) => void;
  dateScheduled: string;
  dateCreated: string;
}) => {
  const { width } = Dimensions.get("screen");
  const navigation = useRouter();
  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
  const { data } = api.room.getRoomId.useQuery({
    userId,
    ownerId,
  });
  let statBarColor;
  if (status == "ACCEPTED") {
    statBarColor = "bg-successColor";
  } else if (status == "CANCELLED") {
    statBarColor = "bg-errorColor";
  } else if (status == "COMPLETED") {
    statBarColor = "bg-primary";
  } else if (status == "ON_THE_WAY") {
    statBarColor = "bg-blueColor";
  } else {
    statBarColor = "bg-warningColor";
  }

  return (
    <View className="bg-bgWhiteShadow mb-3 w-full rounded-md p-3">
      <View className="flex-row space-x-2">
        {transpoType == "MOTORCYCLE" || transpoType == "TRICYCLE" ? (
          <View
            className={`bg-primary w-[32%] items-center justify-center rounded-md`}
          >
            <Ionicons
              name={
                transpoType == "MOTORCYCLE" ? "bicycle-outline" : "bus-outline"
              }
              size={48}
              color="#fff"
            />
          </View>
        ) : (
          <Image
            className="rounded-md"
            source={{
              uri: "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/panglao.jpg",
            }}
            alt="Item image"
            style={{ height: width / 4, width: width / 4 }}
          />
        )}

        <View className="w-[60%] flex-grow">
          <Text className="font-Poppins_700Bold">
            Trip #{String(tripId).padStart(6, "0")}
          </Text>
          <Text
            className="font-poppinsReg text-primary text-lg"
            numberOfLines={2}
          >
            {name}
          </Text>
          <View className="flex-row items-center">
            <Ionicons name="calendar-outline" size={20} />
            <Text className="font-poppinsReg text-textColorDark">
              {moment(dateCreated).format("MMMM D, YYYY")}
            </Text>
          </View>
        </View>
      </View>
      <View className="mt-3 rounded-md bg-gray-200 px-2">
        <View className=" bg-bgWhite mx-3 my-5 flex flex-row divide-x divide-gray-200 rounded-lg border border-gray-200 p-3 dark:divide-gray-700">
          <View className="flex-1 items-center justify-center">
            <View className="flex flex-row">
              {/* <Ionicons name="calendar-outline" size={20} color={primaryColor} /> */}
              <Text className="font-Poppins_700Bold text-center"> 
                Trip Schedule
              </Text>
            </View>
            <Text className="font-poppinsReg text-textColorDark">
              {moment(dateScheduled).format("MMMM D, YYYY")}
            </Text>
          </View>
          <View className="flex-1 items-center justify-center">
            <Text className="font-Poppins_700Bold"> Status</Text>
            <Text
              className={`text-bgWhite bg-textColorDark font-poppinsReg px-3 ${statBarColor} rounded-full`}
            >
              {status}
            </Text>
          </View>
        </View>
        <View className="w-full flex-row space-x-2 py-3">
          <Pressable
            onPress={() => {
              setMapModal(true);
              setModalInfo({
                title:
                  "View Details - Trip #" + String(tripId).padStart(6, "0"),
                action: "VIEW",
                tripid: tripId,
                ownerId: ownerId,
              });
            }}
            className="bg-textColorDark flex-1 items-center justify-center rounded-md py-3"
          >
            <Text className="font-poppinsReg text-bgWhite">View</Text>
          </Pressable>

          {status == "PENDING" && (
            <Pressable
              onPress={() => {
                setMapModal(true);
                setModalInfo({
                  title: "Cancel Booking",
                  action: "CANCELLED",
                  tripid: tripId,
                });
              }}
              className="flex-1 items-center justify-center rounded-md bg-[#e04516] py-3"
            >
              <Text className="font-poppinsReg text-bgWhite">Cancel</Text>
            </Pressable>
          )}

        {data && (
          <Pressable
            onPress={() => {
              navigation.push(`/message/${data?.id}`);
            }}
            className="bg-primary flex-1 items-center justify-center rounded-md py-3"
          >
            <Ionicons
              name="chatbubble-ellipses-outline"
              size={24}
              color="#fff"
            />
          </Pressable>
        )}
        </View>
      </View>
    </View>
  );
};

export default BookingCard;
