import { ReactNode } from "react";
import {
  Modal,
  Pressable,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

type ModalCompProps = {
  modalVisible: boolean;
  setModalVisible: (value: boolean) => void;
  children?: ReactNode;
  title: string;
  icons?: string;
};

type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
const PlaceModal = ({
  modalVisible,
  setModalVisible,
  children,
  title = "Modal",
  icons = "map",
}: ModalCompProps) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}
    >
      <ScrollView className={`flex flex-1 bg-[#313030db]`}>
        <View className=" flex items-center justify-center">
          <View
            className={`bg-bgWhiteShadow flex-center flex min-h-screen w-[100%]`}
          >
            <View className="bg-textColorDark w-100 absolute top-0 z-10 flex  h-16 w-full flex-row items-center justify-between p-2">
              <View className="flex flex-row space-x-2">
                <Ionicons
                  name={icons as IonicIonsname}
                  size={24}
                  color="#fff"
                />
                <Text className="text-bgWhite font-poppinsReg">{title}</Text>
              </View>
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <Ionicons name="close-outline" size={36} color={"#fff"} />
              </Pressable>
            </View>
            <View className="flex-grow bg-white ">{children}</View>
          </View>
        </View>
      </ScrollView>
    </Modal>
  );
};

export default PlaceModal;
