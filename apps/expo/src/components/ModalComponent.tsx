import { useEffect, useRef, type ReactNode } from "react";
import {
  Modal,
  Pressable,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";

type ModalCompProps = {
  modalVisible: boolean;
  setModalVisible: (value: boolean) => void;
  children: ReactNode;
  onSubmit: () => void;
  openFrom: string; //"create" | "update";
  removeContact: () => void;
};
const ModalComponent = ({
  modalVisible,
  setModalVisible,
  children,
  onSubmit,
  openFrom,
  removeContact,
}: ModalCompProps) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}
    >
      {/* bg-[#c0c0c0db] */}
      <ScrollView className={`flex flex-1 bg-[#313030db]`}>
        <View className="mt-[22] flex items-center justify-center">
          <View className={`bg-bgWhiteShadow flex-center flex w-[90%]`}>
            <View className="bg-textColorDark flex h-16 w-full flex-row items-center justify-between p-2">
              <Text className="text-bgWhite font-poppinsReg">
                Add Emergency Contact
              </Text>
              <Pressable onPress={() => setModalVisible(!modalVisible)}>
                <Ionicons name="close-outline" size={24} color={"#fff"} />
              </Pressable>
            </View>
            <View className="flex-grow">{children}</View>
            <View>
              <TouchableOpacity
                onPress={onSubmit}
                className="bg-primary m-5 mb-2 flex h-[60] items-center justify-center rounded-md"
              >
                <Text className="font-poppinsReg p-5 text-[#fff]">
                  {openFrom == "create" ? "Create" : "Update"} Contact
                </Text>
              </TouchableOpacity>
              {openFrom == "update" && (
                <TouchableOpacity
                  onPress={removeContact}
                  className="m-5 mt-1 flex h-[60] flex-row items-center justify-center rounded-md bg-[#CD1818]"
                >
                  <Ionicons name="trash-outline" size={24} color="#fff" />
                  <Text className="font-poppinsReg p-5 pl-1 text-[#fff]">
                    Remove Contact
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    </Modal>
  );
};

export default ModalComponent;
