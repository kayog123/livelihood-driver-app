import React, { useState, type ReactNode } from "react";
import { Image, ScrollView, Text, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

import Card, { SkeletonCard } from "../../components/Card";
import Layout from "../../components/Layout";
import { primaryColor } from "../../helper/const";

type TripCardProps = {
  locationFrom: string;
  locationTo: string;
  statusColor: string;
  icon: ReactNode;
};

const TripCard = ({
  locationFrom,
  locationTo,
  statusColor,
  icon,
}: TripCardProps) => {
  const [height, setHeight] = useState(0);

  const handleLayout = (event: any) => {
    const { height } = event.nativeEvent.layout;
    setHeight(height);
  };
  return (
    <View className="mb-3 flex w-full flex-row flex-wrap items-center justify-between rounded-xl bg-[#fff] px-2 py-4">
      <View className="flex w-[14%] flex-row self-center">{icon}</View>
      <View
        className="relative flex w-[60%] justify-center"
        onLayout={handleLayout}
      >
        <View className="relative left-[-7px]  flex flex-row ">
          <Ionicons name="location" size={16} color="#009387" />
          <Text numberOfLines={1}>{locationFrom}</Text>
        </View>

        <View className="border-l-[2px] border-dotted border-gray-300 py-4"></View>
        <View className="relative left-[-7px]  flex flex-row ">
          <Ionicons name="location" size={16} color="#f05d2a" />
          <Text numberOfLines={1}>{locationTo}</Text>
        </View>
      </View>
      <View className=" flex w-[23%] self-end ">
        <Text>₱ 15.00</Text>
        <Text className={`${statusColor}`}>Completed</Text>
      </View>
    </View>
  );
};

const Activity = () => {
  //   const navigation = useRouter();
  return (
    <Layout topBarTitle="Booked Trips">
      <ScrollView className="flex p-5 pb-32">
        <View className="flex w-full items-center justify-center">
          <Image
            className="  w-full items-stretch  rounded-xl"
            source={require("./../../../assets/travel.png")}
            style={{ width: 350, height: 180 }}
            alt="logo, activity, ride"
          />
        </View>

        {/* <SkeletonCard /> */}
        <View className="bg-primary border-primaryColorShadow mt-10 border-0 border-b-8 border-t-8 p-2">
          <Text className="text-bgWhite text-center text-xl">Booked Trips</Text>
        </View>

        <View className="flex-column mt-5 flex">
          <TripCard
            locationFrom="Taloto, Tagbilaran City "
            locationTo="Bingag, Dauis "
            statusColor="text-green-500"
            icon={<Ionicons name="car-outline" size={44} color="red" />}
          />
          <TripCard
            locationFrom="Taloto, Tagbilaran City"
            locationTo="Bingag, Dauis"
            statusColor="text-green-500"
            icon={<Ionicons name="car-outline" size={44} color="green" />}
          />
          <TripCard
            locationFrom="Taloto, Tagbilaran City"
            locationTo="Bingag, Dauis"
            statusColor="text-green-500"
            icon={<Ionicons name="car-outline" size={44} color="yellow" />}
          />
          <TripCard
            locationFrom="Taloto, Tagbilaran City"
            locationTo="Bingag, Dauis"
            statusColor="text-green-500"
            icon={<Ionicons name="car-outline" size={44} color="yellow" />}
          />
        </View>
        <View className=" mt-5 h-36 w-full"></View>
      </ScrollView>
    </Layout>
  );
};

export default Activity;
