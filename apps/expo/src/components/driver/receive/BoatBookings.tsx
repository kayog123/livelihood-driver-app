import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native";
import { useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";

import { api } from "~/utils/api";
import { TranspoType } from "~/helper/const";
import boatTrips from "./../../../../assets/jsonList/boatTrips.json";
import ItemReceiveCard from "./ItemReceiveCards";

const BoatBookings = () => {
  const { width } = Dimensions.get("screen");
  const { isLoaded, userId } = useAuth();
  if (!isLoaded || !userId) return;
  const { data } = api.booking.fetchByTransType.useQuery({
    transpo_type: TranspoType.BOAT,
  });

  const findPackageById = (id: number, data: any) => {
    for (const packageObj of data) {
      if (packageObj.id === id) {
        return packageObj;
      }
    }
    return null; // Return null if no matching object is found
  };

  return (
    <View className="bg-bgWhiteShadow space-y-4 ">
      {!data ? (
        <Text className="font-poppinsReg text-center">Loading...</Text>
      ) : (
        <FlatList
          className="p-5"
          data={data}
          renderItem={({ item, index }) => {
            let details;
            if (item.tripId) {
              details = findPackageById(item.tripId, boatTrips);
            }
            return (
              <ItemReceiveCard
                key={index}
                item={item}
                details={details}
                userId={userId}
              />
            );
          }}
          keyExtractor={(item: any) => item.id}
        />
      )}
    </View>
  );
};

export default BoatBookings;
