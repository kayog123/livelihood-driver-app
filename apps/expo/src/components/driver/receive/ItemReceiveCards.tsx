import { Fragment, useState } from "react";
import { Dimensions, Image, Text, TouchableOpacity, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import moment from "moment";

import { api } from "~/utils/api";

const ItemReceiveCard = ({
  item,
  details,
  userId,
}: {
  item: any;
  details: any;
  userId: string;
}) => {
  const { width } = Dimensions.get("screen");
  const [processing, setProcessing] = useState<boolean>(false);
  const [acceptedStatus, setAcceptedStatus] = useState<boolean>(false);
  const [reqInfo, setReqInfo] = useState({
    icon: "",
    message: "",
  });
  const acceptBooking = (bookingId: number) => {
    mutate({
      ownerId: userId,
      bookingId: bookingId,
    });
  };

  const { mutate, error } = api.booking.updateBooking.useMutation({
    async onMutate(newPost: any) {
      setProcessing(true);
    },
    async onError(err: any, newPost: any, ctx: any) {
      setReqInfo({
        icon: "close-circle-outline",
        message: err.message,
      });
    },
    async onSuccess() {
      setReqInfo({
        icon: "checkmark-done-outline",
        message: "You accepted the booking!",
      });
    },
    onSettled() {
      setProcessing(false);
      setAcceptedStatus(true);
    },
  });
  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
  return (
    <Fragment>
      {!acceptedStatus ? (
        <View className="mb-5 border-dashed">
          <View className=" bg-primary  absolute right-0 top-2 z-10 flex items-center justify-center px-5">
            <Text className="font-poppinsReg text-bgWhite">
              ₱{" "}
              {item.transpo_rate
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
            </Text>
          </View>
          <View className="bg-bgWhite flex  flex-row space-x-2 rounded-xl border border-b-0 border-[#d4d3d3] p-5">
            <Image
              className=" w-[27%] rounded-md"
              source={{
                uri: "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/panglao.jpg",
              }}
              style={{
                height: width / 5,
                width: width / 5,
              }}
            />
            <View className=" w-[70%] pt-3">
              <Text className="font-poppinsReg">
                Trip #{String(item.id).padStart(6, "0")}
              </Text>
              <Text
                className="font-poppinsReg text-primary text-lg"
                numberOfLines={2}
              >
                {details?.name}
              </Text>
              <Text
                className="font-poppinsReg text-textColorDark "
                numberOfLines={2}
              >
                Trip on: {moment(item.dateScheduled).format("MMMM D, YYYY")}
              </Text>
              {details?.description.map((descItem: string, index: number) => (
                <Text
                  className="font-poppinsReg text-sm"
                  numberOfLines={2}
                  key={index}
                >
                  {descItem}
                </Text>
              ))}
            </View>
          </View>
          <View className="mx-5 overflow-visible border-t border-dashed"></View>
          <View className="bg-bgWhite mt-[-1] flex flex-row items-center justify-center rounded-xl border border-t-0 border-dashed border-[#d4d3d3] p-5">
            {!processing ? (
              <TouchableOpacity
                onPress={() => {
                  acceptBooking(item.id);
                }}
                className="flex flex-row items-center justify-center space-x-2"
              >
                <Ionicons name="checkmark-outline" size={24} color="#18e630" />
                <Text className="text-successColor font-poppinsReg">
                  Accept
                </Text>
              </TouchableOpacity>
            ) : (
              <Text className="font-poppinsReg text-[#8c8a8a]">
                Processing...
              </Text>
            )}

            {/* <View className="flex flex-row items-center justify-center space-x-2">
          <Ionicons name="close-outline" size={24} color="#FF6969" />
          <Text className="text-errorColor font-poppinsReg">Decline</Text>
        </View> */}
          </View>
        </View>
      ) : (
        <View className="bg-bgWhite mb-5 flex-row flex-wrap space-x-2 rounded-lg p-5">
          <Ionicons
            name={reqInfo.icon as IonicIonsname}
            size={24}
            color="#18e630"
          />
          <Text className="font-poppinsReg text-wrap">
            {reqInfo.message}
            <Text className="text-successColor ml-2 underline">View</Text>
          </Text>
        </View>
      )}
    </Fragment>
  );
};

export default ItemReceiveCard;
