import { Fragment, useState } from "react";
import {
  Dimensions,
  Image,
  Pressable,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import { setlockedBooking } from "~/redux/reducer/lockedBookingSlice";

type ReqInfoprop = {
  icon: string;
  message: string;
  colorIcon: string;
};
type DropOffProps = {
  lat: number;
  long: number;
  pick_up_name: string;
};
const LockedOrderCard = ({
  item,
  details,
  userId,
}: {
  item: any;
  details: any;
  userId: string;
}) => {
  const dispatch = useDispatch();
  const { width } = Dimensions.get("screen");
  const [processing, setProcessing] = useState<boolean>(false);
  const [acceptedStatus, setAcceptedStatus] = useState<boolean>(false);
  const [reqInfo, setReqInfo] = useState({
    icon: "",
    message: "",
    colorIcon: "",
  });
  const [dropOffLocation, setDropOffLocation] = useState<DropOffProps>({
    lat: 0,
    long: 0,
    pick_up_name: "",
  });
  const acceptBooking = (bookingId: number) => {
    mutate({
      ownerId: userId,
      bookingId: bookingId,
    });
  };
  const navigation = useRouter();

  const { mutate, error } = api.booking.updateBooking.useMutation({
    async onMutate(newPost: any) {
      setProcessing(true);
    },
    async onError(err: any, newPost: any, ctx: any) {
      setReqInfo({
        icon: "close-circle-outline",
        message: err.message,
        colorIcon: "#e04516",
      });
    },
    async onSuccess() {
      setReqInfo({
        icon: "checkmark-done-outline",
        message: "You accepted the booking!",
        colorIcon: "#18e630",
      });

      dispatch(
        setlockedBooking({
          id: item.id,
          userId: item.userId,
          ownerId: userId,
          status: "ACCEPTED",
          transpo_type: item.transpo_type,
          transpo_rate: item.transpo_rate,
          description: item.description,
          firstname: item.user.firstname,
          lastname: item.user.lastname,
          phone: item.user.phone,
          profile: item.user.profile,
          pickup_lat: item.pickup_lat,
          pickup_long: item.pickup_long,
          pickup_area_name: item.pickup_area_name,
          places: item.places,
        }),
      );
      navigation.push("/driver/booking/lockedorders");
    },
    onSettled() {
      setProcessing(false);
      setAcceptedStatus(true);
    },
  });
  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
  return (
    <Fragment>
      {!acceptedStatus ? (
        <View className="mb-5 border-dashed">
          <View className=" bg-primary  absolute right-0 top-2 z-10 flex items-center justify-center px-5">
            <Text className="font-poppinsReg text-bgWhite">
              ₱{" "}
              {item.transpo_rate
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
            </Text>
          </View>
          <View className="bg-bgWhite flex  flex-row space-x-2 rounded-xl border border-b-0 border-[#d4d3d3] p-5">
            <View className="bg-primary h-25 w-[27%] items-center justify-center rounded-md">
              {item.transpo_type == "TRICYCLE" ? (
                <Image
                  resizeMode="contain"
                  source={require("./../../../../assets/tricycle.png")}
                  style={{ width: width / 7, height: width / 7 }}
                  alt=""
                />
              ) : (
                <Ionicons name="bicycle-outline" size={48} color="#fff" />
              )}
            </View>
            <View className=" w-[70%] pt-3">
              <Text className="font-Poppins_700Bold">
                Trip #{String(item.id).padStart(6, "0")}
              </Text>
              <Text
                className="font-poppinsReg text-primary text-lg"
                numberOfLines={2}
              >
                {item.pickup_area_name}
              </Text>
              <Text
                className="font-poppinsReg text-textColorDark "
                numberOfLines={2}
              >
                Drop off: <Text className="text-xs"> [Hidden Information]</Text>
              </Text>
              {details?.description.map((descItem: string, index: number) => (
                <Text
                  className="font-poppinsReg text-sm"
                  numberOfLines={2}
                  key={index}
                >
                  {descItem}
                </Text>
              ))}
            </View>
          </View>
          <View className="mx-5 overflow-visible border-t border-dashed"></View>
          <View className="bg-bgWhite mt-[-1] flex flex-row items-center justify-center rounded-xl border border-t-0 border-dashed border-[#d4d3d3] p-5">
            {!processing ? (
              <TouchableOpacity
                onPress={() => {
                  acceptBooking(item.id);
                }}
                className="flex flex-row items-center justify-center space-x-2"
              >
                <Ionicons name="checkmark-outline" size={24} color="#18e630" />
                <Text className="text-successColor font-poppinsReg">
                  Accept Booking
                </Text>
              </TouchableOpacity>
            ) : (
              <Text className="font-poppinsReg text-[#8c8a8a]">
                Processing...
              </Text>
            )}

            {/* <View className="flex flex-row items-center justify-center space-x-2">
          <Ionicons name="close-outline" size={24} color="#FF6969" />
          <Text className="text-errorColor font-poppinsReg">Decline</Text>
        </View> */}
          </View>
        </View>
      ) : (
        <View className="bg-bgWhite mb-5 flex-row flex-wrap space-x-2 rounded-lg p-5">
          <Ionicons
            name={reqInfo.icon as IonicIonsname}
            size={24}
            color={reqInfo.colorIcon}
          />
          <Text className="font-poppinsReg text-wrap">
            {reqInfo.message}
            {/* <Text className="text-successColor ml-2 underline">View</Text> */}
          </Text>
        </View>
      )}
    </Fragment>
  );
};

export default LockedOrderCard;
