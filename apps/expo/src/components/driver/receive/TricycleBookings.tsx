import { Fragment, useEffect } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";

import { api } from "~/utils/api";
import { TranspoType } from "~/helper/const";
import { RootState } from "~/redux/store";
import vanTrips from "./../../../../assets/jsonList/vanTrips.json";
import LockedOrderCard from "./LockedOrderCard";

const LockedNotifCard = () => {
  const navigation = useRouter();
  return (
    <View className="bg-bgWhite m-5 rounded-md p-3">
      <TouchableOpacity
        className="flex flex-row space-x-2"
        onPress={() => {
          navigation.push("/driver/booking/lockedorders");
        }}
      >
        <View className="bg-primary w-[20%] items-center justify-center rounded-md p-2">
          <Ionicons name="arrow-forward-outline" size={38} color="#fff" />
        </View>
        <View className="w-[80%] overflow-hidden">
          <View className="flex flex-row items-center space-x-1">
            <Text className="text-textColorDark font-Poppins_700Bold text-lg">
              View Booking
            </Text>
          </View>
          <Text className="text-textColorDark font-poppinsReg">
            You are locked in to a booking
          </Text>
          <Text className="text-textColorDark font-poppinsReg">
            Please complete to accept other booking.
          </Text>
          <View className="absolute bottom-0 right-2 z-[-10] ">
            <Ionicons name="lock-closed-outline" size={110} color="#d8dcdc" />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const TricycleBookings = ({ navigation }: { navigation: any }) => {
  const { width } = Dimensions.get("screen");
  const { isLoaded, userId } = useAuth();
  if (!isLoaded || !userId) return;
  const { data } = api.booking.fetchByTransType.useQuery({
    transpo_type: TranspoType.TRICYCLE,
  });
  const { lockedBooking } = useSelector(
    (state: RootState) => state.lockedBooking,
  );
  const utils = api.useContext();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      refetchResult();
    });
  }, [navigation]);

  const refetchResult = () => {
    utils.booking.fetchByTransType.refetch({
      transpo_type: TranspoType.TRICYCLE,
    });
  };

  const findPackageById = (id: number, data: any) => {
    for (const packageObj of data) {
      if (packageObj.id === id) {
        return packageObj;
      }
    }
    return null; // Return null if no matching object is found
  };

  return (
    <View className="bg-bgWhiteShadow space-y-4">
      {lockedBooking != null ? (
        <LockedNotifCard />
      ) : (
        <Fragment>
          {!data ? (
            <Text className="font-poppinsReg text-center">Loading...</Text>
          ) : (
            <Fragment>
              {data.length > 0 ? (
                <FlatList
                  className="p-5"
                  data={data}
                  renderItem={({ item, index }) => {
                    let details;
                    if (item.tripId) {
                      details = findPackageById(item.tripId, vanTrips);
                    }

                    return (
                      <LockedOrderCard
                        key={index}
                        item={item}
                        details={details}
                        userId={userId}
                      />
                    );
                  }}
                  keyExtractor={(item: any) => item.id}
                />
              ) : (
                <View className="border-primary m-5 border p-5">
                  <Text className="font-poppinsReg text-textColorDark text-center">
                    No bookings. Please wait for new bookings
                  </Text>
                </View>
              )}
            </Fragment>
          )}
        </Fragment>
      )}
    </View>
  );
};

export default TricycleBookings;
