import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  View,
} from "react-native";
import { useRouter } from "expo-router";

import "react-native-gesture-handler";

import { useEffect, useState } from "react";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import BoatBookings from "~/components/driver/receive/BoatBookings";
import MotorBookings from "~/components/driver/receive/MotorBooking";
import TricycleBookings from "~/components/driver/receive/TricycleBookings";
import VansBookings from "~/components/driver/receive/VansBookings";
import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor } from "~/helper/const";

const { height } = Dimensions.get("screen");
type TabListProps = {
  name: string;
  label: string;
  icon: string;
  size: number;
  component: any;
};

const navSetting = {
  animationEnabled: true,
  swipeEnabled: true,
  tabBarShowLabel: true,
  lazy: true,
  tabBarPressColor: "#394867",
  tabBarActiveTintColor: "#ffff",
  tabBarIndicatorStyle: {
    borderBottomColor: primaryColor,
    borderBottomWidth: 4,
  },
  tabBarStyle: {
    backgroundColor: "#05375a",
    height: height / 10,
  },
};
const ReceiveBookingComp = ({ navigation }: { navigation: any }) => {
  const Tab = createMaterialTopTabNavigator();
  const [tablist, setTabList] = useState<TabListProps[]>([]);

  useEffect(() => {
    const choosenTranspoType = async () => {
      const choosenTranspoType = await AsyncStorage.getItem(
        "transpoTypeChoosen",
      );

      if (choosenTranspoType == "tricycle") {
        setTabList([
          {
            name: "TriList",
            label: "Tricycle",
            icon: "car-outline",
            size: 28,
            component: TricycleBookings,
          },
        ]);
      } else {
        setTabList([
          {
            name: "MotorList",
            label: "Motorcycle",
            icon: "bicycle-outline",
            size: 28,
            component: MotorBookings,
          },
        ]);
      }
    };
    const unsubscribe = navigation.addListener("focus", () => {
      choosenTranspoType();
    });
  }, []);

  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
  return (
    <SafeAreaView className="flex min-h-full bg-[#fff]">
      {/* Changes page title visible on the header */}
      <TopBarHeader title="Receive Bookings" />
      {tablist.length > 0 && (
        <Tab.Navigator initialRouteName="BoatList" screenOptions={navSetting}>
          {tablist.map((result, index) => (
            <Tab.Screen
              key={index}
              name={result.name}
              component={result.component}
              options={{
                tabBarLabel: result.label,
                tabBarIcon: ({ color }) => (
                  <View>
                    <Ionicons
                      name={`${result.icon as IonicIonsname}`}
                      color={color}
                      size={result.size}
                    />
                  </View>
                ),
              }}
            />
          ))}
        </Tab.Navigator>
      )}
    </SafeAreaView>
  );
};
export default ReceiveBookingComp;
