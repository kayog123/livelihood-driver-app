import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  Modal,
  Pressable,
  ScrollView,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import AsyncStorage from "@react-native-async-storage/async-storage";
import LottieView from "lottie-react-native";

import Card, { SkeletonCard } from "../../components/Card";
import Layout from "../../components/Layout";
import { secondaryColor } from "../../helper/const";
import TravelServiceOpt from "../bookings/TravelServiceOpt";
import PlaceModal from "../modal/PlaceModal";
import Verify from "./../../../assets/verify.json";

interface navPropsFunc {
  screen: string;
}
const DriverHomePage = () => {
  const navigation = useRouter();
  const [serviceOption, setServiceOption] = useState<string>("");
  const [vehicleModal, setVehicleModal] = useState<boolean>(false);
  const { width } = Dimensions.get("screen");
  const LOGO_SIZE = width / 3;
  const navigate = ({ screen }: navPropsFunc) => {
    navigation.push(`/${screen}`);
  };

  const storedServiceType = async (value: string) => {
    try {
      await AsyncStorage.setItem("transpoTypeChoosen", value);
    } catch (e) {
      console.log(e);
    }
  };

  const onSubmitTranspo = async () => {
    if (!serviceOption) {
      ToastAndroid.show("Please select service type", ToastAndroid.SHORT);
      return;
    }
    await storedServiceType(serviceOption);
    setVehicleModal(false);
  };

  useEffect(() => {
    const fetchSelectedService = async () => {
      try {
        const choosenTranspoType = await AsyncStorage.getItem(
          "transpoTypeChoosen",
        );

        if (!choosenTranspoType) {
          setVehicleModal(true);
        }
      } catch (e) {
        console.log(e);
      }
    };
    fetchSelectedService();
  }, []);
  return (
    <Layout topBarTitle="Home">
      <Modal
        animationType="slide"
        transparent={true}
        visible={vehicleModal}
        onRequestClose={() => {
          setVehicleModal(!vehicleModal);
        }}
      >
        <View className="flex min-h-full items-center justify-center bg-[#1c1e216e] p-10">
          <View className="bg-bgWhite w-full ">
            <View className="bg-textColorDark px-3 py-5">
              <Text className="font-poppinsReg text-bgWhite">
                Mode of Transportation
              </Text>
            </View>
            <View className="space-y-4 p-3">
              <Text className="font-poppinsReg text-textColorDark border-primary mb-5 rounded-md border p-2 text-center">
                You will receive booking depend on the service type you
                selected!
              </Text>
              <TravelServiceOpt
                transpoSelected={serviceOption}
                setSelectedTranspo={setServiceOption}
              />
              <TouchableOpacity
                onPress={onSubmitTranspo}
                className="bg-textColorDark mx-5 rounded-md p-5"
              >
                <Text className="font-poppinsReg text-bgWhite text-center">
                  Submit
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <ScrollView className="bg-bgWhiteShadow  mx-3 flex p-3 pb-2 ">
        <Card>
          <View className=" flex-1 space-y-3">
            <View className="space-y-[-8]">
              <Text className="font-Poppins_700Bold text-md text-lg text-[#fff]">
                Welcome,
              </Text>
            </View>
            <Text className="font-poppinsReg text-[#fff]">
              Have a pleasant day to you!
            </Text>
          </View>
          <Image
            className="flex-1"
            source={require("./../../../assets/logo.png")}
            style={{ width: LOGO_SIZE, height: LOGO_SIZE }}
            alt=""
          />
        </Card>
      </ScrollView>
    </Layout>
  );
};

export default DriverHomePage;
