import {
  Alert,
  Dimensions,
  Image,
  Linking,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";

const CustomerDetails = ({ name, phone }: { name: string; phone: string }) => {
  const { height, width } = Dimensions.get("screen");
  const profile_img_size = width / 5;
  const navigation = useRouter();

  const callNumber = (phone: string) => {
    console.log("callNumber ----> ", phone);
    let phoneNumber = phone;
    if (Platform.OS !== "android") {
      phoneNumber = `telprompt:${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then((supported) => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <View className="bg-primary mx-5 mt-5 flex-row items-center justify-center space-x-2 rounded-md p-3">
      <View className="bg-primaryColorShadow w-[30%] rounded-full p-3">
        <Image
          className="rounded-full p-5"
          source={require("./../../../../assets/user.jpg")}
          style={{ width: profile_img_size, height: profile_img_size }}
          alt="profile image"
        />
      </View>
      <View className="w-[65%]">
        <Text className="font-poppinsReg text-bgWhite text-lg">{name}</Text>
        <Text className="font-poppinsReg text-bgWhite">{phone}</Text>
        <View className="mt-2 flex-row space-x-2 ">
          <TouchableOpacity
            onPress={() => {
              callNumber(phone);
            }}
            className="bg-primaryColorShadow rounded-md p-2"
          >
            <Ionicons name="call-outline" size={24} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.push("/message");
            }}
            className="bg-primaryColorShadow rounded-md p-2"
          >
            <Ionicons name="chatbubbles-outline" size={24} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default CustomerDetails;
