import {
  Alert,
  Dimensions,
  Image,
  ImageSourcePropType,
  Linking,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";

const BusinessInforCard = ({
  business_name,
  address,
  image
}: {
  business_name: string;
  address: string;
  image?: string
}) => {
  const { height, width } = Dimensions.get("screen");
  const profile_img_size = width / 5;
  const navigation = useRouter();

  return (
    <View className="bg-primary mx-5 mt-5 flex-row space-x-2 rounded-md p-3">
      <View className="w-[30%] flex-row">
        <View className="bg-primaryColorShadow  rounded-md p-1">
          <Image
            className="rounded-md "
            source={image ? {uri: image}: require("./../../../../assets/businessplaceholder.png")}
            style={{ width: profile_img_size, height: profile_img_size }}
            alt="business image image"
          />
        </View>
      </View>
      <View className="w-[65%]">
        <Text className="font-poppinsReg text-bgWhite text-lg">
          {business_name}
        </Text>
        <Text className="font-poppinsReg text-bgWhite">{address}</Text>
      </View>
    </View>
  );
};

export default BusinessInforCard;
