import { useState } from "react";
import { Pressable, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import { TripStatus } from "~/helper/const";
//import { TripStatus } from "~/helper/const";
import { NotifyBarProps } from "~/helper/type";
import { setlockedBooking } from "~/redux/reducer/lockedBookingSlice";
import { RootState } from "~/redux/store";

type AlertBoxProps = {
  title: string;
  message: string;
};
const BottomNav = ({
  status,
  bookingId,
  ownerId,
  setNotifyBar,
  setAlertBox,
  setAlertBoxMessage,
}: {
  status: string;
  bookingId: number;
  ownerId: string;
  setNotifyBar: (value: NotifyBarProps) => void;
  setAlertBox: (value: boolean) => void;
  setAlertBoxMessage: (value: { title: string; message: string }) => void;
}) => {
  const [initialStatus, setInitialStatus] = useState<string>(status);
  const [statusDetails, setStatusDetails] = useState<string>(status);
  const { lockedBooking } = useSelector(
    (state: RootState) => state.lockedBooking,
  );
  const [initialAlertBoxDetails, setInitialAlertBoxDetails] =
    useState<AlertBoxProps>({
      title: "",
      message: "",
    });
  const dispatch = useDispatch();
  const { mutate, error } = api.booking.updateBookingStatus.useMutation({
    async onMutate(newPost: any) {
      setNotifyBar({
        show: true,
        message: "Processing",
        type: "waiting",
      });
    },
    async onSuccess() {
      setNotifyBar({
        show: true,
        message: "Process complete!",
        type: "success",
      });
      if (initialStatus == "CANCELLED" || initialStatus == "COMPLETED") {
        setAlertBox(true);
        setAlertBoxMessage(initialAlertBoxDetails);
        dispatch(setlockedBooking(null));
      }
    },
    async onError(err: any, newPost: any, ctx: any) {
      console.log(err);
      setNotifyBar({
        show: true,
        message: "Sorry, an error encountered. Please try again!",
        type: "error",
      });
    },
    onSettled() {
      setStatusDetails(initialStatus);
      //let newData = lockedBooking;
      // if (initialStatus == "CANCELLED" || initialStatus == "COMPLETED") {
      //   lockedBooking!["status"] = initialStatus;
      // }
      //   newData?.status = initialStatus;
      const timer = setTimeout(() => {
        setNotifyBar({
          show: false,
          message: "",
          type: "waiting",
        });
        clearTimeout(timer);
      }, 5000);
    },
  });

  const cancelBooking = () => {
    setInitialStatus("CANCELLED");
    setInitialAlertBoxDetails({
      title: "Booking Cancelled!",
      message:
        "This booking is no longer locked to you! It will be routed to other driver!",
    });
    mutate({
      status: TripStatus.CANCELLED,
      reason: "",
      bookingId,
      ownerId,
    });
  };

  const onTheWayBooking = () => {
    setInitialStatus("ON_THE_WAY");
    mutate({
      status: TripStatus.ON_THE_WAY,
      reason: "",
      bookingId,
      ownerId,
    });
  };

  const PickedUpBooking = () => {
    setInitialStatus("PICKED_UP");
    mutate({
      status: TripStatus.PICKED_UP,
      reason: "",
      bookingId,
      ownerId,
    });
  };

  const completeBooking = () => {
    setInitialStatus("COMPLETED");
    setInitialAlertBoxDetails({
      title: "Booking Completed!",
      message: "Congratulations! You complete the booking.",
    });
    mutate({
      status: TripStatus.COMPLETED,
      reason: "",
      bookingId,
      ownerId,
    });
  };
  return (
    <View className=" bg-bgWhiteShadow bottom-0 h-20 w-full flex-row p-3">
      <View className="  w-full flex-row justify-between space-x-2">
        {statusDetails == "ACCEPTED" && (
          <Pressable
            onPress={cancelBooking}
            className="flex-1 items-center justify-center rounded-md bg-[#e04516] px-5 "
          >
            <Text className="font-poppinsReg text-bgWhite ">
              Cancel Booking
            </Text>
          </Pressable>
        )}
        {statusDetails == "ACCEPTED" && (
          <Pressable
            onPress={onTheWayBooking}
            className="bg-blueColor flex-1 items-center justify-center  rounded-md px-5"
          >
            <Text className="font-poppinsReg text-bgWhite text-xs">
              [Mark as]
            </Text>
            <Text className="font-poppinsReg text-bgWhite ">ON THE WAY</Text>
          </Pressable>
        )}
        {statusDetails == "ON_THE_WAY" && (
          <Pressable
            onPress={PickedUpBooking}
            className="bg-textColorDark flex-1 items-center justify-center  rounded-md px-5"
          >
            <Text className="font-poppinsReg text-bgWhite text-xs">
              [Mark as]
            </Text>
            <Text className="font-poppinsReg text-bgWhite ">PICKED_UP</Text>
          </Pressable>
        )}

        {statusDetails == "PICKED_UP" && (
          <Pressable
            onPress={completeBooking}
            className="flex-1 items-center justify-center rounded-md  bg-[#1ca820] px-5"
          >
            <Text className="font-poppinsReg text-bgWhite ">
              Complete Booking
            </Text>
          </Pressable>
        )}
        {(statusDetails == "CANCELLED" || statusDetails == "COMPLETED") && (
          <View
            className={`flex-1 items-center justify-center rounded-md  ${
              statusDetails == "COMPLETED" ? "bg-[#74c377]" : "bg-[#f5d0bc]"
            } px-5`}
          >
            <Text className="font-poppinsReg text-bgWhite ">
              {statusDetails}
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

export default BottomNav;
