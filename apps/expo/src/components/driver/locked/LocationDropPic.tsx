import { Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";

const LocationDropPic = ({
  text,
  iconColor = "#fff",
  topBorder = false,
  bottomBorder = false,
  iconText = "",
}: {
  text: String;
  iconColor?: string;
  topBorder?: boolean;
  bottomBorder?: boolean;
  iconText?: string;
}) => {
  return (
    <View className="flex flex-row space-x-2">
      <View className=" h-32 w-[20%] items-center justify-center ">
        <View
          className={`border-bgWhite g w-[1] flex-grow ${
            topBorder && "border-l-2"
          } border-dashed`}
        ></View>
        <View className=" bg-primary items-center justify-center rounded-md p-3">
          {iconText && (
            <Text className="text-bgWhite font-poppinsReg text-xs">
              {iconText}
            </Text>
          )}
          <Ionicons name="ios-location" size={28} color={iconColor} />
        </View>
        <View
          className={`border-bgWhite w-[1] flex-grow ${
            bottomBorder && "border-l-2"
          } border-dashed`}
        ></View>
      </View>
      <View className=" w-[70%]">
        <View className="bg-bgWhite mb-5 flex-grow rounded-lg p-3">
          <Text className="font-poppinsReg text-textDarkColor">{text}</Text>
        </View>
      </View>
    </View>
  );
};

export default LocationDropPic;
