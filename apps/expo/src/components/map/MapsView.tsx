import React, { useState } from "react";
import { Text, View } from "react-native";
import MapView, { Marker, PROVIDER_GOOGLE, Polyline } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

type MapVieProps = {
  height?: number;
};

const MapsView = ({ height = 40 }: MapVieProps) => {
  const GOOGLE_API_KEY = "ZId8y9ThVciA8sg7eEj1P-CWZnbb26MeO0K8VKO0LQ8";
  const [coordinates, setCoordinates] = useState([
    {
      latitude: 48.8587741,
      longitude: 2.2069771,
    },
    {
      latitude: 48.8323785,
      longitude: 2.3361663,
    },
  ]);

  return (
    <View className="rounded-md">
      <MapView
        provider={PROVIDER_GOOGLE}
        style={{ height: height }}
        className="w-full rounded-md"
        initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        {/* <Marker
            coordinate={{ latitude: 37.78825, longitude: -122.4324 }}
            image={require("./../../../assets/marker.png")}
            title="Marker"
            description="This is a custom marker size" 
          /> */}
        <MapViewDirections
          origin={coordinates[0]}
          destination={coordinates[1]}
          apikey={GOOGLE_API_KEY} // insert your API Key here
          strokeWidth={4}
          strokeColor="#111111"
        />
      </MapView>
    </View>
  );
};

export default MapsView;
