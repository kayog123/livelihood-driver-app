import React, { Fragment, useEffect, useRef, useState } from "react";
import {
  FlatList,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Autocomplete from "react-native-autocomplete-input";
import MapView, {
  Animated,
  AnimatedRegion,
  Marker,
  PROVIDER_GOOGLE,
} from "react-native-maps";
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from "@expo/vector-icons";

import { HERE_MAP_API_KEY, textColorDark } from "~/helper/const";
import useDebounce from "~/hooks/useDebounce";

type CoordinateProps = {
  latitude: number;
  longitude: number;
};

type PickUpProps = {
  lat: number;
  long: number;
  pick_up_name: string;
};
const GeoMapUpdater = ({
  setGeolocation,
  setMapModal,
  initialLong,
  initialLat,
  searchBar = true,
  initialPickUpName,
}: {
  initialLat: number;
  initialLong: number;
  setGeolocation: (value: PickUpProps) => void;
  setMapModal: (value: boolean) => void;
  searchBar?: boolean;
  initialPickUpName: string;
}) => {
  const [processing, setProcessing] = useState<boolean>(false);
  const [searching, setSearching] = useState<boolean>(false);
  const [coordinate, setCoordinates] = useState<CoordinateProps>({
    latitude: initialLat,
    longitude: initialLong,
  });
  const [search, setSearch] = useState<string>("");
  const debounceSearch = useDebounce({ value: search, delay: 1000 });
  const [toolBar, setToolbar] = useState<boolean>(true);
  const [currentLocation, setCurrentLocation] = useState<PickUpProps>({
    lat: initialLat,
    long: initialLong,
    pick_up_name: initialPickUpName,
  });

  const [searchResultStatus, setSearchResultStatus] = useState<boolean>(false);
  const mapRef = useRef(null);
  const fetchGeoDetails = async (lat: number, long: number) => {
    await fetch(
      `https://revgeocode.search.hereapi.com/v1/revgeocode?at=${lat}%2C${long}&lang=en-US&apiKey=${HERE_MAP_API_KEY}`,
      {
        method: "get",
      },
    )
      .then((response) => response.json())
      .then((result) => {
        setCurrentLocation({
          lat: result.items[0].position.lat,
          long: result.items[0].position.lng,
          pick_up_name: result.items[0].address.label,
        });
      });
  };
  const [addressItem, setAddressItem] = useState([]);
  useEffect(() => {
    fetchList(debounceSearch);
  }, [debounceSearch]);

  const fetchList = async (text: string) => {
    setSearching(true);
    await fetch(
      //`https://geocode.search.hereapi.com/v1/geocode?q=${text}&resultType=places&apiKey=${HERE_MAP_API_KEY}`,
      `https://geocode.search.hereapi.com/v1/geocode?q=${text}&apiKey=${HERE_MAP_API_KEY}`,
      {
        method: "GET",
      },
    )
      .then((res) => res.json())
      .then((result) => {
        setSearchResultStatus(true);
        setAddressItem(result.items == undefined ? [] : result.items);
      })
      .catch((e) => {
        console.log(e);
      });
    setSearching(false);
  };

  return (
    <View>
      {toolBar && (
        <View className="absolute top-16 z-20 w-full overflow-auto p-5 ">
          {/* START TOP TOOLBAR */}
          <View className="bg-bgWhite p-3">
            <Autocomplete
              autoCapitalize="none"
              className="bg-bgWhite font-poppinsReg m5 overflow-visible px-5 shadow-md"
              autoCorrect={false}
              //containerStyle={styles.autocompleteContainer}
              value={search}
              data={addressItem}
              onChangeText={(text) => setSearch(text)}
              placeholder="Search Places..."
              renderResultList={(item: any) => {
                //setSearchResultStatus(item.data.length > 0 ? true : false);
                const boholItems = item.data.filter(
                  (result: any) => result.address.county === "Bohol",
                );
                return (
                  <Fragment>
                    <View>
                      {boholItems.length > 0 && (
                        <Text className="font-poppinsReg px-2 py-3 text-[#6a6a6a]">
                          {boholItems.length} total result
                        </Text>
                      )}
                      {searchResultStatus && (
                        <View className=" bg-textDarkColor w-full justify-center divide-y divide-gray-200 p-3 dark:divide-gray-700">
                          {boholItems.map((result: any, index: number) => (
                            <TouchableOpacity
                              key={index}
                              onPress={() => {
                                setCoordinates({
                                  latitude: result.position.lat,
                                  longitude: result.position.lng,
                                  //pick_up_name: result.title,
                                });
                                console.log("executed");
                                setCurrentLocation({
                                  lat: result.position.lat,
                                  long: result.position.lng,
                                  pick_up_name: result.title,
                                });
                                setSearchResultStatus(false);
                              }}
                              className="flex py-2"
                            >
                              <Text
                                numberOfLines={1}
                                className="font-poppinsReg  "
                              >
                                {result.title}
                              </Text>
                              <Text className="font-poppinsReg mt-[-5] text-xs">
                                {result.address.county}, {result.address.city}{" "}
                                {result.address.postalCode}
                              </Text>
                            </TouchableOpacity>
                          ))}
                        </View>
                      )}
                    </View>
                  </Fragment>
                );
              }}
            />
            {searching && (
              <View className="">
                <Text className="font-poppinsReg text-textColorDark">
                  Searching..
                </Text>
              </View>
            )}
          </View>
          {/* END SIDE TOOLBAR */}
        </View>
      )}

      <MapView
        className="h-full w-full rounded-md"
        onPress={(e) => {
          const geo = e.nativeEvent.coordinate;
          fetchGeoDetails(geo.latitude, geo.longitude);
          setCoordinates(geo);
        }}
        // initialRegion={{
        //   latitude: coordinate.latitude,
        //   longitude: coordinate.longitude,
        //   latitudeDelta: 0.0015,
        //   longitudeDelta: 0.01555,
        // }
        region={{
          latitude: coordinate.latitude,
          longitude: coordinate.longitude,
          latitudeDelta: 0.0015,
          longitudeDelta: 0.01555,
        }}
      >
        <Marker
          draggable
          coordinate={coordinate}
          ref={mapRef}
          //image={require("./../../../assets/marker.png")}
          title="Location address"
          description="This is the pick up location you specified."
          onDragEnd={(e) => {
            const geo = e.nativeEvent.coordinate;
            fetchGeoDetails(geo.latitude, geo.longitude);
            setCoordinates(geo);
          }}
          //onPress={(e) => setCoordinates(e.nativeEvent.coordinate)}
        />
      </MapView>
      {/* START SIDE TOOLBAR */}

      {/* END SIDE TOOLBAR */}
      <View className="absolute bottom-0 left-0 right-0 flex w-full items-center justify-end p-5 pb-12">
        <View className="bg-primary flex  flex-row flex-wrap rounded-lg p-5 pb-10">
          <View className="items-center justify-center">
            <View className="bg-secondaryLight mr-5 h-16 items-center justify-center rounded-lg p-1">
              <Ionicons
                color="white"
                name="navigate-circle-outline"
                size={48}
              />
            </View>
          </View>
          <View className="flex flex-1 ">
            <Text className="text-textColorDark text-md font-poppinsReg">
              Specified Location:
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              {currentLocation.pick_up_name}
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              Latitude : {coordinate.latitude}
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              Longitude : {coordinate.longitude}
            </Text>
          </View>
        </View>
        <Pressable
          className=" absolute bottom-3 w-full"
          onPress={async () => {
            setProcessing(true);
            setGeolocation(currentLocation);
            setProcessing(false);
            setMapModal(false);
          }}
        >
          <LinearGradient
            className="bg-textColorDark flex-column ml-10 mr-10 flex-row items-center justify-center rounded-lg p-5"
            colors={["#05375a", "#031e30d4"]}
          >
            {!processing ? (
              <View className="flex-row  items-center justify-center">
                <Ionicons name="locate-outline" size={28} color={"#fff"} />
                <Text className="text-bgWhite font-poppinsReg">
                  Select Location
                </Text>
              </View>
            ) : (
              <Text className="font-poppinsReg text-bgWhite">
                Processing...
              </Text>
            )}
          </LinearGradient>
        </Pressable>
      </View>
    </View>
  );
};

export default GeoMapUpdater;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#F5FCFF",
    flex: 1,
    padding: 16,
    marginTop: 40,
  },
  autocompleteContainer: {
    backgroundColor: "#ffffff",
    borderWidth: 0,
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: "center",
  },
  itemText: {
    fontSize: 15,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  },
  infoText: {
    textAlign: "center",
    fontSize: 16,
  },
});
