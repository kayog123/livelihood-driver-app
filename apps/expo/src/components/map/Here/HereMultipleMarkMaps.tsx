import { Fragment, useCallback, useEffect, useState } from "react";
import { Text, View } from "react-native";
import MapView, { Marker, Polyline } from "react-native-maps";

import { HERE_MAP_API_KEY } from "~/helper/const";

type TripCoordinatesProp = {
  latitude: number;
  longitude: number;
  place_name: string;
};

type MapMultiplMarkProps = {
  height?: number;
  initialRegionLat: number;
  initialRegionLong: number;
  initialPickUpName: string;
  tripCoordinate: TripCoordinatesProp[];
};

type CoordinateProps = {
  lat: number;
  long: number;
};

const HereMultipleMarkMaps = ({
  height = 40,
  initialRegionLat,
  initialRegionLong,
  initialPickUpName,
  tripCoordinate,
}: MapMultiplMarkProps) => {
  const [regionCoordinates, setRegionCoordinates] = useState<CoordinateProps>({
    lat: initialRegionLat,
    long: initialRegionLong,
  });

  useEffect(() => {
    if (Object.keys(tripCoordinate).length != 0) {
      fetchMultiplePolyLines();
    }
  }, []);

  const fetchMultiplePolyLines = async () => {
    let originGeo = "";
    let routeGeoList = "";
    let first = true;
    tripCoordinate.forEach((item, index) => {
      if (index == 0) {
        originGeo = `${item.latitude},${item.longitude}`;
        //setRegionCoordinates({ lat: item.latitude, long: item.longitude });
      } else {
        const deli = first ? "" : "&via=";
        if (first) {
          first = false;
        }
        routeGeoList =
          routeGeoList + `${deli}${item.latitude},${item.longitude}`;
      }
    });
    console.log(
      `https://router.hereapi.com/v8/routes?origin=${originGeo}&destination=${routeGeoList}&return=summary&transportMode=car&apikey=${HERE_MAP_API_KEY}`,
      `https://router.hereapi.com/v8/routes?origin=${originGeo}&destination=${routeGeoList}!passThrough=true&return=polyline,summary,actions,instructions&spans=notices&transportMode=truck&routingMode=fast&apikey=${HERE_MAP_API_KEY}`,
    );

    const routesList = "";
    return await fetch(
      `https://router.hereapi.com/v8/routes?origin=${originGeo}&destination=${routeGeoList}&return=summary&transportMode=car&apikey=${HERE_MAP_API_KEY}`,
      {
        method: "get",
      },
    )
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        return result.routes;
      });
  };
  return (
    <View>
      <MapView
        //provider={PROVIDER_GOOGLE}
        style={{ height: height }}
        className="w-full rounded-md"
        region={{
          latitude: regionCoordinates.lat,
          longitude: regionCoordinates.long,
          latitudeDelta: 0.0122,
          longitudeDelta: 0.0421,
        }}
      ></MapView>
    </View>
  );
};

export default HereMultipleMarkMaps;
