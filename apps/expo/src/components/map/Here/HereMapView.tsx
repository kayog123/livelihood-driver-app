import { Fragment, useCallback, useEffect, useState } from "react";
import { Text, View } from "react-native";
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from "react-native-maps";

import { FAIR, HERE_MAP_API_KEY } from "~/helper/const";
import { decode } from "~/helper/herepolylines";

type CoordinateProps = {
  latitude: number;
  longitude: number;
};

type MapViewProps = {
  height?: number;
  initialRegionLat: number;
  initialRegionLong: number;
  initialPickUpName: string;
  dropoffLat?: number | null;
  dropoffLong?: number | null;
  dropoffPickUp?: string | null;
  setFair: (value: number) => void;
  transpoSelected: string;
  className?: string;
  setKM?: (value: number) => void;
};
const HereMapView = ({
  height = 40,
  initialRegionLat,
  initialRegionLong,
  initialPickUpName,
  dropoffLong,
  dropoffLat,
  dropoffPickUp,
  setFair,
  setKM,
  transpoSelected,
  className = "",
}: MapViewProps) => {
  const initialRouteCoordinate = [
    {
      latitude: initialRegionLat,
      longitude: initialRegionLong,
    },
  ];
  const [routeCoordinate, setRouteCoordinate] = useState<CoordinateProps[]>(
    initialRouteCoordinate,
  );

  useEffect(() => {
    if (dropoffLong != null && dropoffLat != null) {
      const fetchRouteCoordinates = async () => {
        const coordinates = await fetch(
          `https://router.hereapi.com/v8/routes?transportMode=car&origin=${initialRegionLat},${initialRegionLong}&destination=${dropoffLat},${dropoffLong}&return=polyline,summary&apikey=${HERE_MAP_API_KEY}`,
          {
            method: "GET",
          },
        )
          .then((result) => result.json())
          .then((items) => {
            const serviceFair =
              transpoSelected == "tricycle" ? FAIR.TRICYCLE : FAIR.MOTOR;
            const distance = items.routes[0].sections[0].summary.length;
            const totalKM = distance / 1000;
            if (setKM) {
              setKM(totalKM);
            }
            let amount;
            if (totalKM > 1) {
              amount = totalKM * serviceFair;
            } else {
              amount = serviceFair;
            }
            setFair(amount);
            return decode(items.routes[0].sections[0].polyline);
          })
          .then((coordinates) => {
            const res = coordinates.polyline.map((result: any) => {
              return {
                latitude: result[0],
                longitude: result[1],
              };
            });

            return res;
          })
          .then((items) => setRouteCoordinate(items));
      };
      fetchRouteCoordinates();
    }
  }, []);

  return (
    <View className={className}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={{ height: height }}
        className="w-full rounded-md"
        initialRegion={{
          latitude: initialRegionLat,
          longitude: initialRegionLong,
          latitudeDelta: 0.0122,
          longitudeDelta: 0.0421,
        }}
      >
        <Marker
          coordinate={{
            latitude: initialRegionLat,
            longitude: initialRegionLong,
          }}
          //image={require("./../../../assets/marker.png")}
          title="Pick up Location"
          description={initialPickUpName}
        />
        {dropoffLat != null && dropoffLong != null && dropoffPickUp != null && (
          <Fragment>
            <Marker
              coordinate={{
                latitude: dropoffLat,
                longitude: dropoffLong,
              }}
              //icon={require("./../../../../assets/marker.png")}
              title="Dropoff Location Address"
              description={dropoffPickUp}
            />
            <Polyline
              coordinates={routeCoordinate}
              strokeWidth={3}
              strokeColor="red"
              geodesic={true}
            />
          </Fragment>
        )}
      </MapView>
    </View>
  );
};

export default HereMapView;
