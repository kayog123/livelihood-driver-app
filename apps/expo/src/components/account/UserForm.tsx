import React from "react";
import { TextInput, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

const UserFormBox = () => {
  return (
    <View className="flex gap-3">
      <View
        style={{ borderBottomWidth: 1 }}
        className="mb-25 flex  flex-row items-center gap-2 border-0 border-white"
      >
        <Ionicons name="mail-outline" size={24} color="white" />
        <TextInput
          // value="jovannijungco27@gmail.com"
          className="text-white"
          style={{ paddingVertical: 0 }}
          placeholder="Email Address"
          placeholderTextColor="text-gray-100"
        />
      </View>
      <View
        style={{ borderBottomWidth: 1 }}
        className="mb-25 flex flex-row items-center gap-2 border-0 border-b-2 border-white"
      >
        <Ionicons name="call-outline" size={20} color="white" />
        <TextInput
          value="+63-951-573-5250"
          className="text-white"
          style={{ paddingVertical: 0 }}
          placeholder="Phone Number"
          placeholderTextColor="text-white"
        />
      </View>
    </View>
  );
};

export default UserFormBox;
