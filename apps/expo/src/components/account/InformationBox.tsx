import React, { Fragment } from "react";
import { Text, View } from "react-native";

const InformationBox = () => {
  return (
    // divide-y divide-gray-200 dark:divide-gray-700
    <View className=" flex w-full flex-row divide-x divide-gray-200 dark:divide-gray-700">
      <View className="flex-1 items-center justify-center">
        <Text className="text-bgWhite font-poppinsReg">Today</Text>
        <Text className="text-bgWhite font-poppinsReg text-lg font-bold">
          105k
        </Text>
      </View>
      <View className=" flex-1 items-center justify-center">
        <Text className="text-bgWhite font-poppinsReg">This week</Text>
        <Text className="text-bgWhite font-poppinsReg text-lg font-bold">
          2M
        </Text>
      </View>
      <View className=" flex-1 items-center justify-center">
        <Text className="text-bgWhite font-poppinsReg">This month</Text>
        <Text className="text-bgWhite font-poppinsReg text-lg font-bold">
          20000
        </Text>
      </View>
      {/* <View className="justify-cente flex-1 items-center text-center">
        <Text className="text-textColorDark font-poppinsReg">Activity</Text>
        <Text className="text-bgWhite font-poppinsReg text-lg font-bold">
          484K
        </Text>
      </View> */}
    </View>
  );
};

export default InformationBox;
