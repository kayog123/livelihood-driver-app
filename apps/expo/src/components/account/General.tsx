import React, { type ReactNode } from "react";
import { Alert, Text, TouchableOpacity, View } from "react-native";
import { useRouter } from "expo-router";
import { ClerkProvider, SignedIn, SignedOut, useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";

import { RootState } from "~/redux/store";
import { primaryColor } from "../../helper/const";
import CardBox from "./CardBox";

const SignOut = () => {
  const { isLoaded, signOut } = useAuth();
  const { lockedBooking } = useSelector(
    (state: RootState) => state.lockedBooking,
  );
  const navigation = useRouter();
  const confirmSignOut = () => {
    if (lockedBooking) {
      lockedBookingAlert();
    } else {
      signOutAlert();
    }
  };

  if (!isLoaded) {
    return null;
  }
  const lockedBookingAlert = () => {
    Alert.alert(
      "Can't sign out",
      "You have a locked booking, complete or cancel before signing out!",
      [
        {
          text: "Close",
          onPress: () => {},
          style: "cancel",
        },
        {
          text: "Show",
          onPress: () => navigation.push("/driver/booking/lockedorders"),
        },
      ],
    );
  };
  const signOutAlert = () => {
    Alert.alert("Sign Out", "Are you sure to signout?", [
      {
        text: "Cancel",
        onPress: () => {},
        style: "cancel",
      },
      { text: "OK", onPress: () => signOut() },
    ]);
  };

  return (
    <TouchableOpacity onPress={confirmSignOut}>
      <View className="bg-bgWhite mt-2 flex flex-row items-center rounded-lg border border-solid border-[#e6e6e6] p-3">
        <View className="bg-primary mr-2 rounded-xl p-2">
          <Ionicons name="log-out-outline" size={24} color="#fff" />
        </View>
        <Text className="text-md text-textColorDark font-poppinsReg flex-grow text-sm font-semibold">
          Sign Out
        </Text>
        <Ionicons name="chevron-forward-outline" size={28} color="black" />
      </View>
    </TouchableOpacity>
  );
};
const General = () => {
  return (
    <View>
      <Text className="text-textColorDark font-poppinsReg mt-10 text-xl">
        General
      </Text>
      <CardBox
        title="Help Center"
        icons={<Ionicons name="headset-outline" size={24} color="white" />}
        component="account/customer-service"
      />
      {/* <CardBox
        title="Agreement"
        icons={<Ionicons name="document-outline" size={24} color="white" />}
        component="account/addresses"
      /> */}
      <SignOut />
    </View>
  );
};

export default General;
