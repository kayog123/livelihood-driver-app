import React, { Fragment } from "react";
import { Dimensions, Image, Text, View } from "react-native";
import { useAuth, useUser } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";
import { MotiView, useDynamicAnimation } from "moti";
import { Skeleton } from "moti/skeleton";

import { api } from "~/utils/api";
import LoadingScreen from "../Loading";

type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
const ProfileCard = () => {
  const { userId, isLoaded } = useAuth();
  const { width, height } = Dimensions.get("screen");
  const ProfImgSize = width / 3;

  if (!userId || !isLoaded) return <Text>Please wait...</Text>;

  const { data: userData } = api.user.byId.useQuery({ id: userId });

  return (
    <View>
      {!userData ? (
        <View className="mt-16">
          <Skeleton colorMode="light" width={"100%"} height={height / 5} />
        </View>
      ) : (
        <View className="bg-primary mt-16 rounded-lg p-3 pt-5">
          <View className="flex-column flex w-full gap-4 ">
            <View className="flex flex-row gap-3 ">
              <View className="w-[60%]">
                <Fragment>
                  <Text
                    className=" text-bgWhite font-poppinsReg text-2xl  "
                    numberOfLines={2}
                  >
                    {userData.firstname}
                  </Text>
                  <Text
                    className="text-bgWhite font-poppinsReg text-2xl"
                    numberOfLines={2}
                  >
                    {userData.lastname}
                  </Text>
                </Fragment>
              </View>

              <View className="absolute right-0 top-[-90]  ">
                <MotiView
                  from={{ scale: 0, opacity: 0 }}
                  animate={{ scale: 1, opacity: 1 }}
                  transition={{
                    type: "timing",
                    duration: 800,
                    scale: {
                      type: "spring",
                      delay: 1000,
                    },
                  }}
                >
                  <View className={`bg-primary rounded-full p-3`}>
                    <Image
                      className="rounded-full"
                      source={
                        userData.profile
                          ? { uri: userData.profile }
                          : require("./../../../assets/user.jpg")
                      }
                      style={{ width: ProfImgSize, height: ProfImgSize }}
                      alt=""
                    />
                  </View>
                </MotiView>
              </View>
            </View>
            <View>
              {[
                {
                  text: `${userData.address1}, ${userData.city}`,
                  icon: "location-outline",
                },
                {
                  text: `${userData.email}`,
                  icon: "mail-outline",
                },
                {
                  text: `${userData.phone}`,
                  icon: "call-outline",
                },
              ].map((result: { text: string; icon: string }, index: number) => (
                <View key={index}>
                  <View className="flex flex-row items-center space-x-2">
                    <Ionicons
                      name={result.icon as IonicIonsname}
                      size={24}
                      color="white"
                    />
                    <Text className="text-bgWhite font-poppinsReg">
                      {result.text}
                    </Text>
                  </View>
                </View>
              ))}
            </View>

            {/* <View className="border-bgWhite border-t border-dashed  ">
              <Text className="font-Poppins_700Bold text-bgWhite p-2 text-center text-lg">
                Trip Overview
              </Text>
              <View className="border-primaryColorShadow bg-textColorDark mt-5 flex w-full flex-col rounded-lg border border-solid p-3">
                <InformationBox />
              </View>
            </View> */}
          </View>
        </View>
      )}
    </View>
  );
};

export default ProfileCard;
