import React from "react";
import { SafeAreaView, ScrollView, View } from "react-native";

import TopBarHeader from "../TopBarHearder";
import General from "./General";
import InformationBox from "./InformationBox";
import MyAccount from "./MyAccount";
import MySettings from "./MySettings";
import ProfileCard from "./ProfileCard";

const Account = () => {
  return (
    <SafeAreaView>
      <TopBarHeader title="Account Details" />
      <ScrollView className="flex  bg-gray-100 px-5">
        <ProfileCard />
        {/* <View className="mt-5 w-full p-5">
          <UserFormBox />
        </View>  */}
        <MyAccount />
        {/* <MySettings /> */}
        <General />
        <View className=" mt-5 h-1 w-full"></View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Account;
