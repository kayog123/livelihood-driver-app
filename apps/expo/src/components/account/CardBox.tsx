import React, { type ReactNode } from "react";
import { Pressable, Text, TouchableOpacity, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useRouter } from "expo-router";
import { MotiView, useDynamicAnimation } from "moti";

import { primaryColor } from "../../helper/const";

type CardBox = {
  title: string;
  component: string;
  icons: ReactNode;
};

interface navPropsFunc {
  screen: string;
}

const CardBox = ({ title, icons, component }: CardBox) => {
  const animation = useDynamicAnimation(() => ({
    opacity: 1,
  }));
  const navigation = useRouter();

  const navigate = ({ screen }: navPropsFunc) => {
    navigation.push(`/${screen}`);
  };

  return (
    <Pressable
      onPressIn={() => {
        animation.animateTo({
          scale: 0.9,
        });
      }}
      onPressOut={() => {
        animation.animateTo((current) => ({
          ...current,
          scale: 1,
        }));
      }}
      onPress={() => {
        navigate({
          screen: component, //"account/profile",
        });
      }}
    >
      <MotiView
        state={animation}
        className="bg-bgWhite mt-2 flex flex-row items-center rounded-lg border border-solid border-[#e6e6e6] p-3"
      >
        <View className="bg-primary mr-2 rounded-xl p-2">{icons}</View>
        <Text className="text-md text-textColorDark font-poppinsReg flex-grow text-sm">
          {title}
        </Text>
        <Ionicons name="chevron-forward-outline" size={28} color="black" />
      </MotiView>
    </Pressable>
  );
};
export default CardBox;
