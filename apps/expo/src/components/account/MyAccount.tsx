import type { ReactNode } from "react";
import { Text, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { MotiView, useDynamicAnimation } from "moti";

import { primaryColor } from "../../helper/const";
import CardBox from "./CardBox";

type MyAccountProps = {
  title: string;
  icon: ReactNode;
  component: string;
};
const MY_ACCOUNT_DATA: MyAccountProps[] = [
  {
    title: "Profile Information",
    icon: <Ionicons name="person-circle-outline" size={24} color="white" />,
    component: "account/profile",
  },
  // {
  //   title: "My Addresses", //THIS IS FOR USER ONLY
  //   icon: <Ionicons name="locate-outline" size={24} color="white" />,
  //   component: "account/addresses",
  // },
  {
    title: "Driver Information", //THIS IS FOR DRIVER ONLY
    icon: <Ionicons name="speedometer-outline" size={24} color="white" />,
    component: "driver/information",
  },
  // {
  //   title: "Activate Rewards", //THIS IS FOR USER ONLY
  //   icon: <Ionicons name="cash-outline" size={24} color="white" />,
  //   component: "rewards",
  // },
  {
    title: "Emergency Contact",
    icon: <Ionicons name="medkit-outline" size={24} color="white" />,
    component: "account/emergency",
  },
];
const MyAccount = () => {
  return (
    <View>
      <Text className="text-textColorDark font-poppinsReg mt-10 text-xl">
        My Account
      </Text>
      {MY_ACCOUNT_DATA.map((result: MyAccountProps, index: number) => (
        <CardBox
          key={index}
          title={result.title}
          icons={result.icon}
          component={result.component}
        />
      ))}
    </View>
  );
};

export default MyAccount;
