import React, { type ReactNode } from "react";
import { Text, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

import CardBox from "./CardBox";

const MySettings = () => {
  return (
    <View>
      <Text className="text-textColorDark font-poppinsReg mt-10 text-xl">
        My Settings
      </Text>
      <CardBox
        title="Settings"
        icons={<Ionicons name="cog-outline" size={24} color="white" />}
        component="settings"
      />
      {/* <CardBox
        title="Delete Account"
        icons={<Ionicons name="trash-outline" size={24} color="white" />}
        component="account/profile"
      /> */}
    </View>
  );
};

export default MySettings;
