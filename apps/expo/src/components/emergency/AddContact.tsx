import {
  Ref,
  Fragment,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Text, TextInput, ToastAndroid, View } from "react-native";
import { MaskedTextInput } from "react-native-mask-text";
import { Controller, useForm } from "react-hook-form";

import { api } from "~/utils/api";
import {
  cityList,
  countryList,
  provinceList,
  relationList,
} from "~/helper/const";
import DropdownComponent from "../Dropdown";

type FormValues = {
  id: number;
  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  country: string;
  zipcode: string;
  relationship: string;
  email: string;
  phone: string;
};

const ControlComponent = ({
  control,
  errors,
  fieldName,
  rules = {
    required: fieldName + " is required",
  },
  placeholder,
}: any) => {
  return (
    <Controller
      control={control}
      render={({ field: { onChange, onBlur, value } }) => (
        <Fragment>
          <View className="bg-bgWhite mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
            <TextInput
              className="h-14"
              placeholder={
                !placeholder
                  ? fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
                  : placeholder
              }
              value={value}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
            />
          </View>
          {errors[fieldName] && (
            <Text className="text-errorColor bottom-0 flex">
              {errors[fieldName].message}
            </Text>
          )}
        </Fragment>
      )}
      name={fieldName}
      rules={rules}
    />
  );
};


export interface ContactComponentHandles { 
  onSubmitValidate: () => void,
  onUpdateValidate: () => void,
  removeContact: () =>  void,
}
const AddContact = (
  {
    userId,
    refetchData,
    updateDataList,
    setModalVisible,
    formData,
    openFrom,
    updateDataForm,
  }: {
    userId: string;
    refetchData: () => object;
    updateDataList: (value: object) => void;
    setModalVisible: (value: boolean) => void;
    formData: FormValues | {};
    openFrom: "create" | "update";
    updateDataForm: (value: FormValues) => void;
  },
  ref:  Ref<ContactComponentHandles>,
) => {
  const [prevData, setPrevData] = useState({});
  const [form, setForm] = useState({
    name: "",
    email: "",
    hourlyRate: "",
    birthDay: "",
  });

  const handleForm = (key: string, value: string) => {
    setForm((currentForm) => ({
      ...currentForm,
      [key]: value,
    }));
  };

  const utils = api.useContext();
  const { mutate, error } = api.contact.create.useMutation({
    // async onMutate(newPost: any) {
    //   return await refetchData();
    // },
    async onSuccess() {
      await refetchData();
      showToast({ message: "Success! A contact was added!" });
    },
    async onError(err: any, newPost: any, ctx: any) {
      //err, newPost, ctx
      // If the mutation fails, use the context-value from onMutate
      //updateDataList(prevData);
      showToast({
        message: "Sorry! An issue encountered when saving contact!",
      });
      //utils.contact.list.setData(undefined, ctx.prevData);
    },
    onSettled() {
      // Sync with server once mutation has settled
      setModalVisible(false);
      //utils.contact.byIdAllData.invalidate();
    },
  });
  const updateShow = api.contact.updateVisibilityById.useMutation({
    async onSuccess() {
      await refetchData();
      showToast({ message: "Success! A contact was added!" });
    },
    async onError(err: any, newPost: any, ctx: any) {
      showToast({
        message: "Sorry! An issue encountered when saving contact!",
      });
    },
    onSettled() {
      // Sync with server once mutation has settled
      setModalVisible(false);
      //utils.contact.byIdAllData.invalidate();
    },
  });

  useImperativeHandle(ref, () => ({
    onSubmitValidate: () => {
      onSubmitValidate();
    },
    onUpdateValidate: () => {
      onUpdateValidate();
    },
    removeContact: () => {
      removeContact(formData.id);
    },
  }));

  function showToast({
    message = "Request sent successfully!",
  }: {
    message: string;
  }) {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  }

  useEffect(() => {
    if (formData === undefined || openFrom === "create") {
      return;
    }
    const zipData = { zipcode: formData.zipCode };

    reset({
      ...formData, //reset the data once fetch from prisma
      ...zipData,
    });
  }, [formData]);

  const {
    control,
    handleSubmit,
    getValues,
    reset,
    formState: { errors },
  } = useForm<FormValues>({
    defaultValues: {
      firstname: "",
      lastname: "",
      address1: "",
      address2: "",
      city: "Tagbilaran City",
      state: "Bohol",
      country: "Philippines",
      zipcode: "",
      relationship: "Brother",
      email: "",
      phone: "",
    },
  });

  const onSubmit = async (dataForm: FormValues) => {
    mutate({
      userId: userId,
      firstname: dataForm.firstname,
      lastname: dataForm.lastname,
      email: dataForm.email,
      phone: dataForm.phone,
      address1: dataForm.address1,
      address2: dataForm.address2,
      state: dataForm.state,
      city: dataForm.city,
      country: dataForm.country,
      zipCode: dataForm.zipcode,
      relationship: dataForm.relationship,
    });
  };

  const removeContact = (id: number) => {
    updateShow.mutate({
      id: id,
      show: false,
    });
  };

  const onUpdateSubmit = async (dataForm: FormValues) => {
    updateDataForm(dataForm);
  };

  const onUpdateValidate = handleSubmit(onUpdateSubmit);
  const onSubmitValidate = handleSubmit(onSubmit);

  return (
    <View className="p-5">
      <View className="flex ">
        <View className="mt-5 flex flex-row items-center ">
          <View className="border-primary mx-2 h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted"></View>
          <Text className="text-textColorDark font-Poppins_700Bold my-4 text-sm">
            Choose Relationship
          </Text>
          <View className="border-primary mx-2 h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted"></View>
        </View>
        <View className="">
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                <DropdownComponent
                  bgColor={"#fff"}
                  placeholder="Relationship"
                  value={value}
                  dataList={relationList}
                  onChange={(item) => onChange(item.value)}
                />
                {errors.relationship && (
                  <Text className="text-errorColor">
                    {errors.relationship.message}
                  </Text>
                )}
              </Fragment>
            )}
            name="relationship"
          />
        </View>
      </View>
      <View className="mt-5 flex flex-row items-center ">
        <View className="border-primary mx-2 h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted"></View>
        <Text className="text-textColorDark font-Poppins_700Bold my-4 text-sm">
          Contact Information
        </Text>
        <View className="border-primary mx-2 h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted"></View>
      </View>
      <View className="mt-[-20] flex flex-row gap-x-2">
        <View className="flex-1">
          <ControlComponent
            control={control}
            errors={errors}
            fieldName="firstname"
          />
        </View>
        <View className="flex-1">
          <ControlComponent
            control={control}
            errors={errors}
            fieldName="lastname"
          />
        </View>
      </View>
      <View className="flex-1">
        <ControlComponent
          control={control}
          errors={errors}
          fieldName="email"
          placeholder="Email address"
          rules={{
            required: "Email address is required",
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
              message: "Invalid email address",
            },
          }}
        />
      </View>
      <View className="flex-1">
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <Fragment>
              {/* <DropdownComponent
                bgColor={"#fff"}
                placeholder="Select City"
                value={value}
                dataList={cityList}
                onChange={(item) => onChange(item.value)}
              /> */}
              <View className="bg-bgWhite mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
                <MaskedTextInput
                  value={value}
                  mask="+(63)-999-9999-9999"
                  placeholder="+(63)-000-0000-0000"
                  onChangeText={(value) => {
                    onChange(value);
                  }}
                  keyboardType="numeric"
                  style={{ height: 55 }}
                />
              </View>
              {errors.phone && (
                <Text className="text-errorColor">{errors.phone.message}</Text>
              )}
            </Fragment>
          )}
          name="phone"
          rules={{
            required: "Phone number is required",
          }}
        />
      </View>
      <ControlComponent
        control={control}
        errors={errors}
        fieldName="address1"
        placeholder="Address 1"
      />
      <ControlComponent
        control={control}
        errors={errors}
        fieldName="address2"
        placeholder="Address 2"
      />
      <View className="mt-5">
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <Fragment>
              <DropdownComponent
                bgColor={"#fff"}
                placeholder="Select City"
                value={value}
                dataList={cityList}
                onChange={(item) => onChange(item.value)}
              />
              {errors.city && (
                <Text className="text-errorColor">{errors.city.message}</Text>
              )}
            </Fragment>
          )}
          name="city"
        />
      </View>
      <View className="flex flex-row gap-x-2">
        <View className="mt-5 flex-1">
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                <DropdownComponent
                  placeholder="Province"
                  value={value}
                  dataList={provinceList}
                  onChange={(item) => onChange(item.value)}
                />
                {errors.state && (
                  <Text className="text-errorColor">
                    {errors.state.message}
                  </Text>
                )}
              </Fragment>
            )}
            name="state"
          />
        </View>
        <View className="flex-1">
          {/* <ControlComponent
            control={control}
            errors={errors}
            fieldName="zipcode"
            placeholder="Zip Code"
          /> */}
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                {/* <DropdownComponent
                bgColor={"#fff"}
                placeholder="Select City"
                value={value}
                dataList={cityList}
                onChange={(item) => onChange(item.value)}
              /> */}
                <View className="bg-bgWhite mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
                  <MaskedTextInput
                    value={value}
                    mask="9999"
                    placeholder="0000"
                    onChangeText={(value) => {
                      onChange(value);
                    }}
                    keyboardType="numeric"
                    style={{ height: 55 }}
                  />
                </View>
                {errors.zipcode && (
                  <Text className="text-errorColor">
                    {errors.zipcode.message}
                  </Text>
                )}
              </Fragment>
            )}
            name="zipcode"
            rules={{
              required: "Zip Code is required",
            }}
          />
        </View>
      </View>
      <View className="mt-5">
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <Fragment>
              <DropdownComponent
                placeholder="Country"
                value={value}
                dataList={countryList}
                onChange={(item) => onChange(item.value)}
              />
              {errors.country && (
                <Text className="text-errorColor">
                  {errors.country.message}
                </Text>
              )}
            </Fragment>
          )}
          name="country"
        />
      </View>
    </View>
  );
};

export default forwardRef(AddContact);
