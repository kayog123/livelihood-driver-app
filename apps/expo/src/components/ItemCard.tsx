import React, { useState } from "react";
import {
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  type ImageSourcePropType,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Stack, useRouter } from "expo-router";
import { MotiView } from "moti";
import { Skeleton } from "moti/skeleton";
import { useDispatch } from "react-redux";

import { TripList } from "~/helper/type";
import { setSelectedPlace } from "~/redux/reducer/placesSlice";

type ItemCard = {
  name: string;
  location: string;
  image: ImageSourcePropType;
  selected?: boolean;
  id: number;
  data: TripList;
  grid?: boolean;
  tags?: string;
  desc?: Array<string>;
  width?: string;
  paddingX?: string;
};

const ItemCard = ({
  name,
  location,
  selected = false,
  image,
  id,
  data,
  tags = "boat",
  grid = false,
  desc = [],
  paddingX = "px-5",
}: ItemCard) => {
  const navigation = useRouter();
  const dispatch = useDispatch();
  return (
    <Pressable
      onPress={() => {
        dispatch(setSelectedPlace(data));
        navigation.push({
          pathname: "/boat/triplist",
          params: { selectFrom: name, tagsNav: tags },
        });
      }}
      className={`h-${!grid ? "56" : "56"} flex-1`}
    >
      <View className="overflow-hidden rounded-xl">
        <ImageBackground
          source={image}
          resizeMode="cover"
          className="flex h-full items-center justify-center"
        >
          {/* <View className="absolute top-0 flex w-full flex-row justify-between p-2">
            <View className="flex flex-row space-x-2">
              <Ionicons name="heart" size={20} color="#DB005B" />
              <Text className="text-bgWhite">5.5k people</Text>
            </View>
            <View className="flex flex-row items-center justify-center space-x-1">
              <Text className="text-bgWhite">5.0</Text>
              <Ionicons name="star" size={20} color="#FFE569" />
            </View>
          </View> */}
          <View
            className={`absolute bottom-0 w-full items-center bg-[#1c1e216e] p-2 ${paddingX}`}
          >
            <Text
              className={`text-bgWhite font-poppinsReg rounded-full px-4 text-center
              text-${grid ? "md" : "lg"} shadow-lg`}
              numberOfLines={2}
            >
              {name}
            </Text>
            <Text
              className="text-bgWhiteShadow font-poppinsReg text-center text-xs"
              numberOfLines={2}
            >
              {location}
            </Text>
          </View>

          {selected && (
            <View className="absolute left-0 right-0 ">
              <View className=" bg-primary border-bgWhite mx-auto flex flex-row items-center justify-center rounded-full border-4">
                <Ionicons name="checkmark-outline" size={40} color="#fff" />
              </View>
            </View>
          )}
        </ImageBackground>
      </View>
    </Pressable>
  );
};

export default ItemCard;

const styles = StyleSheet.create({
  textShadow: {
    textShadowColor: "#e3e6e9c2",
    textShadowOffset: { width: -1, height: 0.2 },
    textShadowRadius: 1,
  },
});
