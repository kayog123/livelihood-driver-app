import { Dimensions, Pressable, StyleSheet, View } from "react-native";
import { MotiView } from "moti";
import { Skeleton } from "moti/skeleton";

type SkeletonProps = {
  dark?: boolean;
};
const ItemCardSkeleton = ({ dark = false }: SkeletonProps) => {
  const colorMode = dark ? "dark" : "light";

  return (
    <View>
      <View className="flex w-full flex-row flex-wrap justify-between gap-x-2 gap-y-4">
        {[1, 2, 3, 4, 5, 6].map((items: any, index: number) => (
          <View className="h-48 w-[46%]" key={index}>
            <MotiView
              transition={{
                type: "timing",
              }}
              animate={{ backgroundColor: dark ? "#000000" : "#ffffff" }}
            >
              <Skeleton
                colorMode={colorMode}
                width={"100%"}
                height={"100%"}
                //width={Dimensions.get("screen").width * 0.4}
              />
            </MotiView>
          </View>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  boxContainer: {
    display: "flex",
    flexDirection: "row",
    margin: 5,
  },
});
export default ItemCardSkeleton;
