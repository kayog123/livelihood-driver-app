import { type ReactNode } from "react";
import { MotiView } from "moti";

const PopView = ({
  children,
  delay = 1000,
}: {
  children: ReactNode;
  delay?: number;
}) => {
  return (
    <MotiView
      from={{ scale: 0, opacity: 0 }}
      animate={{ scale: 1, opacity: 1 }}
      transition={{
        type: "timing",
        duration: 800,
        scale: {
          type: "spring",
          delay: delay,
        },
      }}
    >
      {children}
    </MotiView>
  );
};

export default PopView;
