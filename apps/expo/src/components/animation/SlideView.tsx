import { type ReactNode } from "react";
import { Dimensions } from "react-native";
import { MotiView } from "moti";

const SlideView = ({
  children,
  delay = 1000,
  fromStart = "left",
  duration = 1000,
}: {
  children: ReactNode;
  delay?: number;
  fromStart?: "left" | "right";
  duration?: number;
}) => {
  const { width, height } = Dimensions.get("screen");

  const initialValue: {
    translateX?: number;
    opacity?: number;
    translateY?: number;
  } = { opacity: 0 };

  let fromPointX = initialValue;
  let endPointX = initialValue;
  const fromPointY = initialValue;
  const endPointY = initialValue;

  if (fromStart == "left") {
    fromPointX = {
      translateX: -300,
      opacity: 0.5,
    };
    endPointX = { translateX: 0, opacity: 1 };
  }

  if (fromStart == "right") {
    fromPointX = {
      translateX: width,
      opacity: 0.5,
    };
    endPointX = { translateX: 0, opacity: 1 };
  }

  return (
    <MotiView
      from={fromStart == "left" || "right" ? fromPointX : fromPointY}
      animate={fromStart == "left" || "right" ? endPointX : endPointY}
      transition={{
        type: "timing",
        duration: duration,
        scale: {
          type: "spring",
          delay: delay,
        },
      }}
    >
      {children}
    </MotiView>
  );
};

export default SlideView;
