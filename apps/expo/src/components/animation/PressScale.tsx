import { type ReactNode } from "react";
import { Pressable } from "react-native";
import { MotiView, useDynamicAnimation } from "moti";

const PressScale = ({
  children,
  onPress,
}: {
  children: ReactNode;
  onPress?: () => void;
}) => {
  const animation = useDynamicAnimation(() => ({
    scale: 1,
  }));
  return (
    <Pressable
      onPressIn={() => {
        animation.animateTo({
          scale: 0.7,
        });
      }}
      onPressOut={() => {
        animation.animateTo((current) => ({
          ...current,
          scale: 1,
        }));
      }}
      onPress={onPress}
    >
      <MotiView state={animation}>{children}</MotiView>
    </Pressable>
  );
};

export default PressScale;
