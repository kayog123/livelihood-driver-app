import { Fragment } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useRouter } from "expo-router";

const DialogBox = ({
  title = " Congratulations!",
  messageSub = " Thank you for scheduling a trip with us. We hope that you enjoy your trip.",
  additonalInfo = {},
}: {
  title?: string;
  messageSub?: string;
  additonalInfo?: any;
}) => {
  const navigation = useRouter();
  return (
    <ScrollView className="absolute z-50 flex w-full ">
      <View className="flex min-h-screen w-full items-center justify-center bg-[#313030db]">
        <View className="bg-primary border-bgWhite bottom-[-25] z-50 rounded-full border-2 p-3">
          <Ionicons name="checkmark-done-outline" size={24} color="#fff" />
        </View>
        <View className="bg-bgWhite w-[80%] space-y-6 rounded-md p-8 pb-16">
          <Text className="text-textColorDark font-Poppins_800ExtraBold text-center text-xl">
            {title}
          </Text>
          <Text className="text-textColorDark font-poppinsReg text-center">
            {messageSub}
          </Text>

          {Object.keys(additonalInfo).length > 0 && (
            <View className=" bg-bgWhiteShadow  flex-wrap rounded-md p-3">
              {Object.keys(additonalInfo as any).map(
                (result: string, index: number) => (
                  <View className="w-full flex-wrap " key={index}>
                    <View className=" w-[100%]">
                      <Text className="font-Poppins_700Bold text-textColorDark flex-wrap">
                        {result}:{" "}
                        <Text className="font-poppinsReg text-textColorDark ">
                          {additonalInfo[result]}
                        </Text>
                      </Text>
                    </View>
                  </View>
                ),
              )}
            </View>
          )}
        </View>
        <View className="top-[-25] flex flex-row space-x-2">
          <TouchableOpacity
            onPress={() => {
              navigation.push("/");
            }}
            className="bg-textColorDark border-bgWhite flex flex-row items-center justify-center space-x-2 rounded-full border-2 px-5 py-3"
          >
            <Ionicons name="return-up-back-outline" size={18} color="#fff" />
            <Text className="font-poppinsReg text-bgWhite">Home</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.push("/");
            }}
            className="bg-primary border-bgWhite flex flex-row items-center justify-center space-x-2 rounded-full border-2 px-5 py-3"
          >
            <Ionicons name="book-outline" size={18} color="#fff" />
            <Text className="font-poppinsReg text-bgWhite">Details</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default DialogBox;
