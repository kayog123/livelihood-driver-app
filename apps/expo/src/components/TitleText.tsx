import { Text, View } from "react-native";

const TitleText = ({
  title,
  textColor = "textColorDark",
  bgColor = "",
  textSize = "text-md",
}: {
  title: string;
  textColor?: string;
  bgColor?: string;
  textSize?: string;
}) => {
  return (
    <View
      className={`mt-5 flex flex-row items-center ${bgColor} space-x-2 rounded-md`}
    >
      <View
        className={`border-${textColor}  h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted`}
      ></View>
      <Text className={`text-${textColor} font-poppinsReg ${textSize} my-4`}>
        {title}
      </Text>
      <View
        className={`border-${textColor}  h-1 flex-grow border-spacing-2 border-0 border-t-2 border-dotted`}
      ></View>
    </View>
  );
};

export default TitleText;
