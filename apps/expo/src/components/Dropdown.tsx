import { View, StyleSheet } from 'react-native';
import { useState } from 'react';
import { Dropdown } from 'react-native-element-dropdown';
type MyObjectType = {
    label: string;
    value: string;
};

type DropdownProps = {
    placeholder?: string, 
    dataList: MyObjectType[],
    value: string,
    onChange: (value: MyObjectType) => void,
    bgColor?: string
}
  
const DropdownComponent = ({
    placeholder = "Select items", 
    dataList = [ { label: 'Item 1', value: '1' }],
    value,
    onChange,
    bgColor
}: DropdownProps) => {  
    const [isFocus, setIsFocus] = useState(false);
     
    return (
    <View className={`bg-${ bgColor ? `[${bgColor}]`: 'bgWhite'} w-full rounded-xl shadow-md h-14`}> 
      <Dropdown 
            style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
            // placeholderStyle={styles.placeholderStyle}
            // selectedTextStyle={styles.selectedTextStyle}
            // inputSearchStyle={styles.inputSearchStyle}
            // iconStyle={styles.iconStyle}
            data={dataList}
            search
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={ placeholder}
            searchPlaceholder="Search..."
            value={value} 
            onChange={item => {
                onChange(item) 
            }} 
          />
    </View>)
  }

  export default DropdownComponent;

  
const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      padding: 16,
    },
    dropdown: {
      height: 50,
      borderColor: '#fff',
      borderWidth: 0.5,
      borderRadius: 8,
      paddingHorizontal: 8,
    },
  });