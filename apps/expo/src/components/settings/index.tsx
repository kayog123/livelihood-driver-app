import React from "react";
import { ScrollView } from "react-native";

import Card, { SkeletonCard } from "../../components/Card";
import Layout from "../../components/Layout";

const Settings = () => {
  //   const navigation = useRouter();
  return (
    <Layout>
      <ScrollView className="flex  p-5">
        <SkeletonCard />
      </ScrollView>
    </Layout>
  );
};

export default Settings;
