import React, { type ReactNode } from "react";
import { Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";

type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
const BoxWithIcon = ({
  children,
  icons,
}: {
  children: ReactNode;
  icons: IonicIonsname;
}) => {
  return (
    <View className="bg-bgWhite m-5 rounded-md border-b-2 border-[#DDE6ED]">
      <View className="flex-row">
        <View className=" bg-bgWhiteShadow h-5 w-[42%]  rounded-br-full"></View>
        <View className=" border-bgWhiteShadow w-full flex-1 flex-grow items-center justify-center">
          <View className="bg-primary absolute  rounded-full p-5">
            <Ionicons name={`${icons}`} size={20} color="#fff" />
          </View>
        </View>
        <View className=" bg-bgWhiteShadow h-5 w-[42%]  rounded-bl-full"></View>
      </View>
      <View>{children}</View>
    </View>
  );
};

export default BoxWithIcon;
