import React, {
  Fragment,
  useEffect,
  useRef,
  useState,
  type ReactNode,
} from "react";
import {
  Animated,
  Easing,
  Pressable,
  ScrollView,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Stack, useRouter } from "expo-router";
import { useAuth, useUser } from "@clerk/clerk-expo";
import { AntDesign } from "@expo/vector-icons";
import { MotiView } from "moti";

import {
  primaryColor,
  primaryColorShadow,
  secondaryColor,
} from "../helper/const";
import PopView from "./animation/PopView";
import PressScale from "./animation/PressScale";

const AnimatedIonicons = Animated.createAnimatedComponent(Ionicons);

type HeaderProps = {
  title?: string;
};

const headerRight = () => {
  const navigation = useRouter();
  const { isLoaded, isSignedIn, user } = useUser();

  if (!isLoaded || !isSignedIn) {
    return null;
  }
  return (
    <View className="flex flex-row items-center justify-center gap-2 pr-5">
      {/* <Text className="font-poppinsReg text-[#fff]">
        Hello, {user.firstName}!
      </Text> */}
      {/* <Pressable
        onPress={() => {
          navigation.push("/auth/login");
        }}
      >
        <Text className="text-white">Login / Signup</Text>
      </Pressable> */}
      {/* <AntDesign name="bells" size={24} color="white" /> */}
    </View>
  );
};

export const HeaderBar = ({ title }: HeaderProps) => {
  return (
    <Fragment>
      <Stack.Screen
        options={{
          //headerShown: false,
          headerTitle: () => (
            <Text className="font-poppinsReg text-[#fff]">{title}</Text>
          ),
          headerStyle: {
            backgroundColor: primaryColor,
          },
          headerTintColor: "#fff",
          headerShadowVisible: false,
          headerRight: headerRight,
        }}
      />
    </Fragment>
  );
};

type LocationFormProps = {
  icon: ReactNode;
  value: string;
  onChangeText: (e: string) => void;
};

const LocationForm = ({ icon, onChangeText, value }: LocationFormProps) => {
  const navigation = useRouter();

  return (
    <View className="bg-primaryColorShadow mb-2 flex w-full flex-row items-center  justify-items-center  rounded-lg p-1">
      <PopView>
        <PressScale
          onPress={() => {
            navigation.push("/maps");
          }}
        >
          <View className="bg-secondaryLight rounded-lg p-3">{icon}</View>
        </PressScale>
      </PopView>
      <TextInput
        placeholder="Where do you want to go?"
        className="rounded-xxl bg-primaryColorShadow h-12  w-full flex-auto   rounded-lg p-3 text-[#fff]"
        placeholderTextColor="#FFF"
        value={value}
        onChangeText={onChangeText}
      />
      <View className={`rounded-full  p-3`}>
        <Ionicons color="white" name="locate-outline" size={24} />
      </View>
    </View>
  );
};

const Header = ({ title }: HeaderProps) => {
  const navigation = useRouter();
  const [locationFrom, setLocationFrom] = useState<string>(
    "Sta. Cruz, Calape, Bohol, 6328",
  );
  const [locationTo, setLocationTo] = useState<string>("");
  const animatedValue = useRef(new Animated.Value(0)).current;
  const spinValue = useRef(new Animated.Value(0)).current;

  const animation = Animated.loop(
    Animated.sequence([
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]),
  );

  useEffect(() => {
    animation.start();
  }, [animation]);

  const translateY = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 5],
  });

  useEffect(() => {
    Animated.loop(
      Animated.timing(spinValue, {
        toValue: 1,
        duration: 2000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }, []);

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  return (
    <View>
      <HeaderBar title={title} /> 
    </View>
  );
};

export default Header;
