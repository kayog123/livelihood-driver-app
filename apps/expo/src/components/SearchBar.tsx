import React, { useState } from "react";
import { ActivityIndicator, TextInput, View } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import DateTimePicker from "@react-native-community/datetimepicker";
import LottieView from "lottie-react-native";

type SearchProps = {
  searchValue: string;
  setSearch: (value: string) => void;
  placeholder?: string;
  loading?: boolean;
};

const SearchBar = ({
  searchValue,
  setSearch,
  placeholder = "Where do you want to go?",
  loading = false,
}: SearchProps) => {
  return (
    <View className="bg-bgWhiteShadow border-textColorShadow flex w-full flex-row  items-center  justify-items-center rounded-lg p-1">
      <TextInput
        placeholder={placeholder}
        className="rounded-xxl bg-bgWhiteShadow text-textColorDark  h-12 w-full flex-auto rounded-lg p-3"
        placeholderTextColor="#394867"
        value={searchValue}
        onChangeText={setSearch}
      />
      <View className={`rounded-full  p-2`}>
        {loading ? (
          <ActivityIndicator />
        ) : (
          <Ionicons color="#009387" name="search-outline" size={24} />
        )}
      </View>
    </View>
  );
};

export default SearchBar;
