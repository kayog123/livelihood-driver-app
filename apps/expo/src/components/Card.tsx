import React, { type ReactNode } from "react";
import { Text, View } from "react-native";

import { primaryColor, secondaryColor } from "../helper/const";

type Props = {
  title?: string;
  children: ReactNode;
  className?: string;
  boxColor?: string;
};

const Card: React.FC<Props> = ({
  children,
  className,
  boxColor = secondaryColor,
}) => {
  return (
    <View
      //style={{ elevation: 5 }}
      className={`bg-primary mb-5 flex w-full animate-pulse flex-row flex-wrap   rounded-lg p-5 shadow-lg`}
    >
      {children}
    </View>
  );
};

export const SkeletonCard = () => {
  return (
    <View className="mb-5 flex max-w-sm animate-pulse flex-row gap-5">
      <View className="m-2">
        <View className="mb-4 h-2.5 w-48 rounded-full bg-gray-200 dark:bg-gray-700"></View>
        <View className="mb-2.5 h-2 max-w-[360px] rounded-full bg-gray-200 dark:bg-gray-700"></View>
        <View className="mb-2.5 h-2 rounded-full bg-gray-200 dark:bg-gray-700"></View>
        <View className="mb-2.5 h-2 max-w-[330px] rounded-full bg-gray-200 dark:bg-gray-700"></View>
        <View className="mb-2.5 h-2 max-w-[300px] rounded-full bg-gray-200 dark:bg-gray-700"></View>
        <View className="mb-2.5 h-2 max-w-[360px] rounded-full bg-gray-200 dark:bg-gray-700"></View>
      </View>
      <View className="flex h-20 w-full items-center justify-center rounded bg-gray-300 dark:bg-gray-700 sm:w-96"></View>
      <Text className="sr-only">Loading...</Text>
    </View>
  );
};

export default Card;
