import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { PlaceList, TripList } from "~/helper/type";
import type { RootState } from "../store";

export interface PlaceState {
  selectedPlace: TripList;
  placesToVisit: PlaceList[];
  placesToVisitCount: number;
}

const initialState: PlaceState = {
  selectedPlace: {
    id: 0,
    name: "",
    description: [],
    places: [],
    fixed: false,
    image: "",
  },
  placesToVisit: [],
  placesToVisitCount: 0,
};

const placeSlice = createSlice({
  name: "places",
  initialState,
  reducers: {
    setSelectedPlace(state, action: PayloadAction<TripList>) {
      state.selectedPlace = action.payload;
    },
    setPlacesToVisit(state, action: PayloadAction<PlaceList[]>) {
      state.placesToVisit = action.payload;
    },
    updatePlaceTags(state, action: PayloadAction<PlaceList[]>) {
      state.placesToVisit = action.payload;
    },
    removePlacesToVisitItem(state, action: PayloadAction<TripList>) {
      state.placesToVisit = state.placesToVisit.filter((item) => {
        if (item.id != action.payload.id) {
          return item;
        }
      });
    },
  },
});

export const {
  setSelectedPlace,
  setPlacesToVisit,
  removePlacesToVisitItem,
  updatePlaceTags,
} = placeSlice.actions;
export default placeSlice.reducer;
