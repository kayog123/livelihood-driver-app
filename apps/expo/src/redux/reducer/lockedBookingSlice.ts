import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { LockedBookingProps, PlaceList, TripList } from "~/helper/type";
import type { RootState } from "../store";

export interface BusinessPropsState {
  lockedBooking: LockedBookingProps | null;
}

const initialState: BusinessPropsState = {
  lockedBooking: null,
  // {
  //   id: 0,
  //   userId: "",
  //   ownerId: "",
  //   status: "",
  //   transpo_type: "",
  //   transpo_rate: 0,
  //   description: "",
  //   firstname: "",
  //   lastname: "",
  //   phone: "",
  //   profile: "",
  // },
};

const lockedBookingSlice = createSlice({
  name: "lockedBooking",
  initialState,
  reducers: {
    setlockedBooking(state, action: PayloadAction<LockedBookingProps>) {
      state.lockedBooking = action.payload;
    },
  },
});

export const { setlockedBooking } = lockedBookingSlice.actions;
export default lockedBookingSlice.reducer;
