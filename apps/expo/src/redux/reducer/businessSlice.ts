import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { BusinessProps, PlaceList, TripList } from "~/helper/type";
import type { RootState } from "../store";

export interface BusinessPropsState {
  selectedBusiness: BusinessProps;
}

const initialState: BusinessPropsState = {
  selectedBusiness: {
    id: 0,
    name: "",
    address: "",
    image: "",
    lat: 0,
    long: 0,
  },
};

const businessSlice = createSlice({
  name: "places",
  initialState,
  reducers: {
    setSelectedBusiness(state, action: PayloadAction<BusinessProps>) {
      console.log(action.payload);
      state.selectedBusiness = action.payload;
    },
  },
});

export const { setSelectedBusiness } = businessSlice.actions;
export default businessSlice.reducer;
