import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type { RootState } from "../store";

export interface MessagePropsState {
  countMessage: number;
  selectedChatRoom: null | number;
}

const initialState: MessagePropsState = {
  countMessage: 0,
  selectedChatRoom: null,
};

const messagesSlice = createSlice({
  name: "message",
  initialState,
  reducers: {
    setMessageCount(state, action: PayloadAction<number>) {
      state.countMessage = action.payload;
    },
    incrementMessageCount(state) {
      state.countMessage = state.countMessage + 1;
    },
    setChatRoom(state, action: PayloadAction<number>) {
      state.selectedChatRoom = action.payload;
    },
  },
});

export const { setMessageCount, setChatRoom, incrementMessageCount } =
  messagesSlice.actions;
export default messagesSlice.reducer;
