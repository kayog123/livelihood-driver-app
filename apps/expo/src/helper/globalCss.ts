import { Platform, StyleSheet } from "react-native";

export const globalCss = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});
