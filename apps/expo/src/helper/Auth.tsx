import { ReactElement, useEffect, type ComponentType } from "react";
import { Stack, useRouter } from "expo-router";

const Auth = (Component: React.ComponentType) => {
  const navigation = useRouter();

  return <Component />;
};

export default Auth;
