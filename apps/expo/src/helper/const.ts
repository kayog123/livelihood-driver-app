import { NativeModules, Platform } from "react-native";

import { type BoatTranpoProps, type LandTranpoProps } from "./type";

const { StatusBarManager } = NativeModules;
export const primaryColor = "#29990f"; //"#f4511e";
export const primaryColorShadow = "#22A699"; //"#f4511e";
export const secondaryColor = "#f05d2a"; //"#2e1398";
export const secondaryColorLight = "#f05d2a";

export const textColorDark = "#05375a";
export const headerSetting = {
  headerStyle: {
    backgroundColor: primaryColor,
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold",
  },
  headerShadowVisible: false,
};
export const STATUSBAR_HEIGHT =
  Platform.OS === "ios" ? 20 : StatusBarManager.HEIGHT;

export const cityList = [
  {
    label: "Tagbilaran City",
    value: "Tagbilaran City",
  },
  // {
  //   label: "Calape",
  //   value: "Calape",
  // },
];

export const provinceList = [
  {
    label: "Bohol",
    value: "Bohol",
  },
];

export const countryList = [
  {
    label: "Philippines",
    value: "Philippines",
  },
];

export const relationList = [
  {
    label: "Brother",
    value: "Brother",
  },
  {
    label: "Sister",
    value: "Sister",
  },
  {
    label: "Father",
    value: "Father",
  },
  {
    label: "Mother",
    value: "Mother",
  },
  {
    label: "Other",
    value: "Other",
  },
];

export const LandTranpoType: LandTranpoProps[] = [
  {
    id: "car",
    service_name: "Sedan/Car",
    rate: 2800,
    capacity: [1, 4],
    details: [],
  },
  {
    id: "suv",
    service_name: "Private SUV",
    rate: 3200,
    capacity: [5, 7],
    details: [],
  },
  {
    id: "van",
    service_name: "Private Van",
    rate: 3500,
    capacity: [8, 15],
    details: [],
  },
];

export const BoatTranpoType: BoatTranpoProps[] = [
  {
    id: "boat_small",
    service_name: "BOAT SMALL",
    rate: 5000,
    capacity: [1, 12],
    details: [],
  },
  {
    id: "boat_medium",
    service_name: "BOAT MEDIUM",
    rate: 5000,
    capacity: [1, 15],
    details: [],
  },
  {
    id: "boat_large",
    service_name: "BOAT LARGE",
    rate: 5000,
    capacity: [1, 18],
    details: [],
  },
];

export const boatPrice = 4000;

export const boatPackageData = [
  {
    id: 1,
    name: "ISLAND HOPPING",
    description: [],
    places: [
      {
        id: 1,
        name: "BALICASAG ISLAND",
        description: ["100/head environment"],
        lat: 9.52049,
        long: 123.68504,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
        fee: 100,
        events: [
          {
            id: 1,
            event_name: "Dolphin Watching  ( Starts @ 6:30am)",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 2,
            event_name: "Swimming",
            event_lat: 1.12345,
            event_long: 1.54321,
            optional: true,
          },
          {
            id: 3,
            event_name: "Kayaking",
            event_lat: 1.12345,
            event_long: 1.54321,
            optional: true,
          },
        ],
      },
      {
        id: 2,
        name: "VIRGIN ISLAND",
        fee: 100,
        description: [
          "30/head environment fee for local, 100/head environment fee for foreign ",
        ],
        lat: 9.55601,
        long: 123.71742,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 3,
        name: "PADRE PIO CHURCH",
        description: [],
        lat: 9.5596994,
        long: 123.7174828,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
    ],
    image: "cabilaoisland.jpg",
    fixed: false,
  },
  {
    id: 2,
    name: "Scuba Diving",
    description: [],
    places: [
      {
        id: 1,
        name: "Balicasag Island",
        address: "Panglao, Bohol",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        optional: true,
        image: ["cabilaoisland.jpg"],
        events: [
          {
            id: 1,
            event_name: "SCUBA DIVING",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 1,
            event_name: "FREEDIVING",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 1,
            event_name: "TURTLE WATCHING",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 1,
            event_name: "FISH FEEDING (₱350 / HEAD)",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
        ],
      },
    ],
    image: "cabilaoisland.jpg",
    fixed: false,
  },
];

export const vansPackageData = [
  {
    id: 1,
    name: "PANGLAO LAND TOUR",
    description: [
      "🚗 Sedan/Car rate- P2,500 (1-4 pax)",
      "🚙 Private Suv Rate - P2,800 (5-7 pax)",
      "🚐 PRIVATE VAN- P3,000 (8-15 pax)",
      "",
      "INCLUSIONS;",
      "✔️Transpo",
      "✔️Gas",
      "✔️Driver",
    ],
    places: [
      {
        id: 1,
        name: "Panglao Church",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["panglao.jpg"],
        address: "Panglao, Bohol",
        optional: true,
        fee: 40,
      },
      {
        id: 2,
        name: "Panglao High Watch Tower",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 3,
        name: "Moadto Strip Mall",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
        fee: 80,
      },
      {
        id: 4,
        name: "Hinagdanan Cave",
        description: ["50/head for sightseeing P70  with swimming"],
        lat: 1.12345,
        long: 1.54321,
        fee: 50,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
        events: [
          {
            id: 1,
            event_name: "Swimming (Additional 20 pesos)",
            event_lat: 1.12345,
            event_long: 1.54321,
            fee: 20,
            optional: true,
          },
        ],
      },
      {
        id: 5,
        name: "Bohol Bee Farm",
        description: ["( Guest expense for Lunch or Dinner)"],
        lat: 1.12345,
        long: 1.54321,
        fee: 20,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 6,
        name: "SouthFarm Panglao",
        description: ["200/head"],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 7,
        name: "Sight Seeing Alona beacH",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 8,
        name: "Shell Museum",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
    ],
    image: "panglao.jpg",
    fixed: false,
  },
  {
    id: 2,
    name: "ANDA / CANDIJAY TOUR",
    description: [
      "🚗 Sedan/Car Rate- P4,700 group",
      "🚙 Suv rate - P5,000 group",
      "🚌 Van Rate - P5,500 group",
    ],
    places: [
      {
        id: 1,
        name: "Anda Beach",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 2,
        name: "Cadadapan Rice Terraces",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 3,
        name: "Cabagnow Cave",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 4,
        name: "Can-Umantad Falls",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
    ],
    image: "anislagspring.jpg",
    fixed: false,
  },
  {
    id: 3,
    name: "DANAO TOURS",
    description: [],
    places: [
      {
        id: 1,
        name: "DANAO SEA OF CLOUDS",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 2,
        name: "DANAO ADVENTURE PARK",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
        events: [
          {
            id: 1,
            event_name: "THE PLUNGE 700 peso ( weight limit 60kgs",
            event_lat: 1.12345,
            event_long: 1.54321,
            optional: true,
          },
          {
            id: 2,
            event_name: "ZIPLINE 350 peso",
            event_lat: 1.12345,
            event_long: 1.54321,
            optional: true,
          },
          {
            id: 3,
            event_name: "GIANT SWING Solo (500 pesos)",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 4,
            event_name: "GIANT SWING Tandem (800 pesos)",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
          {
            id: 5,
            event_name:
              "THE SKY RIDE (MAX OF 4 PAX PER RIDE, 250 peso per person)",
            event_lat: 1.12345,
            event_long: 1.54321,
          },
        ],
      },
    ],
    image: "barangay.jpg",
    fixed: false,
  },
  {
    id: 4,
    name: "COUNTRY SIDE TOUR",
    description: [],
    places: [
      {
        id: 1,
        name: "Chocolate Hills",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 2,
        name: "ATV Ride in Chocolate",
        description: ["P1,100 each for 1 hour with viewing deck"],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 3,
        name: "Tarsier & Butterfly Sanctuary",
        description: ["P1,100 each for 1 hour with viewing deck"],
        price: 120,
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 4,
        name: "Loboc Floating Restauran",
        description: ["P850 per hea"],
        price: 850,
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 5,
        name: "Man Made Forest",
        description: ["P850 per hea"],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 6,
        name: "Bohol Python",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 7,
        name: "Baclayon Church",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 8,
        name: "Shiphouse",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 9,
        name: "Souvenir",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 10,
        name: "HANGING BRIDGE",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
      {
        id: 11,
        name: "Blood Compact",
        description: [],
        lat: 1.12345,
        long: 1.54321,
        image: ["cabilaoisland.jpg"],
        address: "Panglao, Bohol",
      },
    ],
    image: "barangay.jpg",
    fixed: false,
  },
];

export enum TranspoType {
  VANS = "VANS",
  BOAT = "BOAT",
  MOTORCYCLE = "MOTORCYCLE",
  TRICYCLE = "TRICYCLE",
}

export enum Trip {
  LATLONG = "LATLONG",
  TRIP = "TRIP",
}

export enum TripStatus {
  PENDING = "PENDING",
  ACCEPTED = "ACCEPTED",
  CANCELLED = "CANCELLED",
  COMPLETED = "COMPLETED",
  PICKED_UP = "PICKED_UP",
  ON_THE_WAY = "ON_THE_WAY",
}

export const HERE_MAP_API_KEY = "ZId8y9ThVciA8sg7eEj1P-CWZnbb26MeO0K8VKO0LQ8";

export const FAIR = {
  MOTOR: 15,
  TRICYCLE: 15,
};

export const LIVELIHOOD_CATEGORY = [
  {
    title: "Category 1",
    desc: "This is a description of category 1",
  },
  {
    title: "Category 2",
    desc: "This is a description of category 2",
  },
  {
    title: "Category 3",
    desc: "This is a description of category 3",
  },
  {
    title: "Category 4",
    desc: "This is a description of category 4",
  },
  {
    title: "Category 5",
    desc: "This is a description of category 5",
  },
];

export const navSetting = {
  animationEnabled: true,
  swipeEnabled: true,
  tabBarShowLabel: true,
  lazy: true,
  tabBarPressColor: "#394867",
  tabBarActiveTintColor: "#ffff",
  tabBarIndicatorStyle: {
    borderBottomColor: primaryColor,
    borderBottomWidth: 4,
  },
  tabBarStyle: {
    backgroundColor: "#05375a",
    height: 80,
  },
};

export const messageWrapperStyle = {
  left: {
    backgroundColor: "#3e40422e",
    borderWidth: 0,
    borderColor: "#e6e6e6",
    borderRadius: 10,
    borderTopRightRadius: 0,
  },

  right: {
    backgroundColor: "#009387",
    borderWidth: 0,
    borderColor: "#e6e6e6",
    borderRadius: 10,
    borderTopStartRadius: 0,
  },
};

export const messageTextStyle = {
  left: {
    fontWeight: "500",
    fontSize: 18,
    color: "#111",
  },
  right: {
    fontWeight: "500",
  },
};

export const messageCustomTimeStyle = {
  left: {
    color: "#111",
    fontSize: 13,
  },
  right: {
    color: "#fff",
    fontSize: 13,
  },
};