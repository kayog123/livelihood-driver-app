export type PlaceList = {
  id: number;
  name: string;
  description: Array<string>;
  lat: number;
  long: number;
  image: Array<string>;
  address: string;
  events?: PlaceEvent[];
  show?: boolean;
  price?: number;
  optional?: boolean;
  fee?: number;
};
export type TripList = {
  id: number;
  name: string;
  description: Array<string>;
  places: PlaceList[];
  fixed: boolean;
  image: string;
};

export type PlaceEvent = {
  id: number;
  event_name: string;
  event_long: number;
  event_lat: number;
  fee?: number;
  show?: boolean;
  optional?: boolean;
};

export interface LandTranpoProps {
  id: string;
  service_name: string;
  rate: number;
  capacity: Array<number>;
  details: Array<string>;
}

export interface BoatTranpoProps {
  id: string;
  service_name: string;
  rate: number;
  capacity: Array<number>;
  details: Array<string>;
}

export type BusinessProps = {
  id: number;
  name: string;
  address: string;
  image: string;
  lat: number;
  long: number;
};

export type LockedBookingProps = {
  id: number;
  userId: string;
  ownerId: string;
  status: string;
  transpo_type: string;
  transpo_rate: number;
  description: string;
  firstname: string;
  lastname: string;
  phone: string;
  profile: string;
  pickup_lat: number;
  pickup_long: number;
  pickup_area_name: string;
  places: string;
} | null;

export type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};
