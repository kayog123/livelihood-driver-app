import React, { useEffect } from "react";
import Constants from "expo-constants";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { createWSClient, httpBatchLink, wsLink } from "@trpc/client";
import { createTRPCReact } from "@trpc/react-query";
import superjson from "superjson";

import { type AppRouter } from "@acme/api";

/**
 * A set of typesafe hooks for consuming your API.
 */
export const api = createTRPCReact<AppRouter>();
export { type RouterInputs, type RouterOutputs } from "@acme/api";
export const apiRemastered = () => {
  return createTRPCReact<AppRouter>();
};
/**
 * Extend this function when going to production by
 * setting the baseUrl to your production API URL.
 */
const getBaseUrl = () => {
  /**
   * Gets the IP address of your host-machine. If it cannot automatically find it,
   * you'll have to manually set it. NOTE: Port 3000 should work for most but confirm
   * you don't have anything else running on it, or you'd have to change it.
   *
   * **NOTE**: This is only for development. In production, you'll want to set the
   * baseUrl to your production API URL.
   */
  const debuggerHost = Constants.expoConfig?.hostUri;
  const localhost = debuggerHost?.split(":")[0];
  if (!localhost) {
    return "https://bt-portal.vercel.app";
    throw new Error(
      "Failed to get localhost. Please point to your production server.",
    );
  }
  return `http://${localhost}:3000`;
};

const url = `${getBaseUrl()}/api/trpc`;
function getEndingLink() {
  if (typeof window === "object") {
    return httpBatchLink({
      url,
    });
  }

  const client = createWSClient({
    url:
      process.env.NEXT_PUBLIC_WS_URL ||
      "wss://1266-49-145-170-227.ngrok-free.app/", //"ws://localhost:3001",
  });

  return wsLink<AppRouter>({
    client,
  });
}

/**
 * A wrapper for your app that provides the TRPC context.
 * Use only in _app.tsx
 */

const getApiClient = () => {
  return api.createClient({
    transformer: superjson,
    links: [getEndingLink()],
  });
};

export const TRPCProvider: React.FC<{
  children: React.ReactNode;
  renderer?: boolean;
}> = ({ children, renderer }) => {
  const [queryClient] = React.useState(() => new QueryClient());
  let trpcClient = getApiClient();
  useEffect(() => {
    trpcClient = getApiClient();
  }, [renderer]);

  return (
    <api.Provider client={trpcClient} queryClient={queryClient}>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </api.Provider>
  );
};
