import Constants from "expo-constants";
import {
  Pusher,
  PusherChannel,
  PusherEvent,
  PusherMember,
} from "@pusher/pusher-websocket-react-native";

const pusher = Pusher.getInstance();

export const connectPusher = async () => {
  try {
    // const pusherKey = Constants.expoConfig?.extra?.pusher.key;
    // const pusherCluster = Constants.expoConfig?.extra?.pusher.cluster;
    await pusher.init({
      apiKey: "f38fb6990eb30eddfb07",
      cluster: "ap1",
      onSubscriptionSucceeded,
      onSubscriptionError,
      onSubscriptionCount,
      onError,
      onMemberAdded,
      onMemberRemoved,
      onConnectionStateChange,
    });

    await pusher.connect();
  } catch (e) {
    console.log("ERROR: " + e);
  }
};

export const subscribeChatRoom = async (
  channelName: string,
  onEvent: (event: PusherEvent) => void,
) => {
  try {
    const roomChannel = await pusher.subscribe({
      channelName,
      onEvent,
      onSubscriptionSucceeded(data) {},
    });
    // console.log("Succeed channel connection: " + roomChannel.me);
  } catch (e) {
    console.log("ERROR: " + e);
  }
};

export const unSubscribeChannel = async (channelName: string) => {
  await pusher.unsubscribe({ channelName });
};
export const disconnectPusher = async () => {
  return await pusher.disconnect();
};

export const getChannel = (channelName: string) => {
  return pusher.getChannel(channelName);
};

const onError = (message: string, code: Number, error: any) => {
  console.log(`onError: ${message} code: ${code} exception: ${error}`);
};
function onConnectionStateChange(currentState: string, previousState: string) {
  //setConnectionStatus(currentState);
  console.log(`Connection server: ${currentState}`);
}
const onSubscriptionSucceeded = (data: any) => {
  const channel: PusherChannel | undefined = pusher.getChannel(
    data.channelName,
  );
  if (!channel) {
    return;
  }
  console.log(`Me: ${JSON.stringify(pusher.getChannel(data.channelName))}`);
};
const onSubscriptionCount = (
  channelName: string,
  subscriptionCount: Number,
) => {
  console.log(
    `onSubscriptionCount: ${subscriptionCount}, channelName: ${channelName}`,
  );
};
const onSubscriptionError = (channelName: string, message: string, e: any) => {
  console.log(
    `onSubscriptionError: ${message}, channelName: ${channelName} e: ${e}`,
  );
};
const onMemberAdded = (channelName: string, member: PusherMember) => {
  console.log(`onMemberAdded: ${channelName} user: ${member}`);
  const channel: PusherChannel | undefined = pusher.getChannel(channelName);

  if (!channel) {
    return;
  }

  //onChangeMembers([...channel.members.values()]);
};
const onMemberRemoved = (channelName: string, member: PusherMember) => {
  console.log(`onMemberRemoved: ${channelName} user: ${member}`);
  const channel: PusherChannel | undefined = pusher.getChannel(channelName);

  if (!channel) {
    return;
  }

  //onChangeMembers([...channel.members.values()]);
};
