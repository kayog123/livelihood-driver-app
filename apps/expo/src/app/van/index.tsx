import { useEffect, useState } from "react";
import {
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Stack, useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";

import { primaryColor, vansPackageData } from "~/helper/const";
import { TripList } from "~/helper/type";
import useDebounce from "~/hooks/useDebounce";
import { RootState } from "~/redux/store";
import HeaderScreen from "../../components/HeaderScreen";
import ItemCard from "../../components/ItemCard";

const tags = "vans";

const AllPlaces = () => {
  const [data, setData] = useState<TripList[]>(vansPackageData);
  const [grid, setGrid] = useState<boolean>(false);

  return (
    <View className="mt-10">
      <View className="flex flex-row items-center justify-between px-5">
        <View className="flex flex-row items-center justify-center">
          <Ionicons name="location" size={30} color="#05375a" />
          <Text className="font-Poppins_700Bold text-textColorDark px-2 text-left text-xl">
            Trip List
          </Text>
        </View>
        <View className="flex flex-row items-center justify-center space-x-2">
          <TouchableOpacity
            className={`bg-${!grid ? "primary" : "bgWhite"} rounded-sm p-1`}
            onPress={() => {
              setGrid(false);
            }}
          >
            <Ionicons
              name="list"
              size={30}
              color={!grid ? "#fff" : "#05375a"}
            />
          </TouchableOpacity>
          <TouchableOpacity
            className={`bg-${grid ? "primary" : "bgWhite"} rounded-sm p-1`}
            onPress={() => {
              setGrid(true);
            }}
          >
            <Ionicons name="grid" size={30} color={grid ? "#fff" : "#05375a"} />
          </TouchableOpacity>
        </View>
      </View>

      <View className="m-4 flex pb-10">
        <View className="flex  flex-row flex-wrap justify-between gap-y-4">
          {/* <View className="border-primary bg-bgWhite w-[48%] overflow-hidden rounded-xl border-4 p-1">
            <ItemCard
              name="Virgin Island"
              location="Panglao, Bohol"
              image={require("./../../../assets/places/virgin_island.jpg")}
            />
          </View> */}
          {data.map((result: TripList, index: number) => {
            return (
              <View
                className={`w-${
                  !grid ? "full" : "[48%]"
                } overflow-hidden rounded-xl`}
                key={`allplaces_${index}`}
              >
                <ItemCard
                  name={result.name}
                  location={
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                  }
                  image={{
                    uri:
                      "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
                      result.image,
                  }}
                  id={result.id}
                  data={result}
                  grid={grid}
                  tags="vans"
                />
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};

const VanScreen = () => {
  const currentTag = "vans";
  const navigation = useRouter();
  const [search, setSearch] = useState<string>("");
  const debounceSearch = useDebounce({ value: search, delay: 1000 });
  const [resultDebounce, setResultDebounce] = useState<string>("");
  const { placesToVisit } = useSelector((state: RootState) => state.places);
  const [modal, setModal] = useState<boolean>(false);
  useEffect(() => {
    setResultDebounce(debounceSearch);
  }, [debounceSearch]);

  return (
    <SafeAreaView style={styles.droidSafeArea}>
      <StatusBar
        animated={true}
        backgroundColor={primaryColor}
        // barStyle={statusBarStyle}
        // showHideTransition={statusBarTransition}
        // hidden={hidden}
      />
      <Stack.Screen options={{ headerShown: false }} />
      <ScrollView>
        <HeaderScreen search={search} setSearch={setSearch} />
        <AllPlaces />
      </ScrollView>
    </SafeAreaView>
  );
};

export default VanScreen;

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
