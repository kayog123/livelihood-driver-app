import { useEffect, useRef, useState } from "react";
import {
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  View,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useAuth } from "@clerk/clerk-expo";

import { api } from "~/utils/api";
import LoadingScreen from "~/components/Loading";
import ModalComponent from "~/components/ModalComponent";
import AddContact, { ContactComponentHandles } from "~/components/emergency/AddContact";
import { HeaderBar } from "../../components/Header";
import TopBarHeader from "~/components/TopBarHearder";

type ItemCardProps = {
  name: string;
  address: string;
  address2: string;
  phone: string;
};

const ItemCard = ({ name, address, address2, phone }: ItemCardProps) => {
  return (
    <View
      className="bg-bgWhite mb-3 flex w-full flex-row rounded-md border border-solid border-[#05375a7a] p-2"
      style={styles.shadow}
    >
      <View className="flex items-center justify-center p-1">
        <View className="bg-primary mr-3 flex h-16 w-16 items-center justify-center rounded-lg ">
          <Ionicons name="medkit-outline" size={38} color="#fff" />
        </View>
      </View>
      <View className="border-textColorDark flex-grow border-spacing-2 flex-wrap border-l-2 border-dotted pl-2">
        <Text className="text-primary font-poppinsReg text-xl font-semibold uppercase">
          {name}
        </Text>
        <Text className="text-textColorDark font-poppinsReg text-sm">
          {address}
        </Text>
        <Text
          className="text-textColorDark font-poppinsReg text-sm"
          numberOfLines={1}
        >
          {address2}
        </Text>
        <Text className="text-textColorDark font-poppinsReg text-sm">
          {phone}
        </Text>
      </View>
    </View>
  );
};

const Emergency = () => {
  type formType = "create" | "update";
  type FormValues = {
    id: number;
    firstname: string;
    lastname: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    zipcode: string;
    relationship: string;
    email: string;
    phone: string;
  };
  const [modalVisible, setModalVisible] = useState(false);
  const [openFrom, setOpenFrom] = useState<formType>("create");
  const [formData, setFormData] = useState<FormValues | {}>({});
  const childRef =  useRef<ContactComponentHandles | null>(null);
  const { isLoaded, userId } = useAuth();
  if (!isLoaded || !userId) return;
  const { data } = api.contact.byIdAllData.useQuery({ userId: userId });
  const utils = api.useContext();

  const { mutate, error } = api.contact.update.useMutation({
    async onSuccess() {
      await refetchData();
      showToast({ message: "Successfully updated" });
      setModalVisible(false);
      // await utils.user.byId.invalidate({ id: userId });
    },
  });

  function showToast({
    message = "Request sent successfully!",
  }: {
    message: string;
  }) {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  }

  const updateDataForm = (dataForm: FormValues) => {
    mutate({
      id: dataForm.id,
      firstname: dataForm.firstname,
      lastname: dataForm.lastname,
      email: dataForm.email,
      phone: dataForm.phone,
      address1: dataForm.address1,
      address2: dataForm.address2,
      state: dataForm.state,
      city: dataForm.city,
      country: dataForm.country,
      zipCode: dataForm.zipcode,
      relationship: dataForm.relationship,
    });
  };
 
  const refetchData = async () => {
    await utils.contact.byIdAllData.cancel();
    const prevData = utils.contact.byIdAllData.getData({ userId: userId });
    await utils.contact.byIdAllData.refetch({ userId: userId });
    return { prevData }; //return prev data in case saving fail, we can return it to original data
  };

  const updateDataList = async (data: object) => {
    //await utils.contact.byIdAllData.setData(data);
  };

  const removeContact = () => { 
      childRef.current?.removeContact(); 
  };

  const onSubmit = () => { 
      //access the child function for submition/confirmation of data
      openFrom == "create"
        ? childRef.current?.onSubmitValidate()
        : childRef.current?.onUpdateValidate(); 
  };

  return (
    <SafeAreaView>
      <ModalComponent
        children={
          <AddContact
            setModalVisible={setModalVisible}
            updateDataList={updateDataList}
            refetchData={refetchData}
            ref={childRef}
            userId={userId}
            openFrom={openFrom}
            formData={formData}
            updateDataForm={updateDataForm}
          />
        }
        modalVisible={modalVisible}
        openFrom={openFrom}
        setModalVisible={setModalVisible}
        onSubmit={onSubmit}
        removeContact={removeContact}
      />

      <ScrollView>
        <View className="m-5 ">
          {/* <HeaderBar title="Contacts" /> */}
          <TopBarHeader title="Contacts"/>
          <View className={`flex w-full items-center`}>
            <View className="mb-10 flex w-full flex-row items-center">
              <Text className="font-poppinsReg text-textColorDark mb-3 mt-3 flex-1 text-lg">
                Emergency Contact
              </Text>
              <View className="">
                <Pressable
                  onPress={() => {
                    if (data && isLoaded && data?.length < 3) {
                      setOpenFrom("create");
                      setFormData({});
                      setModalVisible(true);
                    } else {
                      ToastAndroid.show(
                        "Emergency contacts are limit to 3",
                        ToastAndroid.SHORT,
                      );
                    }
                  }}
                  className="bg-primary flex flex-1 flex-row place-items-end items-center justify-center rounded-lg p-1 pl-5 pr-5"
                >
                  <Ionicons color="white" name="add" size={28} />
                  <Text className="align-right text-bgWhite font-poppinsReg uppercase">
                    Add
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
          {(!data || !isLoaded) && <LoadingScreen text="Loading.." />}
          {data?.map((result: any, index: number) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                setModalVisible(true);
                setOpenFrom("update");
                setFormData(result);
              }}
            >
              <ItemCard
                name={`${result.firstname} ${result.lastname}`}
                address={`${result.address1} ${result.address2}, ${result.city}`}
                address2={`${result.state}, ${result.country} ${result.zipCode}`}
                phone={`${result.phone}`}
              />
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1.27,
    shadowRadius: 5.65,
    elevation: 6,
  },
});

export default Emergency;
