import { Fragment, useCallback, useEffect, useRef, useState } from "react";
import {
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { MaskedTextInput } from "react-native-mask-text";
//import DropDownPicker from 'react-native-dropdown-picker';
// import Autocomplete from 'react-native-autocomplete-input';
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";
import { useAuth } from "@clerk/clerk-expo";
import { Controller, useForm } from "react-hook-form";

import { api } from "~/utils/api";
import DropdownComponent from "~/components/Dropdown";
import LoadingScreen from "~/components/Loading";
//import AutoComplete from "react-native-expo-autocomplete-dropdown";
//import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown';
//import SelectDropdown from 'react-native-select-dropdown';
// import { Button } from "../../components";
// import { HeaderBar } from "../../components/Header";
import {
  cityList,
  countryList,
  primaryColor,
  provinceList,
} from "../../helper/const";
import DatePicker from "~/components/DatePicker";
import DateTimePicker, {
  type Event,
} from "@react-native-community/datetimepicker";
import TopBarHeader from "~/components/TopBarHearder";

type FormValues = {
  firstname: string;
  lastname: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  country: string;
  zipcode: string;
  phone: string;
  birthdate: string
};

const ControlComponent = ({
  control,
  errors,
  fieldName,
  rules = {
    required: fieldName + " is required",
  },
  placeholder,
}: any) => {
  return (
    <Controller
      control={control}
      render={({ field: { onChange, onBlur, value } }) => (
        <Fragment>
          <View className="bg-bgWhite mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
            <TextInput
              className=" h-14"
              placeholder={
                !placeholder
                  ? fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
                  : placeholder
              }
              value={value}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
            />
          </View>

          {errors[fieldName] && (
            <Text className="text-errorColor">{errors[fieldName].message}</Text>
          )}
        </Fragment>
      )}
      name={fieldName}
      //rules={rules}
    />
  );
};

const ProfileForm = ({
  userId,
  updateUserClerk,
}: {
  userId: string;
  updateUserClerk: (firstname: string, lastname: string) => void;
}) => {
  const { data } = api.user.byId.useQuery({ id: userId });
  const [submitLoading, setSubmitLoading] = useState<boolean>(false);
  const utils = api.useContext();

  const { mutate, error } = api.user.update.useMutation({
    async onSuccess() {
      showToast({ message: "Successfully updated" });
      setSubmitLoading(false);
      await utils.user.byId.invalidate({ id: userId });
    },
  });

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<FormValues>({
    defaultValues: {
      firstname: data?.firstname,
      lastname: data?.lastname,
      address1: data?.address1,
      address2: data?.address2,
      city: data?.city,
      state: data?.state || "Bohol", //province
      country: data?.country || "Philippines",
      zipcode: data?.zipCode,
      birthdate: data?.birthdate
    },
  });

  useEffect(() => {
    if (data === undefined) {
      return;
    }

    console.log(data);
    reset({
      ...data, //reset the data once fetch from prisma
      zipcode: data?.zipCode,
    });
  }, [data]);

  if (!data)
    return <Text className="text-textColorDark text-center">Loading...</Text>;

  const onSubmit = async (dataForm: FormValues) => {
    setSubmitLoading(true);

    // if (
    //   data?.firstname != dataForm.firstname ||
    //   data?.lastname != dataForm.lastname
    // ) {
    //   updateUserClerk(dataForm.firstname, dataForm.lastname);
    // }

    const bdayDefault = new Date();
    mutate({
      id: userId,
      firstname: dataForm.firstname,
      phone: dataForm.phone,
      lastname: dataForm.lastname,
      address1: dataForm.address1,
      address2: dataForm.address2,
      city: dataForm.city,
      state: dataForm.state,
      country: dataForm.country,
      zipCode: dataForm.zipcode,
      gender: "male",
      birthdate: dataForm.birthdate,
      role: "user",
    });
  };

  function showToast({
    message = "Request sent successfully!",
  }: {
    message: string;
  }) {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  }

  return (
    <SafeAreaView className="m-5 flex items-center">
      {submitLoading && <LoadingScreen text="Submitting..." />}
      <ProfileImgHeader profile={data.profile} />
      <Text className="font-poppinsReg text-textColorDark m-5 text-xl">
        Profile Information
      </Text>
      <View className="flex-column flex w-full pl-5 pr-5">
        <View className="flex flex-row  gap-x-2">
          <View className="flex-1">
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="firstname"
            />
          </View>
          <View className="flex-1">
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="lastname"
            />
          </View>
        </View>
        <View className="flex-1">
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                {/* <DropdownComponent
                bgColor={"#fff"}
                placeholder="Select City"
                value={value}
                dataList={cityList}
                onChange={(item) => onChange(item.value)}
              /> */}

                <View className="bg-bgWhite mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
                  <MaskedTextInput
                    value={value}
                    mask="+(63)-999-9999-9999"
                    placeholder="+(63)-000-0000-0000"
                    onChangeText={(value) => {
                      onChange(value);
                    }}
                    keyboardType="numeric"
                    style={{ height: 55 }}
                  />
                </View>
                {errors.phone && (
                  <Text className="text-errorColor">
                    {errors.phone.message}
                  </Text>
                )}
              </Fragment>
            )}
            name="phone"
            rules={{
              required: "Phone number is required",
            }}
          />
        </View> 
        
        <ControlComponent
          control={control}
          errors={errors}
          fieldName="address1"
          placeholder="Address 1"
        />
        <ControlComponent
          control={control}
          errors={errors}
          fieldName="address2"
          placeholder="Address 2"
        />
        <View className="mt-5">
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                <DropdownComponent
                  placeholder="Select City"
                  value={value}
                  dataList={cityList}
                  onChange={(item) => onChange(item.value)}
                />
                {errors.city && (
                  <Text className="text-errorColor">{errors.city.message}</Text>
                )}
              </Fragment>
            )}
            name="city"
          />
        </View>
        <View className="flex flex-row gap-x-2">
          <View className="mt-5 flex-1">
            <Controller
              control={control}
              render={({ field: { onChange, onBlur, value } }) => (
                <Fragment>
                  <DropdownComponent
                    placeholder="Province"
                    value={value}
                    dataList={provinceList}
                    onChange={(item) => onChange(item.value)}
                  />
                  {errors.state && (
                    <Text className="text-errorColor">
                      {errors.state.message}
                    </Text>
                  )}
                </Fragment>
              )}
              name="state"
            />
          </View>
          <View className="flex-1">
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="zipcode"
              placeholder="Zip Code"
            />
          </View>
        </View>
        <View className="mt-5">
          <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
              <Fragment>
                <DropdownComponent
                  placeholder="Country"
                  value={value}
                  dataList={countryList}
                  onChange={(item) => onChange(item.value)}
                />
                {errors.country && (
                  <Text className="text-errorColor">
                    {errors.country.message}
                  </Text>
                )}
              </Fragment>
            )}
            name="country"
          />
        </View>
      </View>
      <View className="mt-5 w-full">
        <TouchableOpacity
          //className={`"flex  hover:bg-blue-700" h-16 flex-row items-center justify-center gap-1 rounded bg-[${primaryColor}] font-bold text-white`}
          onPress={handleSubmit(onSubmit)}
        >
          <LinearGradient
            className="bg-textColorDark flex-column ml-10 mr-10 flex-row items-center justify-center rounded-lg p-5"
            colors={["#05375a", "#031e30d4"]}
          >
            <Text className="dark:text-bgWhite  text-bgWhite flex uppercase text-neutral-50">
              {submitLoading ? "Updating..." : "Update"}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const ProfileImgHeader = ({ profile }: { profile: string }) => {
  return (
    <View className="flex w-full p-5">
      <View className="bg-primary w-50 items-center justify-center rounded-lg p-5">
        <View className="bg-primaryColorShadow rounded-full p-3">
          <Image
            className="rounded-full p-5"
            source={
              profile ? { uri: profile } : require("./../../../assets/user.jpg")
            }
            style={{ width: 120, height: 120 }}
            alt=""
          />
        </View>
        <View className="absolute bottom-5 right-5 rounded-full bg-neutral-50 p-3 shadow-lg">
          {/* <Ionicons name="camera-outline" size={24} color={primaryColor} /> */}
        </View>
      </View>
    </View>
  );
};

const Profile = () => {
  const { isLoaded, userId, sessionId, getToken } = useAuth();

  if (!isLoaded || !userId) {
    return null;
  }
  const updateUserClerk = async (firstName: string, lastName: string) => {
    await fetch("https://bt-portal.vercel.app/api/clerk/user/updateById", {
      method: "POST",
      body: JSON.stringify({
        userId: userId,
        firstName: firstName,
        lastName: lastName,
      }),
    });
  };
  return (
    <SafeAreaView>
      <ScrollView>
        {/* <HeaderBar title="Profile" /> */}
        <TopBarHeader title="Profile"/>
        <ProfileForm userId={userId} updateUserClerk={updateUserClerk} />
      </ScrollView>
    </SafeAreaView>
  );
};
export default Profile;
