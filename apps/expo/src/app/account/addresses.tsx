import {
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Stack, useRouter } from "expo-router";
import { AntDesign, Ionicons } from "@expo/vector-icons";

import { primaryColor } from "../../helper/const";

const headerRight = () => {
  return (
    <View className="m-3">
      <TouchableOpacity className=" flex flex-row items-center justify-center">
        <AntDesign name="pluscircleo" size={18} color="#fff" />
        <Text className="text-bgWhite ml-2">Add</Text>
      </TouchableOpacity>
    </View>
  );
};

type ItemProps = {
  city: string;
  address: string;
};
const ItemList = ({ city, address }: ItemProps) => {
  const navigation = useRouter();

  return (
    <TouchableOpacity
      className="mb-1 ml-5 mr-5 mt-1 flex flex-row pt-2"
      onPress={() => {
        navigation.push("/maps");
      }}
    >
      <View className="bg-secondaryLight mr-4 mt-2 rounded-full p-3">
        <Ionicons name="ios-location" size={40} color="#fff" />
      </View>
      <View className="border-colorDarkOutline flex-grow justify-center border-t">
        <Text className="text-textColorDark text-xl font-semibold ">
          {city}
        </Text>
        <Text className="text-textColorDark ">{address}</Text>
      </View>
    </TouchableOpacity>
  );
};

const AddressList = () => {
  const DATA = [
    {
      id: 1,
      city: "Calape, Bohol",
      address: " Sitio Tugas, Baranggay Sta. Cruz",
    },
    {
      id: 2,
      city: "Tubigon, Bohol",
      address: " Sitio Tugas, Baranggay Sta. Cruz",
    },
    {
      id: 3,
      city: "Calape, Bohol",
      address: " Sitio Tugas, Baranggay Sta. Cruz",
    },
    {
      id: 4,
      city: "Calape, Bohol",
      address: " Sitio Tugas, Baranggay Sta. Cruz",
    },
  ];

  return (
    <View className="w-full">
      <FlatList
        data={DATA}
        className="h-full"
        renderItem={({ item }) => (
          <ItemList city={item.city} address={item.address} />
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};
const Addresses = () => {
  const navigation = useRouter();
  return (
    <SafeAreaView className="bg-[#fff]" style={styles.droidSafeArea}>
      {/* Changes page title visible on the header */}
      <Stack.Screen
        options={{
          title: "My Addresses",
          headerShown: true,
          headerStyle: {
            backgroundColor: primaryColor,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerRight: headerRight,
          headerShadowVisible: false,
        }}
      />

      <View className="m-5 flex">
        <View className="mb-5 flex w-full items-center justify-center p-5">
          <Text className="text-textColorDark mt-5 text-center text-xl">
            Add addresses as your here for your saved locations
          </Text>
          <Image
            source={require("./../../../assets/location.png")}
            style={{ width: 120, height: 120 }}
            resizeMode="contain"
            alt="location addres png"
          />
        </View>
        <AddressList />
      </View>
    </SafeAreaView>
  );
};

const { width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default Addresses;
