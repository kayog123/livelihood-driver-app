import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter } from "expo-router";

import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor } from "../../helper/const";

const CustomerService = () => {
  const navigation = useRouter();
  return (
    <SafeAreaView className="bg-[#fff] p-5" style={styles.droidSafeArea}>
      <TopBarHeader title="Customer Service" />
      <View className="h-full w-full  items-center justify-center bg-[#fff] ">
        <Image
          className=""
          source={require("./../../../assets/customer-service.png")}
          style={{ width: height_logo, height: height_logo }}
          alt=""
        />
        <Text className="text-textColorDark font-poppinsReg text-md text-center">
          If you have questions and issues. Please reach out to us via phone
          call or email.
        </Text>

        <View className="mt-5">
          <Text className="text-primary font-Poppins_700Bold   text-center text-lg">
            livelihood.app@gmail.com
          </Text>
          <Text className="text-secondaryLight font-poppinsREg text-center text-lg">
            +639-100-000-000
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");
const height_logo = height * 0.34;

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default CustomerService;
