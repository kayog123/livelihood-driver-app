import { FlatList, SafeAreaView, ScrollView, Text, View } from "react-native";
import { useAuth } from "@clerk/clerk-expo";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import moment from "moment";

import { api } from "~/utils/api";
import LoadingScreen from "~/components/Loading";
import TopBarHeader from "~/components/TopBarHearder";

type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
const MyBooking = () => {
  const { isLoaded, userId } = useAuth();
  if (!isLoaded || !userId) return;
  const { data } = api.schedule.fetchByUserId.useQuery({
    userId: userId,
  });

  return (
    <SafeAreaView className="bg-bgWhite min-h-full">
      <TopBarHeader title="My Booking" />
      <View className="bg-bgWhite space-y-4 py-4">
        {/* <Text className="font-poppinsReg text-textColorDark mx-5 text-xl">
          Current Booking
        </Text> */}
        <View className="flex  justify-center">
          {!data || !isLoaded || !userId ? (
            <Text className="font-poppinsReg text-center text-xl">
              Loading...
            </Text>
          ) : (
            <FlatList
              data={data}
              renderItem={({ item }) => {
                let statBarColor;
                if (item.status == "ACCEPTED") {
                  statBarColor = "bg-successColor";
                } else if (item.status == "CANCELLED") {
                  statBarColor = "bg-errorColor";
                } else if (item.status == "COMPLETED") {
                  statBarColor = "bg-textColorDark";
                } else {
                  statBarColor = "bg-warningColor";
                }

                let cardIcon: IonicIonsname;
                if (item.transpo_type == "BOAT") {
                  cardIcon = "boat-outline";
                } else if (item.transpo_type == "VANS") {
                  cardIcon = "car-outline";
                } else if (item.transpo_type == "MOTORCYCLE") {
                  cardIcon = "bicycle-outline";
                } else {
                  cardIcon = "bus-outline";
                }
                return (
                  <View className=" flex w-full cursor-pointer flex-row space-x-2 border-b  border-gray-200 p-3 px-4 py-2 text-white dark:border-gray-600 dark:bg-gray-800">
                    <View className="bg-primary flex items-center justify-center rounded-md p-4">
                      <Ionicons name={`${cardIcon}`} size={48} color="#fff" />
                    </View>
                    <View className="flex">
                      <Text className="font-Poppins_700Bold text-textColorDar">
                        Trip #{item.id}
                      </Text>
                      <Text
                        numberOfLines={4}
                        className="font-poppinsReg text-textColorDark "
                      >
                        Trip date :
                        {moment(item.dateScheduled).format("MMMM D, YYYY")}
                      </Text>
                    </View>
                    <View className={`${statBarColor} absolute px-3`}>
                      <Text className="text-bgWhite">{item.status}</Text>
                    </View>
                  </View>
                );
              }}
              keyExtractor={(item: any) => item.id}
            />
          )}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default MyBooking;
