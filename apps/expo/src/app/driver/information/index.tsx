import { Fragment, useEffect, useState } from "react";
import {
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { LinearGradient } from "expo-linear-gradient";
import { useAuth } from "@clerk/clerk-expo";
import { Controller, useForm } from "react-hook-form";

import { api } from "~/utils/api";
import NotificationBar from "~/components/NotificationBar";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor, textColorDark } from "~/helper/const";

type FormValues = {
  license: string;
  type: string;
  vehicle: string;
  permit: string;
  description: string;
};
type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};

const DriverDetails = () => {
  const { width } = Dimensions.get("screen");
  const [submitLoading, setSubmitLoading] = useState<boolean>(false);
  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });
  const [createStatus, setCreateStatus] = useState(false);
  const { isLoaded, userId, sessionId, getToken } = useAuth();

  if (!isLoaded || !userId) {
    return null;
  }
  const { data, isLoading, isFetching } = api.info.byId.useQuery({
    userId: userId,
  });
  useEffect(() => {
    if (data) {
      setValue("license", data.license);
      setValue("type", data.type);
      setValue("vehicle", data.vehicle);
      setValue("permit", data.permit);
      setValue("description", data.description);
    }
  }, [data]);
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
    setValue,
  } = useForm<FormValues>({
    defaultValues: {
      license: data?.license ?? "",
      type: data?.type ?? "",
      vehicle: data?.vehicle ?? "",
      permit: data?.permit ?? "",
      description: data?.description ?? "",
    },
  });

  const utils = api.useContext();

  const queryFunctions = {
    async onMutate(newPost: any) {
      setNotifyBar({
        show: true,
        message: " Please wait..",
        type: "waiting",
      });
    },
    async onSuccess() {
      await utils.info.byId.refetch({ userId: userId });
      setNotifyBar({
        show: true,
        message: "Successfully saved!",
        type: "success",
      });
    },
    async onError(err: any, newPost: any, ctx: any) {
      setNotifyBar({
        show: true,
        message: err?.message,
        type: "error",
      });
    },
    onSettled() {
      setSubmitLoading(false);
      const timer = setTimeout(() => {
        setNotifyBar({
          show: false,
          message: "",
          type: "waiting",
        });
        clearTimeout(timer);
      }, 5000);
    },
  };
  const create = api.info.create.useMutation(queryFunctions);
  const update = api.info.update.useMutation(queryFunctions);

  function showToast({
    message = "Request sent successfully!",
  }: {
    message: string;
  }) {
    ToastAndroid.show(message, ToastAndroid.SHORT);
  }

  const onSubmit = async (dataForm: FormValues) => {
    console.log("data", data);
    setSubmitLoading(true);
    if (!data && !createStatus) {
      create.mutate({
        userId: userId,
        license: dataForm.license,
        type: dataForm.type,
        vehicle: dataForm.vehicle,
        permit: dataForm.permit,
        description: dataForm.description,
      });
      setCreateStatus(true);
    } else {
      if (!data) return;
      update.mutate({
        id: data.id,
        userId: userId,
        license: dataForm.license,
        type: dataForm.type,
        vehicle: dataForm.vehicle,
        permit: dataForm.permit,
        description: dataForm.description,
      });
    }
  };

  return (
    <SafeAreaView>
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type} //notifyBar.type
        />
      </View>
      <TopBarHeader title="Driver Information" />

      <ScrollView className="bg-bgWhite min-h-screen w-full p-5">
        <View className="flex ">
          <Text className="font-Poppins_700Bold text-textColorDark text-center  text-xl">
            Driver Details
          </Text>
          <Text className="font-poppinsReg text-textColorDark text-center text-sm">
            Please provide the correct detail of the following required fields.
          </Text>
        </View>
        {isLoading ? (
          <ActivityIndicator color={primaryColor} size={24} className="my-10" />
        ) : (
          <View className="mx-5 mt-5">
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="license"
            />
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="type"
            />
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="vehicle"
            />
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="permit"
            />
            <ControlComponent
              control={control}
              errors={errors}
              fieldName="description"
            />
            <View className="mt-5 w-full">
              <TouchableOpacity
                //className={`"flex  hover:bg-blue-700" h-16 flex-row items-center justify-center gap-1 rounded bg-[${primaryColor}] font-bold text-white`}
                onPress={handleSubmit(onSubmit)}
              >
                <LinearGradient
                  className="bg-textColorDark flex-column ml-10 mr-10 flex-row items-center justify-center rounded-lg p-5"
                  colors={["#05375a", "#031e30d4"]}
                >
                  <Text className="dark:text-bgWhite  text-bgWhite flex uppercase text-neutral-50">
                    {submitLoading ? "Processing..." : "Update"}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const ControlComponent = ({
  control,
  errors,
  fieldName,
  rules = {
    required: fieldName + " is required",
  },
  placeholder,
}: any) => {
  return (
    <Controller
      control={control}
      render={({ field: { onChange, onBlur, value } }) => (
        <Fragment>
          <View className="bg-bgWhiteShadow mt-5 w-full rounded-xl pl-3 pr-3 shadow-md">
            <TextInput
              className=" h-14"
              placeholder={
                !placeholder
                  ? fieldName.charAt(0).toUpperCase() + fieldName.slice(1)
                  : placeholder
              }
              value={value}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
            />
          </View>

          {errors[fieldName] && (
            <Text className="text-errorColor">{errors[fieldName].message}</Text>
          )}
        </Fragment>
      )}
      name={fieldName}
      rules={rules}
    />
  );
};

export default DriverDetails;
