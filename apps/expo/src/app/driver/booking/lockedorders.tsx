import { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  View,
} from "react-native";
import { useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";

import DialogBox from "~/components/DialogBox";
import NotificationBar from "~/components/NotificationBar";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import BottomNav from "~/components/driver/locked/BottomNav";
import BusinessInforCard from "~/components/driver/locked/BusinessInforCard";
import CustomerDetails from "~/components/driver/locked/CustomerDetails";
import LocationDropPic from "~/components/driver/locked/LocationDropPic";
import HereMapView from "~/components/map/Here/HereMapView";
import MapsView from "~/components/map/MapsView";
import { HERE_MAP_API_KEY } from "~/helper/const";
import { NotifyBarProps } from "~/helper/type";
import { RootState } from "~/redux/store";

type AlertBoxProps = {
  title: string;
  message: string;
};

type DropOffProps = {
  lat: number;
  long: number;
  pick_up_name: string;
};
const LockedOrders = () => {
  const { height, width } = Dimensions.get("screen");
  const [mapRenderStatus, setMapRenderStatus] = useState(false);
  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });
  const [alertBoxMessage, setAlertBoxMessage] = useState<AlertBoxProps>({
    title: "",
    message: "",
  });
  const [alertBox, setAlertBox] = useState<boolean>(false);
  const { lockedBooking } = useSelector(
    (state: RootState) => state.lockedBooking,
  );
  const [bookingInfo, setBookingInfo] = useState(
    lockedBooking != null
      ? lockedBooking
      : {
          id: 0,
          userId: "",
          ownerId: "",
          status: "",
          transpo_type: "",
          transpo_rate: 0,
          description: "",
          firstname: "",
          lastname: "",
          phone: "",
          profile: "",
          pickup_lat: 0,
          pickup_long: 0,
          pickup_area_name: "",
          places: "",
        },
  );
  const [dropOffLocation, setDropOffLocation] = useState<DropOffProps>({
    lat: 0,
    long: 0,
    pick_up_name: "",
  });
  useEffect(() => {
    fetchGeoDetails(
      Number(bookingInfo.places.split(",")[0]),
      Number(bookingInfo.places.split(",")[1]),
    );
  }, []);

  const DESC_BUSINESS = bookingInfo.description.split('==END==');
  const businessDetails = DESC_BUSINESS ? JSON.parse(DESC_BUSINESS[0]!) : '';
  const description = DESC_BUSINESS ? DESC_BUSINESS[1] : '';
  useEffect(() => {
    if (
      dropOffLocation.lat != 0 &&
      dropOffLocation.long != 0 &&
      dropOffLocation.pick_up_name != ""
    ) {
      setMapRenderStatus(true);
    }
  }, [dropOffLocation]);

  const fetchGeoDetails = async (lat: number, long: number) => {
    setMapRenderStatus(false);
    await fetch(
      `https://revgeocode.search.hereapi.com/v1/revgeocode?at=${lat}%2C${long}&lang=en-US&apiKey=${HERE_MAP_API_KEY}`,
      {
        method: "get",
      },
    )
      .then((response) => response.json())
      .then((result) => {
        setDropOffLocation({
          lat: result.items[0].position.lat,
          long: result.items[0].position.lng,
          pick_up_name: result.items[0].address.label,
        });
      });
  };

  const fullname = bookingInfo.firstname + " " + bookingInfo.lastname;

  // const refetchBookingDetails = () => {};
  return (
    <SafeAreaView className="h-full w-full">
      <TopBarHeader
        title={
          `Locked Bookings - ` +
          `Trip #` +
          String(lockedBooking?.id).padStart(6, "0")
        }
      />
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type}
        />
      </View>
      {alertBox && (
        <DialogBox
          title={alertBoxMessage.title}
          messageSub={alertBoxMessage.message}
        />
      )}
      <ScrollView>
        {/* <MapsView height={height / 3} /> */}

        {mapRenderStatus && (
          <HereMapView
            height={200}
            initialRegionLong={Number(bookingInfo.pickup_long)} //
            initialRegionLat={Number(bookingInfo.pickup_lat)} //
            initialPickUpName={bookingInfo.pickup_area_name}
            dropoffLat={dropOffLocation.lat} //
            dropoffLong={dropOffLocation.long} //
            dropoffPickUp={dropOffLocation.pick_up_name} //
            setFair={() => {}}
            transpoSelected={"tricycle"} // bookingInfo.transpo_type
          />
        )}

        {businessDetails && <BusinessInforCard
          business_name={businessDetails.businessDetails.businessName}
          address={businessDetails.businessDetails.address}
        />}
        {/* <CustomerDetails name={fullname} phone={bookingInfo.phone} /> */}
        <View className="bg-textColorDark m-5 items-center justify-center rounded-lg">
          <View className="mx-5">
            <TitleText
              title="Booking Details"
              textSize="text-lg"
              textColor="bgWhite"
            />
          </View>
          <LocationDropPic
            text={bookingInfo.pickup_area_name}
            bottomBorder={true}
            iconText="Pickup"
          />
          <LocationDropPic
            text={dropOffLocation.pick_up_name}
            topBorder={true}
            iconText="Dropoff"
          />
        </View>

        <View className="mx-5">
          <Text className="font-poppinsReg mb-5 text-center">
            Note/Additional Details
          </Text>
          <View className="">
            {description!.trim() == "" ? (
              <Text className="font-poppinsReg text-center italic text-[#757575]">No additional notes</Text>
            ) : (
              <View className="px-5"> 
                <Text className="font-poppinsReg text-center">
                {`${description}`}
                </Text>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
      <View className="bottom-0">
        <BottomNav
          status={bookingInfo.status}
          bookingId={bookingInfo.id}
          ownerId={bookingInfo.ownerId}
          setNotifyBar={setNotifyBar}
          setAlertBox={setAlertBox}
          setAlertBoxMessage={setAlertBoxMessage}
        />
      </View>
    </SafeAreaView>
  );
};

export default LockedOrders;
