import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  View,
} from "react-native";
import { useRouter } from "expo-router";

import "react-native-gesture-handler";
import { ReactNode } from "react";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Layout from "~/components/Layout";
import TopBarHeader from "~/components/TopBarHearder";
import BoatBookings from "~/components/driver/receive/BoatBookings";
import MotorBookings from "~/components/driver/receive/MotorBooking";
import TricycleBookings from "~/components/driver/receive/TricycleBookings";
import VansBookings from "~/components/driver/receive/VansBookings";
import { navSetting, primaryColor } from "~/helper/const";

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();
const ReceiveBooking = () => {
  const navigation = useRouter();

  type TabListProps = {
    name: string;
    label: string;
    icon: string;
    size: number;
    component: any;
  };

  const tablist: TabListProps[] = [
    {
      name: "BoatList",
      label: "Boats",
      icon: "boat-outline",
      size: 28,
      component: BoatBookings,
    },
    {
      name: "VansList",
      label: "Vans",
      icon: "bus-outline",
      size: 28,
      component: VansBookings,
    },
    {
      name: "MotorList",
      label: "Motor",
      icon: "bicycle-outline",
      size: 28,
      component: MotorBookings,
    },
    {
      name: "TriList",
      label: "Tricycle",
      icon: "car-outline",
      size: 28,
      component: TricycleBookings,
    },
  ];

  type IonicIonsname = React.ComponentProps<typeof Ionicons>["name"];
  return ( 
    <SafeAreaView>
      <TopBarHeader title="Receive Bookings"/>
      {/* Changes page title visible on the header */}
      <Tab.Navigator initialRouteName="BoatList" screenOptions={navSetting}>
        {tablist.map((result, index) => (
          <Tab.Screen
            name={result.name}
            component={result.component}
            options={{
              tabBarLabel: result.label,
              tabBarIcon: ({ color }) => (
                <View>
                  <Ionicons
                    name={`${result.icon as IonicIonsname}`}
                    color={color}
                    size={result.size}
                  />
                </View>
              ),
            }}
          />
        ))}
      </Tab.Navigator> 
      </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");
const height_logo = height * 0.24;

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default ReceiveBooking;
