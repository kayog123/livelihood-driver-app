import React, { useState } from "react";
import {
  Dimensions,
  Pressable,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Geocoder from "react-native-geocoding";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";
import { useLocalSearchParams, useRouter } from "expo-router";

import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor } from "../../helper/const";

type coordinateProps = {
  latitude: number;
  longitude: number;
};

const Maps = () => {
  const navigation = useRouter();
  const [coordinate, setCoordinates] = useState<coordinateProps>({
    latitude: 9.672948,
    longitude: 123.873001,
  });
  return (
    <SafeAreaView className="bg-[#ffff]" style={styles.droidSafeArea}>
      {/* Changes page title visible on the header */}
      <TopBarHeader title="Specify Destination" />
      <MapView
        provider={PROVIDER_GOOGLE}
        className="h-full w-full rounded-md"
        initialRegion={{
          latitude: 9.672948,
          longitude: 123.873001,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        <Marker
          draggable
          coordinate={coordinate}
          image={require("./../../../assets/marker.png")}
          title="Location address"
          description="This is the pick up location you specified."
          onDragEnd={(e) => setCoordinates(e.nativeEvent.coordinate)}
          //onPress={(e) => setCoordinates(e.nativeEvent.coordinate)}
        />
      </MapView>
      <View className="absolute bottom-0 left-0 right-0 flex w-full items-center justify-end p-5 pb-12">
        <View className="bg-primary flex  flex-row flex-wrap rounded-lg p-5 pb-10">
          <View className="items-center justify-center">
            <View className="bg-secondaryLight mr-5 h-16 items-center justify-center rounded-lg p-1">
              <Ionicons
                color="white"
                name="navigate-circle-outline"
                size={48}
              />
            </View>
          </View>
          <View className="flex flex-1 ">
            <Text className="text-textColorDark text-md font-poppinsReg">
              Specified Location:
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              Taloto, Tagbilaran, City 6300
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              Latitude : {coordinate.latitude}
            </Text>
            <Text className="text-bgWhite font-poppinsReg">
              Longitude : {coordinate.longitude}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          className=" absolute bottom-3 w-full"
          onPress={() => {
            // navigation.push({
            //   pathname:
            //     returnPageLink == undefined ? "/" : returnPageLink.toString(),
            //   params: {
            //     selectedLocation: {
            //       pickup_long: coordinate.longitude,
            //       pickup_lat: coordinate.latitude,
            //       place_name: "Example Place",
            //     },
            //   }, //
            // });
          }}
        >
          <LinearGradient
            className="bg-textColorDark flex-column ml-10 mr-10 flex-row items-center justify-center rounded-lg p-5"
            colors={["#05375a", "#031e30d4"]}
          >
            <Ionicons name="locate-outline" size={28} color={"#fff"} />
            <Text className="text-bgWhite ">Select Location</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const { width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default Maps;
