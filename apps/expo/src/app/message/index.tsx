import React, { Fragment, useEffect, useState } from "react";
import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Bubble, GiftedChat, IMessage, Send } from "react-native-gifted-chat";
import uuid from "react-native-uuid";
import { useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";
import { PusherEvent } from "@pusher/pusher-websocket-react-native";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import { subscribeChatRoom, unSubscribeChannel } from "~/utils/PusherServer";
import NotificationBar from "~/components/NotificationBar";
import TopBarHeader from "~/components/TopBarHearder";
import {
  messageCustomTimeStyle,
  messageTextStyle,
  messageWrapperStyle,
  textColorDark,
} from "~/helper/const";
import { setMessageCount } from "~/redux/reducer/messageSlice";
import { RootState } from "~/redux/store";

declare global {
  interface Array<T> {
    getMessageitem(data: T): Array<T>;
  }
}
type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};
type SenderProps = {
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
  profile: string;
};
const ProfileDetails = ({ chatSender }: { chatSender: SenderProps }) => {
  return (
    <View className="w-full bg-[#dfe6e2] px-3 py-5">
      <View className="flex flex-row justify-start space-x-2 ">
        <View className="bg-textColorDark flex h-[70px] w-[70px] items-center justify-center rounded-full">
          <Image
            className="rounded-full p-5"
            source={
              chatSender.profile
                ? { uri: chatSender.profile }
                : require("./../../../assets/user.jpg")
            }
            style={{ width: 60, height: 60 }}
            alt=""
          />
        </View>
        <View
          className="ml-[12px] flex-1 rounded-md bg-[#fafafa] px-2 py-2"
          style={{
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 10 },
            shadowOpacity: 0.8,
            shadowRadius: 20,
            elevation: 7,
          }}
        >
          <Text className=" text-textColorDark font-poppinsReg text-[16px] font-semibold ">
            {chatSender.firstname} {chatSender.lastname}
          </Text>
          <View className="flex-row items-center space-x-2">
            <Ionicons name="mail" color={textColorDark} size={18} />
            <Text
              className="text-secondaryLight font-poppinsReg text-[14px]"
              numberOfLines={1}
            >
              {chatSender.email}
            </Text>
          </View>
          <View className="flex-row items-center space-x-2">
            <Ionicons name="call" color={textColorDark} size={18} />
            <Text
              className="text-secondaryLight font-poppinsReg text-[14px]"
              numberOfLines={1}
            >
              {chatSender.phone}
            </Text>
          </View>
          {/* <View className=" flex flex-row gap-1 pt-2">
            <View className=" flex flex-1 flex-row items-center space-x-2">
              <View className="bg-primary h-[10px] w-[10px] rounded-full"></View>
              <Text className="text-textColorDark font-poppinsReg flex-1 text-[15px] underline">
                Active Bookings (3)
              </Text>
            </View>
          </View> */}
        </View>
      </View>
    </View>
  );
};

type ConnectionStatusProps =
  | "CONNECTING"
  | "CONNECTED"
  | "DISCONNECTING"
  | "DISCONNECTED"
  | "RECONNECTING";

const Message = () => {
  const { userId, isLoaded } = useAuth();
  const { selectedChatRoom: roomId, countMessage } = useSelector(
    (state: RootState) => state.message,
  );
  const dispatch = useDispatch();

  if (!userId || !isLoaded) {
    return (
      <View className="mx-5 w-full p-5">
        <Text className="font-poppinsReg text-textColorDark">
          No user details!
        </Text>
      </View>
    );
  }

  const [messages, setMessages] = useState<IMessage[]>([]);
  const [senderProfile, setSenderProfile] = useState<SenderProps>();

  const [connectionStatus, setConnectionStatus] =
    useState<string>("CONNECTING");
  const channelName = `chatroom__${roomId}-messages`;

  useEffect(() => {
    subscribeChatRoom(channelName, (event: PusherEvent) => {
      if (event.eventName == `chatroom-messages-update`) {
        const { data } = event;
        const messageReceive = JSON.parse(data);
        const senderProfile =
          userId == usersData?.userId ? usersData.owner : usersData?.user;

        if (messageReceive.userId != userId) {
          const newMessage: IMessage = {
            _id: uuid.v4().toString(),
            text: messageReceive.message,
            createdAt: messageReceive.dateCreated,
            received: true,
            sent: true,
            user: {
              _id: messageReceive.user.id,
              name: `${senderProfile?.firstname} ${senderProfile?.lastname}`,
              avatar: senderProfile?.profile
                ? senderProfile.profile
                : require("./../../../assets/user.jpg"),
            },
          };
          setMessages((previousMessages) => {
            return GiftedChat.append(previousMessages, [
              { ...newMessage, received: true },
            ]);
          });
          dispatch(setMessageCount(countMessage - 1)); //message counter
        }
      }
    });
    return () => {
      unSubscribeChannel(channelName);
    };
  }, []);
  const limit = 14;
  const { data, fetchNextPage, hasNextPage, isFetching } =
    api.message.queryInfiniteMessage.useInfiniteQuery(
      {
        limit: limit, // this is optional - remember
        userId: userId!,
        chatRoom: `${roomId}`,
      },
      {
        getNextPageParam: (lastPage: any) => {
          return lastPage.nextCursor;
        },
        enabled: !!userId && !!roomId,
      },
    );

  const { data: usersData } = api.room.roomById.useQuery(
    {
      roomId: Number(roomId),
    },
    {
      enabled: !!roomId,
    },
  );

  const { mutate, error } = api.message.createMessage.useMutation({
    async onMutate(newPost: any) {
      let userFormat: IMessage = {
        _id: newPost.messageId,
        createdAt: new Date(),
        text: newPost.message,
        pending: true,
        user: {
          _id: userId,
          name: "fullname",
          avatar: require("./../../../assets/user.jpg"),
        },
      };
      setMessages((previousMessages) =>
        GiftedChat.append(previousMessages, [userFormat]),
      );
    },
    async onSuccess(newPost: any) {
      const updateMessageList = messages.map((messageList: IMessage) => {
        if (messageList._id == newPost.messageId) {
          console.log("messageList", messageList);
          return {
            _id: messageList._id,
            createdAt: messageList.createdAt,
            text: messageList.text,
            received: true,
            user: {
              _id: messageList.user._id,
              name: messageList.user.name,
              avatar: messageList.user.avatar,
            },
          };
        }
        return messageList;
      });

      setMessages(updateMessageList);
      //alert(JSON.stringify(newPost));
    },
  });

  const isReadMessages = api.message.markMessageAsRead.useMutation({
    async onSuccess(newPost: any) {},
  });

  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });

  const hideNotificationBar = ({
    timerAlive = 3000,
  }: {
    timerAlive: number;
  }) => {
    const timer = setTimeout(() => {
      setNotifyBar({
        show: false,
        message: "",
        type: "success",
      });
      clearTimeout(timer);
    }, timerAlive);
  };

  const handleFetchNextPage = () => {
    fetchNextPage();
  };

  useEffect(() => {
    allMessageQuery().then((messageList: any) => {
      setMessages(messageList);
    });
  }, [data]);

  useEffect(() => {
    if (usersData) {
      const sender =
        userId == usersData.userId ? usersData.owner : usersData.user;
      setSenderProfile(sender);
    }
  }, [usersData]);

  const allMessageQuery = () => {
    const messagesResult = new Promise((resolve, reject) => {
      let messageList: IMessage[] = [];
      data?.pages.forEach((result: any, index: number) => {
        messageList = [...messageList, ...result.items.getMessageitem()];
      });
      resolve(messageList);
      // reject(new Error('Failed to fetch data')); // Uncomment this line to simulate an error
    });

    return messagesResult;
  };

  Array.prototype.getMessageitem = function <T>(this: any): T[] {
    const unreadMessageList: Array<Array<string>> = [];
    return this.map((messageItem: any, currentIndex: number) => {
      const readUserList = messageItem.isRead.split(",");
      const isRead = checkIfUserReadMessage(readUserList, userId);

      if (!isRead) {
        const markAsRead =
          messageItem.isRead == "" ? userId : `${messageItem.isRead},${userId}`;
        const messageId = messageItem.id;
        const messageToUpdate = [messageId.toString(), markAsRead];
        unreadMessageList.push(messageToUpdate);
        const newMessageCount = countMessage < 1 ? 0 : countMessage - 1;
        dispatch(setMessageCount(newMessageCount));
      }

      if (currentIndex + 1 == this.length) {
        markAsRead(unreadMessageList);
      }

      let userFormat: IMessage = {
        _id: messageItem.id,
        createdAt: new Date(messageItem.dateCreated),
        text: messageItem.message,
        received: true,
        sent: true,
        user: {
          _id: messageItem.user.id,
          name: messageItem.user.firstname,
          avatar: messageItem.user.profile
            ? messageItem.user.profile
            : require("./../../../assets/user.jpg"),
        },
      };

      if (messageItem.user.id == userId) {
        userFormat.sent = true;
      } else {
        userFormat.received = true;
      }
      const messageSender: IMessage = userFormat;
      return messageSender;
    });
  };

  const markAsRead = (messageToUpdateArray: Array<Array<string>>) => {
    //-console.log("data to mark as read", messageToUpdateArray);
    if (messageToUpdateArray.length > 0) {
      isReadMessages.mutate({
        dataList: messageToUpdateArray,
      });
    }
  };

  const checkIfUserReadMessage = (arrayList: Array<string>, userId: string) => {
    return arrayList.includes(userId);
  };
  const onSend = async (newMessages: IMessage[] = []) => {
    const messageData = newMessages[0];
    if (messageData == undefined || !usersData) {
      console.log("No message data or no receiver data");
      return;
    }
    mutate({
      messageId: uuid.v4().toString(),
      roomId: Number(roomId),
      userId: userId,
      message: messageData.text,
      isRead: userId, //[userId, userId2]  --> format
      receiverId:
        userId == usersData.userId ? usersData.owner.id : usersData.user.id,
    });
  };

  const renderBubble = (props: any) => {
    return (
      <SafeAreaView style={{ alignItems: "flex-start" }}>
        <Bubble
          {...props}
          wrapperStyle={messageWrapperStyle}
          textStyle={messageTextStyle}
        />
      </SafeAreaView>
    );
  };

  return (
    <Fragment>
      <TopBarHeader title={`Message`} />
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type} //notifyBar.type
        />
      </View>
      {usersData && (
        <ProfileDetails
          chatSender={
            userId == usersData.userId ? usersData.owner : usersData.user
          }
        />
      )}
      <View style={styles.overlay}>
        <GiftedChat
          messages={messages}
          onSend={onSend}
          user={{
            _id: userId,
          }}
          isTyping={true}
          isLoadingEarlier={isFetching}
          loadEarlier={hasNextPage}
          onLoadEarlier={() => handleFetchNextPage()}
          scrollToBottom={true}
          timeTextStyle={messageCustomTimeStyle}
          renderBubble={renderBubble}
          renderSend={(props) => {
            return (
              <Send {...props}>
                <View className="relative right-[4px] top-[-10px]">
                  <Ionicons name="send" size={24} color="#009387" />
                </View>
              </Send>
            );
          }}
        />
      </View>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#f5f5f5",
    flex: 1,
  },
  overlay: {
    backgroundColor: "#fefefe",
    flex: 1,
  },
});

export default Message;
