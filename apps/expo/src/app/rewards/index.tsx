import {
  Dimensions,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter } from "expo-router";

import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor } from "../../helper/const";

const Rewards = () => {
  const navigation = useRouter();
  return (
    <SafeAreaView className="bg-[#fff] p-5" style={styles.droidSafeArea}>
      {/* Changes page title visible on the header */}
      <TopBarHeader title="Activate Rewards" />
      <View className="h-full w-full  items-center justify-center">
        <Image
          className=""
          source={require("./../../../assets/rewards.png")}
          style={{ width: height_logo, height: height_logo }}
          alt=""
        />
        <Text className="text-textColorDark font-poppinsReg text-center">
          Coming Soon!
        </Text>
        {/* <Text className="text-textColorDark font-poppinsReg text-center">
          You can claim and activate you reward here!
        </Text>
        <View className="m-5 mt-5 flex flex-row">
          <TextInput
            value=""
            placeholder="PROMO CODE"
            className="bg-colorDarkOutline  font-poppinsReg flex-grow rounded-bl-lg rounded-tl-lg p-4 shadow-md"
          />
          <TouchableOpacity className="bg-textColorDark items-center justify-center rounded-br-lg rounded-tr-lg p-3">
            <Text className="font-poppinsReg text-center uppercase text-[#fff] ">
              Activate
            </Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </SafeAreaView>
  );
};

const { width, height } = Dimensions.get("screen");
const height_logo = height * 0.24;

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default Rewards;
