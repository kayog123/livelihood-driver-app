import {
  Dimensions,
  ImageBackground,
  SafeAreaView,
  Text,
  View,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import LottieView from "lottie-react-native";

import ItemCard from "~/components/ItemCard";
import TopBarHeader from "~/components/TopBarHearder";
import { LIVELIHOOD_CATEGORY } from "~/helper/const";

const Categories = () => {
  const { width, height } = Dimensions.get("screen");
  const logoSize = width / 1.5;
  const img = require("./../../../assets/business/bread.jpg");

  return (
    <SafeAreaView>
      <TopBarHeader title="Categories" />
      <ScrollView className="h-full w-full   ">
        {LIVELIHOOD_CATEGORY.map((category, index) => (
          <View
            key={index}
            className="bg-bgWhite overflow-hiddenp-1 m-3 mx-5 overflow-hidden rounded-xl"
          >
            <ImageBackground
              source={img}
              resizeMode="cover"
              className="flex h-full w-full"
              style={{ height: height / 5 }}
            >
              <View className="absolute top-0 w-full bg-[#1c1e216e] p-2 px-5">
                <Text
                  className="text-bgWhite font-poppinsReg text-md rounded-full    shadow-lg"
                  numberOfLines={2}
                >
                  {category.title}
                </Text>
                <Text
                  className="text-bgWhiteShadow font-poppinsReg   text-xs"
                  numberOfLines={2}
                >
                  {category.desc}
                </Text>
              </View>
            </ImageBackground>
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Categories;
