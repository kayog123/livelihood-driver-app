import React from "react";
import { SafeAreaView, Text, View } from "react-native";
import { Stack, useRouter } from "expo-router";

import GetStarted from "../components/get-started";
import NavigatorTab from "../components/navigator";
import Auth from "../helper/Auth";
import Layout from "~/components/Layout";

const Index = () => {
  return (  
      <Layout topBarTitle="Home" tabBarShow={false}> 
        <View className="w-full h-full">
        <NavigatorTab />
        </View>
      </Layout> 
  );
};

export default Index;
