import { useEffect, useState } from "react";
import { Dimensions, SafeAreaView, Text, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-async-storage/async-storage";

import TopBarHeader from "~/components/TopBarHearder";
import TravelServiceOpt from "~/components/bookings/TravelServiceOpt";

const Settings = () => {
  const [transpoSelected, setSelectedTranspo] = useState<string>("");
  const { width } = Dimensions.get("screen");
  const logoSize = width / 1.5;

  useEffect(() => {
    const fetchSelectedService = async () => {
      try {
        const choosenTranspoType = await AsyncStorage.getItem(
          "transpoTypeChoosen",
        );
        if (choosenTranspoType) {
          setSelectedTranspo(choosenTranspoType);
        }
      } catch (e) {
        console.log(e);
      }
    };
    fetchSelectedService();
    //AsyncStorage.clear();
  }, []);

  const storedServiceType = async (value: string) => {
    try {
      await AsyncStorage.setItem("transpoTypeChoosen", value);
    } catch (e) {
      console.log(e);
    }
  };

  const updateTranspoType = async (value: string) => {
    await storedServiceType(value);
    setSelectedTranspo(value);
  };
  return (
    <SafeAreaView>
      <TopBarHeader title="Settings" />
      <ScrollView className="h-full w-full py-5">
        <View className="bg-textColorDark mx-5 rounded-lg">
          <Text className="font-poppinsReg text-textColorDark">
            Service Type
          </Text>
          <TravelServiceOpt
            textColor="text-bgWhite"
            transpoSelected={transpoSelected}
            setSelectedTranspo={updateTranspoType}
            text="Choose service type"
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Settings;
