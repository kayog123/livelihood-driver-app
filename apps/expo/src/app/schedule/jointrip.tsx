import { useEffect, useState } from "react";
import {
  Pressable,
  SafeAreaView,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useLocalSearchParams } from "expo-router";
import { useAuth } from "@clerk/clerk-expo";
import { Ionicons } from "@expo/vector-icons";

import { api } from "~/utils/api";
import DialogBox from "~/components/DialogBox";
import NotificationBar from "~/components/NotificationBar";
import TopBarHeader from "~/components/TopBarHearder";
import ItemPlaceList from "~/components/schedule/ItemPlaceList";
import PassengerCountView from "~/components/schedule/PassengerCountView";
import { boatPackageData, vansPackageData } from "~/helper/const";

type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};

const PassengerCounter = ({
  min = 0,
  max = 15,
  passenger,
  setPassenger,
}: {
  min?: number;
  max?: number;
  passenger: number;
  setPassenger: (value: number) => void;
}) => {
  const increment = () => {
    if (passenger >= max) return;
    setPassenger(passenger + 1);
  };

  const decrement = () => {
    if (passenger <= min) return;
    setPassenger(passenger - 1);
  };

  return (
    <View className="flex flex-row">
      <Pressable onPress={increment}>
        <LinearGradient
          className="items-center justify-center rounded-bl-lg rounded-tl-lg  p-3"
          colors={["#009387", "#118586"]}
        >
          <Text className="font-poppinsReg text-bgWhite">+</Text>
        </LinearGradient>
      </Pressable>
      <TextInput
        className="bg-bgWhite w-[40%] px-2 py-3 text-center"
        keyboardType="numeric"
        value={passenger.toString()}
        onChangeText={(value) => {
          if (/^\d+$/.test(value)) {
            setPassenger(min);
          }
          if (Number(value) <= 0) {
            return;
          }
          if (Number(value) > max) {
            setPassenger(15);
            return;
          }
          setPassenger(Number(value));
        }}
      />
      <Pressable onPress={decrement}>
        <LinearGradient
          className="bg-textColorDark flex-column  flex-row items-center justify-center rounded-br-lg  rounded-tr-lg  p-3"
          colors={["#009387", "#118586"]}
        >
          <Text className="font-poppinsReg text-bgWhite">-</Text>
        </LinearGradient>
      </Pressable>
    </View>
  );
};

const findPackageById = (id: number, data: any) => {
  for (const packageObj of data) {
    if (packageObj.id === id) {
      return packageObj;
    }
  }
  return null; // Return null if no matching object is found
};

const JoinTrip = () => {
  const params = useLocalSearchParams();
  const {
    tripId,
    place,
    transpo_type,
    capacity,
    currentPassengerCount,
    status,
    schedule,
    bookingId,
  } = params;
  const [itemList, setItemList] = useState<any>([]);
  const [passenger, setPassenger] = useState<number>(1);
  const { isLoaded, userId } = useAuth();
  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });
  const [alertBox, setAlertBox] = useState<boolean>(false);
  const [joinStatus, setJoinStatus] = useState<boolean>(false);
  if (
    tripId == undefined ||
    typeof place != "string" ||
    (transpo_type == undefined && typeof transpo_type != "string") ||
    capacity == undefined ||
    currentPassengerCount == undefined ||
    typeof status != "string" ||
    schedule == undefined ||
    typeof schedule != "string" ||
    bookingId == undefined
  )
    return;

  if (!isLoaded || !userId) {
    return null;
  }

  const data = transpo_type == "BOAT" ? boatPackageData : vansPackageData;

  useEffect(() => {
    const resultDetails = place.split(",").map((placeItem: string) => {
      const splitPlace = placeItem.split("[");
      const placeId = Number(splitPlace[0]);
      const TripData = findPackageById(Number(tripId), data);
      const resultData = findPackageById(placeId, TripData.places);
      if (splitPlace[1] != undefined) {
        return {
          ...resultData,
          events: splitPlace[1]
            .replaceAll("]", "")
            .split("|")
            .map((eventItem) => {
              return findPackageById(Number(eventItem), resultData.events);
            }),
        };
      }
      return {
        ...resultData,
        show: true,
      };
    });

    setItemList(resultDetails);
  }, []);

  useEffect(() => {
    validateJoinStatus();
  });

  const validateJoinStatus = () => {
    if (Number(currentPassengerCount) >= Number(capacity)) {
      setJoinStatus(true);
      return;
    }
    if (
      status.toLowerCase() == "cancelled" ||
      status.toLowerCase() == "completed"
    ) {
      setJoinStatus(true);
      return;
    }
  };
  // const removePassenger = api.passengers.deleteAllRecord.useMutation({
  //   async onSuccess() {
  //     console.log("remove", data);
  //   },
  // });
  // const removeAllPassengerRecord = () => {
  //   removePassenger.mutate({});
  // };

  const { mutate, error } = api.passengers.create.useMutation({
    async onMutate(newPost: any) {
      setNotifyBar({
        show: true,
        message: "Creating schedule! Please wait..",
        type: "waiting",
      });
    },
    async onSuccess() {
      setAlertBox(true);
    },
    async onError(err: any, newPost: any, ctx: any) {
      setNotifyBar({
        show: true,
        message: err?.message,
        type: "error",
      });
    },
    onSettled() {
      const timer = setTimeout(() => {
        setNotifyBar({
          show: false,
          message: "",
          type: "waiting",
        });
        clearTimeout(timer);
      }, 5000);
    },
  });

  const onSubmitJoin = () => {
    let paramsData = {
      userId: userId,
      passenger_count: passenger,
      capacity: Number(capacity),
      bookingId: Number(bookingId),
    };

    mutate(paramsData);
  };

  return (
    <SafeAreaView className="h-full w-full">
      <TopBarHeader title="Join a booking Trip" />
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type} //notifyBar.type
        />
      </View>
      {alertBox && (
        <DialogBox
          title="Thank you"
          messageSub="You have successfully joined a trip. Enjoy your vacation."
        />
      )}
      <ScrollView>
        {Number(currentPassengerCount) >= Number(capacity) && (
          <View className="bg-warningColor flex-row items-center justify-center space-x-2 p-3">
            <Ionicons
              name="information-circle-outline"
              size={24}
              color="#fff"
            />
            <Text className="font-poppinsReg text-bgWhite">
              This trip is full. You may look another trip!
            </Text>
          </View>
        )}

        <PassengerCountView
          tripId={Number(bookingId)}
          countLimit={Number(capacity)}
          currentCount={Number(currentPassengerCount)}
          status={status}
          travelBy={
            typeof transpo_type == "string" &&
            transpo_type.toLowerCase() == "boat"
              ? "boat"
              : "vans"
          }
          schedule={schedule}
        />

        <View className="mx-5 mt-10 flex flex-row items-center justify-center space-x-3">
          <View className="border-textColorDark flex flex-grow border-0 border-b border-dashed"></View>
          <Text className="font-poppinsReg text-bgWhite bg-primary rounded-full px-3 text-xl">
            List of Routes
          </Text>
          <View className="border-textColorDark flex flex-grow border-0 border-b border-dashed"></View>
        </View>
        <ItemPlaceList data={itemList} viewOnly={true} />
      </ScrollView>
      <View className="bg-bgWhite h-20 flex-row items-center justify-center px-5">
        <View className=" w-[35%] px-3">
          <PassengerCounter
            passenger={passenger}
            setPassenger={setPassenger}
            min={1}
            max={Number(capacity) - Number(currentPassengerCount)}
          />
        </View>
        <TouchableOpacity
          className={`${
            joinStatus ? "bg-[#9ee3ce]" : "bg-primary"
          }  flex-2  mx-5 flex h-16 flex-grow items-center justify-center rounded-md p-4`}
          onPress={onSubmitJoin}
          disabled={joinStatus}
        >
          <View
            className={`flex  flex-row items-center justify-center space-x-2 rounded-md`}
          >
            <Text className="text-bgWhite font-poppinsReg text-center text-lg">
              {joinStatus ? "Can't" : ""} Join Trip
            </Text>
            <Ionicons name="arrow-forward-outline" size={28} color="#fff" />
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default JoinTrip;
