import React, { Fragment, useEffect, useRef, useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useLocalSearchParams, useRouter } from "expo-router";
import { useAuth } from "@clerk/clerk-expo";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import BoxWithIcon from "~/components/BoxWithIcon";
import DialogBox from "~/components/DialogBox";
import NotificationBar from "~/components/NotificationBar";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import GeoMapUpdater from "~/components/map/GeoMapUpdater";
import PlaceModal from "~/components/modal/PlaceModal";
import { RootState } from "~/redux/store";
import ConfirmBox from "../../components/ConfirmBox";
import DatePicker from "../../components/DatePicker";

interface PickUpProps {
  id?: number;
  name: string;
  location: string;
  lat: number;
  long: number;
}

type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};

const VanPickUpButton = ({
  setMapModal,
  pickUpSelected,
}: {
  setMapModal: (value: boolean) => void;
  pickUpSelected: PickUpProps | undefined;
}) => {
  const navigation = useRouter();
  return (
    <TouchableOpacity
      onPress={() => {
        // navigation.push({
        //   pathname: "/maps",
        //   params: { returnPageLink: "/schedule/pickup" },
        // });
        setMapModal(true);
      }}
      className="bg-primary mx-5 flex flex-row items-center justify-center space-x-2 rounded-md p-5"
    >
      <Ionicons name="map-outline" size={24} color="#fff" />
      <Text className="text-bgWhite font-poppinsReg">
        {pickUpSelected ? pickUpSelected.name : "Choose Location"}
      </Text>
    </TouchableOpacity>
  );
};

const BoatPickUpList = ({
  pickUpAreaList,
  pickUpSelected,
  setPickUpSelected,
}: {
  pickUpAreaList: PickUpProps[];
  pickUpSelected: PickUpProps | undefined;
  setPickUpSelected: (value: PickUpProps) => void;
}) => {
  return (
    <View className="space-y-2">
      {pickUpAreaList?.map((result: PickUpProps, index: number) => {
        let selectedCheck = false;
        if (pickUpSelected != undefined) {
          if (result.id == pickUpSelected.id) {
            selectedCheck = true;
          }
        }

        return (
          <TouchableOpacity
            key={index}
            onPress={() => setPickUpSelected(result)}
            className={`bg-colorDarkOutline ${
              selectedCheck ? "border-primary border" : ""
            } flex flex-row items-center rounded-xl px-10 py-3`}
          >
            <Ionicons
              name={`${selectedCheck ? "checkmark-circle" : "ellipse-outline"}`}
              size={36}
              color={`#05375a`}
            />
            <View className="pl-5">
              <Text className="text-primary font-poppinsReg text-lg">
                {result.name}
              </Text>
              <Text className="font-poppinsReg">{result.location}</Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const PickUp = () => {
  const { isLoaded, userId } = useAuth();
  const navigation = useRouter();
  const [pickUpSelected, setPickUpSelected] = useState<
    PickUpProps | undefined
  >();
  const [date, setDate] = useState(new Date());
  const [notes, setNotes] = useState<string>("");
  const [noteFocus, setNoteFocus] = useState<boolean>(false);
  const [dateSelected, setSelectedDate] = useState<boolean>(false);
  const [alertBox, setAlertBox] = useState<boolean>(false);
  const { placesToVisit } = useSelector((state: RootState) => state.places);
  const [mapModal, setMapModal] = useState<boolean>(false);
  const params = useLocalSearchParams();

  const {
    tagsNav,
    transpo_rate,
    passengers,
    place_activity_fee,
    land_transpo_type,
    allowOther,
    tripId,
    transpoCapacity,
  } = params;

  if (typeof tagsNav != "string" || typeof land_transpo_type != "string")
    return;

  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });

  const { mutate, error } = api.schedule.create.useMutation({
    async onMutate(newPost: any) {
      setNotifyBar({
        show: true,
        message: "Creating schedule! Please wait..",
        type: "waiting",
      });
    },
    async onSuccess() {
      setAlertBox(true);
    },
    async onError(err: any, newPost: any, ctx: any) {
      setNotifyBar({
        show: true,
        message: "Sorry, an error encountered. Please try again!",
        type: "error",
      });
    },
    onSettled() {
      setNotifyBar({
        show: false,
        message: "",
        type: "waiting",
      });
    },
  });

  if (!isLoaded || !userId) {
    return null;
  }

  const createSchedule = () => {
    let placeString = "";
    placesToVisit.forEach((result, index) => {
      if (result.show) {
        placeString = placeString + result.id;
        if (result.events) {
          let eventsString = "";
          result.events.map((events, eventIndex) => {
            if (result.events != undefined && events.show) {
              let separator = "";
              if (eventIndex < result.events?.length - 1) {
                separator = "|";
              }
              eventsString = eventsString + events.id + separator;
            }
          });
          if (eventsString != "") {
            eventsString = "[" + eventsString + "]";
          }
          placeString = placeString + eventsString;
        }
        let placeSeparator = "";
        if (index < placesToVisit.length - 1) {
          placeSeparator = ",";
        }
        placeString = placeString + placeSeparator;
      }
    });
    if (placeString == "") {
      ToastAndroid.show(
        "No places selected. Choose a place to visit!",
        ToastAndroid.SHORT,
      );
      return;
    }

    if (pickUpSelected == undefined) {
      console.log("pick up location was not selected!");
      return;
    }

    let paramsData = {
      userId: userId,
      type: "TRIP",
      pickup_lat: pickUpSelected.lat,
      pickup_long: pickUpSelected.long,
      pickup_area_name: pickUpSelected.name,
      notes: notes,
      places: placeString,
      dateScheduled: date,
      transpo_rate: Number(transpo_rate),
      passengers: Number(passengers),
      place_activity_fee: Number(place_activity_fee),
      transpo_type: tagsNav,
      allowed_other: Boolean(allowOther),
      tripId: Number(tripId),
      passenger_capacity: Number(transpoCapacity),
      land_transpo_type: land_transpo_type,
    };

    mutate(paramsData);
  };

  const pickUpAreaList = [
    {
      id: 1,
      name: "Alona Beach",
      location: "Panglao, Bohol",
      lat: 9,
      long: 23.99,
    },
    {
      id: 2,
      name: "Panglao Beach",
      location: "Panglao, Bohol",
      lat: 9,
      long: 23.99,
    },
  ];

  return (
    <SafeAreaView className="h-full w-full">
      <PlaceModal
        modalVisible={mapModal}
        setModalVisible={setMapModal}
        title="Specify Location"
      >
        <GeoMapUpdater
          setGeolocation={(value) => {
            setPickUpSelected({
              lat: value.lat,
              long: value.long,
              name: value.pick_up_name,
              location: value.pick_up_name,
            });
          }}
          setMapModal={setMapModal}
        />
      </PlaceModal>
      <TopBarHeader title="Select pick up area" />
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type}
        />
      </View>
      {alertBox && <DialogBox />}
      <ScrollView className="py-5">
        <BoxWithIcon icons="calendar-outline">
          <View className="px-5">
            <TitleText title="Set pick up date and time" textSize="text-md" />
          </View>
          <DatePicker
            date={date}
            setDate={setDate}
            dateSelected={dateSelected}
            setSelectedDate={setSelectedDate}
          />
        </BoxWithIcon>
        <BoxWithIcon icons="location-outline">
          <View className=" gap-y-2 rounded-md px-5 py-3 ">
            <View className="p-5 pb-2 pt-0">
              <TitleText
                title={`${
                  tagsNav == "boat"
                    ? "Available pick up area"
                    : "Choose a pick up location"
                }`}
                textSize="text-md"
              />
            </View>
            {tagsNav == "boat" ? (
              <BoatPickUpList
                pickUpAreaList={pickUpAreaList}
                pickUpSelected={pickUpSelected}
                setPickUpSelected={setPickUpSelected}
              />
            ) : (
              <Fragment>
                <VanPickUpButton
                  setMapModal={setMapModal}
                  pickUpSelected={pickUpSelected}
                />
              </Fragment>
            )}
          </View>
        </BoxWithIcon>
        <BoxWithIcon icons="pencil">
          <View className="gap-y-2 rounded-md px-5 py-3 ">
            <View className="px-5">
              <TitleText title="Leave an important note" textSize="text-md" />
            </View>
            <TextInput
              editable
              multiline={true}
              numberOfLines={5}
              onFocus={() => setNoteFocus(true)}
              onBlur={() => setNoteFocus(false)}
              className={`bg-bgWhiteShadow font-poppinsReg border-${
                noteFocus ? "primary" : "bgWhiteShadow"
              } w-full rounded-md border px-3 py-5 `}
              placeholder="Leave your note here.."
              value={notes}
              onChangeText={setNotes}
              style={{ textAlignVertical: "top" }}
            />
          </View>
        </BoxWithIcon>
      </ScrollView>
      <ConfirmBox
        cancelName="Back"
        cancelPress={() => {
          navigation.back();
        }}
        confirmName="Proceed"
        confirmIcon={
          <Ionicons
            name="arrow-forward-circle-outline"
            size={24}
            color={`#fff`}
          />
        }
        confirmPress={() => {
          if (pickUpSelected != undefined) {
            pickUpSelected != undefined;
            if (!dateSelected) {
              ToastAndroid.show(
                "Please select a schedule date",
                ToastAndroid.SHORT,
              );
              return;
            }

            //This is a success function next
            createSchedule();
            //navigation.push("/schedule/summary");
          } else {
            ToastAndroid.show(
              "Please choose a pick up area!",
              ToastAndroid.SHORT,
            );
          }
        }}
      />
    </SafeAreaView>
  );
};

export default PickUp;
