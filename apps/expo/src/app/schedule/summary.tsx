import React, { useEffect, useRef, useState } from "react";
import {
  Animated,
  Image,
  ImageBackground,
  Pressable,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Stack, useLocalSearchParams, useRouter } from "expo-router";
import { useSelector } from "react-redux";

import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import ItemPlaceList from "~/components/schedule/ItemPlaceList";
import { PlaceProps } from "~/redux/reducer/placesSlice";
import { RootState } from "~/redux/store";
import ConfirmBox from "../../components/ConfirmBox";
import MapsView from "../../components/map/MapsView";
import { primaryColor } from "../../helper/const";

const AnimatedIonicons = Animated.createAnimatedComponent(Ionicons);

const BookSummary = () => {
  const navigation = useRouter();
  const [show, setShow] = useState(false);
  const animatedValue = useRef(new Animated.Value(0)).current;
  const animationValue = useRef(new Animated.Value(0)).current;
  const [dataList, setDataList] = useState<PlaceProps[]>([]);
  const params = useLocalSearchParams();
  const { tagsNav } = params;
  const { placesToVisit, placesToVisitCount } = useSelector(
    (state: RootState) => state.places,
  );
  const animation = Animated.loop(
    Animated.sequence([
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }),
    ]),
  );

  React.useEffect(() => {
    animation.start();
  }, [animation]);

  useEffect(() => {
    setDataList(
      placesToVisit.filter((result: PlaceProps) => result.tags == tagsNav),
    );
  }, [placesToVisit]);

  const translateY = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 5],
  });
  const colorInterpolation = animationValue.interpolate({
    inputRange: [0, 1],
    outputRange: ["#009387", "#000"],
  });

  const startAnimation = () => {
    Animated.loop(
      Animated.timing(animationValue, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
      }),
    ).start();
  };

  useEffect(() => {
    startAnimation(); // Start the animation when the component mounts
  }, []);

  return (
    <SafeAreaView className="h-full w-full">
      <TopBarHeader title="Trip summary" />
      <ScrollView>
        <MapsView height={200} />
        <View className=" w-full gap-y-3 p-5">
          <TitleText title="Selected Routes" />
          {/* <View
            className={`{ rounded-lg bg-[#fff] p-3 shadow-lg ${show && "pb-5"}`}
          >
            <View className="relative w-full flex-row flex-wrap">
              <View className="mr-[10px] w-[25%]">
                <View className=" border-primaryColorShadow bg-textColorDark h-[80] w-[80] overflow-hidden rounded-xl border-2">
                  <Image
                    className=""
                    source={require("./../../../assets/places/virgin_island.jpg")}
                    style={{
                      width: 80,
                      height: 110,
                      bottom: 0,
                      position: "absolute",
                    }}
                    resizeMode="cover"
                    alt=""
                  />
                </View>
              </View>

              <View className="w-[71%]">
                <Text className="font-poppinsReg text-primaryColorShadow text-xl">
                  Virgin Island
                </Text>
                <Text>Estimated Travel: 1 hr. 30 min</Text>
              </View>
              <Animated.View
                className="absolute bottom-0 right-[-6px] flex w-full flex-row justify-end"
                style={{
                  transform: [{ translateY }],
                }}
              >
                {show == false ? (
                  <TouchableOpacity onPress={onPressDown}>
                    <AnimatedIonicons
                      name="chevron-down-circle-outline"
                      size={34}
                      style={{ color: colorInterpolation }}
                      // color={`${colorInterpolation}`}
                      // color={`$[{ colorInterpolation }]`}
                    />
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity onPress={onPressUp}>
                    <AnimatedIonicons
                      name="chevron-up-circle-outline"
                      size={34}
                      style={{ color: colorInterpolation }}
                    />
                  </TouchableOpacity>
                )}
              </Animated.View>
            </View>
            {show && (
              <View className="flex-column flex w-full items-center">
                <View className="relative  flex flex-row items-end ">
                  <View className="h-full w-[1px] bg-[#000]"></View>
                  <View className="h-[1px] w-[9%] bg-[#000]"></View>
                  <View className="realtive bg-primary top-[5px] flex  h-[15px] w-[15px] self-end rounded-full"></View>

                  <View className="relative top-[7px] mt-[15px] flex w-[82%] flex-row">
                    <Text className=" rounded-[2px] border-[1px] border-solid border-[#ada7a7] px-2   text-lg text-[#133a1e]">
                      Diving
                    </Text>
                  </View>
                </View>

                <View className="relative  flex flex-row items-end ">
                  <View className="h-full w-[1px] bg-[#000]"></View>
                  <View className="h-[1px] w-[9%] bg-[#000]"></View>
                  <View className="realtive bg-primary top-[5px] flex  h-[15px] w-[15px] self-end rounded-full"></View>

                  <View className="relative top-[7px] mt-[15px] flex w-[82%] flex-row">
                    <Text className=" rounded-[2px] border-[1px] border-solid border-[#ada7a7] px-2   text-lg text-[#133a1e]">
                      Island Hopping
                    </Text>
                  </View>
                </View>

                <View className="relative  flex flex-row items-end ">
                  <View className="h-full w-[1px] bg-[#000]"></View>
                  <View className="h-[1px] w-[9%] bg-[#000]"></View>
                  <View className="realtive bg-primary top-[5px] flex  h-[15px] w-[15px] self-end rounded-full"></View>

                  <View className="relative top-[7px] mt-[15px] flex w-[82%] flex-row">
                    <Text className=" rounded-[2px] border-[1px] border-solid border-[#ada7a7] px-2  text-lg text-[#133a1e]">
                      Dolphin Watching
                    </Text>
                  </View>
                </View>
              </View>
            )}
          </View> */}
          <ItemPlaceList data={dataList} />
        </View>
      </ScrollView>

      <ConfirmBox
        cancelName="Cancel"
        cancelPress={() => {
          navigation.back();
        }}
        confirmName="Proceed"
        confirmIcon={
          <Ionicons
            name="arrow-forward-circle-outline"
            size={24}
            color={`#fff`}
          />
        }
        confirmPress={() => {
          navigation.push({
            pathname: "/schedule/pickup",
            params: { tagsNav },
          });
        }}
      />
    </SafeAreaView>
  );
};

export default BookSummary;
