import { Fragment, useState } from "react";
import { FlatList, Image, SafeAreaView, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { useLocalSearchParams, useRouter } from "expo-router";
import { Ionicons } from "@expo/vector-icons";
import moment from "moment";

import { api } from "~/utils/api";
import TopBarHeader from "~/components/TopBarHearder";
import { boatPackageData, vansPackageData } from "~/helper/const";
import { TripList } from "~/helper/type";

const JoinCard = ({ data, tagsNav }: { data: any; tagsNav: string }) => {
  const navigation = useRouter();

  const findPackageById = (id: number, transpo_type: string) => {
    const dataResult =
      transpo_type == "boat" ? boatPackageData : vansPackageData;

    for (const packageObj of dataResult) {
      if (packageObj.id === id) {
        return packageObj;
      }
    }

    return null; // Return null if no matching object is found
  };

  return (
    <View className="space-y-8 ">
      <FlatList
        data={data}
        className="p-5"
        renderItem={({ item, index }) => {
          const detail = findPackageById(
            item.tripId,
            item.transpo_type.toLowerCase(),
          );
          return (
            <View
              className="bg-textColorDark mb-5 overflow-hidden rounded-tl-lg rounded-tr-lg"
              key={index}
            >
              <View className="">
                <View className="absolute top-4 z-10 ">
                  <Text className="text-bgWhite bg-primary p-2">
                    {moment(item.dateScheduled).format("MMMM D, YYYY")}
                  </Text>
                  <View className="bg-blueColor px-2 py-1">
                    <Text className="text-bgWhite ">{item.status}</Text>
                  </View>
                </View>
                <Image
                  source={{
                    uri:
                      "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
                      detail?.image,
                  }}
                  resizeMethod="auto"
                  style={{
                    height: 200,
                  }}
                />
              </View>
              <View className="flex-row justify-between space-x-1 space-y-2 p-3">
                <View className="w-[75%]">
                  <Text
                    className="text-bgWhite font-poppinsReg text-xl"
                    numberOfLines={2}
                  >
                    {detail?.name}
                  </Text>
                  <View className="space-y-[-5]">
                    <Text className="font-poppinsReg text-[#e3d7d7] ">
                      Passenger: {item.passengers_total_count} /{" "}
                      {item.passenger_capacity} people
                    </Text>
                    <View className="flex flex-row items-center  space-x-1">
                      <Text className="font-poppinsReg text-[#e3d7d7] ">
                        Status: {item.status}
                      </Text>
                    </View>
                  </View>
                  <Text className="font-poppinsReg text-primary">
                    "We need
                    <Text className="font-Poppins_700Bold">
                      &nbsp;
                      {item.passenger_capacity - item.passengers_total_count}
                      &nbsp;
                    </Text>
                    more..."
                  </Text>
                </View>
                <TouchableOpacity
                  className="bg-primary rounded-md p-5"
                  onPress={() => {
                    navigation.push({
                      pathname: "/schedule/jointrip",
                      params: {
                        place: item.places,
                        tripId: item.tripId,
                        bookingId: item.id,
                        transpo_type: item.transpo_type,
                        capacity: item.passenger_capacity,
                        currentPassengerCount: item.passengers_total_count,
                        status: item.status,
                        schedule: moment(item.dateScheduled).format(
                          "MMMM D, YYYY",
                        ),
                      },
                    });
                  }}
                >
                  <Ionicons
                    name="arrow-forward-outline"
                    size={24}
                    color="#fff"
                  />
                </TouchableOpacity>
              </View>
            </View>
          );
        }}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

const BookingList = () => {
  const { tagsNav } = useLocalSearchParams();

  if (typeof tagsNav != "string") return;

  const { data } = api.schedule.fetchByTranspoType.useQuery({
    transpo_type: tagsNav,
  });
  return (
    <SafeAreaView>
      <TopBarHeader title="Join a Trip" />
      <View>
        {!data ? (
          <Text className="font-poppinsReg text-center">Loading</Text>
        ) : (
          <JoinCard data={data} tagsNav={tagsNav} />
        )}
      </View>
    </SafeAreaView>
  );
};

export default BookingList;
