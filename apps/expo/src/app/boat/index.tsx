import React, { Fragment, useEffect, useState } from "react";
import {
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter } from "expo-router";
import { useSelector } from "react-redux";

import { api } from "~/utils/api";
import Header, { HeaderBar } from "~/components/Header";
import LoadingScreen from "~/components/Loading";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import PlaceModal from "~/components/modal/PlaceModal";
import HopppingPassenger from "~/components/schedule/HopinPassenger";
import ItemPlaceList from "~/components/schedule/ItemPlaceList";
import ItemCardSkeleton from "~/components/skeleton/boat/ItemCardSkeleton";
import { TripList } from "~/helper/type";
import useDebounce from "~/hooks/useDebounce";
import ConfirmBox from "../../components/ConfirmBox";
import ItemCard from "../../components/ItemCard";
import SearchBar from "../../components/SearchBar";
import { boatPackageData, primaryColor } from "../../helper/const";

const PlacesList = () => {
  const [data, setData] = useState<TripList[]>(boatPackageData);

  return (
    <View className="my-5 flex flex-row flex-wrap justify-between gap-y-4">
      {data.length <= 0 ? (
        <View className="border-primary flex w-full flex-row items-center justify-center rounded-md border border-solid">
          <Ionicons
            name="information-circle-outline"
            size={18}
            color={"#F79327"}
          />
          <Text className="font-Poppins_700Bold m-5 ml-1 text-center">
            No data available
          </Text>
        </View>
      ) : (
        data?.map((result: TripList, index: number) => {
          return (
            <View
              className=" w-full overflow-hidden rounded-xl"
              key={`allplaces_${index}`}
            >
              <ItemCard
                name={result.name}
                location={
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "
                }
                image={{
                  uri:
                    "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
                    result.image,
                }}
                data={result}
                id={result.id}
                tags="boat"
              />
            </View>
          );
        })
      )}
    </View>
  );
};
const BoatScreen = () => {
  const navigation = useRouter();
  const navigateToPlace = () => {
    navigation.push("/boat/place");
  };

  return (
    <SafeAreaView className="bg-[#fff]" style={styles.droidSafeArea}>
      {/* Changes page title visible on the header */}

      {/* ===== MODAL BOX ===== */}
      <View>
        <TopBarHeader title="Schedule a Boat Trip" />
      </View>

      {/* ===== SEARCH BOX ===== */}
      {/* <LinearGradient
        className="  items-center justify-center rounded-lg p-2 pb-[18] pt-[18]"
        colors={["#ffffff", "transparent"]}
      >
        <View className="flex flex-row p-5 pb-0">
          <SearchBar searchValue={search} setSearch={setSearch} />
        </View>
      </LinearGradient> */}

      {/* ===== CONTENT ===== */}
      <ScrollView className="w-full space-y-5 px-5">
        <HopppingPassenger tagsFrom="boat" />
        <PlacesList />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: StatusBar.currentHeight,
  },
  textShadow: {
    textShadowColor: "#e3e6e9c2",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 1,
  },
  boxShadow: {
    shadowColor: "##151616c2",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 1.27,
    shadowRadius: 5.65,
    elevation: 6,
  },
});

export default BoatScreen;
