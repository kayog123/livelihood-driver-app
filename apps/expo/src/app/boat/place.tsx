import { Fragment, useEffect, useRef, useState } from "react";
import {
  Animated,
  Dimensions,
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Stack, useRouter, useSearchParams } from "expo-router";
import { StatusBar } from "expo-status-bar";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";

import { api } from "~/utils/api";
import LoadingScreen from "~/components/Loading";
import NotificationBar from "~/components/NotificationBar";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import { primaryColor } from "~/helper/const";
import { useIdExists } from "~/hooks/useIdExist";
import {
  removePlacesToVisitItem,
  setPlacesToVisit,
  setSelectedPlace,
  updatePlaceTags,
} from "~/redux/reducer/placesSlice";
import { RootState } from "~/redux/store";
import ConfirmBox from "../../components/ConfirmBox";

type SelectedProps = {
  id: number;
  tag_name: string;
  price: number;
  description?: string;
  active: boolean; //database if use or not
  show?: boolean; // for setting active
};

const BoatItem = () => {
  const navigation = useRouter();
  const scrollX = new Animated.Value(0);
  const { width, height } = Dimensions.get("screen");
  const [tags, setTags] = useState<SelectedProps[]>([]);
  const { selectedPlace, placesToVisit } = useSelector(
    (state: RootState) => state.places,
  );

  const verifyAdded = useIdExists(placesToVisit, selectedPlace, "id");
  const dispatch = useDispatch();

  const AnimatedView = Animated.createAnimatedComponent(View);

  const animatedValue = useRef(new Animated.Value(0)).current;
  const spinValue = useRef(new Animated.Value(0)).current;
  const translateY = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 5],
  });
  const animation = Animated.loop(
    Animated.sequence([
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]),
  );

  const photos = [
    // "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
    //   selectedPlace.image,
    {
      uri:
        "https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/" +
        selectedPlace.image,
    },
    require("./../../../assets/boat-trip.jpg"),
    require("./../../../assets/boat-trip.jpg"),
    require("./../../../assets/boat-trip.jpg"),
    require("./../../../assets/boat-trip.jpg"),
    // { uri: 'https://cdn.skillflow.io/resources/img/skillflowninja.png' }
  ];

  useEffect(() => {
    animation.start();
  }, [animation]);
  return (
    <SafeAreaView style={styles.droidSafeArea} className="flex w-full">
      {/* <Stack.Screen options={{ title: "Login", headerShown: false }} /> */}
      <TopBarHeader title="Place Details" />

      <ScrollView
        className="bg-primary flex-1"
        horizontal={true}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
        scrollEventThrottle={16}
      >
        {photos.map((source, i) => {
          return (
            <Image
              key={i} // we will use i for the key because no two (or more) elements in an array will have the same index
              style={{ width, height: height - StatusBar.length }}
              source={source}
              alt="images"
            />
          );
        })}
      </ScrollView>
      <View className="absolute bottom-0 w-full bg-[#1c1e216e] p-5">
        <Text
          style={styles.textShadow}
          className="font-Poppins_700Bold text-primary text-xl"
        >
          Balicasag Island
        </Text>
        <Text className="font-poppinsReg text-md text-bgWhite">
          🕒 Est. Time Travel: 1hr. 30 min.
        </Text>
        <Text className="font-poppinsReg text-md text-bgWhite mt-3">
          🍁Description:
        </Text>
        <Text
          className="font-poppinsReg text-md text-bgWhite"
          numberOfLines={4}
        >
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
        <Pressable className="absolute right-4 top-4">
          <Ionicons name="chevron-up" size={28} color="#fff" />
        </Pressable>
      </View>
      {/* <View className="flex w-full flex-1 items-center  px-5 pb-10">
          <View className="bg-bgWhite border-primary relative top-[-50] ml-10  mr-10  w-full items-center rounded-xl  border p-2">
            <Text className="font-Poppins_700Bold text-textColorDark text-xl">
              Balicasag Island
            </Text>
            <Text className="font-poppinsReg text-md">Address</Text>
            <Text className="font-poppinsReg text-md">
              Est. Time Travel: 1hr. 30 min.
            </Text>
            <View className="flex flex-row items-center justify-center">
              <Text className="font-poppinsReg text-textColorDark mr-1 text-xl">
                5.0
              </Text>
              <Ionicons name="star" size={24} color="#FFD95A" />
              <Ionicons name="star" size={24} color="#FFD95A" />
              <Ionicons name="star" size={24} color="#FFD95A" />
              <Ionicons name="star" size={24} color="#FFD95A" />
            </View>
          </View>

          <View className="w-full">
            <View className="mt-5">
              <Text className="text-textColorDark font-poppinsReg bottom-2 mb-2 text-center text-2xl">
                Place Event / Add On's
              </Text>
              <Text className="font-poppinsReg text-center">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500
              </Text>
            </View>
          </View>
        </View> */}
      {/* <ConfirmBox
        cancelName="Back"
        cancelPress={() => {
          navigation.back();
        }}
        confirmName={`${verifyAdded ? "Update" : "Add to list"}`}
        confirmPress={() => {
          if (verifyAdded) {
            const updatedPlacesEvenTags = placesToVisit.map((result) => {
              if (result.id == selectedPlace.id) {
                result.eventTags = tags.filter((result) => {
                  if (result.show) {
                    return {
                      id: result.id,
                      tag_name: result.tag_name,
                    };
                  }
                });
                return result;
              }
            });

            //dispatch(setPlacesToVisit(updatedPlacesEvenTags));
            ToastAndroid.show("Updated!", ToastAndroid.SHORT);
          } else {
            ToastAndroid.show("Place added to your list!", ToastAndroid.SHORT);
            const copySelectedPlaces = selectedPlace;

            const updatedSelectedPlaces = {
              ...copySelectedPlaces, // spread the original object attributes
              eventTags: tags.filter((result) => {
                if (result.show) {
                  return {
                    id: result.id,
                    tag_name: result.tag_name,
                  };
                }
              }), // add a new attribute
            };

            dispatch(
              setPlacesToVisit([...placesToVisit, updatedSelectedPlaces]),
            );
          }
        }}
      /> */}
    </SafeAreaView>
  );
};

export default BoatItem;

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
  textShadow: {
    textShadowColor: "#e3e6e9c2",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 0.5,
  },
});
