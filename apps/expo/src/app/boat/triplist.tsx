import { useEffect, useState } from "react";
import { Dimensions, SafeAreaView, ScrollView, Text, View } from "react-native";
import * as Location from "expo-location";
import { Stack, useRouter, useSearchParams } from "expo-router";
import { useDispatch, useSelector } from "react-redux";

import BoxWithIcon from "~/components/BoxWithIcon";
import ConfirmBox from "~/components/ConfirmBox";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import HereMapView from "~/components/map/Here/HereMapView";
import HereMultipleMarkMaps from "~/components/map/Here/HereMultipleMarkMaps";
import MapsView from "~/components/map/MapsView";
import CarType from "~/components/schedule/CarType";
import HopppingPassenger from "~/components/schedule/HopinPassenger";
import ItemPlaceList from "~/components/schedule/ItemPlaceList";
import PassengerCounter from "~/components/schedule/PassengerCounter";
import { BoatTranpoType, LandTranpoType, boatPrice } from "~/helper/const";
import { PlaceEvent, PlaceList } from "~/helper/type";
import { setPlacesToVisit } from "~/redux/reducer/placesSlice";
import { RootState } from "~/redux/store";

const BreakDownPrev = ({ price }: { price: number }) => {
  return (
    <View className="bg-textColorDark flex w-full flex-row p-3">
      <View className="w-full">
        <View className=" bg-[#01192ae3]pg w-full items-center">
          <Text className="font-poppinsReg text-[#dfdcdc] ">Total Amount</Text>
          <View className="flex flex-row items-center space-x-2">
            <View>
              <Text className="font-poppinsReg text-3xl text-[#fff] ">
                ₱ {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
              </Text>
            </View>
            <Text className="font-poppinsReg text-bgWhite text-sm">
              / person
            </Text>
          </View>
          <Text className="font-poppinsReg text-bgWhite">
            (Entrance and Activities Fee Only)
          </Text>
        </View>
      </View>
    </View>
  );
};

type CurrentGeoCode = {
  latitude: number;
  longitude: number;
};
type TripCoordinatesProp = {
  latitude: number;
  longitude: number;
  place_name: string;
};
const TripList = () => {
  const initialVal = LandTranpoType[0];
  const initialValue =
    typeof LandTranpoType[0] !== "undefined" ? LandTranpoType[0] : {};
  const height = Dimensions.get("screen").height / 3;
  const { selectFrom, tagsNav } = useSearchParams();
  const { selectedPlace } = useSelector((state: RootState) => state.places);
  const dispatch = useDispatch();
  const navigation = useRouter();
  const [placesData, setPlacesData] = useState<PlaceList[]>([]);
  const [tripCoordinate, setTripCoordinates] = useState<TripCoordinatesProp[]>(
    [],
  );
  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [selectedTranspo, setSelectedTranspo] = useState<string>(
    LandTranpoType[0]?.id !== undefined ? LandTranpoType[0]?.id : "",
  );
  const capacity =
    tagsNav == "boat"
      ? BoatTranpoType[0]?.capacity[1]
      : LandTranpoType[0]?.capacity[1];

  const [transpoCapacity, setTranspoCapacity] = useState<number>(
    capacity != undefined ? capacity : 0,
  );

  const [selectedTranspoRate, setSelectedTranspoRate] = useState<number>(
    tagsNav == "boat"
      ? boatPrice
      : LandTranpoType[0]?.rate !== undefined
      ? LandTranpoType[0]?.rate
      : 0,
  );
  const [passenger, setPassenger] = useState(1);
  const [placeActivityFee, setPlaceActivitiesFee] = useState<number>(0);
  const [preLoad, setPreLoad] = useState(false);
  const [allowOther, setAllowOther] = useState<boolean>(false);
  const [currentLocation, setCurrentLocation] = useState<CurrentGeoCode>();
  const [locErrMessage, setLocErrorMessage] = useState<string>("");
  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setLocErrorMessage("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});

      setCurrentLocation({
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      });
    })();
  }, []);
  useEffect(() => {
    let fee = 0;
    let tripGeoCodes: TripCoordinatesProp[] = [];
    const dataUpdate = selectedPlace.places?.map(
      (place: PlaceList, index: number) => {
        tripGeoCodes.push({
          latitude: place.lat,
          longitude: place.long,
          place_name: place.name,
        });
        if (place.events) {
          place.events = place.events.map(
            (events: PlaceEvent, index: number) => {
              if (events.fee && typeof events.fee == "number") {
                fee = fee + events.fee;
              }
              return { ...events, show: true };
            },
          );
        }
        if (place.fee && typeof place.fee == "number") {
          fee = fee + place.fee;
        }
        return { ...place, show: true };
      },
    );
    setTripCoordinates(tripGeoCodes);
    setPlaceActivitiesFee(fee);
    setPlacesData(dataUpdate);
    setPreLoad(true);
  }, []);

  useEffect(() => {
    if (preLoad) {
      computeTotalAmount();
    }
  }, [placesData]);

  const computeTotalAmount = () => {
    let fee = 0;
    placesData?.map((place: PlaceList, index: number) => {
      if (place.show == true) {
        if (place.fee && typeof place.fee == "number") {
          fee = fee + place.fee;
        }
        place.events?.map((eventsResult) => {
          if (eventsResult.show == true) {
            if (eventsResult.fee && typeof eventsResult.fee == "number") {
              fee = fee + eventsResult.fee;
            }
          }
        });
      }
    });
    setPlaceActivitiesFee(fee);
  };

  const togglePlace = (arrayIndex: number, value: boolean) => {
    const newPlaceData = [...placesData];
    let tripGeoCodes: TripCoordinatesProp[] = [];
    const newData = newPlaceData.map((data, index) => {
      if (arrayIndex == index) {
        if (value) {
          tripGeoCodes.push({
            latitude: data.lat,
            longitude: data.long,
            place_name: data.name,
          });
        }
      } else {
        tripGeoCodes.push({
          latitude: data.lat,
          longitude: data.long,
          place_name: data.name,
        });
      }

      if (arrayIndex == index) {
        return {
          ...data,
          show: value,
        };
      }
      return data;
    });
    console.log(tripGeoCodes);
    setTripCoordinates(tripGeoCodes);
    setPlacesData(newData);
  };

  const toggleEvents = (placeId: number, eventId: number, value: boolean) => {
    const newPlaceData = [...placesData];
    const newValue = newPlaceData.map((data) => {
      if (data.id == placeId) {
        const eventsDetails = data.events?.map((eventsResult) => {
          if (eventsResult.id == eventId) {
            return { ...eventsResult, show: value };
          }
          return eventsResult;
        });
        return {
          ...data,
          events: eventsDetails,
        };
      }
      return data;
    });
    setPlacesData(newValue);
  };

  return (
    <SafeAreaView className="h-full w-full">
      <ScrollView>
        <TopBarHeader title="Choose place" />
        {/* <MapsView height={height} /> */}

        {locErrMessage == "" && currentLocation != undefined ? (
          <HereMultipleMarkMaps
            height={height}
            initialPickUpName="Test"
            initialRegionLat={currentLocation?.latitude}
            initialRegionLong={currentLocation?.longitude}
            tripCoordinate={tripCoordinate}
          />
        ) : (
          <Text className="font-poppinsReg text-bgWhite"></Text>
        )}

        <BoxWithIcon icons="list">
          {selectedPlace.description.length > 0 && (
            <View className="mx-5 my-5">
              {selectedPlace.description.map((items) => (
                <Text className="text-textColorDark font-poppinsReg">
                  {items}
                </Text>
              ))}
            </View>
          )}
        </BoxWithIcon>

        <PassengerCounter
          passenger={passenger}
          setPassenger={setPassenger}
          min={1}
          max={tagsNav == "boat" ? BoatTranpoType[0]?.capacity[1] : 18}
          setAllowOther={setAllowOther}
          allowOther={allowOther}
          setTranspoCapacity={setTranspoCapacity}
        />
        {tagsNav == "vans" && (
          <CarType
            setSelectedTranspoRate={setSelectedTranspoRate}
            people={passenger}
            setSelectedTranspo={setSelectedTranspo}
            selectedTranspo={selectedTranspo}
            setTranspoCapacity={setTranspoCapacity}
          />
        )}
        <View className="px-5">
          <TitleText title={`Routes of ${selectFrom}`} />
        </View>
        <ItemPlaceList
          data={placesData}
          togglePlace={togglePlace}
          toggleEvents={toggleEvents}
        />
      </ScrollView>
      <BreakDownPrev price={placeActivityFee} />
      <ConfirmBox
        cancelPress={() => {
          navigation.back();
        }}
        confirmName="Continue"
        cancelName="Back"
        confirmPress={() => {
          dispatch(setPlacesToVisit(placesData));
          let params: {
            tagsNav?: string | undefined | string[];
            transpo_rate?: number;
            passengers: number;
            place_activity_fee: number;
            land_transpo_type: string;
            allowOther: boolean;
            tripId: number;
            transpoCapacity: number;
          } = {
            tagsNav: tagsNav,
            transpo_rate: selectedTranspoRate,
            passengers: passenger,
            place_activity_fee: placeActivityFee,
            land_transpo_type: selectedTranspo,
            allowOther: allowOther,
            tripId: selectedPlace.id,
            transpoCapacity: transpoCapacity,
          };
          navigation.push({
            pathname: "/schedule/pickup",
            params: params,
          });
        }}
      />
    </SafeAreaView>
  );
};

export default TripList;
