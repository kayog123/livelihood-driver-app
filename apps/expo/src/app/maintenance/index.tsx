import { Dimensions, SafeAreaView, Text, View } from "react-native";
import LottieView from "lottie-react-native";

import TopBarHeader from "~/components/TopBarHearder";

const Maintenance = () => {
  const { width } = Dimensions.get("screen");
  const logoSize = width / 1.5;
  return (
    <SafeAreaView>
      <TopBarHeader title="Under Maintenance" />
      <View className="h-full w-full  items-center justify-center">
        <LottieView
          autoPlay
          style={{
            width: logoSize,
            height: logoSize,
          }}
          source={require("./../../../assets/maintenance.json")}
        />
        <Text className="text-textColorDark font-poppinsReg text-center">
          This page is under maintenance!
        </Text>
        {/* <Text className="text-textColorDark font-poppinsReg text-center">
          You can claim and activate you reward here!
        </Text>
        <View className="m-5 mt-5 flex flex-row">
          <TextInput
            value=""
            placeholder="PROMO CODE"
            className="bg-colorDarkOutline  font-poppinsReg flex-grow rounded-bl-lg rounded-tl-lg p-4 shadow-md"
          />
          <TouchableOpacity className="bg-textColorDark items-center justify-center rounded-br-lg rounded-tr-lg p-3">
            <Text className="font-poppinsReg text-center uppercase text-[#fff] ">
              Activate
            </Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </SafeAreaView>
  );
};

export default Maintenance;
