import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { Dropdown } from "react-native-element-dropdown";
import { ScrollView } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Location from "expo-location";
import { useRouter, useSearchParams } from "expo-router";
import { useAuth } from "@clerk/clerk-expo";
import { Skeleton } from "moti/skeleton";
import { useSelector } from "react-redux";

import { api } from "~/utils/api";
import TravelServiceOpt from "~/components/bookings/TravelServiceOpt";
import BoxWithIcon from "~/components/BoxWithIcon";
import DialogBox from "~/components/DialogBox";
import GeoMapUpdater from "~/components/map/GeoMapUpdater";
import HereMapView from "~/components/map/Here/HereMapView";
import PlaceModal from "~/components/modal/PlaceModal";
import NotificationBar from "~/components/NotificationBar";
import TitleText from "~/components/TitleText";
import TopBarHeader from "~/components/TopBarHearder";
import {
  FAIR,
  HERE_MAP_API_KEY,
  primaryColor,
  textColorDark,
} from "~/helper/const";
import { RootState } from "~/redux/store";
import ConfirmBox from "../../components/ConfirmBox"; 
import { TranspoType } from "~/helper/const";
const SearchLocationInput = ({
  pickup_location,
  setPickUpLocation,
  drop_off_location,
  setMapModal,
  setMapGeo,
}: {
  pickup_location: PickUpProps;
  drop_off_location: PickUpProps | undefined;
  setPickUpLocation: (value: PickUpProps) => void;
  setMapModal: (value: boolean) => void;
  setMapGeo: (value: MapGeoProps) => void;
}) => {
  return (
    <View className="flex-column m-0 flex w-full gap-2 p-3">
      <View className="bg-bgWhiteShadow flex w-full flex-row items-center justify-items-center rounded-lg">
        <View className="flex-6 flex flex-row p-1">
          <TouchableOpacity
            className="bg-secondaryLight rounded-lg p-3"
            // onPress={() => {
            //   setMapModal(true);
            //   setMapGeo("pick_up");
            // }}
          >
            <Ionicons color="white" name="location-outline" size={24} />
          </TouchableOpacity>
          <TextInput
            value={pickup_location.pick_up_name}
            placeholder="Pick up at"
            className="rounded-xxl bg-bgWhiteShadow text-textColorDark  h-12 w-full flex-auto rounded-lg p-3"
            placeholderTextColor={textColorDark}
          />
          <View className={`rounded-full  p-3`}>
            <Ionicons color={primaryColor} name="search-outline" size={24} />
          </View>
        </View>
        <View className="flex-2"></View>
      </View>
      <View className="bg-bgWhiteShadow flex w-full flex-row items-center justify-items-center rounded-lg">
        <View className="flex-6 flex flex-row p-1">
          <TouchableOpacity
            className="bg-secondaryLight rounded-lg p-3"
            onPress={() => {
              setMapModal(true);
              setMapGeo("drop_off");
            }}
          >
            <Ionicons color="white" name="location-outline" size={24} />
          </TouchableOpacity>
          <TextInput
            value={
              drop_off_location != undefined
                ? drop_off_location.pick_up_name
                : ""
            }
            placeholder="Where do you want to go?"
            className={`rounded-xxl bg-bgWhiteShadow text-textColorDark  h-12 w-full flex-auto rounded-lg p-3`}
            placeholderTextColor={textColorDark}
          />
          <View className={`rounded-full  p-3`}>
            <Ionicons color={primaryColor} name="locate-outline" size={24} />
          </View>
        </View>
        <View className="flex-2"></View>
      </View>
    </View>
  );
};

const BreakDownPrev = ({
  fair = 0,
  transpoSelected,
}: {
  fair: number;
  transpoSelected: string;
}) => {
  const fairAmount = transpoSelected == "tricycle" ? FAIR.TRICYCLE : FAIR.MOTOR;
  return (
    <View className="bg-textColorDark flex w-full flex-row p-3">
      <View className="w-full">
        <View className=" bg-[#01192ae3]pg w-full items-center">
          <Text className="font-poppinsReg text-[#dfdcdc] ">Total Fair</Text>
          <Text className="font-poppinsReg text-3xl text-[#fff] ">
            ₱ {fair.toFixed(2)}
          </Text>
          <Text className="font-poppinsReg text-[#dfdcdc] ">
            (₱ {fairAmount.toFixed(2)} / km)
          </Text>
        </View>
      </View>
    </View>
  );
};

type NotifyBarProps = {
  show: boolean;
  message: string;
  type: "success" | "error" | "warning" | "waiting";
};

type PickUpProps = {
  lat: number;
  long: number;
  pick_up_name: string;
};

type MapGeoProps = "drop_off" | "pick_up";
const Book = () => {
  const [notes, setNotes] = useState<string>("");
  const [noteFocus, setNoteFocus] = useState<boolean>(false);
  const [alertBox, setAlertBox] = useState<boolean>(false);
  const [transpoSelected, setSelectedTranspo] = useState<string>("");
  const [mapModal, setMapModal] = useState<boolean>(false);
  const [mapGeo, setMapGeo] = useState("drop_off");
  const [errorMsg, setErrorMsg] = useState<string | null>(null);
  const [pickup_location, setPickUpLocation] = useState<PickUpProps>({
    lat: 1.23323,
    long: 9.2323,
    pick_up_name: "", //address or name on the geolocation
  });

  const [fair, setFair] = useState<number>(0);
  const [distance, setDistance] = useState<number>(0);
  const [km, setKM] = useState(0);
  const [drop_off_location, setDropOffLocation] = useState<PickUpProps>();
  const [reloadMap, setReloadMap] = useState<boolean>(true);

  const [notifyBar, setNotifyBar] = useState<NotifyBarProps>({
    show: false,
    message: "",
    type: "success",
  });
  const { selectedBusiness } = useSelector(
    (state: RootState) => state.business,
  );
  const { isLoaded, userId } = useAuth();
  const navigation = useRouter();
  const params = useSearchParams();
  const { choice, tagsFrom } = params;

  // if (typeof choice != "string") return;
  if (!isLoaded || !userId) {
    return null;
  }
  const { height } = Dimensions.get("screen");
  const height_logo = height * 0.14;

  useEffect(() => {
    if (checkBusinessStatus()) {
      setPickUpLocation({
        lat: selectedBusiness.lat,
        long: selectedBusiness.long,
        pick_up_name: selectedBusiness.name,
      });
    }
  }, []);

  useEffect(() => {
    setReloadMap(false);
    const timer = setTimeout(() => {
      setReloadMap(true);
    }, 1000);

    return () => {
      clearTimeout(timer);
    };
  }, [pickup_location, drop_off_location]);

  useEffect(()=>{ 
    if (typeof choice == "string"){
      setSelectedTranspo(choice)
    }
  },[choice])
  useEffect(() => {
    (async () => {
      if (checkBusinessStatus()) {
        setPickUpLocation({
          lat: selectedBusiness.lat,
          long: selectedBusiness.long,
          pick_up_name: selectedBusiness.name,
        });
      } else {
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== "granted") {
          setErrorMsg("Permission to access location was denied");
          return;
        }

        let location = await Location.getCurrentPositionAsync({});

        setPickUpLocation(
          await fetchGeoDetails(
            location.coords.latitude,
            location.coords.longitude,
          ),
        );
      }
    })();
  }, []);

  const fetchGeoDetails = async (lat: number, long: number) => {
    return await fetch(
      `https://revgeocode.search.hereapi.com/v1/revgeocode?at=${lat}%2C${long}&lang=en-US&apiKey=${HERE_MAP_API_KEY}`,
      {
        method: "get",
      },
    )
      .then((response) => response.json())
      .then((result) => {
        return {
          lat: result.items[0].access[0].lat,
          long: result.items[0].access[0].lng,
          pick_up_name: result.items[0].address.label,
        };
      });
  };

  const { mutate, error } = api.booking.create.useMutation({
    async onMutate(newPost: any) {
      setNotifyBar({
        show: true,
        message: "Creating schedule! Please wait..",
        type: "waiting",
      });
    },
    async onSuccess() {
      setAlertBox(true);
    },
    async onError(err: any, newPost: any, ctx: any) {
      setNotifyBar({
        show: true,
        message: err?.message,
        type: "error",
      });
    },
    onSettled() {
      const timer = setTimeout(() => {
        setNotifyBar({
          show: false,
          message: "",
          type: "waiting",
        });
        clearTimeout(timer);
      }, 5000);
    },
  });

  const onSubmitBooking = () => {
    if (drop_off_location == undefined) {
      setNotifyBar({
        show: true,
        type: "warning",
        message: "Please select a drop off location on the map",
      });
      return;
    }

    const business = {
      title: "=== TAGB LIVELIHOOD APP ==",
      businessDetails: {
        businessName: selectedBusiness.name,
        address: selectedBusiness.address,
        image: selectedBusiness.image,
      },
    };
    const description = `${JSON.stringify(business)}==END==${notes}`;
    let paramsData = {
      userId: userId,
      places: `${drop_off_location.lat},${drop_off_location.long}`, //geolocation of dropoff
      transpo_type: transpoSelected == 'tricycle' ? TranspoType.TRICYCLE : TranspoType.MOTORCYCLE,
      transpo_rate: fair,
      pickup_lat: pickup_location.lat,
      pickup_long: pickup_location.long,
      pickup_area_name: pickup_location.pick_up_name,
      passenger_capacity: 1,
      description: description,
      dateScheduled: new Date(),
    };

    mutate(paramsData);
  };

  const checkBusinessStatus = () => {
    return tagsFrom != undefined && tagsFrom == "business";
  };

  return (
    <SafeAreaView className="bg-bgWhiteShadow" style={styles.droidSafeArea}>
      <PlaceModal
        modalVisible={mapModal}
        setModalVisible={setMapModal}
        title="Specify Location"
      >
        <GeoMapUpdater
          setGeolocation={
            mapGeo == "drop_off" ? setDropOffLocation : setPickUpLocation
          }
          setMapModal={setMapModal}
          initialLat={pickup_location.lat}
          initialLong={pickup_location.long}
          initialPickUpName={pickup_location.pick_up_name}
        />
      </PlaceModal>
      <TopBarHeader title="Confirm Booking" />
      <View className="z-10">
        <NotificationBar
          showNofify={notifyBar.show} //notifyBar.show
          message={notifyBar.message} //notifyBar.message
          type={notifyBar.type} //notifyBar.type
        />
      </View>
      {/* alertBox */}
      {alertBox && (
        <DialogBox
          title="Thank you"
          messageSub="You have successfully create a booking."
          additonalInfo={{
            "Pick up point": pickup_location.pick_up_name,
            "Drop point": drop_off_location?.pick_up_name,
            "Total KM": km,
            "Total Amount": "₱ " + fair.toFixed(2),
          }}
        />
      )}
      <ScrollView className="">
        {checkBusinessStatus() && (
          <View className="bg-primary border-primary m-5 mb-0 rounded-md border p-3">
            <Text className="font-poppinsReg text-bgWhite text-center text-lg">
              Pick up Location
            </Text>
            <View className="bg-primary border-primaryColorShadow mt-4 flex-row space-x-2 rounded-md border">
              <Image
                className="bg-primary w-[30%] rounded-md"
                source={require("./../../../assets/business/bread.jpg")}
                style={{ width: height_logo, height: height_logo }}
                alt=""
              />
              <View className="flex w-[60%] p-2">
                <Text
                  className="font-Poppins_700Bold text-bgWhite text-xl"
                  numberOfLines={2}
                >
                  {selectedBusiness.name}
                </Text>
                <Text
                  className="font-poppinsReg text-bgWhiteShadow"
                  numberOfLines={2}
                >
                  {selectedBusiness.address}
                </Text>
              </View>
            </View>
          </View>
        )}
        <View className="bg-bgWhite m-5 mb-0 rounded-lg">
          <SearchLocationInput
            pickup_location={pickup_location}
            setPickUpLocation={setPickUpLocation}
            drop_off_location={drop_off_location}
            setMapModal={setMapModal}
            setMapGeo={setMapGeo}
          />
          <TravelServiceOpt
            transpoSelected={transpoSelected}
            setSelectedTranspo={setSelectedTranspo}
          />
        </View>
        <View className="rounded-xl p-5">
          {reloadMap ? (
            <HereMapView
              height={200}
              initialRegionLong={pickup_location.long}
              initialRegionLat={pickup_location.lat}
              initialPickUpName={pickup_location.pick_up_name}
              dropoffLat={drop_off_location?.lat}
              dropoffLong={drop_off_location?.long}
              dropoffPickUp={drop_off_location?.pick_up_name}
              setFair={setFair}
              setKM={setKM}
              transpoSelected={transpoSelected}
            />
          ) : (
            <Skeleton colorMode="light" width={"100%"} height={200} />
          )}
        </View>
        <BoxWithIcon icons="pencil">
          <View className="gap-y-2 rounded-md px-5 py-3 ">
            <View className="px-5">
              <TitleText title="Leave an important note" textSize="text-md" />
            </View>
            <TextInput
              editable
              multiline={true}
              numberOfLines={5}
              onFocus={() => setNoteFocus(true)}
              onBlur={() => setNoteFocus(false)}
              className={`bg-bgWhiteShadow font-poppinsReg border-${
                noteFocus ? "primary" : "bgWhiteShadow"
              } w-full rounded-md border px-3 py-5 `}
              placeholder="Leave your note here.."
              value={notes}
              onChangeText={setNotes}
              style={{ textAlignVertical: "top" }}
            />
          </View>
        </BoxWithIcon>
      </ScrollView>
      <View className=" bottom-0 w-full">
        <BreakDownPrev fair={fair} transpoSelected={transpoSelected} />
        <ConfirmBox
          cancelName="Cancel"
          cancelPress={() => {
            navigation.back();
          }}
          confirmName="Confirm"
          confirmPress={onSubmitBooking}
        />
      </View>
    </SafeAreaView>
  );
};

const { width } = Dimensions.get("screen");

const styles = StyleSheet.create({
  droidSafeArea: {
    flex: 1,
    //paddingTop: Platform.OS === "android" ? 25 : 0,
  },
});

export default Book;
