import React, { useEffect } from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Stack } from "expo-router";
import { StatusBar } from "expo-status-bar";
import { useAuth } from "@clerk/clerk-expo";
import {
  Poppins_100Thin,
  Poppins_200ExtraLight,
  Poppins_400Regular,
  Poppins_700Bold,
  Poppins_800ExtraBold,
  useFonts,
} from "@expo-google-fonts/poppins";
import { PusherEvent } from "@pusher/pusher-websocket-react-native";
import { useDispatch, useSelector } from "react-redux";

import { TRPCProvider } from "~/utils/api";
import { connectPusher, subscribeChatRoom } from "~/utils/PusherServer";
import { incrementMessageCount } from "~/redux/reducer/messageSlice";
import { RootState } from "~/redux/store";

// This is the main layout of the app
// It wraps your pages with the providers they need
const RootLayout = () => {
  const [fontsLoaded] = useFonts({
    Poppins_100Thin,
    Poppins_200ExtraLight,
    Poppins_400Regular,
    Poppins_700Bold,
    Poppins_800ExtraBold,
  });
  const { userId, isLoaded } = useAuth();
  const dispatch = useDispatch();
  const { countMessage } = useSelector((state: RootState) => state.message);

  if (!userId && !isLoaded) return;

  useEffect(() => {
    connectPusher();
    subscribeChatRoom(`user__${userId}-chatrooms`, (event: PusherEvent) => {
      if (event.eventName == `chatrooms-count`) {
        dispatch(incrementMessageCount());
      }
    });
  }, []);

  if (!fontsLoaded) {
    return null;
  } else {
    return (
      <TRPCProvider>
        <SafeAreaProvider>
          <Stack
            screenOptions={{
              headerStyle: {
                backgroundColor: "#29990f",
              },
            }}
          />
          <StatusBar />
        </SafeAreaProvider>
      </TRPCProvider>
    );
  }
};

export default RootLayout;
