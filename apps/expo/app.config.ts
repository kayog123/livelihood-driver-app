import type { ExpoConfig } from "@expo/config";

const APP_NAME = "Livelihood Driver App";

const defineConfig = (): ExpoConfig => ({
  name: APP_NAME,
  slug: "expo",
  scheme: "expo",
  version: "1.0.0",
  orientation: "portrait",
  icon: "./assets/logo.png",
  userInterfaceStyle: "light",
  splash: {
    image: "./assets/logo.png",
    resizeMode: "contain",
    backgroundColor: "#29990f",
  },
  updates: {
    fallbackToCacheTimeout: 0,
  },
  assetBundlePatterns: ["**/*"],
  ios: {
    supportsTablet: true,
    bundleIdentifier: "your.bundle.identifier",
  },
  android: {
    adaptiveIcon: {
      foregroundImage: "./assets/logo.png",
      backgroundColor: "#29990f", //"#1F104A",
    },
    config: {
      googleMaps: {
        apiKey: "AIzaSyASUSnVq961UdnrJzrRI3GmAs8B7J4r6f4",
      },
    },
    package: "com.kayog123.livelihood.driver",
  },
  extra: {
    eas: {
      //projectId: "c3f71a8d-c36e-495f-b4db-46dd977a9bab",
    },
    pusher: {
      key: "f38fb6990eb30eddfb07",
      cluster: "ap1",
      appId: "1638709",
      secret: "f23a3c7dd6661ef6255e",
    },
  },
  plugins: [
    "./expo-plugins/with-modify-gradle.js",
    [
      "expo-location",
      {
        locationAlwaysAndWhenInUsePermission: `Allow ${APP_NAME} to use your location.`,
      },
    ],
  ],
});

export default defineConfig;
