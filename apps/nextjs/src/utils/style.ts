import { cx } from "classix";
import { twMerge } from "tailwind-merge";

export type Argument = string | boolean | null | undefined;

export const tw = (...args: Argument[]) => twMerge(cx(...args));
