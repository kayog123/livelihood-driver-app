import Container from "~/components/Container";
import Layout from "~/components/Layout";

const Contact = () => {
  return (
    <Layout title="Contact">
      <Container wrapperClassName="bg-gray-100 pb-24 pt-48">
        <h1 className="text-4xl font-light">Get in touch</h1>
      </Container>
    </Layout>
  );
};

export default Contact;
