import Container from "~/components/Container";
import Layout from "~/components/Layout";

const Help = () => {
  return (
    <Layout title="Help">
      <Container wrapperClassName="bg-gray-100 pb-24 pt-48">
        <h1 className="text-4xl font-light">Ask for help</h1>
      </Container>
    </Layout>
  );
};

export default Help;
