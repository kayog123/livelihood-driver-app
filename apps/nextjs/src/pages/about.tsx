import Container from "~/components/Container";
import Layout from "~/components/Layout";

const About = () => {
  return (
    <Layout title="About">
      <Container wrapperClassName="bg-gray-100 pb-24 pt-48">
        <h1 className="text-4xl font-light">About us</h1>
      </Container>
    </Layout>
  );
};

export default About;
