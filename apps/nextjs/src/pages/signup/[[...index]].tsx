import { type GetServerSidePropsContext } from "next";
import { SignUp } from "@clerk/nextjs";
import { getAuth } from "@clerk/nextjs/server";

import Layout from "~/components/Layout";

const SignupPage = () => {
  return (
    <Layout>
      <div className="flex-1 pt-24">
        <div className="container flex flex-col items-center justify-center py-16">
          <SignUp
            path="/signup"
            routing="path"
            signInUrl="/login"
            redirectUrl="/owner"
          />
        </div>
      </div>
    </Layout>
  );
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const { userId } = getAuth(ctx.req);

  if (userId) {
    return {
      redirect: {
        destination: "/owner",
      },
    };
  }

  return {
    props: {},
  };
};

export default SignupPage;
