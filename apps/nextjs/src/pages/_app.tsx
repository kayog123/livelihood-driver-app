import type { AppProps } from "next/app";
import { ClerkProvider } from "@clerk/nextjs";

import { api } from "~/utils/api";
import Wrapper from "~/components/Wrapper";

import "../styles/globals.css";

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <ClerkProvider {...pageProps}>
      <Wrapper>
        <Component {...pageProps} />
      </Wrapper>
    </ClerkProvider>
  );
};

export default api.withTRPC(MyApp);
