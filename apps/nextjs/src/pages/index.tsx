import Link from "next/link";
import _ from "lodash";

import Container from "~/components/Container";
import Layout from "~/components/Layout";

const HomePage = () => {
  // const messages = [
  //   {
  //     id: 1,
  //     userIds: "user1,user2",
  //     message: "",
  //     isRead: "",
  //   },
  //   {
  //     id: 1,
  //     userIds: "user3,user1",
  //     message: "",
  //     isRead: "",
  //   },
  //   {
  //     id: 1,
  //     userIds: "user1,user3",
  //     message: "",
  //     isRead: "",
  //   },
  //   {
  //     id: 1,
  //     userIds: "user2,user1",
  //     message: "",
  //     isRead: "",
  //   },
  //   {
  //     id: 1,
  //     userIds: "user1,user2",
  //     message: "",
  //     isRead: "",
  //   },
  //   {
  //     id: 1,
  //     userIds: "user1,user3",
  //     message: "",
  //     isRead: "",
  //   },
  // ];
  // const groupedMessages = _.groupBy(messages, (m) => {
  //   const ids = m.userIds.split(",");
  //   const otherUserId = ids.find((id) => id !== "user1");

  //   return otherUserId;
  // });
  // console.log(messages);
  // console.log(groupedMessages);

  // const { data: chatRooms } = api.chatRoom.getChatRoomsByOwner.useQuery({
  //   ownerId: "",
  //   initialValue: []
  // });
  // const { data: chatRoomsLatestMessages } =
  //   api.message.getLatestMessagesByChatRoom.useQuery({
  //     chatRoomIds: "",
  //     enabled: !!chatRooms.length,
  //     initialValue: []
  //   });

  return (
    <Layout>
      <Container
        wrapperClassName="bg-gray-100 py-36 pb-12"
        className="container flex h-[640px] gap-16"
      >
        <div className="flex flex-1 flex-col justify-center">
          <h1 className="mb-12 text-7xl font-light leading-[1.15]">
            Behold bohol's first trip app for the boholanos
          </h1>
          <p className="mb-8 text-xl font-light">
            Download behold bohol trip app now!
          </p>
          <div className="flex gap-4">
            <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
              Google Play
            </button>
            <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
              App Store
            </button>
            <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
              App Gallery
            </button>
          </div>
        </div>

        <div className="flex basis-2/5 items-center justify-end">
          <div className="h-[560px] w-72 rounded-3xl border-8 border-black bg-white"></div>
        </div>
      </Container>

      <Container wrapperClassName="py-24 bg-gray-200">
        <h2 className="mb-24 text-center text-5xl font-light">App features</h2>

        <div className="flex flex-wrap justify-center gap-8">
          <div className="flex h-80 basis-1/4 flex-col items-center justify-center rounded-lg bg-white p-4">
            <div className="mb-4 h-32 w-32 rounded-full bg-teal-100"></div>
            <p className="mb-4 text-center text-xl font-light">Feature</p>
            <p className="text-center text-sm font-light text-gray-500">
              This is an example description of the feature.
            </p>
          </div>
          <div className="flex h-80 basis-1/4 flex-col items-center justify-center rounded-lg bg-white p-4">
            <div className="mb-4 h-32 w-32 rounded-full bg-teal-100"></div>
            <p className="mb-4 text-center text-xl font-light">Feature</p>
            <p className="text-center text-sm font-light text-gray-500">
              This is an example description of the feature.
            </p>
          </div>
          <div className="flex h-80 basis-1/4 flex-col items-center justify-center rounded-lg bg-white p-4">
            <div className="mb-4 h-32 w-32 rounded-full bg-teal-100"></div>
            <p className="mb-4 text-center text-xl font-light">Feature</p>
            <p className="text-center text-sm font-light text-gray-500">
              This is an example description of the feature.
            </p>
          </div>
          <div className="flex h-80 basis-1/4 flex-col items-center justify-center rounded-lg bg-white p-4">
            <div className="mb-4 h-32 w-32 rounded-full bg-teal-100"></div>
            <p className="mb-4 text-center text-xl font-light">Feature</p>
            <p className="text-center text-sm font-light text-gray-500">
              This is an example description of the feature.
            </p>
          </div>
          <div className="flex h-80 basis-1/4 flex-col items-center justify-center rounded-lg bg-white p-4">
            <div className="mb-4 h-32 w-32 rounded-full bg-teal-100"></div>
            <p className="mb-4 text-center text-xl font-light">Feature</p>
            <p className="text-center text-sm font-light text-gray-500">
              This is an example description of the feature.
            </p>
          </div>
        </div>
      </Container>

      <Container wrapperClassName="bg-gray-100 py-24" className="flex gap-24">
        <div className="flex w-2/5 items-center justify-center">
          <div className="h-[640px] w-80 rounded-3xl border-8 border-black bg-white"></div>
        </div>

        <div className="flex flex-1 flex-col justify-center">
          <p className="mb-4 text-teal-600">EXAMPLE TEXT</p>
          <h2 className="mb-4 text-4xl">This is just and example text</h2>
          <p className="mb-8 text-gray-500">
            This is just an example description for the above mentioned title,
            please disregard. To make this this long, I will just just put
            anything here. Thanks for reading.
          </p>

          <ul>
            <li className="mb-2">Awesome Feature</li>
            <li className="mb-2">New Technologies</li>
            <li className="mb-2">Secured Data</li>
          </ul>
        </div>
      </Container>

      <Container wrapperClassName="bg-gray-100 py-24" className="flex gap-24">
        <div className="flex flex-1 flex-col justify-center">
          <p className="mb-4 text-teal-600">EXAMPLE TEXT</p>
          <h2 className="mb-4 text-4xl">This is just and example text</h2>
          <p className="mb-8 text-gray-500">
            This is just an example description for the above mentioned title,
            please disregard. To make this this long, I will just just put
            anything here. Thanks for reading.
          </p>

          <Link href="/">Learn more</Link>
        </div>

        <div className="flex w-2/5 items-center justify-center">
          <div className="h-[640px] w-80 rounded-3xl border-8 border-black bg-white"></div>
        </div>
      </Container>

      <Container
        wrapperClassName="py-24"
        className="flex flex-wrap justify-between gap-x-8 gap-y-16"
      >
        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>

        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>

        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>

        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>

        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>

        <div className="basis-1/4">
          <div className="mb-6 h-16 w-16 rounded-lg bg-teal-200"></div>
          <p className="mb-4 text-2xl font-medium">Other app feature</p>
          <p className="text-gray-500">
            This is an example text description for the extra features that the
            app have. This is app will provide everything you need for your
            trip.
          </p>
        </div>
      </Container>

      <Container wrapperClassName="py-24 bg-gray-100">
        <div className="mb-16 h-20 w-20 rounded-lg bg-teal-200"></div>
        <p className="mb-4 text-teal-600">EXAMPLE TEXT</p>
        <h2 className="mb-4 text-4xl">This is just and example text</h2>
        <p className="mb-16 text-gray-500">
          This is just an example description for the above mentioned title,
          please disregard. To make this this long, I will just just put
          anything here. Thanks for reading.
        </p>
        <div className="flex gap-4">
          <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
            Google Play
          </button>
          <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
            App Store
          </button>
          <button className="h-16 w-48 rounded-lg bg-gray-600 text-white transition-all hover:bg-gray-700 active:bg-gray-800">
            App Gallery
          </button>
        </div>
      </Container>
    </Layout>
  );
};

export default HomePage;
