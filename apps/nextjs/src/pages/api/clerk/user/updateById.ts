import { type NextApiRequest, type NextApiResponse } from "next";
import axios from "axios";
import qs from "qs";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { userId, firstName, lastName } = JSON.parse(req.body);

  const data = qs.stringify({
    first_name: firstName,
    last_name: lastName,
  });

  const config = {
    method: "patch",
    maxBodyLength: Infinity,
    url: "https://api.clerk.com/v1/users/" + userId,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization:
        "Bearer sk_test_zXUzrGeUNEot6zwlOPmRIcjexuMauscPpOHlNNyD1l",
    },
    data: data,
  };

  const result = axios
    .request(config)
    .then((response: any) => {
      return res.send({ status: "success", response: response.data });
    })
    .catch((error: any) => {
      return { status: "error", error: JSON.stringify(error) };
    });
};

export default handler;
