import type { NextApiRequest, NextApiResponse } from "next";
import type { WebhookEvent } from "@clerk/clerk-sdk-node";

import { prisma } from "@acme/db";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const evt = req.body as WebhookEvent;

    switch (evt.type) {
      case "user.created": {
        const data = evt.data;
        const emailAddress = data.email_addresses.find(
          (ea) => ea.id === data.primary_email_address_id,
        );
        const phoneNumber = data.phone_numbers.find(
          (pn) => pn.id === data.primary_phone_number_id,
        );

        const exist = await prisma.user.count({
          where: {
            id: data.id,
          },
        });

        if (exist > 0)
          return res.send({
            status: "error",
            error: "user already exist in database.",
          });

        const result = await prisma.user.create({
          data: {
            id: data.id,
            email: emailAddress?.email_address || "",
            status: emailAddress?.verification?.status || "",
            phone: phoneNumber?.phone_number || "",
            profile: data?.image_url,
          },
        });

        return res.send({
          status: "user.created",
          data: JSON.stringify(result),
        });
      }

      case "user.updated": {
        const data = evt.data;
        const phoneNumber = data.phone_numbers.find(
          (pn) => pn.id === data.primary_phone_number_id,
        );

        const exist = await prisma.user.count({
          where: {
            id: data.id,
          },
        });

        if (exist === 0)
          return res.send({
            status: "error",
            error: "user data not exist in database.",
          });

        const result = await prisma.user.update({
          where: {
            id: data.id,
          },
          data: {
            email: data.email_addresses?.[0]?.email_address || "",
            status: data.email_addresses?.[0]?.verification?.status || "",
            phone: phoneNumber?.phone_number || "",
          },
        });

        return res.send({
          status: "user.updated",
          data: JSON.stringify(result),
        });
      }

      case "user.deleted": {
        const data = evt.data;

        const exist = await prisma.user.count({
          where: {
            id: data.id,
          },
        });

        if (exist === 0)
          return res.send({
            status: "error",
            error: "user data not exist in database.",
          });

        const result = await prisma.user.delete({
          where: {
            id: data.id,
          },
        });

        return res.send({
          status: "user.deleted",
          data: JSON.stringify(result),
        });
      }
    }
  } catch (error: any) {
    console.log(error);
    return res.send({ status: "error", error: JSON.stringify(error) });
  }
};

export default handler;
