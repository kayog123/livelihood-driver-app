import { type GetServerSidePropsContext } from "next";
import { SignIn } from "@clerk/nextjs";
import { getAuth } from "@clerk/nextjs/server";

import Layout from "~/components/Layout";

const LoginPage = () => {
  return (
    <Layout title="Login">
      <div className="flex-1 pt-24">
        <div className="container flex items-center justify-center py-16">
          <SignIn
            path="/login"
            routing="path"
            signUpUrl="/signup"
            redirectUrl="/owner"
          />
        </div>
      </div>
    </Layout>
  );
};

export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
  const { userId } = getAuth(ctx.req);

  if (userId) {
    return {
      redirect: {
        destination: "/owner",
      },
    };
  }

  return {
    props: {},
  };
};

export default LoginPage;
