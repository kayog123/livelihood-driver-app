import UserLayout from "~/components/UserLayout";

const Home = () => {
  return <UserLayout title="Home" siteTitle="Owner"></UserLayout>;
};

export default Home;
