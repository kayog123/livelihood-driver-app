import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useAuth } from "@clerk/nextjs";
import { format, intlFormatDistance } from "date-fns";
import { ArrowLeft, CheckCheck, Send } from "lucide-react";
import InfiniteScroll from "react-infinite-scroll-component";

import { type ChatRoom, type Message, type User } from "@acme/db";

import { api } from "~/utils/api";
import { pusherClient } from "~/utils/pusher";
import { tw } from "~/utils/style";
import InputText from "~/components/InputText";
import UserLayout from "~/components/UserLayout";

interface ChatRoomData extends ChatRoom {
  user: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
  owner: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
  messages: Message[];
}

interface MessageData extends Message {
  user: Pick<User, "id" | "email" | "firstname" | "lastname" | "profile">;
}

const Messages = () => {
  const [chatRooms, setChatRooms] = useState<ChatRoomData[]>([]);
  const { userId } = useAuth();
  const router = useRouter();
  const [chatRoomIdMaybe, ...urlQueryRest] = (router.query.index ??
    []) as string[];
  const chatRoomId = (chatRoomIdMaybe && parseInt(chatRoomIdMaybe)) || null;
  const chatRoomData =
    (chatRooms.length > 0 &&
      chatRoomId &&
      chatRooms.find((cr) => cr.id === chatRoomId)) ||
    null;

  const utils = api.useContext();
  const chatRoomAllInfinite = api.chatRoom.allInfinite.useInfiniteQuery(
    {
      userId: userId!,
    },
    {
      getNextPageParam: (lastPage) => lastPage.nextCursor,
      refetchOnWindowFocus: false,
      enabled: !!userId,
    },
  );
  const chatRoomCreate = api.chatRoom.create.useMutation();

  const onCreateChatRoom = async (uid: string) => {
    try {
      await chatRoomCreate.mutateAsync({
        ownerId: userId!,
        userId: uid,
        isEnabled: true,
      });
    } catch (err) {
      console.log("ERROR:", err);
    }
  };

  useEffect(() => {
    if (chatRoomAllInfinite.data) {
      const chatRoomArr = chatRoomAllInfinite.data.pages
        .map((p) => p.chatRooms)
        .reduce((p, c) => [...p, ...c], []);

      setChatRooms(chatRoomArr);
    }
  }, [chatRoomAllInfinite.data]);

  useEffect(() => {
    if (urlQueryRest.length > 0) {
      void router.replace("/owner/messages");
    }
  }, [router, urlQueryRest]);

  // update chatrooms on new chatroom
  useEffect(() => {
    const channelName = `user__${userId}-chatroom`;
    const channelExist = pusherClient
      .allChannels()
      .find((ch) => ch.name === channelName);

    if (userId && !channelExist) {
      pusherClient.subscribe(channelName);

      const updateChatrooms = async (data: ChatRoomData) => {
        const newChatRoom: ChatRoomData = {
          ...data,
          dateCreated:
            typeof data.dateCreated === "string"
              ? new Date(data.dateCreated)
              : data.dateCreated,
          dateUpdated:
            typeof data.dateUpdated === "string"
              ? new Date(data.dateUpdated)
              : data.dateUpdated,
        };

        if (newChatRoom.userId === userId || newChatRoom.ownerId === userId) {
          await utils.chatRoom.allInfinite.cancel();
          utils.chatRoom.allInfinite.setInfiniteData({ userId }, (oldData) => {
            if (oldData && oldData.pages.length > 0) {
              return {
                ...oldData,
                pages: [
                  { chatRooms: [newChatRoom], nextCursor: undefined },
                  ...oldData.pages,
                ],
              };
            }
          });
        }
      };

      pusherClient.bind("chatroom-new", updateChatrooms);
    }
  }, [userId, utils.chatRoom.allInfinite]);

  // update chatroom on new message
  useEffect(() => {
    const channelName = `user__${userId}-chatrooms`;
    const channelExist = pusherClient
      .allChannels()
      .find((ch) => ch.name === channelName);

    if (userId && !channelExist) {
      pusherClient.subscribe(`user__${userId}-chatrooms`);

      const updateChatroom = async (data: ChatRoomData) => {
        const updatedChatRoom: ChatRoomData = {
          ...data,
          dateCreated:
            typeof data.dateCreated === "string"
              ? new Date(data.dateCreated)
              : data.dateCreated,
          dateUpdated:
            typeof data.dateUpdated === "string"
              ? new Date(data.dateUpdated)
              : data.dateUpdated,
        };

        if (
          updatedChatRoom.userId === userId ||
          updatedChatRoom.ownerId === userId
        ) {
          await utils.chatRoom.allInfinite.cancel();

          utils.chatRoom.allInfinite.setInfiniteData({ userId }, (oldData) => {
            if (oldData && oldData.pages.length > 0) {
              return {
                ...oldData,
                pages: oldData.pages.map((p) => ({
                  ...p,
                  chatRooms: p.chatRooms.map((cr) =>
                    cr.id === updatedChatRoom.id
                      ? { ...cr, ...updatedChatRoom }
                      : cr,
                  ),
                })),
              };
            }
          });
        }
      };

      pusherClient.bind("chatrooms-update", updateChatroom);
    }
  }, [userId, utils.chatRoom.allInfinite]);

  return (
    <UserLayout
      title="Messages"
      headerClassName="border-b border-b-gray-200"
      contentClassName="flex flex-col p-0"
    >
      <div className="flex flex-1">
        <MessagesSide chatRooms={chatRooms} selectedChatRoomId={chatRoomId} />
        {!!chatRoomData && <MessagesContent {...chatRoomData} />}
      </div>
    </UserLayout>
  );
};

const MessagesSide = ({
  chatRooms,
  selectedChatRoomId,
}: {
  chatRooms: ChatRoomData[];
  selectedChatRoomId: number | null;
}) => {
  return (
    <div className="flex h-[calc(100vh-theme('spacing.16'))] w-full flex-col overflow-y-hidden border-r border-gray-200 bg-white md:w-72">
      <div className="h-[72px] p-4">
        <InputText wrapperClassName="mb-0" placeholder="Search" />
      </div>

      <div className="overflow-y-auto">
        {chatRooms.map((chatRoom) => (
          <MessagesSideChatRoom
            key={`chatRoom-${chatRoom.id}`}
            {...chatRoom}
            selected={selectedChatRoomId === chatRoom.id}
          />
        ))}
      </div>
    </div>
  );
};

const MessagesSideChatRoom = ({
  id,
  ownerId,
  owner,
  user,
  dateUpdated,
  messages,
  selected,
}: ChatRoomData & { selected: boolean }) => {
  const { userId } = useAuth();
  const userData = user.id !== userId ? user : owner;
  const { firstname, lastname, email, profile } = userData;
  const [timeDisplay, setTimeDisplay] = useState<string>("");

  const displayName = !!(firstname && lastname)
    ? `${firstname} ${lastname}`
    : email;

  const latestMessage = messages[0] || null;
  const isLatestMessageByMe = !!(
    latestMessage &&
    userId &&
    latestMessage.userId === userId
  );
  const isRead = !!(
    latestMessage &&
    userId &&
    latestMessage.isRead.includes(userId)
  );

  useEffect(() => {
    setTimeDisplay(
      intlFormatDistance(dateUpdated, new Date(), {
        style: "narrow",
        numeric: "always",
      }),
    );

    const interval = setInterval(() => {
      setTimeDisplay(
        intlFormatDistance(dateUpdated, new Date(), {
          style: "narrow",
          numeric: "always",
        }),
      );
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [dateUpdated]);

  return (
    <Link
      href={`/owner/messages/${id}`}
      className={tw(
        "flex h-16 items-center gap-2 p-4 transition-all hover:bg-gray-200",
        selected && "bg-gray-200",
      )}
    >
      <div className="relative h-8 w-8 basis-8 overflow-hidden rounded-full bg-gray-300">
        <Image src={profile} alt={displayName} fill />
      </div>

      <div className="flex-1 overflow-hidden">
        <p className="flex-1 overflow-hidden text-ellipsis whitespace-nowrap text-sm font-medium">
          {displayName}
        </p>
        {/* {latestMessage?.message && (
          <p
            className={tw(
              "text-sm font-medium text-gray-800",
              isRead || (isLatestMessageByMe && "text-normal text-gray-600"),
            )}
          >
            {isLatestMessageByMe ? "You: " : ""}
            {latestMessage?.message}
          </p>
        )} */}
      </div>

      <div className="flex flex-col items-end">
        <p className="text-xs text-gray-600">{timeDisplay}</p>
        <div className="flex items-center">
          {isRead ? (
            <CheckCheck className="text-teal-600" size={16} />
          ) : (
            <div className="flex h-4 items-center justify-center rounded-full bg-red-600 px-1 text-[10px] text-white">
              <span className="leading-none">new</span>
            </div>
          )}
        </div>
      </div>
    </Link>
  );
};

const MessagesContent = ({ id: chatRoomId, owner, user }: ChatRoomData) => {
  const [messages, setMessages] = useState<MessageData[]>([]);
  const [message, setMessage] = useState<string>("");
  const { userId } = useAuth();
  const userData = user.id !== userId ? user : owner;
  const { firstname, lastname, email, profile } = userData;

  const displayName = !!(firstname && lastname)
    ? `${firstname} ${lastname}`
    : email;

  const messagesContainerRef = useRef<null | InfiniteScroll>(null);

  const utils = api.useContext();
  const messageInfinite = api.message.byChatRoomIdInfinite.useInfiniteQuery(
    { chatRoomId },
    {
      getNextPageParam: (lastPage) => lastPage.nextCursor,
      refetchOnWindowFocus: false,
    },
  );
  const messageCreate = api.message.create.useMutation();

  const onSendMessage = async () => {
    try {
      await messageCreate.mutateAsync({ chatRoomId, userId: userId!, message });
      setMessage("");
    } catch (err) {
      console.log("ERROR:", err);
    }
  };

  const canSubmit = !messageCreate.isLoading && message && userId;

  useEffect(() => {
    if (messageInfinite.data) {
      const messageArr = messageInfinite.data.pages
        .map((p) => p.messages)
        .reduce((p, c) => [...p, ...c], []);

      setMessages(messageArr);
    }
  }, [messageInfinite.data]);

  useEffect(() => {
    if (!messageInfinite.isRefetching) {
      const scrollableElement =
        messagesContainerRef.current?.getScrollableTarget();

      if (scrollableElement) {
        scrollableElement.scrollTo({
          top: 0,
          behavior: "smooth",
        });
      }
    }
  }, [messageInfinite.isRefetching]);

  useEffect(() => {
    const channelName = `chatroom__${chatRoomId}-messages`;
    const channelExist = pusherClient
      .allChannels()
      .find((ch) => ch.name === channelName);

    if (userId && chatRoomId && !channelExist) {
      pusherClient.subscribe(channelName);
      const updateMessages = async (data: MessageData) => {
        const newMessage: MessageData = {
          ...data,
          dateCreated:
            typeof data.dateCreated === "string"
              ? new Date(data.dateCreated)
              : data.dateCreated,
          dateUpdated:
            typeof data.dateUpdated === "string"
              ? new Date(data.dateUpdated)
              : data.dateUpdated,
        };

        if (newMessage.chatRoomId === chatRoomId) {
          await utils.message.byChatRoomIdInfinite.cancel();
          utils.message.byChatRoomIdInfinite.setInfiniteData(
            { chatRoomId },
            (oldData) => {
              if (oldData && oldData.pages.length > 0) {
                return {
                  ...oldData,
                  pages: [
                    { messages: [newMessage], nextCursor: undefined },
                    ...oldData.pages,
                  ],
                };
              }
            },
          );
        }

        const scrollableElement =
          messagesContainerRef.current?.getScrollableTarget();

        if (scrollableElement) {
          setTimeout(() => {
            scrollableElement.scrollTo({
              top: scrollableElement.scrollHeight,
              behavior: "smooth",
            });
          }, 500);
        }
      };

      pusherClient.bind("chatroom-messages-update", updateMessages);
    }
  }, [userId, chatRoomId, utils.message.byChatRoomIdInfinite]);

  return (
    <div className="fixed bottom-0 left-0 right-0 top-0 flex flex-1 flex-col overflow-y-hidden bg-white md:static md:h-[calc(100vh-theme('spacing.16'))]">
      <div className="z-10 flex h-[72px] items-center gap-2 border-b border-b-gray-200 p-4">
        <div className="md:hidden">
          <Link href="/owner/messages">
            <ArrowLeft size={24} />
          </Link>
        </div>

        <div className="relative h-10 w-10 overflow-hidden rounded-full bg-gray-300">
          <Image src={profile} alt={displayName} fill />
        </div>

        <div>
          <p className="text-sm font-medium">{displayName}</p>
          <p className="text-xs text-gray-600">
            {userData?.email} | +639987654321
          </p>
        </div>
      </div>

      <div
        id="messages-container"
        className="flex h-[calc(100vh-(theme('spacing.16')+72px))] flex-1 flex-col-reverse overflow-y-auto bg-gray-100"
      >
        <InfiniteScroll
          ref={messagesContainerRef}
          dataLength={messages.length}
          next={messageInfinite.fetchNextPage}
          hasMore={messageInfinite.hasNextPage ?? false}
          loader={<p>loading...</p>}
          inverse={true}
          scrollableTarget="messages-container"
          className="flex max-h-full flex-col-reverse gap-2 p-4"
        >
          {messages.map((m) => {
            const isMe = userId === m.userId;

            return (
              <div
                key={`message-${m.id}`}
                className={tw(isMe && "self-end", !isMe && "self-start")}
              >
                <div
                  className={tw(
                    "mb-1 rounded-lg px-3 py-2",
                    isMe &&
                      "rounded-br-none border border-gray-200 bg-white text-gray-800",
                    !isMe &&
                      "rounded-bl-none border border-teal-600 bg-teal-600 text-white",
                  )}
                >
                  <p>{m.message}</p>
                </div>
                <p
                  className={tw("text-xs text-gray-600", isMe && "text-right")}
                >
                  {format(m.dateCreated, "hh:mm a")}
                </p>
              </div>
            );
          })}
        </InfiniteScroll>
      </div>

      <div className="">
        <form
          onSubmit={(e) => {
            e.preventDefault();
            void onSendMessage();
          }}
        >
          <div className="flex h-16 items-center gap-4 border-t border-t-gray-200 p-2">
            <InputText
              wrapperClassName="flex-1 mb-0"
              className="border-none focus:ring-0"
              placeholder="Send your message..."
              onChange={setMessage}
              value={message}
            />
            <button
              className="rounded-full p-2 outline-none focus-visible:ring-1 active:opacity-75 disabled:opacity-50"
              disabled={!canSubmit}
              type="submit"
            >
              <Send
                className="-translate-x-1 rotate-45 text-teal-600"
                size={24}
              />
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Messages;
