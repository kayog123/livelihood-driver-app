import InputText from "~/components/InputText";

const OwnerInfo = () => {
  return (
    <div className="flex min-h-screen items-center justify-center">
      <div className="w-96">
        <InputText />
      </div>
    </div>
  );
};

export default OwnerInfo;
