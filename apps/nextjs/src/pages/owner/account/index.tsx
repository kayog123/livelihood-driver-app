import { useEffect, useState, type FormEvent } from "react";
import { type NextPage } from "next";
import Image from "next/image";
import { useUser } from "@clerk/nextjs";

import { type User } from "@acme/db";

import { api } from "~/utils/api";
import Button from "~/components/Button";
import InputSelect from "~/components/InputSelect";
import InputText from "~/components/InputText";
import UserLayout from "~/components/UserLayout";

const Account: NextPage = () => {
  const [firstname, setFirstname] = useState<string>("");
  const [lastname, setLastname] = useState<string>("");
  const [birthdate, setBirthdate] = useState<string>("");
  const [gender, setGender] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [address1, setAddress1] = useState<string>("");
  const [address2, setAddress2] = useState<string>("");
  const [city, setCity] = useState<string>("");
  const [state, setState] = useState<string>("");
  const [country, setCountry] = useState<string>("");
  const [zipCode, setZipCode] = useState<string>("");
  const [image, setImage] = useState<File | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const { user } = useUser();
  const { data } = api.user.byId.useQuery(
    { id: user?.id || "" },
    { enabled: !!user?.id, refetchOnWindowFocus: false },
  );
  const utils = api.useContext();
  const { mutate, isLoading } = api.user.update.useMutation({
    onMutate: async (updatedUser) => {
      await utils.user.byId.cancel();
      const prevData = utils.user.byId.getData();
      if (user) {
        utils.user.byId.setData({ id: user.id }, () => updatedUser as User);
      }

      return { prevData };
    },
  });

  const canSubmit =
    firstname &&
    lastname &&
    birthdate &&
    gender &&
    phone &&
    address1 &&
    city &&
    state &&
    country &&
    zipCode &&
    (firstname !== data?.firstname ||
      lastname !== data?.lastname ||
      birthdate !== data?.birthdate ||
      gender !== data?.gender ||
      phone !== data?.phone ||
      address1 !== data?.address1 ||
      city !== data?.city ||
      state !== data?.state ||
      country !== data?.country ||
      zipCode !== data?.zipCode);

  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (user && canSubmit) {
      mutate({
        id: user.id,
        firstname,
        lastname,
        birthdate,
        gender,
        phone,
        address1,
        address2,
        city,
        state,
        country,
        zipCode,
        role: "owner",
      });
    }
  };

  useEffect(() => {
    if (data) {
      setFirstname(data.firstname);
      setLastname(data.lastname);
      setBirthdate(data.birthdate);
      setGender(data.gender);
      setPhone(data.phone);
      setAddress1(data.address1);
      setAddress2(data.address2);
      setCity(data.city);
      setState(data.state);
      setCountry(data.country);
      setZipCode(data.zipCode);
    }
  }, [data]);

  return (
    <UserLayout title="Account">
      <div className="relative mb-4 h-32 w-32 overflow-hidden rounded-full bg-app-primary">
        {data?.profile && (
          <Image src={data.profile} alt={data?.firstname} fill />
        )}
      </div>
      <form className="md:w-3/4 lg:w-1/2 xl:w-1/3" onSubmit={onSubmit}>
        <div className="mb-4">
          <p>Personal Information</p>
          <InputText
            label="First name"
            name="firstname"
            onChange={setFirstname}
            value={firstname}
          />
          <InputText
            label="Last name"
            name="lastname"
            onChange={setLastname}
            value={lastname}
          />
          <InputText
            label="Birthdate"
            type="date"
            name="birthdate"
            onChange={setBirthdate}
            value={birthdate}
          />
          <InputSelect
            label="Gender"
            name="gender"
            options={[
              {
                label: "Male",
                value: "male",
              },
              {
                label: "Female",
                value: "female",
              },
            ]}
            onChange={setGender}
            value={gender}
          />
          <InputText
            label="Phone Number"
            name="phone"
            leftContent="+63"
            onChange={setPhone}
            value={phone}
          />
        </div>
        <div className="mb-4">
          <p>Address</p>
          <InputText
            label="Address line 1"
            name="address1"
            onChange={setAddress1}
            value={address1}
          />
          <InputText
            label="Address line 2"
            name="address2"
            onChange={setAddress2}
            value={address2}
          />
          <InputText label="City" name="city" onChange={setCity} value={city} />
          <InputText
            label="State"
            name="state"
            onChange={setState}
            value={state}
          />
          <InputText
            label="Country"
            name="country"
            onChange={setCountry}
            value={country}
          />
          <InputText
            label="Zip/Postal code"
            name="zipCode"
            onChange={setZipCode}
            value={zipCode}
          />
        </div>

        <Button type="submit" disabled={!canSubmit} loading={isLoading}>
          Save
        </Button>
      </form>
    </UserLayout>
  );
};

export default Account;
