import { format } from "date-fns";
import _ from "lodash";

import { api } from "~/utils/api";
import BookingCard from "~/components/BookingCard";
import UserLayout from "~/components/UserLayout";

const Bookings = () => {
  const { data: bookings } = api.booking.all.useQuery(undefined, {
    initialData: [],
    refetchOnWindowFocus: false,
  });

  const bookingsByMonth = _.groupBy(bookings, (b) =>
    format(b.dateScheduled, "MMMM yyyy"),
  );

  return (
    <UserLayout title="Bookings">
      {_.map(bookingsByMonth, (bs, k) => {
        return (
          <div key={k} className="mb-4 last:mb-0">
            <h2 className="mb-2 text-lg font-medium">{k}</h2>

            <div className="flex flex-col gap-2">
              {bs.map((booking) => {
                return (
                  <BookingCard key={`booking-${booking.id}`} {...booking} />
                );
              })}
            </div>
          </div>
        );
      })}
    </UserLayout>
  );
};

export default Bookings;
