import { useEffect, useState } from "react";
import { useUser } from "@clerk/nextjs";
import { addDays, differenceInDays, format, parse, startOfDay } from "date-fns";
import _ from "lodash";

import { type TranspoType } from "@acme/db";

import { api } from "~/utils/api";
import Button from "~/components/Button";
import InputText from "~/components/InputText";
import UserLayout from "~/components/UserLayout";
import trips, { type TripPackage } from "~/const/trips";

type TripCardProps = {
  isSelected: boolean;
  onSelect: (placesIds: number[]) => void;
  disabled: boolean;
} & TripPackage;

const TripCard = (props: TripCardProps) => {
  const { id, name, isSelected, onSelect, disabled, places = [] } = props;
  const [selectedPlaces, setSelectedPlaces] = useState<number[]>([]);

  const handleSelectPlace = (placeId: number) => {
    const placeExist = !!selectedPlaces.includes(placeId);

    if (placeExist) setSelectedPlaces((sp) => sp.filter((p) => p !== placeId));
    else setSelectedPlaces((sp) => _.sortBy(_.uniq([...sp, placeId])));
  };

  useEffect(() => {
    setSelectedPlaces(places.map((p) => p.id));
  }, [places]);

  return (
    <div>
      <h3 className="font-medium">{name}</h3>
      <div className="mb-2">
        {places.map((place) => {
          const isPlaceSelected = selectedPlaces.includes(place.id);

          return (
            <label
              key={`place-${place.id}`}
              className="flex items-center gap-2"
            >
              <input
                type="checkbox"
                name={`pkg-${id}-place-${place.id}`}
                checked={isPlaceSelected}
                value={place.id}
                onChange={(e) => handleSelectPlace(Number(e.target.value))}
              />
              <span>{place.name}</span>
            </label>
          );
        })}
      </div>

      <Button
        className="w-auto"
        color={isSelected ? "danger" : "primary"}
        onClick={() => onSelect(selectedPlaces)}
        disabled={disabled}
      >
        {isSelected ? "Deselect" : "Select"}
      </Button>
    </div>
  );
};

const TestBooking = () => {
  const [selected, setSelected] = useState<{
    tripId: number;
    pkgId: number;
    placesIds: number[];
    type: TranspoType;
  } | null>(null);
  const [dateScheduled, setDateScheduled] = useState<string>("");
  const { user } = useUser();
  const { mutateAsync: createBooking, isLoading } =
    api.booking.createBooking.useMutation();

  const isDateScheduledValid =
    dateScheduled &&
    differenceInDays(
      startOfDay(parse(dateScheduled, "yyyy-MM-dd", new Date())),
      startOfDay(new Date()),
    ) >= 2;

  const canBook = !!(user?.id && selected && isDateScheduledValid);

  const handleBook = async () => {
    if (canBook && user?.id && selected) {
      try {
        await createBooking({
          userId: user.id,
          ownerId: "",
          tripId: selected.tripId,
          pkgId: selected.pkgId,
          type: "TRIP",
          status: "PENDING",
          transpo_type: selected.type,
          land_transpo_type: "VANS",
          place_activity_fee: 0,
          transpo_rate: 0,
          passenger_capacity: 0,
          description: "",
          places: selected.placesIds.toString(),
          pickup_lat: 0,
          pickup_long: 0,
          pickup_area_name: "",
          dateCreated: new Date(),
          dateUpdated: new Date(),
          dateScheduled: parse(dateScheduled, "yyyy-MM-dd", new Date()),
          allowed_other: false,
        });
        alert("Success!");
      } catch (error) {
        alert("Failed!");
      }
    }
  };

  return (
    <UserLayout title="Test Booking">
      {trips.map((trip) => {
        return (
          <div key={`trip-${trip.id}`} className="mb-4 last:mb-0">
            <h2 className="mb-4 font-medium">{trip.name}</h2>
            <div className="grid grid-cols-4 gap-4">
              {trip.packages.map((pkg) => {
                const isSelected =
                  selected?.tripId === trip.id && selected?.pkgId === pkg.id;

                const handleSelect = (placesIds: number[]) =>
                  isSelected
                    ? setSelected(null)
                    : setSelected({
                        tripId: trip.id,
                        pkgId: pkg.id,
                        placesIds,
                        type: trip.type,
                      });

                return (
                  <TripCard
                    key={`pkg-${pkg.id}`}
                    isSelected={isSelected}
                    onSelect={handleSelect}
                    disabled={!user?.id}
                    {...pkg}
                  />
                );
              })}
            </div>
          </div>
        );
      })}

      <InputText
        type="date"
        min={format(addDays(new Date(), 2), "yyyy-MM-dd")}
        onChange={setDateScheduled}
        value={dateScheduled}
      />

      <Button
        className="w-auto"
        disabled={!canBook}
        loading={isLoading}
        onClick={handleBook}
      >
        Book Now
      </Button>
    </UserLayout>
  );
};

export default TestBooking;
