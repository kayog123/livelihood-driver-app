import { useAuth } from "@clerk/nextjs";

import { api } from "~/utils/api";
import TripCard from "~/components/TripCard";
import UserLayout from "~/components/UserLayout";
import trips from "~/const/trips";

const Trips = () => {
  const { userId } = useAuth();
  const {
    data: offeredTrips,
    isFetched,
    ...rest
  } = api.offeredTrip.byUserId.useQuery(
    { userId: userId || "" },
    { enabled: !!userId, initialData: [], refetchOnWindowFocus: false },
  );

  return (
    <UserLayout title="Trips">
      {trips.map((trip) => {
        const { id, name, description, packages, type } = trip;

        return (
          <div key={`trip-${id}`}>
            <h2 className="mb-2 text-xl font-medium">{name}</h2>

            <div className="last-of-type:0 mb-8 grid auto-rows-auto gap-4 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5">
              {packages.map((pkg) => {
                const offeredTrip =
                  offeredTrips.find(
                    (ot) =>
                      ot.userId === userId &&
                      ot.tripId === id &&
                      ot.pkgId === pkg.id,
                  ) || null;

                return (
                  <TripCard
                    key={`trip-${id}-${pkg.id}`}
                    tripId={id}
                    offeredTrip={offeredTrip}
                    isLoaded={isFetched}
                    type={type}
                    {...pkg}
                  />
                );
              })}
            </div>
          </div>
        );
      })}

      {/* <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
        {trips.map((trip) => {
          const { id, name, Icon } = trip;

          return (
            <button key={id}>
              <div className="flex h-96 flex-col items-center justify-center rounded-2xl border border-gray-200 p-4 transition-all hover:bg-gray-100">
                <Icon className="mb-8" size={124} />
                <h3 className="text-2xl font-medium">{name}</h3>
              </div>
            </button>
          );
        })}
      </div> */}
    </UserLayout>
  );
};

export default Trips;
