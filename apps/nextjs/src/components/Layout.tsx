import { type ReactNode } from "react";
import Head from "next/head";

import { tw } from "~/utils/style";
import Footer from "./Footer";
import Header from "./Header";

interface LayoutProps {
  title?: string;
  className?: string;
  children?: ReactNode;
}

const Layout = (props: LayoutProps) => {
  const { title, className, children } = props;

  return (
    <>
      <Head>
        <title>{title ? `Bohol Tour - ${title}` : "Bohol Tour"}</title>
      </Head>
      <div className="flex min-h-screen flex-col">
        <Header />
        <main className={tw("flex flex-1 flex-col", className)}>
          {children}
        </main>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
