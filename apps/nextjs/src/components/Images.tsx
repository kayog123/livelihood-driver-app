import Image, { type ImageProps as NextImageProps } from "next/image";

type ImageProps = Omit<NextImageProps, "src" | "alt">;

export const ImgLogo = ({ fill, ...rest }: ImageProps) => (
  <Image
    src="/images/logo.png"
    alt="logo"
    fill={fill}
    {...(fill ? {} : { width: 296, height: 144 })}
    {...rest}
  />
);
