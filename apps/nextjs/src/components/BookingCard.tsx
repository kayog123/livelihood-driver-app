import { format } from "date-fns";
import _ from "lodash";
import {
  Calendar,
  ChevronDown,
  ChevronRight,
  MapPin,
  User2,
} from "lucide-react";

import { type Booking, type User } from "@acme/db";

import trips from "~/const/trips";
import Button from "./Button";

type BookingCardProps = {
  user: User;
} & Booking;

const BookingCard = (props: BookingCardProps) => {
  const { dateScheduled, type, tripId, pickup_lat, pickup_long, user } = props;

  const isTrip = type === "TRIP";
  const isLatLng = type === "LATLONG";

  const tripObj =
    (isTrip && tripId && trips.find((t) => t.id === tripId)) || null;

  return (
    <div className="flex rounded-lg border border-gray-200">
      <div className="flex aspect-square h-20 flex-col items-center justify-center">
        <div className="w-full border-r border-r-gray-200">
          <p className="text-center text-sm">{format(dateScheduled, "E")}</p>
          <p className="text-center text-2xl font-bold">
            {format(dateScheduled, "dd")}
          </p>
        </div>
      </div>

      <div className="flex flex-1 p-4">
        <div className="flex items-center">
          <div>
            {isTrip && (
              <>
                <div className="mb-1 flex items-center gap-2 text-sm">
                  <MapPin size={16} />
                  <span>{tripObj?.name}</span>
                </div>
                <div className="flex items-center gap-2 text-sm">
                  <Calendar size={16} />
                  <span>{format(dateScheduled, "MMMM dd, yyyy")}</span>
                </div>
              </>
            )}

            {isLatLng && (
              <>
                <div className="mb-1 flex items-center gap-2 text-sm">
                  <MapPin size={16} />
                  <span>{`${pickup_lat}, ${pickup_long}`}</span>
                </div>
                <div className="flex items-center gap-2 text-sm">
                  <MapPin size={16} />
                  <span></span>
                </div>
              </>
            )}
          </div>
        </div>
        <div></div>
      </div>

      <div className="flex items-center p-4">
        {/* <button className="inline-flex items-center gap-1 rounded-lg bg-gray-200 p-2 text-gray-800 transition-all hover:bg-gray-300">
          <span className="leading-[1.15]">Edit</span>
          <ChevronDown size={20} />
        </button> */}
        <Button>Accept</Button>
      </div>
    </div>
  );
};

export default BookingCard;
