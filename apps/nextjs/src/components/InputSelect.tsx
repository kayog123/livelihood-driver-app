import { type ChangeEvent, type ComponentProps } from "react";

import { tw } from "~/utils/style";

interface Option {
  label: string;
  value: string;
}
interface InputSelectProps extends Omit<ComponentProps<"select">, "onChange"> {
  label?: string;
  wrapperClassName?: string;
  labelClassName?: string;
  inputContainerClassName?: string;
  options?: Option[];
  onChange?: (value: string) => void;
}

const InputSelect = (props: InputSelectProps) => {
  const {
    label,
    wrapperClassName,
    labelClassName,
    inputContainerClassName,
    className,
    options = [],
    name,
    placeholder,
    onChange,
    ...rest
  } = props;

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    onChange && onChange(e.target.value);
  };

  return (
    <div className={tw("mb-2", wrapperClassName)}>
      {label && (
        <label className={tw("text-sm", labelClassName)}>{label}</label>
      )}
      <div
        className={tw("flex items-center rounded-md", inputContainerClassName)}
      >
        <select
          className={tw(
            "focus:ring-app-primary h-10 w-full flex-1 border border-gray-300 bg-gray-100 px-2.5 text-sm outline-none first:rounded-l-md last:rounded-r-md focus:ring-1",
            className,
          )}
          onChange={handleChange}
          {...rest}
        >
          {(placeholder || name) && (
            <option value="">{placeholder || `Select ${name}`}</option>
          )}
          {options.map((o) => (
            <option key={o.value} value={o.value}>
              {o.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default InputSelect;
