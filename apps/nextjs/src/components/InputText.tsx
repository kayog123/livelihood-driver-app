import { type ChangeEvent, type ComponentProps } from "react";

import { tw } from "~/utils/style";

type InputTextProps = {
  label?: string;
  wrapperClassName?: string;
  labelClassName?: string;
  inputContainerClassName?: string;
  leftContent?: string;
  onChange?: (value: string) => void;
  size?: "sm" | "base";
} & Omit<ComponentProps<"input">, "onChange" | "size">;

const InputText = (props: InputTextProps) => {
  const {
    label,
    wrapperClassName,
    labelClassName,
    inputContainerClassName,
    className,
    leftContent,
    onChange,
    size = "base",
    ...rest
  } = props;

  const isSm = size === "sm";
  const isBase = size === "base";

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(e.target.value);
  };

  return (
    <div className={tw("mb-2", wrapperClassName)}>
      {label && (
        <label className={tw("text-sm", labelClassName)}>{label}</label>
      )}
      <div className={tw("flex items-stretch", inputContainerClassName)}>
        {leftContent && (
          <div className="flex items-center border border-r-0 border-gray-300 px-3 first:rounded-l-md last:rounded-r-md">
            <span className="text-sm text-gray-600">{leftContent}</span>
          </div>
        )}
        <input
          className={tw(
            "w-full flex-1 border border-gray-300 px-3 text-sm outline-none transition-all first:rounded-l-md last:rounded-r-md focus:ring-1 focus:ring-teal-400",
            isBase && "h-10",
            isSm && "h-8",
            className,
          )}
          onChange={handleChange}
          {...rest}
        />
      </div>
    </div>
  );
};

export default InputText;
