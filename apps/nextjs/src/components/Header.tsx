import Link from "next/link";
import { useUser } from "@clerk/nextjs";

import { ImgLogo } from "./Images";

const Header = () => {
  const { isSignedIn } = useUser();

  return (
    <header className="relative select-none">
      <div className="container absolute left-0 right-0 top-0 flex h-24 w-full items-center justify-between bg-transparent">
        <Link className="relative h-12 w-24" href="/">
          <ImgLogo className="object-contain" fill />
        </Link>

        <nav>
          <ul className="flex gap-8">
            <li>
              <Link
                className="transition-all hover:text-teal-700 active:text-teal-800"
                href="/"
              >
                Home
              </Link>
            </li>
            <li>
              <Link
                className="transition-all hover:text-teal-700 active:text-teal-800"
                href="/about"
              >
                About Us
              </Link>
            </li>
            <li>
              <Link
                className="transition-all hover:text-teal-700 active:text-teal-800"
                href="/help"
              >
                Help
              </Link>
            </li>
            <li>
              <Link
                className="transition-all hover:text-teal-700 active:text-teal-800"
                href="/contact"
              >
                Contact Us
              </Link>
            </li>
            <li>
              <Link
                className="rounded-lg bg-gray-600 px-6 py-3 font-medium text-white transition-all hover:bg-gray-700 active:bg-gray-800"
                href={isSignedIn ? "/owner" : "/signup"}
              >
                Be Our Partner
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
