import { type ComponentProps, type ReactNode } from "react";
import { Loader } from "lucide-react";

import { tw } from "~/utils/style";

interface ButtonProps extends ComponentProps<"button"> {
  loading?: boolean;
  icon?: ReactNode;
  color?: "primary" | "danger";
}

const Button = (props: ButtonProps) => {
  const {
    className,
    type = "button",
    children,
    disabled,
    loading = false,
    icon,
    color = "primary",
    ...rest
  } = props;

  const isPrimary = color === "primary";
  const isDanger = color === "danger";

  return (
    <button
      className={tw(
        "relative h-10 w-full overflow-hidden rounded-md px-4 text-sm text-white transition-all disabled:cursor-not-allowed disabled:opacity-50",
        isPrimary && "bg-teal-600 hover:bg-teal-700 disabled:hover:bg-teal-600",
        isDanger && "bg-red-600 hover:bg-red-700 disabled:hover:bg-red-600",
        className,
      )}
      type={type}
      disabled={disabled || loading}
      {...rest}
    >
      {loading && (
        <div className="absolute bottom-0 left-0 right-0 top-0 flex h-full w-full items-center justify-center gap-1">
          <Loader className="animate-spin" size={16} />
        </div>
      )}
      <div
        className={tw(
          "flex items-center justify-center gap-2",
          loading && "opacity-0",
        )}
      >
        {icon}
        {children}
      </div>
    </button>
  );
};

export default Button;
