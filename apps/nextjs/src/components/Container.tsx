import { type ReactNode } from "react";

import { tw } from "~/utils/style";

interface ContainerProps {
  wrapperClassName?: string;
  className?: string;
  children?: ReactNode;
}

const Container = (props: ContainerProps) => {
  const { wrapperClassName, className, children } = props;

  return (
    <div className={tw("", wrapperClassName)}>
      <div className={tw("container", className)}>{children}</div>
    </div>
  );
};

export default Container;
