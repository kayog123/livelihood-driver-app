import Link from "next/link";

import { ImgLogo } from "./Images";

const Footer = () => {
  return (
    <footer className="py-16">
      <div className="container grid grid-cols-4 gap-16">
        <div>
          <div className="relative mb-4 h-16 w-24">
            <ImgLogo className="object-contain" fill />
          </div>
          <p className="text-sm font-light text-gray-600">
            4th District, Pagina, Jagna 6308
          </p>
          <p className="mb-4 text-sm font-light text-gray-600">
            beholdbohol@gmail.com
          </p>
          <p className="text-sm font-light text-gray-600">Globe: 09987654321</p>
          <p className="text-sm font-light text-gray-600">Smart: 09987654321</p>
        </div>

        <div>
          <p className="mb-4 text-lg font-medium">About</p>

          <ul>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">About Us</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Career</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Contact Us</Link>
            </li>
          </ul>
        </div>

        <div>
          <p className="mb-4 text-lg font-medium">Quick Links</p>

          <ul>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">News & Updates</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Pedicab Rider</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Pedicab Owner</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Pedicab Rewards</Link>
            </li>
          </ul>
        </div>

        <div>
          <p className="mb-4 text-lg font-medium">Information</p>

          <ul>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Help</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Privacy Policy</Link>
            </li>
            <li className="mb-2 text-sm font-light text-gray-600">
              <Link href="/">Terms of Use</Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
