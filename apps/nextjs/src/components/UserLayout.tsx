import { useState, type ComponentProps } from "react";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { useAuth } from "@clerk/nextjs";
import {
  CalendarCheck,
  Home,
  LogOut,
  Map,
  MessagesSquare,
  Milestone,
  User,
} from "lucide-react";

import { tw } from "~/utils/style";
import { ImgLogo } from "./Images";

interface UserLayoutProps {
  title?: string;
  siteTitle?: string;
  headerRight?: ComponentProps<"div">["children"];
  className?: string;
  contentClassName?: string;
  children?: ComponentProps<"div">["children"];
  headerClassName?: string;
}

const UserLayout = (props: UserLayoutProps) => {
  const {
    title,
    siteTitle,
    headerRight,
    className,
    contentClassName,
    children,
    headerClassName,
  } = props;
  const [sideOpen, setSideOpen] = useState<boolean>(true);
  const { signOut } = useAuth();
  const { pathname } = useRouter();

  return (
    <>
      <Head>
        <title>
          {!!(siteTitle || title)
            ? `Behold Bohol Trip - ${siteTitle || title}`
            : "Behold Bohol Trip"}
        </title>
      </Head>
      <div className="bg-gray-50">
        <div className="relative">
          <aside
            className={tw(
              "fixed bottom-0 left-0 top-0 flex w-0 flex-col overflow-hidden bg-teal-600 md:w-72",
              sideOpen && "w-24",
            )}
          >
            <Link href="/owner" className="block px-4 py-8 md:p-8">
              <div className="relative h-12">
                <ImgLogo className="object-contain" fill />
              </div>
            </Link>
            <ul className="flex-1 p-4">
              <li className="mb-1 text-base">
                <Link
                  href="/owner"
                  className={tw(
                    "flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                    pathname === "/owner" && "bg-teal-700",
                  )}
                >
                  <Home size={24} />
                  <span className="hidden md:inline-block">Home</span>
                </Link>
              </li>

              <li className="mb-1 text-base">
                <Link
                  href="/owner/trips"
                  className={tw(
                    "flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                    pathname.includes("/trips") && "bg-teal-700",
                  )}
                >
                  <Milestone size={24} />
                  <span className="hidden md:inline-block">Trips</span>
                </Link>
              </li>

              <li className="mb-1 text-base">
                <Link
                  href="/owner/map"
                  className={tw(
                    "flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                    pathname.includes("/map") && "bg-teal-700",
                  )}
                >
                  <Map size={24} />
                  <span className="hidden md:inline-block">Map</span>
                </Link>
              </li>

              <li className="mb-1 text-base">
                <Link
                  href="/owner/messages"
                  className={tw(
                    "flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                    pathname.includes("/messages") && "bg-teal-700",
                  )}
                >
                  <MessagesSquare size={24} />
                  <span className="hidden md:inline-block">Messages</span>
                </Link>
              </li>

              <li className="mb-1 text-base">
                <Link
                  href="/owner/bookings"
                  className={tw(
                    "flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                    pathname.includes("/bookings") && "bg-teal-700",
                  )}
                >
                  <CalendarCheck size={24} />
                  <span className="hidden md:inline-block">Bookings</span>
                </Link>
              </li>
            </ul>
            <div className="p-4">
              <Link
                href="/owner/account"
                className={tw(
                  "mb-1 flex h-16 items-center justify-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800 md:justify-start",
                  pathname.includes("/account") && "bg-teal-700",
                )}
              >
                <User size={24} />
                <span className="hidden md:inline-block">Account</span>
              </Link>
              <button
                className="flex w-full items-center gap-4 rounded-2xl p-4 text-lg font-medium text-white transition-all hover:bg-teal-700 active:bg-teal-800"
                onClick={() => void signOut()}
              >
                <LogOut size={24} />
                <span className="hidden md:inline-block">Logout</span>
              </button>
            </div>
          </aside>
          <main
            className={tw(
              "ml-24 flex h-screen flex-col overflow-y-hidden bg-white md:ml-72",
              className,
            )}
          >
            <div
              className={tw(
                "flex h-16 items-center justify-between p-4",
                headerClassName,
              )}
            >
              {!!title && (
                <div>
                  <h2 className="text-xl font-medium">{title}</h2>
                </div>
              )}

              <div>{headerRight}</div>
            </div>
            <div
              className={tw(
                "max-h-[calc(100vh-theme('spacing.16'))] flex-1 overflow-y-auto p-4",
                contentClassName,
              )}
            >
              {children}
            </div>
          </main>
        </div>
      </div>
    </>
  );
};

export default UserLayout;
