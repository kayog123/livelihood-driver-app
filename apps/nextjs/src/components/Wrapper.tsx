import { useEffect } from "react";
import { useRouter } from "next/router";
import { useUser } from "@clerk/nextjs";

interface WrapperProps {
  children?: any;
}

const Wrapper = (props: WrapperProps) => {
  const { children } = props;
  const { isSignedIn } = useUser();
  const router = useRouter();

  const pathname = router.pathname;

  useEffect(() => {
    if (isSignedIn && pathname.includes("owner")) {
    }
  }, []);

  return <>{children}</>;
};

export default Wrapper;
