import Image from "next/image";
import { useAuth } from "@clerk/nextjs";

import { type OfferedTrip, type TranspoType } from "@acme/db";

import { api } from "~/utils/api";
import { tw } from "~/utils/style";
import { type TripPackage } from "~/const/trips";
import Button from "./Button";

type TripCardProps = {
  tripId: number;
  offeredTrip?: OfferedTrip | null;
  isLoaded?: boolean;
  type: TranspoType;
} & TripPackage;

const TripCard = (props: TripCardProps) => {
  const {
    tripId,
    id: pkgId,
    name: pkgName,
    description: pkgDescription,
    image: pkgImage,
    offeredTrip,
    isLoaded = false,
    places,
    type,
  } = props;
  const { userId } = useAuth();

  const utils = api.useContext();
  const { mutate: toggleActivate, isLoading } =
    api.offeredTrip.toggleActivate.useMutation({
      async onMutate(newOt) {
        await utils.offeredTrip.byUserId.cancel();
        const prevData = utils.offeredTrip.byUserId.getData() || [];
        const exist = prevData.find(
          (ot) => ot.userId === newOt.userId && ot.tripId === newOt.tripId,
        );

        if (userId) {
          if (exist) {
            utils.offeredTrip.byUserId.setData({ userId }, (old) => [
              ...(old || []).map((ot) =>
                ot.id === exist.id
                  ? {
                      ...ot,
                      ...newOt,
                    }
                  : ot,
              ),
            ]);
          } else {
            utils.offeredTrip.byUserId.setData({ userId }, (old) => [
              ...(old || []),
              {
                ...newOt,
                id: (prevData[prevData.length - 1]?.id || 1) + 1,
                isActive: true,
              },
            ]);
          }
        }

        return { prevData };
      },
      onError(err, newOt, ctx) {
        if (userId) {
          utils.offeredTrip.byUserId.setData({ userId }, ctx?.prevData);
        }
      },
      onSettled() {
        utils.offeredTrip.byUserId.invalidate();
      },
    });

  const handleToggle = async (pkgId: number) => {
    if (userId) {
      toggleActivate({ userId, tripId, pkgId, type });
    }
  };

  const uri = `https://s3.ap-southeast-1.amazonaws.com/media.conceptmobile.net/bohol/features/${pkgImage}`;

  return (
    <div className="flex flex-col justify-between overflow-hidden rounded-xl border border-gray-200 bg-cover bg-no-repeat">
      <div className="relative h-40 w-full overflow-hidden">
        <Image className="object-cover" src={uri} alt={pkgName} fill />
        <div className="relative z-10 flex h-full items-center justify-center bg-black/40">
          <p className="text-center text-white">{pkgName}</p>
        </div>
      </div>

      {pkgDescription.length > 0 && (
        <div className="w-full p-4 text-center text-sm">
          {pkgDescription.map((pd, i) => {
            if (!pd) return <br key={`trip-${tripId}-${pkgId}-desc-${i}`} />;

            return <p key={`trip-${tripId}-${pkgId}-desc-${i}`}>{pd}</p>;
          })}
        </div>
      )}

      <div className="text-center">
        <Button
          className="rounded-none"
          onClick={() => handleToggle(pkgId)}
          color={offeredTrip?.isActive === true ? "danger" : "primary"}
          loading={isLoading}
          disabled={!isLoaded}
        >
          {offeredTrip?.isActive === true ? "Remove Offer" : "Offer"}
        </Button>
      </div>
    </div>
  );
};

export default TripCard;
