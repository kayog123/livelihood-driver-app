import { type TranspoType } from "@acme/db";

export type Trip = {
  id: number;
  name: string;
  description: string;
  image?: string;
  fixed?: boolean;
  packages: TripPackage[];
  type: TranspoType;
};

export type TripPackage = {
  id: number;
  name: string;
  description: string[];
  image: string;
  fixed: boolean;
  places?: TripPlace[];
};

export type TripPlace = {
  id: number;
  name: string;
  description: string[];
  lat: number;
  long: number;
  image: string[];
  address: string;
  price?: number;
  optional?: boolean;
  fee?: number;
  events?: TripPlaceEvent[];
};

export type TripPlaceEvent = {
  id: number;
  event_name: string;
  event_lat: number;
  event_long: number;
  fee?: number;
  optional?: boolean;
};

const trips = [
  {
    id: 1,
    name: "Van Tour",
    description: "",
    type: "VANS",
    packages: [
      {
        id: 1,
        name: "PANGLAO LAND TOUR",
        description: [
          "🚗 Sedan/Car rate- P2,500 (1-4 pax)",
          "🚙 Private Suv Rate - P2,800 (5-7 pax)",
          "🚐 PRIVATE VAN- P3,000 (8-15 pax)",
          "",
          "INCLUSIONS:",
          "✔️ Transpo",
          "✔️ Gas",
          "✔️ Driver",
        ],
        places: [
          {
            id: 1,
            name: "Panglao Church",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["panglao.jpg"],
            address: "Panglao, Bohol",
            optional: true,
            fee: 40,
          },
          {
            id: 2,
            name: "Panglao High Watch Tower",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 3,
            name: "Moadto Strip Mall",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
            fee: 80,
          },
          {
            id: 4,
            name: "Hinagdanan Cave",
            description: ["50/head for sightseeing P70  with swimming"],
            lat: 1.12345,
            long: 1.54321,
            fee: 50,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
            events: [
              {
                id: 1,
                event_name: "Swimming (Additional 20 pesos)",
                event_lat: 1.12345,
                event_long: 1.54321,
                fee: 20,
                optional: true,
              },
            ],
          },
          {
            id: 5,
            name: "Bohol Bee Farm",
            description: ["( Guest expense for Lunch or Dinner)"],
            lat: 1.12345,
            long: 1.54321,
            fee: 20,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 6,
            name: "SouthFarm Panglao",
            description: ["200/head"],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 7,
            name: "Sight Seeing Alona beacH",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 8,
            name: "Shell Museum",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
        ],
        image: "panglao.jpg",
        fixed: false,
      },
      {
        id: 2,
        name: "ANDA / CANDIJAY TOUR",
        description: [
          "🚗 Sedan/Car Rate- P4,700 group",
          "🚙 Suv rate - P5,000 group",
          "🚌 Van Rate - P5,500 group",
        ],
        places: [
          {
            id: 1,
            name: "Anda Beach",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 2,
            name: "Cadadapan Rice Terraces",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 3,
            name: "Cabagnow Cave",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 4,
            name: "Can-Umantad Falls",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
        ],
        image: "anislagspring.jpg",
        fixed: false,
      },
      {
        id: 3,
        name: "DANAO TOURS",
        description: [],
        places: [
          {
            id: 1,
            name: "DANAO SEA OF CLOUDS",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 2,
            name: "DANAO ADVENTURE PARK",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
            events: [
              {
                id: 1,
                event_name: "THE PLUNGE 700 peso ( weight limit 60kgs",
                event_lat: 1.12345,
                event_long: 1.54321,
                optional: true,
              },
              {
                id: 2,
                event_name: "ZIPLINE 350 peso",
                event_lat: 1.12345,
                event_long: 1.54321,
                optional: true,
              },
              {
                id: 3,
                event_name: "GIANT SWING Solo (500 pesos)",
                event_lat: 1.12345,
                event_long: 1.54321,
              },
              {
                id: 4,
                event_name: "GIANT SWING Tandem (800 pesos)",
                event_lat: 1.12345,
                event_long: 1.54321,
              },
              {
                id: 5,
                event_name:
                  "THE SKY RIDE (MAX OF 4 PAX PER RIDE, 250 peso per person)",
                event_lat: 1.12345,
                event_long: 1.54321,
              },
            ],
          },
        ],
        image: "barangay.jpg",
        fixed: false,
      },
      {
        id: 4,
        name: "COUNTRY SIDE TOUR",
        description: [],
        places: [
          {
            id: 1,
            name: "Chocolate Hills",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 2,
            name: "ATV Ride in Chocolate",
            description: ["P1,100 each for 1 hour with viewing deck"],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 3,
            name: "Tarsier & Butterfly Sanctuary",
            description: ["P1,100 each for 1 hour with viewing deck"],
            price: 120,
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 4,
            name: "Loboc Floating Restauran",
            description: ["P850 per hea"],
            price: 850,
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 5,
            name: "Man Made Forest",
            description: ["P850 per hea"],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 6,
            name: "Bohol Python",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 7,
            name: "Baclayon Church",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 8,
            name: "Shiphouse",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 9,
            name: "Souvenir",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 10,
            name: "HANGING BRIDGE",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 11,
            name: "Blood Compact",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
        ],
        image: "barangay.jpg",
        fixed: false,
      },
    ],
  },
  {
    id: 2,
    name: "Boat Tour",
    description: "",
    type: "BOAT",
    packages: [
      {
        id: 1,
        name: "ISLAND HOPPING",
        description: [],
        places: [
          {
            id: 1,
            name: "BALICASAG ISLAND",
            description: ["100/head environment"],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
            fee: 100,
            events: [
              {
                id: 1,
                event_name: "Dolphin Watching  ( Starts @ 6:30am)",
                event_lat: 1.12345,
                event_long: 1.54321,
              },
            ],
          },
          {
            id: 2,
            name: "VIRGIN ISLAND",
            fee: 100,
            description: [
              "30/head environment fee for local, 100/head environment fee for foreign ",
            ],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
          {
            id: 3,
            name: "PADRE PIO CHURCH",
            description: [],
            lat: 1.12345,
            long: 1.54321,
            image: ["cabilaoisland.jpg"],
            address: "Panglao, Bohol",
          },
        ],
        image: "cabilaoisland.jpg",
        fixed: false,
      },
    ],
  },
] satisfies Trip[];

export default trips;
